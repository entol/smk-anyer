$(document).ready(function () {
    App.init();
            
    function initSelect2(container) {
        if (container) {
            $(container).find('.xcrud-view select').select2();
        } else {
            $(".xcrud-view").find("select").select2();
        }
    }

    function destroySelect2(container) {
        if (container) {
            $('.xcrud-view select', container).each(function() {
                var $that = $(this);
                if ($that.data('select2')) {
                    $that.select2('destroy');
                }
            });
        } else {
            $(".xcrud-view").find("select").each(function() {
                var $that = $(this);
                if ($that.data('select2')) {
                    $that.select2('destroy');
                }
            });
        }
    }
            
    // xcrud select2
    // destroy
    $(document).on("xcrudbeforerequest", function(event, container) {
        destroySelect2(container);
    });

    // initialize
    $(document).on("ready xcrudafterrequest", function(event, container) {
        initSelect2(container);

        $('.xcrud-view ul.nav', container || window.document.body).on('shown.bs.tab', function (e) {
            var id = $(e.target).attr('href');
            if (id != null && id != '') {
                destroySelect2('#'+ id);
            } else {
                destroySelect2();
            }
        });
    });

    // on tabs
    $('.xcrud-view ul.nav').on('shown.bs.tab', function (e) {
        var id = $(e.target).attr('href');
        if (id != null && id != '') {
            destroySelect2('#'+ id);
        } else {
            destroySelect2();
        }
    });

    // destroy
    $(document).on("xcrudbeforedepend", function(event, container, data) {
        try {
            $('.xcrud-view select[name="' + data.name + '"]', container).select2('destroy');
        } catch (e) {

        }
    });

    // initialize
    $(document).on("xcrudafterdepend", function(event, container, data) {
        $('.xcrud-view select[name="' + data.name + '"]', container).select2();
    });

    // destoy initialize
    $(window).on('resize', function() {
        destroySelect2();
        initSelect2();
    });
    
    setTimeout(function() {
        $('.alert').not('.alert-keep').fadeOut(500, function() {
            $('.alert').alert('close');
        });
    }, 5000);
});
