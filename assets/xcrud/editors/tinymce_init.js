tinyMCE.init({
	mode: "textareas", // important
	editor_selector: "editor-instance", // important

	valid_elements: '*[*]',
	height: "250",
	// selector: 'div.page-editor',
	// inline: true,
	// skin: 'custom',
	// fixed_toolbar_container: "#panel-toolbar",
	fontsize_formats: "6pt 7pt 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt 36pt 48pt 72pt",
	font_formats: "Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Courier New=courier new,courier;Helvetica=helvetica;Times New Roman=times new roman,times;",
	plugins: [
		'link image',
		'searchreplace visualblocks charmap',
		'table paste textcolor code'
	],
    contextmenu: "link image inserttable | cell row column deletetable",
	toolbar: [
		'undo redo copy cut paste | alignleft aligncenter alignright alignjustify | fontsizeselect fontselect',
		'bold italic underline | bullist numlist | forecolor backcolor | image'
	],
	external_plugins: {
		// 'pfs_image_base64': 'plugins/pfs_image_base64/plugin.js',
	},
	table_default_attributes: {
		// class: 'table table-bordered',
		style: 'width: 100%'
	},
	table_class_LV: [{
			title: 'none',
			value: ''
		},
		{
			title: 'table',
			value: 'table'
		},
		{
			title: 'table table-bordered',
			value: 'table table-bordered'
		},
	],
	paste_data_images: true,
	images_upload_handler: (blobInfo, success, failure) => {
		success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
  },
  
  file_picker_types: 'image', 
  
  // and here's our custom image picker
  file_picker_callback: function(cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    
    // Note: In modern browsers input[type="file"] is functional without 
    // even adding it to the DOM, but that might not be the case in some older
    // or quirky browsers like IE, so you might want to add it to the DOM
    // just in case, and visually hide it. And do not forget do remove it
    // once you do not need it anymore.

    input.onchange = function() {
      var file = this.files[0];
      
      var reader = new FileReader();
      reader.onload = function () {
        // Note: Now we need to register the blob in TinyMCEs image blob
        // registry. In the next release this part hopefully won't be
        // necessary, as we are looking to handle it internally.
        var id = 'blobid' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };
    
    input.click();
  }
});
