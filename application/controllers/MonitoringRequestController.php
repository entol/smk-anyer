<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class MonitoringRequestController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Monitoring Request', 'monitoring-request/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->columns('kode,kode_project,permintaan_asal,permintaan_asal_detail,tanggal_request,status');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail,status,tanggal_request', '', 'Request', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');

        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);
        // $xcrud->relation('kode_gudang_asal', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        // $xcrud->relation('kode_project_asal', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        $xcrud->label('kode_gudang_asal', 'Gudang Asal');
        $xcrud->label('kode_project_asal', 'Project Asal');
        $xcrud->label('tanggal_request', 'Tanggal Request');
        $xcrud->label('tanggal_shipment', 'Tanggal Shipment');
        $xcrud->label('tanggal_required', 'Tanggal Order');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
         
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        $xcrud->order_by('id', 'desc');

        $items = $xcrud->nested_table('Material','id','t_request_barang','id_request'); // nested table
        $items->columns('kode_barang,qty,note,approved,approved_at, remark, approved_pm, approved_pm_at, remark_pm');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        $items->relation('approved_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $items->relation('approved_pm_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        // $items->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        // $items->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        $items->label('approved', 'Approved');
        $items->label('approved_at', 'Approved At');
        $items->label('approved_by', 'Approved By');
        $items->label('approved_pm', 'Approved PM');
        $items->label('approved_pm_at', 'Approved PM At');
        $items->label('approved_pm_by', 'Approved PM By');
        $items->label('remark_pm', 'Remark PM');
        $items->label('remark', 'Remark');
        
        $items->unset_view();

        // Riwayat
        $history = $xcrud->nested_table('Riwayat','id','t_request_history','id_request'); // nested table
        $history->columns('status, created_at, created_by');
        
        $history->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $history->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        
        $history->label('created_at', 'Tanggal');
        $history->label('created_by', 'Oleh');
        $history->unset_view();

        $this->layout->render('monitoring-request/index', [
            'box' => true,
            'boxTitle' => 'Monitoring Request',
            'title' => 'Monitoring Request',
            'pageTitle' => 'Request',
            'pageSubTitle' => 'Semua Request',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}