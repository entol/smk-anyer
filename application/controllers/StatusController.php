<?php

class StatusController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Status', 'status/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_status');
        $xcrud->columns('kode,status,deskripsi');
        $xcrud->fields('kode,status,deskripsi,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $xcrud->fields('kode,status,deskripsi', false, false, 'add');
        $xcrud->fields('kode,status,deskripsi', false, false, 'edit');

        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'ID');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,status');
        $xcrud->unset_csv();
        $xcrud->unset_print();

        $xcrud->before_insert('status_before_insert', realpath(__DIR__ .'/../callbacks/status.php'));
        // $xcrud->after_insert('status_after_insert', realpath(__DIR__ .'/../callbacks/status.php'));
        $xcrud->before_update('status_before_update', realpath(__DIR__ .'/../callbacks/status.php'));
        // $xcrud->after_update('status_after_update', realpath(__DIR__ .'/../callbacks/status.php'));
        // $xcrud->before_remove('status_before_remove', realpath(__DIR__ .'/../callbacks/status.php'));
        // $xcrud->after_remove ('status_after_remove', realpath(__DIR__ .'/../callbacks/status.php'));

        $this->layout->render('status/index', [
            'box' => true,
            'boxTitle' => 'Status',
            'title' => 'Status',
            'pageTitle' => 'Status',
            'pageSubTitle' => 'Semua Status',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}