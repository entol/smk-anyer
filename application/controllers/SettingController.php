<?php

class SettingController extends CI_Controller
{
    public function actionIndex()
    {
		$this->breadcrumbs->push('Dashboard', 'site');
		$this->breadcrumbs->push('Authentiaction', 'menu/index#');
		$this->breadcrumbs->push('Setting', 'setting');
		
		$xcrud = xcrud_get_instance();
        $xcrud->table('c_setting');
        $xcrud->unset_add();
        $xcrud->unset_view();
        $xcrud->unset_remove();
        $xcrud->unset_list();
        $xcrud->change_type('app_description', 'text');
        $xcrud->change_type('app_keywords', 'text');
        $xcrud->change_type('theme', 'select', 'material', [
            'material' => 'Material',
            'html' => 'Color Admin',
            'transparent' => 'Transparent',
            // 'admin_lte' => 'Admin LTE'
        ]);
        $xcrud->change_type('login_theme', 'select', 'login1', [
            'login1' => 'Login 1',
            'login2' => 'Login 2',
            'login3' => 'Login 3',
        ]);
        $xcrud->change_type('login_background_selection', 'select', 'N', [
            'Y' => 'Yes',
            'N' => 'No'
        ]);
        $xcrud->change_type('allow_register', 'select', 'N', [
            'Y' => 'Yes',
            'N' => 'No'
        ]);
        $xcrud->change_type('language', 'select', 'indonesian', [
            'english' => 'English',
            'indonesian' => 'Bahasa indonesia'
        ]);
        $xcrud->set_lang('save_edit', 'l:save');
        $xcrud->relation('login_background', 'c_background', 'id', 'name');

        $xcrud->validation_required('company_name');
        $xcrud->validation_required('app_name');
        $xcrud->validation_required('app_name_abbr');
        $xcrud->validation_required('app_title');
        $xcrud->validation_required('app_description');
        $xcrud->validation_required('theme');
        $xcrud->validation_required('login_theme');
        $xcrud->validation_required('login_background');
        $xcrud->validation_required('login_background_selection');
        $xcrud->validation_required('login_description');
        $xcrud->validation_required('allow_register');
        $xcrud->validation_required('language');

		$this->layout->render('setting/index', [
            'xcrud' => $xcrud->render('edit', 1),
			'box' => true,
			'boxTitle' => 'General Setting',
			'title' => 'General Setting',
            'pageTitle' => 'General Setting',
            'pageSubTitle' => 'System Setting'
		]);
    }

    public function actionBackground()
    {
        $this->breadcrumbs->push('Dashboard', 'site');
        $this->breadcrumbs->push('Settings', 'setting/index#');
        $this->breadcrumbs->push('background', 'setting/background');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('c_background');
        $xcrud->change_type('background', 'image', '', [
            'path' => FCPATH .'assets/color-admin/img/background/'
        ]);

        $this->layout->renderString($xcrud->render(), [
            'box' => true,
            'boxTitle' => 'General Setting',
            'title' => 'General Setting',
            'pageTitle' => 'General Setting',
            'pageSubTitle' => 'System Setting',
            'activeMenu' => 'setting-background'
        ]);
    }
}
