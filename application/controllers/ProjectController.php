<?php

class ProjectController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Project', 'project/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_project');
        $xcrud->columns('kode,singkatan,nama,kota,provinsi,alamat,manager,start,end, status');
        $xcrud->fields('kode,singkatan,nama,kota,provinsi,alamat,manager,start,end, status', false, false, 'create');
        $xcrud->fields('kode,singkatan,nama,kota,provinsi,alamat,manager,start,end, status', false, 'Project', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        if ($this->ion_auth->in_group('pm')) {
            $xcrud->fields('singkatan,nama,kota,provinsi,alamat,start,end, status', false, 'Project', 'edit');
        } else {
            $xcrud->fields('kode,singkatan,nama,kota,provinsi,alamat,manager,start,end, status', false, 'Project', 'edit');
        }

        $xcrud->relation('manager', 'm_karyawan', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('start', 'date');
        $xcrud->change_type('end', 'date');
        $xcrud->change_type('status', 'radio', 'Y', [
            'Y' => 'Aktif',
            'N' => 'Deactive'
        ]);
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'ID');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('nama', 'Nama');
        $xcrud->label('kota', 'Kota');
        $xcrud->label('provinsi', 'Provinsi');
        $xcrud->label('alamat', 'Alamat');
        $xcrud->label('manager', 'Project Manager');
        $xcrud->label('start', 'Start');
        $xcrud->label('end', 'End');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,singkatan,nama,kota,provinsi,alamat,manager');
        
        if (!$this->ion_auth->in_button('karyawan-create')) {
            $xcrud->unset_add();
        }
        
        if (!$this->ion_auth->in_button('karyawan-update')) {
            $xcrud->unset_edit();
        } elseif ($this->ion_auth->in_group(['manager'])) {
            $xcrud->unset_edit(true, 'manager', '!=', $this->ion_auth->user_data('id_peg'));
        }
        
        if (!$this->ion_auth->in_button('karyawan-delete')) {
            $xcrud->unset_remove();
        }

        $xcrud->before_insert('project_before_insert', realpath(__DIR__ .'/../callbacks/project.php'));
        // $xcrud->after_insert('project_after_insert', realpath(__DIR__ .'/../callbacks/project.php'));
        $xcrud->before_update('project_before_update', realpath(__DIR__ .'/../callbacks/project.php'));
        // $xcrud->after_update('project_after_update', realpath(__DIR__ .'/../callbacks/project.php'));
        // $xcrud->before_remove('project_before_remove', realpath(__DIR__ .'/../callbacks/project.php'));
        // $xcrud->after_remove ('project_after_remove', realpath(__DIR__ .'/../callbacks/project.php'));
        
        
        $members = $xcrud->nested_table('Member','kode','t_project_member','kode_project'); // nested table
        // $members->default_tab('Member');
        $members->columns('kode_project,id_karwayan');
        $members->fields('kode_project,id_karwayan,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $members->fields('id_karwayan', false, false, 'add');
        $members->fields('id_karwayan', false, false, 'edit');

        $members->relation('id_karwayan', 'm_karyawan', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $members->relation('created_by', 'a_users', 'id', ['id', 'nik', 'nama'], '', '', '', ' - ');
        $members->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $members->label('id', 'Id');
        $members->label('kode_project', 'Kode Project');
        $members->label('id_karwayan', 'Karyawan');
        $members->label('created_at', 'Created At');
        $members->label('created_by', 'Created By');
        $members->label('updated_at', 'Updated At');
        $members->label('updated_by', 'Updated By');
        $members->label('ip_address', 'IP Address');
        $members->label('useragent', 'User Agent');

        $members->validation_required('kode_project,id_karwayan');

        $members->before_insert('project_member_before_insert', realpath(__DIR__ .'/../callbacks/project-member.php'));
        // $members->after_insert('project_member_after_insert', realpath(__DIR__ .'/../callbacks/project-member.php'));
        $members->before_update('project_member_before_update', realpath(__DIR__ .'/../callbacks/project-member.php'));
        // $members->after_update('project_member_after_update', realpath(__DIR__ .'/../callbacks/project-member.php'));
        // $members->before_remove('project_member_before_remove', realpath(__DIR__ .'/../callbacks/project-member.php'));
        // $members->after_remove ('project_member_after_remove', realpath(__DIR__ .'/../callbacks/project-member.php'));

        $this->layout->render('project/index', [
            'box' => true,
            'boxTitle' => 'Project',
            'title' => 'Project',
            'pageTitle' => 'Project',
            'pageSubTitle' => 'Semua Project',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}