<?php

use app\models\Project;
use app\models\Perbaikan;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class PerbaikanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Perbaikan', 'perbaikan/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_perbaikan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,status');
        $xcrud->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,draft', false, 'Perbaikan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        $xcrud->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,draft', false, 'Perbaikan', 'create');
        $xcrud->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,draft', false, 'Perbaikan', 'edit');
        
        if ($isPm) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi', "m_lokasi_barang_vd.kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], "qty_baik > 0 AND m_barang_vd.lokasi_project IN ('". implode("', '", $kodeProject) ."')", '', '', ' - ');
            } else {
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], '1=2', '', '', ' - ');
            }
        } else {
            $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
            $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], 'qty_baik > 0', '', '', ' - ', null, 'kode_lokasi', 'kode_lokasi');
        }
        
        $xcrud->relation('status', 'm_status_perbaikan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,keterangan,jenis_perbaikan,draft');
        $xcrud->pass_default('tanggal_pendaftaran', date('Y-m-d'));
        $xcrud->pass_default('draft', 'Y');
        $xcrud->column_pattern('estimasi_waktu','{value} Hari');

        $xcrud->before_insert('perbaikan_before_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_insert('perbaikan_after_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->before_update('perbaikan_before_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_update('perbaikan_after_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        // $xcrud->before_remove('perbaikan_before_remove', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_remove ('perbaikan_after_remove', realpath(__DIR__ .'/../callbacks/perbaikan.php'));

        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('kepala_mekanik', 'Kepala Mekanik');
        $xcrud->label('tanggal_pendaftaran', 'Tanggal Pendaftaran');
        $xcrud->label('estimasi_waktu', 'Estimasi Waktu');
        $xcrud->label('status', 'Status');
        $xcrud->label('jenis_perbaikan', 'Jenis Perbaikan');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('estimasi_waktu','Estimas dalam hari');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');
        
        $xcrud->where("status IN ('DR', 'RQ')");
        
        // jika tidak memiliki akses add
        if (!$this->ion_auth->in_button('maintenance-create')) {
            $xcrud->unset_add();
        }
        
        // jika tidak memiliki akses edit
        if (!$this->ion_auth->in_button('maintenance-update')) {
            $xcrud->unset_edit();
        } else {
            $xcrud->unset_edit(true, 'status', '!=', 'DR');
        }
        
        // jika tidak memiliki akses delete
        if (!$this->ion_auth->in_button('maintenance-delete')) {
            $xcrud->unset_remove();
        } else {
            $xcrud->unset_remove(true, 'status', '!=', 'DR');
        }
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        $xcrud->order_by('id', 'desc');
        
        // Parts
        $parts = $xcrud->nested_table('Parts','id','t_perbaikan_parts','id_perbaikan'); // nested table
        $parts->columns('part,keterangan,jenis_perbaikan,status');
        $parts->fields('part,keterangan,jenis_perbaikan,status,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'create');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'edit');

        $parts->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $parts->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $parts->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $parts->change_type('status', 'select', 'W', [
            'W' => 'Sedang Perbaikan',
            'D' => 'Fixed',
            'F' => 'Failed'
        ]);
        
        $parts->pass_var('status', 'W');

        $parts->validation_required('part,keterangan,jenis_perbaikan');
        $parts->before_insert('perbaikan_parts_before_insert', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_insert('perbaikan_parts_after_insert', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        $parts->before_update('perbaikan_parts_before_update', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_update('perbaikan_parts_after_update', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->before_remove('perbaikan_parts_before_remove', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_remove ('perbaikan_parts_after_remove', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
    
        $parts->label('part', 'Nama Part');
        $parts->label('keterangan', 'Keterangan');
        $parts->label('jenis_perbaikan', 'Jenis Perbaikan');
        $parts->label('status', 'Status');
        $parts->label('created_at', 'Created At');
        $parts->label('created_by', 'Created By');
        $parts->label('updated_at', 'Updated At');
        $parts->label('updated_by', 'Updated By');
        $parts->label('ip_address', 'Ip Address');
        
        $this->layout->render('perbaikan/index', [
            'box' => true,
            'boxTitle' => 'Perbaikan',
            'title' => 'Perbaikan',
            'pageTitle' => 'Perbaikan',
            'pageSubTitle' => 'Semua Perbaikan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionLists()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Daftar Perbaikan', 'perbaikan/lists');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_perbaikan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,status');
        $xcrud->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,draft', false, 'Perbaikan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        if ($isPm) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi', "m_lokasi_barang_vd.kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], "m_barang_vd.lokasi_project IN ('". implode("', '", $kodeProject) ."')", '', '', ' - ');
            } else {
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], '1=2', '', '', ' - ');
            }
        } else {
            $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
            $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ', null, 'kode_lokasi', 'kode_lokasi');
        }
        
        $xcrud->relation('status', 'm_status_perbaikan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,keterangan,jenis_perbaikan,draft');
        $xcrud->pass_default('tanggal_pendaftaran', date('Y-m-d'));
        $xcrud->pass_default('draft', 'Y');
        $xcrud->column_pattern('estimasi_waktu','{value} Hari');

        $xcrud->before_insert('perbaikan_before_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_insert('perbaikan_after_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->before_update('perbaikan_before_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_update('perbaikan_after_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        // $xcrud->before_remove('perbaikan_before_remove', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $xcrud->after_remove ('perbaikan_after_remove', realpath(__DIR__ .'/../callbacks/perbaikan.php'));

        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('kepala_mekanik', 'Kepala Mekanik');
        $xcrud->label('tanggal_pendaftaran', 'Tanggal Pendaftaran');
        $xcrud->label('estimasi_waktu', 'Estimasi Waktu');
        $xcrud->label('status', 'Status');
        $xcrud->label('jenis_perbaikan', 'Jenis Perbaikan');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('estimasi_waktu','Estimas dalam hari');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');
        
        $xcrud->where("status IN ('RQ', 'AP', 'MT')");
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        $xcrud->order_by('id', 'desc');
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        if ($this->ion_auth->in_button('maintenance-proses')) {
            $xcrud->button('javascript:;', 'Proses', 'fa fa-fw fa-check-circle', 'btn-success btn-proses', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'RQ']);
        }
        
        if ($this->ion_auth->in_button('maintenance-selesai')) {
            $xcrud->button('javascript:;', 'Berhasil', 'fa fa-fw fa-thumbs-up', 'btn-primary btn-fixed', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'MT']);
            $xcrud->button('javascript:;', 'Gagal', 'fa fa-fw fa-exclamation-triangle', 'btn-inverse btn-failed', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'MT']);
        }

        // Parts
        $parts = $xcrud->nested_table('Parts','id','t_perbaikan_parts','id_perbaikan'); // nested table
        $parts->columns('part,keterangan,jenis_perbaikan,status');
        $parts->fields('part,keterangan,jenis_perbaikan,status,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'create');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'edit');

        $parts->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $parts->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $parts->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $parts->change_type('status', 'select', 'W', [
            'W' => 'Sedang Perbaikan',
            'D' => 'Fixed',
            'F' => 'Failed'
        ]);
        
        $parts->pass_var('status', 'W');

        $parts->validation_required('part,keterangan,jenis_perbaikan');
        $parts->before_insert('perbaikan_parts_before_insert', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_insert('perbaikan_parts_after_insert', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        $parts->before_update('perbaikan_parts_before_update', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_update('perbaikan_parts_after_update', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->before_remove('perbaikan_parts_before_remove', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
        // $parts->after_remove ('perbaikan_parts_after_remove', realpath(__DIR__ .'/../callbacks/perbaikan-parts.php'));
    
        $parts->label('part', 'Nama Part');
        $parts->label('keterangan', 'Keterangan');
        $parts->label('jenis_perbaikan', 'Jenis Perbaikan');
        $parts->label('status', 'Status');
        $parts->label('created_at', 'Created At');
        $parts->label('created_by', 'Created By');
        $parts->label('updated_at', 'Updated At');
        $parts->label('updated_by', 'Updated By');
        $parts->label('ip_address', 'Ip Address');
        
        $this->layout->render('perbaikan/index', [
            'box' => true,
            'boxTitle' => 'Daftar Perbaikan',
            'title' => 'Daftar Perbaikan',
            'pageTitle' => 'Perbaikan',
            'pageSubTitle' => 'Semua Perbaikan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionProses($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Perbaikan::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Data perbaikan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            
            // Approve
            $model->status = 'AP';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if (!$model->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
        
            // Maintenance
            $model->status = 'MT';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if (!$model->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            $barang = $model->barang;
            $barang->qty_perbaikan = $barang->qty_perbaikan + 1; // repair
            $barang->qty_baik = $barang->qty_baik - 1; // repair
            $barang->updated_at = $currentDateTime;
            $barang->updated_by = $userId;
            $barang->ip_address = $this->input->ip_address();
            $barang->useragent = $this->agent->agent_string();
            if (!$barang->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
                    
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    
    public function actionFixed($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Perbaikan::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Data perbaikan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            
            // Fixed
            $model->status = 'FX';
            $model->tanggal_selesai = $currentDate;
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if (!$model->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            // update barang
            $barang = $model->barang;
            $barang->qty_baik = $barang->qty_baik + 1; // repair
            $barang->qty_perbaikan = $barang->qty_perbaikan - 1; // repair
            $barang->updated_at = $currentDateTime;
            $barang->updated_by = $userId;
            $barang->ip_address = $this->input->ip_address();
            $barang->useragent = $this->agent->agent_string();
            if (!$barang->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionFailed($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Perbaikan::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Data perbaikan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate= date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            
            // Failed
            $model->tanggal_selesai = $currentDate;
            $model->status = 'FA';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if (!$model->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }

            // update barang
            $barang = $model->barang;
            $barang->qty_perbaikan = $barang->qty_perbaikan - 1; // repair
            $barang->qty_rusak = $barang->qty_rusak + 1; // rusak
            $barang->updated_at = $currentDateTime;
            $barang->updated_by = $userId;
            $barang->ip_address = $this->input->ip_address();
            $barang->useragent = $this->agent->agent_string();
            if (!$barang->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
}