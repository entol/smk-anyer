<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use app\models\SuratJalan;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;
use Dompdf\Dompdf;

class PengirimanBarangController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Pengiriman', 'pengiriman/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        /*print_r(var_dump($isMatecon));
        exit(); */
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->columns('kode,kode_project,permintaan_asal,permintaan_asal_detail,tanggal_request, status');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail', '', '', 'view');
        
        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);
        $xcrud->relation('kode_gudang_asal', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('kode_project_asal', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['selanjutnya'], '', '', '', ' - ');
        $xcrud->change_type('approved', 'radio', '', [
            'Y' => 'Ya',
            'N' => 'Tidak',
        ]);
        
        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        $xcrud->label('kode_gudang_asal', 'Gudang Asal');
        $xcrud->label('kode_project_asal', 'Project Asal');
        $xcrud->label('tanggal_request', 'Tanggal Request');
        $xcrud->label('tanggal_shipment', 'Tanggal Shipment');
        $xcrud->label('tanggal_required', 'Tanggal Order');
        $xcrud->label('status', 'Status');
        
        if ($this->ion_auth->in_button('pengiriman-proses')) {
            $xcrud->button('javascript:;', 'Proses Semua', 'fas fa-fw fa-people-carry', 'btn-success btn-proses-semua', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'AP']);
        }
        
        if ($this->ion_auth->in_button('pengiriman-surat-jalan')) {
            $xcrud->button('javascript:;', 'Surat Keluar', 'fas fa-fw fa-envelope-open-text', 'btn-inverse btn-proses-surat', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'PR']);
            $xcrud->button('javascript:;', 'Cetak Ulang', 'fas fa-fw fa-envelope-open-text', 'btn-inverse btn-proses-surat', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'SH']);
        }
         
        $xcrud->validation_required('kode,kode_project,permintaan_asal');
        $xcrud->unset_edit();
        $xcrud->unset_add();
        $xcrud->unset_remove();
        
        // if ($this->ion_auth->in_group('pm')) {
            // $kodeProject = Project::getMyProject(true);
            // if ($kodeProject && count($kodeProject)) {
                // $xcrud->where("kode_project IN ('". implode("', '", $kodeProject) ."')");
            // } else {
                // $xcrud->where("1=2");
            // }
        // }
        
        if ($isMatecon) {
            // PM Approve, Verification Matecon & Request Warehouse
            $xcrud->where("status IN ('AP', 'PR', 'SH')");
        } elseif ($isPm) {
            $xcrud->where("status IN ('AP', 'PR', 'SH')");
            // my target project
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project_asal IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        } else {
            $xcrud->where("1=2");
        }
        $xcrud->order_by('id', 'desc');

        $xcrud->column_callback('status', 'pengiriman_barang_status', realpath(__DIR__ .'/../callbacks/pengiriman-barang.php'));
        
        // Items
        $items = $xcrud->nested_table('Material','id','t_request_barang','id_request'); // nested table
        $items->columns('kode_barang,qty,note,remark,status');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        $items->relation('status', 'm_status', 'kode', ['status'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        // $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        $items->label('status', 'Status');
        $items->where("approved = 'Y'");
        
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        $items->unset_csv();
        $items->unset_print();
        
        // -------------------------
        // Highlight
        // -------------------------
        $items->highlight_row('status', '=', 'PR', '', 'info');
        $items->highlight_row('status', '=', 'SH', '', 'success');
        
        // -------------------------
        // Button
        // -------------------------
        if ($this->ion_auth->in_button('pengiriman-proses')) {
            $items->button('javascript:;', 'Proses', 'fas fa-fw fa-shipping-fast', 'btn-primary btn-proses', [
                'enable_label' => true,
                'data-id' => '{id}',
                'title' => 'Proses'
            ], ['status', '=', '']);
        }

        $this->layout->render('pengiriman-barang/index', [
            'box' => true,
            'boxTitle' => 'Pengiriman Material',
            'title' => 'Pengiriman',
            'pageTitle' => 'Pengiriman',
            'pageSubTitle' => 'Semua Pengiriman Material',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionProses($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = RequestBarang::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            $model->status = 'PR';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if ($model->save()) {
                
                $request = $model->request;
                $dp = $request->items;
                $isWaiting = false;
                
                foreach($dp as $item) {
                    if ($item->approved == 'Y') {
                        if ($item->status != 'PR') {
                            $isWaiting = true;
                        }
                    }
                }
                
                if ($isWaiting === false) { // jika semua item telah diproses
                    $request->status = 'PR';
                    $request->updated_at = $currentDateTime;
                    $request->updated_by = $userId;
                    $request->ip_address = $this->input->ip_address();
                    $request->useragent = $this->agent->agent_string();
                    $request->save();
                }
                
                DB::commit();
                return $this->output->set_output(json_encode([
                    'status' => 1,
                    'message' => '',
                    'data' => ''
                ]));
            } else {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionProsesSemua($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Request::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            $model->status = 'PR';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if ($model->save()) {
                $dp = $model->items;
                foreach($dp as $item) {
                    if ($item->approved == 'Y') {
                        $item->status = 'PR';
                        $item->updated_at = $currentDateTime;
                        $item->updated_by = $userId;
                        $item->ip_address = $this->input->ip_address();
                        $item->useragent = $this->agent->agent_string();
                        $item->save();
                    }
                }
                
                DB::commit();
                return $this->output->set_output(json_encode([
                    'status' => 1,
                    'message' => '',
                    'data' => ''
                ]));
            } else {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }

    public function actionLoadSurat($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Request::find($id);
        if ($model) {
            $userId = $this->ion_auth->user_data('id');
            $currentDateTime = date('Y-m-d H:i:s');
            $note = $this->input->post('note');
            
            try {
                DB::beginTransaction();
                $suratJalan = SuratJalan::where('kode_request', $model->kode)->first();
                if ($suratJalan) {
                    return $this->output->set_output(json_encode([
                        'status' => 1,
                        'message' => '',
                        'data' => $suratJalan->toArray()
                    ]));
                } else {
                    $suratJalan = new SuratJalan;
                    $suratJalan->kode_request = $model->kode;
                
                    if ($model->permintaan_asal == 'W') {
                        $suratJalan->from = $model->gudangAsal->kode .' - '. $model->gudangAsal->gudang;
                    } else {
                        $suratJalan->from = $model->projectAsal->kode .' - '. $model->projectAsal->singkatan;
                    }

                    $suratJalan->to = $model->projectTujuan->kode .' - '. $model->projectTujuan->singkatan;
                    $suratJalan->address = $model->projectTujuan->alamat;
                    $suratJalan->delivery_date = date('Y-m-d');
                    $suratJalan->created_at = $currentDateTime;
                    $suratJalan->created_by = $userId;
                    $suratJalan->updated_at = $currentDateTime;
                    $suratJalan->updated_by = $userId;
                    $suratJalan->ip_address = $this->input->ip_address();
                    $suratJalan->useragent = $this->agent->agent_string();
                    if ($suratJalan->save()) {
                        $model->id_surat_jalan = $suratJalan->id;
                        $model->save();
                        DB::commit();

                        return $this->output->set_output(json_encode([
                            'status' => 1,
                            'message' => '',
                            'data' => $suratJalan->toArray()
                        ]));
                    }
                }
                DB::rollBack();
            } catch (\Exception $ex) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => $ex->getMessage(),
                    'data' => null
                ]));
            }
        }
        
        return $this->output->set_output(json_encode([
            'status' => 0,
            'message' => 'Permintaan tidak ditemukan',
            'data' => ''
        ]));
    }

    public function actionProsesSurat($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Request::find($id);
        if ($model) {
            $suratJalan = SuratJalan::where('kode_request', $model->kode)->first();
            if ($suratJalan) {

                $userId = $this->ion_auth->user_data('id');
                $currentDateTime = date('Y-m-d H:i:s');
                $note = $this->input->post('note');
                
                try {
                    DB::beginTransaction();
                    $model->status = 'SH';
                    $model->updated_at = $currentDateTime;
                    $model->updated_by = $userId;
                    $model->ip_address = $this->input->ip_address();
                    $model->useragent = $this->agent->agent_string();
                    if ($model->save()) {
                        $suratJalan->address = $this->input->post('address');
                        $suratJalan->delivery_date = $this->input->post('delivery-date');
                        $suratJalan->carrier_name = $this->input->post('carrier-name');
                        $suratJalan->vehicle = $this->input->post('vehicle');
                        $suratJalan->attn = $this->input->post('attn');
                        $suratJalan->delivery_no = $this->input->post('delivery-no');
    
                        $suratJalan->updated_at = $currentDateTime;
                        $suratJalan->updated_by = $userId;
                        $suratJalan->ip_address = $this->input->ip_address();
                        $suratJalan->useragent = $this->agent->agent_string();
    
                        if ($suratJalan->save()) {
                            DB::commit();
                            return $this->output->set_output(json_encode([
                                'status' => 1,
                                'message' => '',
                                'data' => null
                            ]));
                        }   
                    }
                    DB::rollBack();
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => $ex->getMessage(),
                        'data' => null
                    ]));
                }
            }
        }
        
        return $this->output->set_output(json_encode([
            'status' => 0,
            'message' => 'Permintaan tidak ditemukan',
            'data' => ''
        ]));
    }
    
    public function actionCetakSurat($id)
    {
        $model = Request::find($id);
        if (!$model) {
            show_404();
        }
        $surat = $model->suratJalan;
        if (!$surat) {
            show_404();
        }

        // delivery date
        $deliveryDate = $surat->delivery_date;
        try {
            $dt = Carbon::createFromFormat('Y-m-d', $deliveryDate);
            $deliveryDate = $dt->format('l, d M Y');
        } catch (\Exception $ex) {
            $deliveryDate = $surat->delivery_date;
        }
        
        $items = $model->itemsApproved;

        $this->export->pdf('surat-jalan-rpt', [
            'model' => $model,
            'items' => $items,
            'total' => count($items),
            'surat' => $surat,
            'deliveryDate' => $deliveryDate,
            // 'logo' => 'data:image/png;base64,'. base64_encode(file_get_contents(FCPATH .'assets/wp/logo2.png')),
            'dompdf' => [
                'name' => 'surat-jalan.pdf'
            ]
        ]);
    }
}