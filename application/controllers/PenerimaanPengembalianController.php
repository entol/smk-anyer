<?php

use app\models\Project;
use app\models\Pengembalian;
use app\models\PengembalianBarang;
use app\models\PengembalianHistory;
use app\models\Barang;
use app\models\BarangHistory;
use app\models\BarangItems;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class PenerimaanPengembalianController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Pengembalian', 'pengembalian/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_pengembalian');
        $xcrud->columns('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, 'Pengembalian', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, false, 'create');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, false, 'edit');

        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('kode_gudang', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode Pengembalian');
        $xcrud->label('kode_project', 'Kode Project');
        $xcrud->label('kode_gudang', 'Kode Gudang');
        $xcrud->label('tanggal_pengembalian', 'Tanggal Pengembalian');
        $xcrud->label('tanggal_sampai', 'Tanggal Sampai');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,kode_project,kode_gudang');
        $xcrud->unset_edit();
        $xcrud->unset_add();
        $xcrud->unset_remove();

        if ($this->ion_auth->in_button('pengembalian-terima')) {
            $xcrud->button('javascript:;', 'Terima', 'fas fa-fw fa-people-carry', 'btn-primary btn-terima', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'SH']);
        }
        // if ($this->ion_auth->in_button('pengembalian-batal')) {
        //     $xcrud->button('javascript:;', 'Batal', 'fas fa-fw fa-exclamation-triangle', 'btn-danger btn-batal', [
        //         'enable_label' => true,
        //         'data-id' => '{id}'
        //     ], ['status', '=', 'SH']);
        // }

        $xcrud->order_by('id', 'desc');
        $xcrud->where("status in ('SH', 'RC')");
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }

        // Items
        $items = $xcrud->nested_table('Material','id','t_pengembalian_barang','id_pengembalian'); // nested table
        $items->columns('kode_barang,qty,kondisi,note,remark');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        $items->relation('kondisi', 'm_kondisi', 'kode', ['kode', 'kondisi'], '', '', '', ' - ');
        // $items->relation('status', 'm_status', 'kode', ['status'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        
        $this->layout->render('penerimaan-pengembalian/index', [
            'box' => true,
            'boxTitle' => 'Penerimaan Material',
            'title' => 'Penerimaan Material',
            'pageTitle' => 'Penerimaan',
            'pageSubTitle' => 'Semua Penerimaan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionTerima($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Pengembalian::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Data pengembalian tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            $model->status = 'RC';
            $model->tanggal_sampai = date('Y-m-d');
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            
            if ($model->save()) {
                $dp = $model->items;
                foreach($dp as $item) {
                    $barang = $item->barang;

                    // pengurangan atau menghapus barang-item lama
                    $barangAsal = BarangItems::where('lokasi_project', $model->kode_project)
                        ->where('kode_barang', $item->kode_barang)
                        ->where('lokasi', 'P')
                        ->first();
                    if (!$barangAsal) {
                        DB::rollBack();
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Material '. $barang->kode .' - '. $barang->deskripsi .' sudah tidak dilokasi project',  
                            'data' => ''
                        ]));
                    } else {
                        $barangAsalQty = $barangAsal->qty_baik + $barangAsal->qty_perbaikan + $barangAsal->qty_rusak + $barangAsal->qty_hilang;
                        if (($barangAsalQty - $item->qty) <= 0) {
                            $barangAsal->delete();
                        } else {
                            if ($item->kondisi == 'G') {
                                $barangAsal->qty_baik = $barangAsal->qty_baik - $item->qty;
                            } elseif ($item->kondisi == 'R') {
                                $barangAsal->qty_perbaikan = $barangAsal->qty_perbaikan - $item->qty;
                            } elseif ($item->kondisi == 'D') {
                                $barangAsal->qty_rusak = $barangAsal->qty_rusak - $item->qty;
                            } else {
                                $barangAsal->qty_hilang = $barangAsal->qty_hilang - $item->qty;
                            }
                            $barangAsal->updated_at = $currentDateTime;
                            $barangAsal->updated_by = $userId;
                            $barangAsal->ip_address = $this->input->ip_address();
                            $barangAsal->useragent = $this->agent->agent_string();
                            $barangAsal->save();
                        }
                    }
                    
                    // penambahan qty pada target
                    $barangTujuan = BarangItems::where('lokasi_gudang', $model->kode_gudang)
                                    ->where('kode_barang', $item->kode_barang)
                                    ->where('lokasi', 'W')
                                    ->first();
                    if (!$barangTujuan) {
                        $barangTujuan = new BarangItems;
                        $barangTujuan->kode_barang = $item->kode_barang;
                        $barangTujuan->lokasi = 'W';
                        $barangTujuan->lokasi_gudang = $model->kode_gudang;
                        $barangTujuan->created_at = $currentDateTime;
                        $barangTujuan->created_by = $userId;
                        $barangTujuan->qty_baik = 0;
                        $barangTujuan->qty_perbaikan = 0;
                        $barangTujuan->qty_rusak = 0;
                        $barangTujuan->qty_hilang = 0;
                    }

                    if ($item->kondisi == 'G') {
                        $barangTujuan->qty_baik = $barangTujuan->qty_baik + $item->qty;
                    } elseif ($item->kondisi == 'R') {
                        $barangTujuan->qty_perbaikan = $barangTujuan->qty_perbaikan + $item->qty;
                    } elseif ($item->kondisi == 'D') {
                        $barangTujuan->qty_rusak = $barangTujuan->qty_rusak + $item->qty;
                    } else {
                        $barangTujuan->qty_hilang = $barangTujuan->qty_hilang + $item->qty;
                    }
                    $barangTujuan->updated_at = $currentDateTime;
                    $barangTujuan->updated_by = $userId;
                    $barangTujuan->ip_address = $this->input->ip_address();
                    $barangTujuan->useragent = $this->agent->agent_string();
                    $barangTujuan->save();

                    // Model Barang
                    $barang->lokasi = 'W';
                    $barang->lokasi_gudang = $model->kode_gudang;
                    $barang->lokasi_project = null;
                    $barang->updated_at = $currentDateTime;
                    $barang->updated_by = $userId;
                    $barang->ip_address = $this->input->ip_address();
                    $barang->useragent = $this->agent->agent_string();
                    $barang->save();
                    
                    // create history
                    $history = new BarangHistory;
                    $history->kode_barang = $item->kode_barang;
                    $history->lokasi = 'W';
                    $history->lokasi_gudang = $model->kode_gudang;;
                    $history->lokasi_project = null;
                    $history->created_at = $currentDateTime;
                    $history->created_by = $userId;
                    $history->updated_at = $currentDateTime;
                    $history->updated_by = $userId;
                    $history->ip_address = $this->input->ip_address();
                    $history->useragent = $this->agent->agent_string();
                    $history->save();
                }
                
                DB::commit();
                return $this->output->set_output(json_encode([
                    'status' => 1,
                    'message' => '',
                    'data' => ''
                ]));
            } else {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
}