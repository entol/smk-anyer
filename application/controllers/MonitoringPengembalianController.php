<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class MonitoringPengembalianController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Monitoring Pengembalian', 'monitoring-pengembalian/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_pengembalian');
        $xcrud->columns('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, 'Pengembalian', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');

        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('kode_gudang', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode Pengembalian');
        $xcrud->label('kode_project', 'Kode Project');
        $xcrud->label('kode_gudang', 'Kode Gudang');
        $xcrud->label('tanggal_pengembalian', 'Tanggal Pengembalian');
        $xcrud->label('tanggal_sampai', 'Tanggal Sampai');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,kode_project,kode_gudang');
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        $xcrud->order_by('id', 'desc');
        
        // Items
        $items = $xcrud->nested_table('Material','id','t_pengembalian_barang','id_pengembalian'); // nested table
        $items->columns('kode_barang,qty,note,remark');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        // $items->relation('status', 'm_status', 'kode', ['status'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('qty', 'Quantity');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        $items->unset_csv();
        $items->unset_print();

        $this->layout->render('monitoring-pengembalian/index', [
            'box' => true,
            'boxTitle' => 'Monitoring Pengembalian',
            'title' => 'Monitoring Pengembalian',
            'pageTitle' => 'Pengembalian',
            'pageSubTitle' => 'Semua Pengembalian',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}