<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\BarangItems;
use app\models\BarangVd;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class RequestValidationController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Request', 'request/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->columns('kode,kode_project,permintaan_asal,permintaan_asal_detail,tanggal_request');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail', '', '', 'view');
        
        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);
        $xcrud->relation('kode_gudang_asal', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('kode_project_asal', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->change_type('approved', 'radio', '', [
            'Y' => 'Ya',
            'N' => 'Tidak',
        ]);
        $xcrud->relation('approved_by', 'a_buttons', 'id', ['id'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        $xcrud->label('kode_gudang_asal', 'Gudang Asal');
        $xcrud->label('kode_project_asal', 'Project Asal');
        $xcrud->label('tanggal_request', 'Tanggal Request');
        $xcrud->label('tanggal_shipment', 'Tanggal Shipment');
        $xcrud->label('tanggal_required', 'Tanggal Order');
        $xcrud->label('status', 'Status');
        
		$xcrud->button(site_url('request-validation/detail/{id}'), 'Validasi', 'fa fa-fw fa-search', 'btn-success', [
			'enable_label' => true
		]);
         
        $xcrud->validation_required('kode,kode_project,permintaan_asal');
        $xcrud->unset_edit();
        $xcrud->unset_add();
        $xcrud->unset_remove();
        
        
        if ($isPm) {
            // Request & Verification PM
            $xcrud->where("status IN ('RQ', 'VRP')");
            
            // my target project
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project_asal IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        } elseif ($isMatecon) {
            // PM Approve, Verification Matecon & Request Warehouse
            $xcrud->where("status IN ('APP', 'VR') OR (permintaan_asal = 'W' AND status = 'RQ')");
        } else {
            $xcrud->where("1=2");
        }
        $xcrud->order_by('id', 'desc');

        $this->layout->render('request-validation/index', [
            'box' => true,
            'boxTitle' => 'Validasi',
            'title' => 'Validasi',
            'pageTitle' => 'Validasi',
            'pageSubTitle' => 'Semua Permintaan Material',
            'isPm' => $isPm,
            'xcrudContent' => $xcrud->render()
        ]);
    }

    public function actionDetail($id)
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Request', 'request/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');

        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail', '', '', 'view');
        
        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);

        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        
        $xcrud->validation_required('kode,kode_project,permintaan_asal');

        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_list();
        $xcrud->unset_remove();

        $this->layout->render('request-validation/detail', [
            'box' => true,
            'boxTitle' => 'Detail Validasi',
            'title' => 'Detail Validasi',
            'pageTitle' => 'Validasi',
            'pageSubTitle' => 'Semua Permintaan Material',
            'isPm' => $isPm,
            'id' => $id,
            'xcrudContent' => $xcrud->render('view', $id)
        ]);
    }

    public function actionDetailVerifikasi($id)
    {
        $this->output->set_content_type('application/json');
        $isPm = $this->ion_auth->in_group('pm');
        
        $dataProvider = RequestBarang::with('barang')->where('id_request', $id)->get();
        $results = [];

        if ($dataProvider) {
            $approvedStatus = [
                'S' => 'System',
                'Y' => 'Ya',
                'N' => 'Tidak',
                'W' => 'Menunggu'
            ];

            foreach($dataProvider as $data) {
                $results[] = [
                    'DT_RowId' => $data->id,
                    'empty' => '<span></span>',
                    'action' => '',
                    'kode' => $data->kode_barang,
                    'nama_barang' => $data->kode_barang .' - '. $data->barang->deskripsi,
                    'qty' => $data->qty,
                    'note' => $data->note,
                    'approved' => @$approvedStatus[$data->approved],
                    'approved_by' => $data->approved_by,
                    'remark' => $data->remark,
                    'approved_pm' => @$approvedStatus[$data->approved_pm],
                    'approved_pm_by' => $data->approved_pm_by,
                    'remark_pm' => $data->remark_pm,
                    'is_approved' => $isPm ? $data->approved_pm : $data->approved
                ];
            }
        }
        echo json_encode([
            'data' => $results
        ]);
    }
    
    public function actionApprove()
    {
        $this->output->set_content_type('application/json');

        $isPm = $this->ion_auth->in_group('pm');
        $id = $this->input->post('data');
        $status = $this->input->post('status');
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        $dataProvider = RequestBarang::with(['request', 'barang'])->whereIn('id', $id)->get();
        if (!$dataProvider) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan tidak ditemukan',
                'data' => ''
            ]));
        }

        try {
            $idRequest = null;
            DB::beginTransaction();
            foreach($dataProvider as $model) {
                $idRequest = $model->id_request;
                if ($status == 'Y') {
                    $request = $model->request;
                    $barang = $model->barang;

                    $barangExists = BarangVd::where('kode', $model->kode_barang)->where(function($query) use ($request) {
                        $query->where('lokasi', $request->permintaan_asal);
                        if ($request->permintaan_asal == 'W') {
                            $query->where('lokasi_gudang', $request->kode_gudang_asal);
                        } else {
                            $query->where('lokasi_project', $request->kode_project_asal);
                        }
                    })->first();

                    if (!$barang) {
                        DB::rollBack();
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Material '. $model->kode_barang .' tidak terdaftar di system',
                            'data' => ''
                        ]));
                    } elseif (!$barangExists) {
                        DB::rollBack();
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Material '. $barang->kode .' - '. $barang->deskripsi .' sudah tidak di Lokasi tersebut',
                            'data' => ''
                        ]));
                    } elseif ($barangExists && $barangExists->qty_baik < $model->qty) {
                        DB::rollBack();
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Permintaan '. $model->qty .' material '. $barang->kode .' - '. $barang->deskripsi .' tidak dapat dipenuhi. Jumlah hanya tersedia '. $barangExists->qty_baik,
                            'data' => ''
                        ]));
                    }
                }
                
                if ($isPm) {
                    $model->approved_pm = $status;
                    $model->approved_pm_at = $currentDateTime;
                    $model->approved_pm_by = $userId;
                    $model->remark_pm = $note;
                    
                    if ($status == 'N') {
                        $model->approved = 'W';
                        $model->approved_at = null;
                        $model->approved_by = null;
                    }
                } else {
                    $model->approved = $status;
                    $model->approved_at = $currentDateTime;
                    $model->approved_by = $userId;
                    $model->remark = $note;
                }
                
                $model->updated_at = $currentDateTime;
                $model->updated_by = $userId;
                $model->ip_address = $this->input->ip_address();
                $model->useragent = $this->agent->agent_string();
                if (!$model->save()) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => 'Gagal menyimpan data',
                        'data' => ''
                    ]));
                }
            } // endforeach

            if ($idRequest) {
                $request = Request::find($idRequest);
                if (!$request) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => 'Data tidak ditemukan',
                        'data' => ''
                    ]));
                }

                // status verifikasi
                if (!in_array($request->status, ['VR', 'VRP'])) {
                    $request->status = $isPm ? 'VRP' : 'VR';
                    $request->updated_at = $currentDateTime;
                    $request->updated_by = $userId;
                    $request->ip_address = $this->input->ip_address();
                    $request->useragent = $this->agent->agent_string();
                    $request->save();
                }

                $isApproved = true;
                $isRejected = true;
                foreach($request->items as $item) {
                    if ($isPm) {
                        if ($item->approved_pm == 'W') {
                            $isApproved = false;
                        }
                        if ($item->approved_pm == 'Y') {
                            $isRejected = false;
                        }
                    } else {
                        if ($item->approved == 'W') {
                            $isApproved = false;
                        }
                        if ($item->approved == 'Y') {
                            $isRejected = false;
                        }
                    }
                }

                // jika masih ada yang W, maka status belum berubah
                // jika ada yang approve, maka bukan reject
                if ($isApproved) {
                    if ($isPm) {
                        $request->status = $isRejected ? 'RJP' : 'APP';
                    } else {
                        $request->status = $isRejected ? 'RJ' : 'AP';
                    }
                    $request->updated_at = $currentDateTime;
                    $request->updated_by = $userId;
                    $request->ip_address = $this->input->ip_address();
                    $request->useragent = $this->agent->agent_string();
                    if (!$request->save()) {
                        DB::rollBack();
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Gagal menyimpan data',
                            'data' => ''
                        ]));
                    }
                }
            }
            
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionUpdateQty($id)
    {
        $this->output->set_content_type('application/json');
        $model = RequestBarang::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan tidak ditemukan',
                'data' => ''
            ]));
        }
        $lokasi = $model->lokasi();
        if ($lokasi) {
            $qty = $this->input->post('qty');
            
            if ($lokasi->qty == 0) {
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Qty kosong',
                    'data' => ''
                ]));
            } elseif ($qty > $lokasi->qty_baik) {
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Qty tidak bisa lebih dari <b>'. $lokasi->qty_baik .'</b>',
                    'data' => ''
                ]));
            } else {
                $userId = $this->ion_auth->user_data('id');
                $currentDateTime = date('Y-m-d H:i:s');
                
                try {
                    $model->qty = $qty;
                    $model->updated_at = $currentDateTime;
                    $model->updated_by = $userId;
                    $model->ip_address = $this->input->ip_address();
                    $model->useragent = $this->agent->agent_string();
                    if ($model->save()) {
                        return $this->output->set_output(json_encode([
                            'status' => 1,
                            'message' => '',
                            'data' => ''
                        ]));
                    } else {
                        return $this->output->set_output(json_encode([
                            'status' => 0,
                            'message' => 'Gagal menyimpan data',
                            'data' => ''
                        ]));
                    }
                } catch (\Exception $ex) {
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => $ex->getMessage(),
                        'data' => ''
                    ]));
                }
            }
        }
        
        return $this->output->set_output(json_encode([
            'status' => 0,
            'message' => 'Permintaan tidak ditemukan',
            'data' => ''
        ]));
        
        
    }
}