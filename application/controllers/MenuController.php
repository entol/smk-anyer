<?php

class MenuController extends CI_Controller
{
    public function actionIndex()
    {
		$this->breadcrumbs->push('Dashboard', 'site');
		$this->breadcrumbs->push('Authentiaction', 'menu/index#');
		$this->breadcrumbs->push('Menu', 'menu');
		
		Xcrud_config::$auto_xss_filtering = false;
		$xcrud = xcrud_get_instance();
		
		$xcrud->table('a_menu');
		$xcrud->no_editor('url');
		$xcrud->change_type('url', 'text');
		$xcrud->relation('parent_id', 'a_menu', 'id', ['text', 'id'], '', 'text asc', '', ' - ');
		$xcrud->order_by('parent_id', 'asc');
		$xcrud->order_by('sort_order', 'asc');
		
		$routers = $this->ion_auth->get_routers(false, true);
		$class = ['' => '- none -'];
		$method = ['' => '- none -'];
		
		foreach($routers as $className => $og) {
			$class[$className] = $className;
			foreach($og as $m) {
				$method[$m] = $m;
			}
		}

		$modules = ['' => '- none -'];
		foreach($this->ion_auth->get_modules() as $module) {
			$modules[$module] = $module;
		}
		
		ksort($class);
		ksort($method);
		ksort($modules);
		$xcrud->change_type('controller_name', 'select', '', $class);
		$xcrud->change_type('method_name', 'select', '', $method);
		$xcrud->change_type('module_name', 'select', '', $modules);
		$xcrud->buttons_position('left');
		
		$this->layout->renderString($xcrud->render(), [
			'box' => true,
			'boxTitle' => 'Menu',
			'title' => 'Menu',
			'pageTitle' => 'Menu',
			'pageSubTitle' => 'Admin Menu'
		]);
		
		// $this->load->library('grocery_CRUD');
		// $crud = new grocery_CRUD();

		// $crud->set_table('a_menu');
		// $crud->set_subject('Menu');

		// $output = $crud->render();

		
		// $this->layout->renderString($output->output, array_merge([
		// 	'box' => true,
		// 	'boxTitle' => 'Menu',
		// 	'title' => 'Menu',
		// 	'selectedMenu' => 'Menu'
		// ], (array) $output));
    }
}
