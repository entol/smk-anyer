<?php

class GudangController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Gudang', 'gudang/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_gudang');
        $xcrud->columns('kode,gudang,kota,provinsi,alamat');
        $xcrud->fields('kode,gudang,kota,provinsi,alamat', false, false, 'view');
        $xcrud->fields('kode,gudang,kota,provinsi,alamat', false, false, 'add');
        $xcrud->fields('kode,gudang,kota,provinsi,alamat', false, false, 'edit');

        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'ID');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('gudang', 'Gudang');
        $xcrud->label('kota', 'Kota');
        $xcrud->label('provinsi', 'Provinsi');
        $xcrud->label('alamat', 'Alamat');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,gudang');
        $xcrud->unset_csv();
        $xcrud->unset_print();

        $xcrud->before_insert('gudang_before_insert', realpath(__DIR__ .'/../callbacks/gudang.php'));
        // $xcrud->after_insert('gudang_after_insert', realpath(__DIR__ .'/../callbacks/gudang.php'));
        $xcrud->before_update('gudang_before_update', realpath(__DIR__ .'/../callbacks/gudang.php'));
        // $xcrud->after_update('gudang_after_update', realpath(__DIR__ .'/../callbacks/gudang.php'));
        // $xcrud->before_remove('gudang_before_remove', realpath(__DIR__ .'/../callbacks/gudang.php'));
        // $xcrud->after_remove ('gudang_after_remove', realpath(__DIR__ .'/../callbacks/gudang.php'));

        $this->layout->render('gudang/index', [
            'box' => true,
            'boxTitle' => 'Gudang',
            'title' => 'Gudang',
            'pageTitle' => 'Gudang',
            'pageSubTitle' => 'Semua Gudang',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}