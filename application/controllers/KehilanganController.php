<?php

use app\models\Project;
use app\models\Kehilangan;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class KehilanganController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Kehilangan', 'kehilangan/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_kehilangan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,kondisi,keterangan,tanggal,status');

        $xcrud->fields('kode,kode_barang,kode_lokasi,keterangan,deskripsi,tanggal,status', false, 'Kehilangan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        $xcrud->fields('kode_lokasi,kode_barang,kondisi,keterangan,deskripsi,tanggal,draft', false, 'Kehilangan', 'create');
        $xcrud->fields('kode_lokasi,kode_barang,kondisi,keterangan,deskripsi,tanggal,draft', false, 'Kehilangan', 'edit');
        
        if ($isPm) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi', "m_lokasi_barang_vd.kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], "qty_baik > 0 AND m_barang_vd.lokasi_project IN ('". implode("', '", $kodeProject) ."')", '', '', ' - ');
            } else {
                $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], '1=2', '', '', ' - ');
            }
        } else {
            $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
            $xcrud->relation('kode_barang', 'm_barang_vd', 'kode', ['kode', 'deskripsi'], 'qty_baik > 0', '', '', ' - ', null, 'kode_lokasi', 'kode_lokasi');
        }
        
        $xcrud->no_editor('deskripsi');
        $xcrud->relation('status', 'm_status_kehilangan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);
        $xcrud->change_type('kondisi', 'radio', 'G', [
            'G' => 'G - Good',
            'R' => 'R - Repair',
            'D' => 'D - Damage' 
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,kondisi,keterangan,deskripsi,tanggal,draft');
        $xcrud->pass_default('tanggal', date('Y-m-d'));
        $xcrud->pass_default('draft', 'Y');

        $xcrud->before_insert('kehilangan_before_insert', realpath(__DIR__ .'/../callbacks/kehilangan.php'));
        $xcrud->after_insert('kehilangan_after_insert', realpath(__DIR__ .'/../callbacks/kehilangan.php'));
        $xcrud->before_update('kehilangan_before_update', realpath(__DIR__ .'/../callbacks/kehilangan.php'));

        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('kondisi', 'Kondisi Terakhir');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('tanggal', 'Perkiraan Kehilangan');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('kondisi','Kondisi barang terakhir?');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');
        
        $xcrud->where("status IN ('DR', 'RQ')");
        
        // jika tidak memiliki akses create
        if (!$this->ion_auth->in_button('kehilangan-create')) {
            $xcrud->unset_add();
        }
        
        // jika tidak memiliki akses edit
        if (!$this->ion_auth->in_button('kehilangan-update')) {
            $xcrud->unset_edit();
        } else {
            $xcrud->unset_edit(true, 'status', '!=', 'DR');
        }
        
        // jika tidak memiliki akses delete
        if (!$this->ion_auth->in_button('kehilangan-delete')) {
            $xcrud->unset_remove();
        } else {
            $xcrud->unset_remove(true, 'status', '!=', 'DR');
        }
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        $xcrud->order_by('id', 'desc');
        
        $this->layout->render('kehilangan/index', [
            'box' => true,
            'boxTitle' => 'Kehilangan',
            'title' => 'Kehilangan',
            'pageTitle' => 'Kehilangan',
            'pageSubTitle' => 'Semua Kehilangan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionLists()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Daftar Kehilangan', 'kehilangan/lists');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_kehilangan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,kondisi,keterangan,tanggal,status');

        $xcrud->fields('kode,kode_barang,kode_lokasi,keterangan,deskripsi,tanggal,status', false, 'Kehilangan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
        $xcrud->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi']);
        
        $xcrud->no_editor('deskripsi');
        $xcrud->relation('status', 'm_status_kehilangan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);
        $xcrud->change_type('kondisi', 'radio', 'G', [
            'G' => 'G - Good',
            'R' => 'R - Repair',
            'D' => 'D - Damage' 
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,keterangan,deskripsi,tanggal,draft');
        $xcrud->pass_default('tanggal', date('Y-m-d'));
        $xcrud->pass_default('draft', 'Y');
        
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();

        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('tanggal', 'Perkiraan Kehilangan');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');
        
        $xcrud->where("status IN ('RQ')");
        
        if ($this->ion_auth->in_button('kehilangan-proses')) {
            $xcrud->button('javascript:;', 'Proses', 'far fa-fw fa-check-circle', 'btn-primary btn-proses', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'RQ']);
        }
        if ($this->ion_auth->in_button('kehilangan-batal')) {
            $xcrud->button('javascript:;', 'Batal', 'far fa-fw fa-times-circle', 'btn-inverse btn-batal', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'RQ']);
        }
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        $xcrud->order_by('id', 'desc');
        
        $this->layout->render('kehilangan/index', [
            'box' => true,
            'boxTitle' => 'Daftar Kehilangan',
            'title' => 'Daftar Kehilangan',
            'pageTitle' => 'Daftar Kehilangan',
            'pageSubTitle' => 'Semua Kehilangan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionProses($status, $id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Kehilangan::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Data kehilangan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            
            // Approve
            $model->status = $status;
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent = $this->agent->agent_string();
            if (!$model->save()) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            if ($status == 'AP') {
                $barang = $model->barang;
                if (!$barang) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => 'Material tidak ditemukan',
                        'data' => ''
                    ]));
                }

                if ($model->kondisi == 'G') {
                    $barang->qty_baik = $barang->qty_baik - 1;
                    $barang->qty_hilang = $barang->hilang + 1;
                } elseif ($model->kondisi == 'R') {
                    $barang->qty_perbaikan = $barang->qty_perbaikan - 1;
                    $barang->qty_hilang = $barang->hilang + 1;
                } elseif ($model->kondisi == 'D') {
                    $barang->qty_rusak = $barang->qty_rusak - 1;
                    $barang->qty_hilang = $barang->hilang + 1;
                }
                $barang->updated_at = $currentDateTime;
                $barang->updated_by = $userId;
                $barang->ip_address = $this->input->ip_address();
                $barang->useragent = $this->agent->agent_string();
                if (!$barang->save()) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => 'Gagal menyimpan data',
                        'data' => ''
                    ]));
                }
            }

            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
}