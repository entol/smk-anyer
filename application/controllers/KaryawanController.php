<?php

class KaryawanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Karyawan', 'karyawan/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_karyawan');
        $xcrud->columns('nik,nama,tanggal_lahir,tempat_lahir,email,id_jabatan,user_login');
        $xcrud->fields('nik,nama,tanggal_lahir,tempat_lahir,email,id_jabatan,user_login', false, false, 'view');
        $xcrud->fields('nik,nama,tanggal_lahir,tempat_lahir,email,id_jabatan,user_login', false, false, 'add');
        $xcrud->fields('nik,nama,tanggal_lahir,tempat_lahir,email,id_jabatan,user_login', false, false, 'edit');

        $xcrud->relation('id_jabatan', 'm_jabatan', 'id', ['jabatan'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'name'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'name'], '', '', '', ' - ');
        
        $xcrud->change_type('user_login', 'radio', 'N', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);

        $xcrud->label('id', 'ID');
        $xcrud->label('nik', 'NIK');
        $xcrud->label('nama', 'Nama');
        $xcrud->label('tanggal_lahir', 'Tanggal Lahir');
        $xcrud->label('tempat_lahir', 'Tempat Lahir');
        $xcrud->label('email', 'Email');
        $xcrud->label('id_jabatan', 'Jabatan');
        $xcrud->label('user_login', 'User Login');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('nama,id_jabatan,user_login');
        
        if (!$this->ion_auth->in_button('karyawan-create')) {
            $xcrud->unset_add();
        }
        
        if (!$this->ion_auth->in_button('karyawan-update')) {
            $xcrud->unset_edit();
        }
        
        if (!$this->ion_auth->in_button('karyawan-delete')) {
            $xcrud->unset_remove();
        }
        
        $xcrud->before_insert('karyawan_before_insert', realpath(__DIR__ .'/../callbacks/karyawan.php'));
        $xcrud->after_insert('karyawan_after_insert', realpath(__DIR__ .'/../callbacks/karyawan.php'));
        $xcrud->before_update('karyawan_before_update', realpath(__DIR__ .'/../callbacks/karyawan.php'));
        // $xcrud->after_update('karyawan_after_update', realpath(__DIR__ .'/../callbacks/karyawan.php'));
        // $xcrud->before_remove('karyawan_before_remove', realpath(__DIR__ .'/../callbacks/karyawan.php'));
        $xcrud->after_remove ('karyawan_after_remove', realpath(__DIR__ .'/../callbacks/karyawan.php'));

        $this->layout->render('karyawan/index', [
            'box' => true,
            'boxTitle' => 'Karyawan',
            'title' => 'Karyawan',
            'pageTitle' => 'Karyawan',
            'pageSubTitle' => 'Semua Karyawan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}