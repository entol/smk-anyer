<?php

class StatusPerbaikanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Status Perbaikan', 'status-perbaikan/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_status_perbaikan');
        $xcrud->columns('kode,status');
        $xcrud->fields('kode,status,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $xcrud->fields('kode,status', false, false, 'create');
        $xcrud->fields('kode,status', false, false, 'edit');

        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,status');

        $xcrud->before_insert('status_perbaikan_before_insert', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));
        // $xcrud->after_insert('status_perbaikan_after_insert', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));
        $xcrud->before_update('status_perbaikan_before_update', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));
        // $xcrud->after_update('status_perbaikan_after_update', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));
        // $xcrud->before_remove('status_perbaikan_before_remove', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));
        // $xcrud->after_remove ('status_perbaikan_after_remove', realpath(__DIR__ .'/../callbacks/status-perbaikan.php'));

        $this->layout->render('status-perbaikan/index', [
            'box' => true,
            'boxTitle' => 'Status Perbaikan',
            'title' => 'Status Perbaikan',
            'pageTitle' => 'Status Perbaikan',
            'pageSubTitle' => 'Semua Status Perbaikan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}