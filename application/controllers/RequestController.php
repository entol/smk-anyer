<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class RequestController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Request', 'request/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->columns('kode,kode_project,permintaan_asal,permintaan_asal_detail,tanggal_request,status');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail', '', 'Lokasi', 'view');
        $xcrud->fields('status,tanggal_request,tanggal_shipment,tanggal_required', '', 'Status', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');

        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);
        $xcrud->relation('kode_gudang_asal', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('kode_project_asal', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        $xcrud->label('kode_gudang_asal', 'Gudang Asal');
        $xcrud->label('kode_project_asal', 'Project Asal');
        $xcrud->label('tanggal_request', 'Tanggal Request');
        $xcrud->label('tanggal_shipment', 'Tanggal Shipment');
        $xcrud->label('tanggal_required', 'Tanggal Order');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
         
        $xcrud->validation_required('kode,kode_project,permintaan_asal');
        
        if (!$this->ion_auth->in_button('request-create')) {
            $xcrud->unset_add();
        }
        
        if (!$this->ion_auth->in_button('request-update')) {
            $xcrud->unset_edit();
        }
        
        if (!$this->ion_auth->in_button('request-delete')) {
            $xcrud->unset_remove();
        }
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        $xcrud->where("status IN ('DR', 'RQ', 'RJ', 'RM')");
        $xcrud->order_by('id', 'desc');

        // $xcrud->before_insert('request_before_insert', realpath(__DIR__ .'/../callbacks/request.php'));
        // $xcrud->after_insert('request_after_insert', realpath(__DIR__ .'/../callbacks/request.php'));
        // $xcrud->before_update('request_before_update', realpath(__DIR__ .'/../callbacks/request.php'));
        // $xcrud->after_update('request_after_update', realpath(__DIR__ .'/../callbacks/request.php'));
        // $xcrud->before_remove('request_before_remove', realpath(__DIR__ .'/../callbacks/request.php'));
        $xcrud->after_remove ('request_after_remove', realpath(__DIR__ .'/../callbacks/request.php'));
        
        $items = $xcrud->nested_table('Material','id','t_request_barang','id_request'); // nested table
        $items->columns('kode_barang,qty,note,approved,approved_by,approved_pm,approved_pm_by');
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        $items->relation('approved_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $items->relation('approved_pm_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        // $items->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        // $items->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);
        $items->highlight_row('approved', '=', 'Y', '', 'success');

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        $items->label('approved', 'Approved');
        $items->label('approved_at', 'Approved At');
        $items->label('approved_by', 'Approved By');
        $items->label('approved_pm', 'Approved PM');
        $items->label('approved_pm_at', 'Approved PM At');
        $items->label('approved_pm_by', 'Approved PM By');
        $items->label('remark', 'Remark');
        $items->label('created_at', 'Created At');
        $items->label('created_by', 'Created By');
        $items->label('updated_at', 'Updated At');
        $items->label('updated_by', 'Updated By');
        $items->label('ip_address', 'IP Address');
        $items->label('useragent', 'User Agent');

        $this->layout->render('request/index', [
            'box' => true,
            'boxTitle' => 'Request',
            'title' => 'Request',
            'pageTitle' => 'Request',
            'pageSubTitle' => 'Semua Request',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionCreate()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Request', 'request/index');
        $this->breadcrumbs->push('Tambah', 'request/create');
        
        $dpProject = Project::all()->keyBy('kode');
        $optProject = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['nama'];
        }, $dpProject->toArray()));
        
        $dpGudang = Gudang::all()->keyBy('kode');
        $optGudang = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['gudang'];
        }, $dpGudang->toArray()));
        
        if ($this->ion_auth->in_group('pm')) {
            $dpMyProject = Project::getMyProject()->keyBy('kode');
            $optMyProject = array_merge(['' => ' - none - '], array_map(function($item) {
                return $item['kode'] .' - '. $item['nama'];
            }, $dpMyProject->toArray()));
        } else {
            $optMyProject = $optProject;
        }
        
        $this->layout->render('request/create', [
            'box' => false,
            'title' => 'Tambah Request',
            'pageTitle' => 'Request',
            'pageSubTitle' => 'Tambah',
            'activeMenu' => 'request',
            'dpProject' => $dpProject,
            'optProject' => $optProject,
            'optMyProject' => $optMyProject,
            'dpGudang' => $dpGudang,
            'optGudang' => $optGudang
        ]);
    }
    
    public function actionUpdate($pk)
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Request', 'request/index');
        $this->breadcrumbs->push('Update', 'request/update');
        
        $request = Request::find($pk);
        $items = $request->items;
        if (!$request) {
            show_404();
        }
        
        $dpProject = Project::where('kode', '!=', $request->kode_project)->get()->keyBy('kode');
        $optProject = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['nama'];
        }, $dpProject->toArray()));
        
        
        $dpGudang = Gudang::all()->keyBy('kode');
        $optGudang = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['gudang'];
        }, $dpGudang->toArray()));
        
        $this->layout->render('request/update', [
            'box' => false,
            'title' => 'Update Request',
            'pageTitle' => 'Request',
            'pageSubTitle' => 'Update',
            'activeMenu' => 'request',
            'request' => $request,
            'items' => $items,
            'dpProject' => $dpProject,
            'optProject' => $optProject,
            'dpGudang' => $dpGudang,
            'optGudang' => $optGudang
        ]);
    }
    
    public function actionCreateProcess()
    {
        $this->output->set_content_type('application/json');
        
        $project = $this->input->post('project');
        $draft = $this->input->post('draft');
        $permintaanAsal = $this->input->post('permintaan_asal');
        $kodeGudangAsal = $this->input->post('kode_gudang_asal');
        $kodeProjectAsal = $this->input->post('kode_project_asal');
        $tanggalShipment = $this->input->post('tannggal_shipment');
        $tanggalRequired = $this->input->post('tanggal_required');
        $kdBarang = $this->input->post('kode_barang'); // array
        $qty = $this->input->post('qty'); // array
        $note = $this->input->post('note'); // array
        
        if (empty($project) || trim($project) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Project Tujuan tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($permintaanAsal) || trim($permintaanAsal) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan Asal tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdBarang)) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Material tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $rqTpl = 'RQ'. date('ymd');
        
        $last = Request::whereBetween('created_at', [$currentDate .' 00:00:00', $currentDate .' 23:59:59'])
            ->whereNotNull('kode')
            ->orderBy('id', 'desc')
            ->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = str_replace($rqTpl, '', $last->kode);
        }
        
        DB::beginTransaction();
        try {
            $error = false;
            
            $request = new Request;
            $request->kode_project = $project;
            $request->permintaan_asal = $permintaanAsal;
            $request->kode_gudang_asal = $kodeGudangAsal;
            $request->kode_project_asal = $kodeProjectAsal;
            $request->tanggal_request = $currentDate;
            $request->tanggal_shipment = $tanggalShipment == '' ? null : $tanggalShipment;
            $request->tanggal_required = $tanggalRequired == '' ? null : $tanggalRequired;
            $request->status = $draft == 'Y' ? 'DR' : 'RQ';
            $request->draft = $draft;
            
            $request->created_at = $currentDateTime;
            $request->created_by = $userId;
            $request->updated_at = $currentDateTime;
            $request->updated_by = $userId;
            $request->ip_address = $this->input->ip_address();
            $request->useragent = $this->agent->agent_string();
            if ($request->save()) {
                $request->kode = $rqTpl . ($increment + 1);
                $request->save();
                
                foreach($kdBarang as $k => $kodeBarang) {
                    $item = new RequestBarang;
                    $item->id_request = $request->id;
                    $item->kode_barang = $kodeBarang;
                    $item->qty = $qty[$k];
                    $item->note = $note[$k];
                    
                    if ($permintaanAsal == 'W') {
                        $item->approved_pm = 'S';
                        $item->approved_pm_at = $currentDateTime;
                        // $item->approved_pm_by = $userId;
                    } else {
                        $item->approved_pm = 'W';
                    }
                    $item->approved = 'W';
                    
                    $item->created_at = $currentDateTime;
                    $item->created_by = $userId;
                    $item->updated_at = $currentDateTime;
                    $item->updated_by = $userId;
                    $item->ip_address = $this->input->ip_address();
                    $item->useragent = $this->agent->agent_string();
                    if (!$item->save()) {
                        $error = true;
                    }
                }
            } else {
                $error = true;
            }
            
            if ($error) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            $this->session->set_flashdata('success', '<b>Berhasil!</b> Anda berhasil menambahkan permintaan material baru.');
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' =>  $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionUpdateProcess($id)
    {
        $this->output->set_content_type('application/json');
        
        $request = Request::find($id);
        if (!$request) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Request tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $project = $request->kode_project;
        $draft = $this->input->post('draft');
        $permintaanAsal = $this->input->post('permintaan_asal');
        $kodeGudangAsal = $this->input->post('kode_gudang_asal');
        $kodeProjectAsal = $this->input->post('kode_project_asal');
        $tanggalShipment = $this->input->post('tannggal_shipment');
        $tanggalRequired = $this->input->post('tanggal_required');
        $kdBarang = $this->input->post('kode_barang'); // array
        $qty = $this->input->post('qty'); // array
        $note = $this->input->post('note'); // array
        
        if (empty($permintaanAsal) || trim($permintaanAsal) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan Asal tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdBarang)) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Material tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $rqTpl = 'RQ'. date('ymd');
        
        $last = Request::where('tanggal_request', $currentDate)->orderBy('id', 'desc')->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = str_replace($rqTpl, '', $last->kode);
        }
        
        DB::beginTransaction();
        try {
            $error = false;
            
            $request->draft = $draft;
            $request->status = $draft == 'Y' ? 'DR' : 'RQ';
            $request->permintaan_asal = $permintaanAsal;
            $request->kode_gudang_asal = $kodeGudangAsal;
            $request->kode_project_asal = $kodeProjectAsal;
            $request->tanggal_shipment = $tanggalShipment == '' ? null : $tanggalShipment;
            $request->tanggal_required = $tanggalRequired == '' ? null : $tanggalRequired;
            
            $request->updated_at = $currentDateTime;
            $request->updated_by = $userId;
            $request->ip_address = $this->input->ip_address();
            $request->useragent = $this->agent->agent_string();
            
            if ($request->save()) {
                // on remove
                foreach($request->items as $item) {
                    if (!in_array($item->kode_barang, array_values($kdBarang))) {
                        $item->delete();
                    }
                }
                
                // insert new
                foreach($kdBarang as $k => $kodeBarang) {
                    $item = RequestBarang::where('kode_barang', $kodeBarang)->where('id_request', $request->id)->first();
                    if (!$item) {
                        $item = new RequestBarang;
                        $item->id_request = $request->id;
                        $item->kode_barang = $kodeBarang;
                        $item->created_at = $currentDateTime;
                        $item->created_by = $userId;
                        $item->approved = 'W';

                        if ($permintaanAsal == 'W') {
                            $item->approved_pm = 'S';
                            $item->approved_pm_at = $currentDateTime;
                            // $item->approved_pm_by = $userId;
                        } else {
                            $item->approved_pm = 'W';
                        }
                    }
                    
                    if ($item->qty != $qty[$k]) {
                        // perubahan jumlah harus approve ulang
                        if ($permintaanAsal == 'W') {
                            $item->approved_pm = 'S';
                            $item->approved_pm_at = $currentDateTime;
                            // $item->approved_pm_by = $userId;
                        }
                        $item->approved = 'W';
                        $item->approved_at = null;
                        $item->approved_by = null;
                    }
                    
                    $item->qty = $qty[$k];
                    $item->note = $note[$k];
                    $item->updated_at = $currentDateTime;
                    $item->updated_by = $userId;
                    $item->ip_address = $this->input->ip_address();
                    $item->useragent = $this->agent->agent_string();
                    
                    if (!$item->save()) {
                        $error = true;
                    }
                }
            } else {
                $error = true;
            }
            
            if ($error) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            $this->session->set_flashdata('success', '<b>Berhasil!</b> Anda berhasil memperbaharui permintaan material.');
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' =>  $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionLookupBarang()
    { 
        $this->output->set_content_type('application/json');
        $kdb = $this->input->get('kdb');
        $kdi = $this->input->get('kdi');
        $asal = $this->input->get('asal');
        $kda = $this->input->get('kda');
        
        $this->db->select('i.id,b.kode,b.kode_kategori,k.kategori,b.deskripsi,b.brand,b.tipe,b.tahun,i.lokasi_gudang,i.lokasi_project,i.qty_baik');
        $this->db->from('m_barang_items i');
        $this->db->join('m_barang b', 'b.kode = i.kode_barang');
        $this->db->join('m_kategori_material k', 'k.kode = b.kode_kategori');
        
        if ($asal == 'W') {
            $this->db->where('i.lokasi_gudang', $kda);
        } else {
            $this->db->where('i.lokasi_project', $kda);
        }
        if ($kdb != null && $kdb != '') {
            $this->db->where_not_in('i.id', explode(',', $kdb));
        }
        $this->db->where('qty_baik >', 0);

        $results = [];
        $query = $this->db->get();
        if ($query && $query->num_rows()) {
            $dataProvider = $query->result_object();
            foreach($dataProvider as $model) {
                $results[] = [
                    'DT_RowId' => $model->kode,
                    'empty' => '',
                    'id' => $model->id,
                    'kode' => $model->kode,
                    'kode_kategori' => $model->kode_kategori,
                    'kategori' => $model->kode_kategori .' - '. $model->kategori,
                    'deskripsi' => $model->deskripsi,
                    'brand' => $model->brand,
                    'tipe' => $model->tipe,
                    'tahun' => $model->tahun,
                    // 'kondisi' => $model->kondisi .' - '. $model->condition,
                    'gudang' => $model->lokasi_gudang,
                    'project' => $model->lokasi_project,
                    'asal' => $asal,
                    'quantity' => $model->qty_baik,
                ];
            }
        }

        echo json_encode([
            'data' => $results
        ]);
    }
}