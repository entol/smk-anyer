<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class MonitoringPerbaikanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Monitoring Perbaikan', 'monitoring-perbaikan/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_perbaikan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,status');
        $xcrud->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,draft', false, 'Perbaikan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');

        $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
        $xcrud->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi']);
    
        $xcrud->relation('status', 'm_status_perbaikan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,keterangan,jenis_perbaikan,draft');
        $xcrud->column_pattern('estimasi_waktu','{value} Hari');
        
        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('kepala_mekanik', 'Kepala Mekanik');
        $xcrud->label('tanggal_pendaftaran', 'Tanggal Pendaftaran');
        $xcrud->label('estimasi_waktu', 'Estimasi Waktu');
        $xcrud->label('status', 'Status');
        $xcrud->label('jenis_perbaikan', 'Jenis Perbaikan');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('estimasi_waktu','Estimas dalam hari');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');

        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        
        $xcrud->order_by('id', 'desc');
        
        // Parts
        $parts = $xcrud->nested_table('Parts','id','t_perbaikan_parts','id_perbaikan'); // nested table
        $parts->columns('part,keterangan,jenis_perbaikan,status');
        $parts->fields('part,keterangan,jenis_perbaikan,status,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'create');
        $parts->fields('part,keterangan,jenis_perbaikan', false, false, 'edit');

        $parts->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $parts->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $parts->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $parts->change_type('status', 'select', 'W', [
            'W' => 'Sedang Perbaikan',
            'D' => 'Fixed',
            'F' => 'Failed'
        ]);
        
        $parts->validation_required('part,keterangan,jenis_perbaikan');

        $parts->label('part', 'Nama Part');
        $parts->label('keterangan', 'Keterangan');
        $parts->label('jenis_perbaikan', 'Jenis Perbaikan');
        $parts->label('status', 'Status');
        $parts->label('created_at', 'Created At');
        $parts->label('created_by', 'Created By');
        $parts->label('updated_at', 'Updated At');
        $parts->label('updated_by', 'Updated By');
        $parts->label('ip_address', 'Ip Address');
        
        // History
        $history = $xcrud->nested_table('Riwayat','id','t_perbaikan_history','id_perbaikan'); // nested table
        $history->columns('status,created_at,created_by');
        
        $history->relation('status', 'm_status_perbaikan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $history->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $history->label('status', 'Status');
        $history->label('created_at', 'Created At');
        $history->label('created_by', 'Created By');
        $history->label('updated_at', 'Updated At');
        $history->label('updated_by', 'Updated By');
        $history->label('ip_address', 'Ip Address');
        

        $this->layout->render('monitoring-perbaikan/index', [
            'box' => true,
            'boxTitle' => 'Monitoring Perbaikan',
            'title' => 'Monitoring Perbaikan',
            'pageTitle' => 'Perbaikan',
            'pageSubTitle' => 'Semua Perbaikan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}