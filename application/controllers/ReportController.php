<?php

use app\models\Barang;
use app\models\BarangKondisiVd;
use app\models\BarangItemsKondisiVd;
use app\models\KategoriMaterial;
use app\models\Kondisi;
use app\models\Gudang;
use app\models\Project;

class ReportController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function actionIndex()
    {

    }
    
    public function actionRekapMaterial()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Report', 'report/index');
        $this->breadcrumbs->push('Rekap Material', 'report/rekap-material');

        // selection
        $kategori = $this->input->get('kategori');
        $kondisi = $this->input->get('kondisi');
        $lokasi = $this->input->get('lokasi');
        $phrase = $this->input->get('search-phrase');
        $column = $this->input->get('search-column');
        $cari = $this->input->get('cari');
        $print = $this->input->get('print');
        
        // Print
        if ($print == 1 || $print == 2) {
            $query = BarangItemsKondisiVd::query();
            $query->with(['barang', 'condition', 'project', 'gudang']);
            if ($kategori != '*' && !empty($kategori)) {
                $query->where('kode_kategori', $kategori);
            }
            if ($kondisi != '*' && !empty($kondisi)) {
                $query->where('kondisi', $kondisi);
            }
            if ($lokasi != '*' && !empty($lokasi)) {
                $query->where('kode_project', $lokasi);
            }

            if (!empty($phrase)) {
                if (!empty($column)) {
                    // not empty
                    $query->where(function($query) use ($phrase, $column) {
                        $column = str_replace('m_barang_items_kondisi_vd.', '', $column);
                        if (in_array($column, ['kode', 'kategori', 'deskripsi', 'brand', 'tipe', 'tahun', 'qty'])) {
                            $query->where($column, 'like', '%'. $phrase .'%');
                        } elseif ($column == 'kondisi') {
                            $query->whereHas('condition', function($query) use ($phrase) {
                                $query->where('kondisi', 'like', '%'. $phrase .'%');
                            });
                        } elseif ($column == 'nama_lokasi') {
                            $query->orWhereHas('project', function($query) use ($phrase) {
                                $query->where('singkatan', 'like', '%'. $phrase .'%');
                            });
                            $query->orWhereHas('gudang', function($query) use ($phrase) {
                                $query->where('gudang', 'like', '%'. $phrase .'%');
                            });
                        }
                    });
                } else {
                    $query->where(function($query) use ($phrase) {
                        $query->orWhere('kode', 'like', '%'. $phrase .'%');
                        $query->orWhere('kategori', 'like', '%'. $phrase .'%');
                        $query->orWhere('deskripsi', 'like', '%'. $phrase .'%');
                        $query->orWhere('brand', 'like', '%'. $phrase .'%');
                        $query->orWhere('tipe', 'like', '%'. $phrase .'%');
                        $query->orWhere('tahun', 'like', '%'. $phrase .'%');
                        $query->orWhere('qty', 'like', '%'. $phrase .'%');
                        $query->orWhereHas('condition', function($query) use ($phrase) {
                            $query->where('kondisi', 'like', '%'. $phrase .'%');
                        });
                        $query->orWhereHas('project', function($query) use ($phrase) {
                            $query->where('singkatan', 'like', '%'. $phrase .'%');
                        });
                        $query->orWhereHas('gudang', function($query) use ($phrase) {
                            $query->where('gudang', 'like', '%'. $phrase .'%');
                        });
                    });
                }
            }

            $query->orderBy("kode_lokasi", 'asc');
            $query->orderBy("kode_barang", 'asc');
            $query->orderBy("id", 'desc');

            $dataProvider = $query->get();
            
            if ($print == 1) {
                // pdf
                return $this->export->pdf('report/rekap-material-rpt', [
                    'dataProvider' => $dataProvider,
                    'total' => count($dataProvider),
                    'dompdf' => [
                        'orientation' => 'landscape',
                        'name' => 'rekap-material.pdf'
                    ]    
                ], true);
            } else {
                // csv
                return $this->export->csv($dataProvider, [
                    'No',
                    'Kode Material',
                    'Kategori',
                    'Deskripsi',
                    'Brand',
                    'Size / Type',
                    'Tahun',
                    'Kondisi',
                    'Quantity',
                    'Lokasi'
                ], function($item, $index) {
                    return [
                        $index + 1,
                        $item->kode,
                        $item->kategori,
                        $item->deskripsi,
                        $item->brand,
                        $item->tipe,
                        $item->tahun,
                        $item->condition->kondisi,
                        $item->qty,
                        $item->getNamaLokasi()
                    ];
                }, [
                    'name' => 'rekap-material.csv'
                ]);
            }
        }
        
        // kategori material
        $dpKategoriMaterial = KategoriMaterial::all()->keyBy('kode');
        $opKategoriMaterial = ['*' => ' - semua -'];
        foreach($dpKategoriMaterial as $mdMaterial) {
            $opKategoriMaterial[$mdMaterial->kode] = $mdMaterial->kode.' - '. $mdMaterial->kategori;
        }
        
        // kondisi
        $dpKondisi = Kondisi::all()->keyBy('kode');
        $opKondisi = ['*' => ' - semua -'];
        foreach($dpKondisi as $mdKondisi) {
            $opKondisi[$mdKondisi->kode] = $mdKondisi->kode.' - '. $mdKondisi->kondisi;
        }

        // lokasi
        $opLokasi = ['*' => ' - semua - '];

        $dpLokasiGudang = Gudang::all()->keyBy('kode');
        foreach($dpLokasiGudang as $mdLokasi) {
            $opLokasi[$mdLokasi->kode] = $mdLokasi->kode.' - '. $mdLokasi->gudang;
        }

        $dpLokasiProject = Project::all()->keyBy('kode');
        foreach($dpLokasiProject as $mdLokasi) {
            $opLokasi[$mdLokasi->kode] = $mdLokasi->kode.' - '. $mdLokasi->singkatan;
        }

        // xcrud
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_barang_items_kondisi_vd');
        $xcrud->columns('kode,kategori,deskripsi,brand,tipe,tahun,kondisi,qty,nama_lokasi');
        
        // $xcrud->relation('kode_kategori', 'm_kategori_material', 'kode', ['kode', 'kategori'], '', '', '', ' - ');
        $xcrud->relation('kondisi', 'm_kondisi', 'kode', ['kode', 'kondisi'], '', '', '', ' - ');
        
        $xcrud->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));
        
        $xcrud->label('kode', 'Kode Material');
        $xcrud->label('kategori', 'Kategori');
        $xcrud->label('model', 'Serial No');
        $xcrud->label('brand', 'Brand');
        $xcrud->label('tipe', 'Size/Tipe');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('tahun', 'Tahun');
        $xcrud->label('satuan', 'Satuan');
        $xcrud->label('nama_lokasi', 'Lokasi');
        $xcrud->label('qty', 'Qty');
        
        $xcrud->order_by("kode_lokasi", 'asc');
        $xcrud->order_by("kode_barang", 'asc');
        $xcrud->order_by("id", 'desc');

        // $xcrud->column_callback('kondisi', 'barang_kondisi_cc', realpath(__DIR__ .'/../callbacks/barang.php'));
    
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_view();
        $xcrud->unset_remove();
        // $xcrud->unset_search();
        $xcrud->unset_sortable();

        // cari
        if ($cari == '1') {
            if ($kategori != '*' && !empty($kategori)) {
                $xcrud->where('kode_kategori', $kategori);
            }
            if ($kondisi != '*' && !empty($kondisi)) {
                $xcrud->where('kondisi', $kondisi);
            }
            if ($lokasi != '*' && !empty($lokasi)) {
                $xcrud->where("(lokasi_project = '{$lokasi}' OR lokasi_gudang = '{$lokasi}')");
            }
        } else {
            $xcrud->where('1=0');
        }
    
        $this->layout->render('report/rekap-material', [
            'box' => true,
            'boxTitle' => 'Rekap Material',
            'title' => 'Rekap Material',
            'pageTitle' => 'Report',
            'pageSubTitle' => 'Rekap Material',
            'xcrudContent' => $xcrud->render(),
            'opKategoriMaterial' => $opKategoriMaterial,
            'opKondisi' => $opKondisi,
            'opLokasi' => $opLokasi,
            'kategori' => $kategori,
            'kondisi' => $kondisi,
            'lokasi' => $lokasi,
            'phrase' => $phrase,
            'column' => $column
        ]);
    }
    
    public function actionHistoryDistribusiMaterial()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Report', 'report/index');
        $this->breadcrumbs->push('Rekap History Distribusi', 'report/history-distribusi-material');

        // selection
        $kategori = $this->input->get('kategori');
        $kondisi = $this->input->get('kondisi');
        $material = $this->input->get('material');
        $lokasi = $this->input->get('lokasi');
        $phrase = $this->input->get('search-phrase');
        $column = $this->input->get('search-column');
        $cari = $this->input->get('cari');
        $print = $this->input->get('print');
        
        // Print
        if ($print == 1 || $print == 2) {
            $subDistribusi = '(SELECT GROUP_CONCAT(distribusi SEPARATOR \';\') FROM (
                SELECT h.*, (CASE WHEN lokasi = \'W\' THEN CONCAT(g.gudang) ELSE CONCAT(p.nama) END) distribusi FROM m_barang_history h
                LEFT JOIN m_gudang g ON (g.kode = h.lokasi_gudang)
                LEFT JOIN t_project p ON (p.kode = h.lokasi_project)
                ) x WHERE x.kode_barang = kode)';
            $query = BarangKondisiVd::query();
            $query->select('*');
            $query->selectRaw($subDistribusi .' distribusi');


            $query->with(['barang', 'condition']);
            if ($kategori != '*' && !empty($kategori)) {
                $query->where('kode_kategori', $kategori);
            }
            if ($kondisi != '*' && !empty($kondisi)) {
                $query->where('kondisi', $kondisi);
            }
            if ($material != '' && !empty($material)) {
                $query->where('kode', $material);
            }
            if ($lokasi != '*' && !empty($lokasi)) {
                $query->where('kode_lokasi', $lokasi);
            }

            if (!empty($phrase)) {
                if (!empty($column)) {
                    // not empty
                    $query->where(function($query) use ($phrase, $column, $subDistribusi) {
                        $column = str_replace('m_barang_kondisi_vd.', '', $column);
                        if (in_array($column, ['kode', 'kategori', 'deskripsi', 'brand', 'tipe', 'tahun', 'qty'])) {
                            $query->where($column, 'like', '%'. $phrase .'%');
                        } elseif ($column == 'kondisi') {
                            $query->whereHas('condition', function($query) use ($phrase) {
                                $query->where('kondisi', 'like', '%'. $phrase .'%');
                            });
                        } elseif ($column == 'distribusi') {
                            $query->orWhereRaw($subDistribusi ." LIKE '%{$phrase}%'");
                        }
                    });
                } else {
                    $query->where(function($query) use ($phrase, $subDistribusi) {
                        $query->orWhere('kode', 'like', '%'. $phrase .'%');
                        $query->orWhere('kategori', 'like', '%'. $phrase .'%');
                        $query->orWhere('deskripsi', 'like', '%'. $phrase .'%');
                        $query->orWhere('brand', 'like', '%'. $phrase .'%');
                        $query->orWhere('tipe', 'like', '%'. $phrase .'%');
                        $query->orWhere('tahun', 'like', '%'. $phrase .'%');
                        $query->orWhere('qty', 'like', '%'. $phrase .'%');
                        $query->orWhereHas('condition', function($query) use ($phrase) {
                            $query->where('kondisi', 'like', '%'. $phrase .'%');
                        });
                        $query->orWhereRaw($subDistribusi ." LIKE '%{$phrase}%'");
                    });
                }
            }

            $query->orderBy("kode_lokasi", 'asc');
            $query->orderBy("kode_barang", 'asc');
            $query->orderBy("id", 'desc');

            $dataProvider = $query->get();
            
            if ($print == 1) {
                // pdf
                return $this->export->pdf('report/history-distribusi-material-rpt', [
                    'dataProvider' => $dataProvider,
                    'total' => count($dataProvider),
                    // 'logo' => 'data:image/png;base64,'. base64_encode(file_get_contents(FCPATH .'assets/wp/logo2.png')),
                    'dompdf' => [
                        'orientation' => 'landscape',
                        'name' => 'history-distribusi-material.pdf'
                    ]    
                ], true);
            } else {
                // csv
                return $this->export->csv($dataProvider, [
                    'No',
                    'Kode Material',
                    'Kategori',
                    'Deskripsi',
                    'Brand',
                    'Size / Type',
                    'Tahun',
                    'Kondisi',
                    'Quantity',
                    'Lokasi',
                    'Distribusi',
                ], function($item, $index) {
                    return [
                        $index + 1,
                        $item->kode,
                        $item->kategori,
                        $item->deskripsi,
                        $item->brand,
                        $item->tipe,
                        $item->tahun,
                        $item->condition->kondisi,
                        $item->qty,
                        $item->barang->getNamaLokasi(),
                        $item->distribusi
                    ];
                }, [
                    'name' => 'rekap-material.csv'
                ]);
            }
        }
        
        // kategori material
        $dpKategoriMaterial = KategoriMaterial::all()->keyBy('kode');
        $opKategoriMaterial = ['*' => ' - semua -'];
        foreach($dpKategoriMaterial as $mdMaterial) {
            $opKategoriMaterial[$mdMaterial->kode] = $mdMaterial->kode.' - '. $mdMaterial->kategori;
        }
        
        // kondisi
        $dpKondisi = Kondisi::all()->keyBy('kode');
        $opKondisi = ['*' => ' - semua -'];
        foreach($dpKondisi as $mdKondisi) {
            $opKondisi[$mdKondisi->kode] = $mdKondisi->kode.' - '. $mdKondisi->kondisi;
        }

        // lokasi
        $opLokasi = ['*' => ' - semua - '];

        $dpLokasiGudang = Gudang::all()->keyBy('kode');
        foreach($dpLokasiGudang as $mdLokasi) {
            $opLokasi[$mdLokasi->kode] = $mdLokasi->kode.' - '. $mdLokasi->gudang;
        }

        $dpLokasiProject = Project::all()->keyBy('kode');
        foreach($dpLokasiProject as $mdLokasi) {
            $opLokasi[$mdLokasi->kode] = $mdLokasi->kode.' - '. $mdLokasi->singkatan;
        }
        
        // xcrud
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_barang_kondisi_vd');
        $xcrud->columns('kode,kategori,deskripsi,brand,tipe,tahun,kondisi,qty,nama_lokasi, distribusi');
        
        $xcrud->relation('kondisi', 'm_kondisi', 'kode', ['kondisi'], '', '', '', ' - ');
        
        $xcrud->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));
        
        $xcrud->subselect('distribusi', "SELECT GROUP_CONCAT(distribusi SEPARATOR ';') FROM (
            SELECT h.*, (CASE WHEN lokasi = 'W' THEN CONCAT(g.gudang) ELSE CONCAT(p.nama) END) distribusi FROM m_barang_history h
            LEFT JOIN m_gudang g ON (g.kode = h.lokasi_gudang)
            LEFT JOIN t_project p ON (p.kode = h.lokasi_project)
            ) x WHERE kode_barang = {kode}");
        
        $xcrud->label('kode', 'Kode Material');
        $xcrud->label('kode_kategori', 'Kategori');
        $xcrud->label('model', 'Serial No');
        $xcrud->label('brand', 'Brand');
        $xcrud->label('tipe', 'Size/Tipe');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('tahun', 'Tahun');
        $xcrud->label('satuan', 'Satuan');
        $xcrud->label('qty', 'Quantity');
        $xcrud->label('nama_lokasi', 'Lokasi Terakhir');
        $xcrud->label('distribusi', 'Distribusi');
        
        $xcrud->order_by("kode_lokasi", 'asc');
        $xcrud->order_by("kode_barang", 'asc');
        $xcrud->order_by("id", 'desc');

        // $xcrud->column_callback('kondisi', 'barang_kondisi_cc', realpath(__DIR__ .'/../callbacks/barang.php'));
        $xcrud->column_width('distribusi','200px'); 
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_view();
        $xcrud->unset_remove();
        // $xcrud->unset_search();
        $xcrud->unset_sortable();

        // cari
        if ($cari == '1') {
            if ($kategori != '*' && !empty($kategori)) {
                $xcrud->where('kode_kategori', $kategori);
            }
            if ($kondisi != '*' && !empty($kondisi)) {
                $xcrud->where('kondisi', $kondisi);
            }
            if ($lokasi != '*' && !empty($lokasi)) {
                $xcrud->where("(lokasi_project = '{$lokasi}' OR lokasi_gudang = '{$lokasi}')");
            }
            if ($material != '' && !empty($material)) {
                $xcrud->where('kode', $material);
            }
        } else {
            $xcrud->where('1=0');
        }
    
        $this->layout->render('report/history-distribusi-material', [
            'box' => true,
            'boxTitle' => 'History Distribusi Material',
            'title' => 'History Distribusi Material',
            'pageTitle' => 'Report',
            'pageSubTitle' => 'History Distribusi Material',
            'xcrudContent' => $xcrud->render(),
            'opKategoriMaterial' => $opKategoriMaterial,
            'opKondisi' => $opKondisi,
            'opLokasi' => $opLokasi,
            'kategori' => $kategori,
            'kondisi' => $kondisi,
            'lokasi' => $lokasi,
            'material' => $material,
            'phrase' => $phrase,
            'column' => $column
        ]);
    }
}