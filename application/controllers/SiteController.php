<?php

class SiteController extends CI_Controller
{
    public function actionIndex()
    {
        
		$this->layout->render('site/index', [
            'pageTitle' => 'Dashboard'
		]);
    }
}
