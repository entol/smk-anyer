<?php

use app\models\Project;
use app\models\Pengembalian;
use app\models\PengembalianBarang;
use app\models\PengembalianHistory;
use app\models\Barang;
use app\models\BarangHistory;
use app\models\Gudang;
use app\models\SuratJalan;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class PengembalianController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Pengembalian', 'pengembalian/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_pengembalian');
        $xcrud->columns('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, 'Pengembalian', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, false, 'create');
        $xcrud->fields('kode,kode_project,kode_gudang,tanggal_pengembalian,tanggal_sampai,status', false, false, 'edit');

        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('kode_gudang', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode Pengembalian');
        $xcrud->label('kode_project', 'Kode Project');
        $xcrud->label('kode_gudang', 'Kode Gudang');
        $xcrud->label('tanggal_pengembalian', 'Tanggal Pengembalian');
        $xcrud->label('tanggal_sampai', 'Tanggal Sampai');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,kode_project,kode_gudang');
        
        if (!$this->ion_auth->in_button('pengembalian-create')) {
            $xcrud->unset_add();
        }
        
        if (!$this->ion_auth->in_button('pengembalian-update')) {
            $xcrud->unset_edit();
        } else {
            $xcrud->unset_edit(true, 'status', 'in', ['RT', 'SH']);
        }
        
        if (!$this->ion_auth->in_button('pengembalian-delete')) {
            $xcrud->unset_remove();
        } else {
            $xcrud->unset_remove(true, 'status', 'in', ['RT', 'SH']);
        }
        
        if ($this->ion_auth->in_button('pengembalian-surat-jalan')) {
            $xcrud->button('javascript:;', 'Surat Keluar', 'fas fa-fw fa-envelope-open-text', 'btn-inverse btn-proses-surat', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'RT']);
            
            $xcrud->button('javascript:;', 'Cetak Ulang', 'fas fa-fw fa-envelope-open-text', 'btn-inverse btn-proses-surat', [
                'enable_label' => true,
                'data-id' => '{id}'
            ], ['status', '=', 'SH']);
        }
        
        $xcrud->after_remove ('pengembalian_after_remove', realpath(__DIR__ .'/../callbacks/pengembalian.php'));

        $xcrud->order_by('id', 'desc');
        $xcrud->where("status in ('DR', 'RT', 'SH')");
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }

        // Items
        $items = $xcrud->nested_table('Material','id','t_pengembalian_barang','id_pengembalian'); // nested table
        $items->columns('kode_barang,kondisi,qty,note,remark');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        $items->relation('kondisi', 'm_kondisi', 'kode', ['kode', 'kondisi'], '', '', '', ' - ');
        // $items->relation('status', 'm_status', 'kode', ['status'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        $items->unset_csv();
        $items->unset_print();
        $this->layout->render('pengembalian/index', [
            'box' => true,
            'boxTitle' => 'Pengembalian Material',
            'title' => 'Pengembalian Material',
            'pageTitle' => 'Pengembalian',
            'pageSubTitle' => 'Semua Pengembalian',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionCreate()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Pengembalian', 'pengembalian/index');
        $this->breadcrumbs->push('Tambah', 'pengembalian/create');
        
        $dpProject = Project::all()->keyBy('kode');
        $optProject = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['nama'];
        }, $dpProject->toArray()));
        
        if ($this->ion_auth->in_group('pm')) {
            $dpMyProject = Project::getMyProject()->keyBy('kode');
            $optMyProject = array_merge(['' => ' - none - '], array_map(function($item) {
                return $item['kode'] .' - '. $item['nama'];
            }, $dpMyProject->toArray()));
        } else {
            $optMyProject = $optProject;
        }
        
        $dpGudang = Gudang::all()->keyBy('kode');
        $optGudang = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['gudang'];
        }, $dpGudang->toArray()));
        
        $this->layout->render('pengembalian/create', [
            'box' => false,
            'title' => 'Tambah Pengembalian',
            'pageTitle' => 'Pengembalian',
            'pageSubTitle' => 'Tambah',
            'activeMenu' => 'pengembalian',
            'dpProject' => $dpProject,
            'optProject' => $optProject,
            'dpGudang' => $dpGudang,
            'optGudang' => $optGudang,
            'optMyProject' => $optMyProject
        ]);
    }
    
    public function actionUpdate($pk)
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Pengembalian', 'pengembalian/index');
        $this->breadcrumbs->push('Update', 'pengembalian/update');
        
        $pengembalian = Pengembalian::find($pk);
        $items = $pengembalian->items;
        if (!$pengembalian) {
            show_404();
        }
        
        $dpProject = Project::where('kode', '!=', $pengembalian->kode_project)->get()->keyBy('kode');
        $optProject = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['nama'];
        }, $dpProject->toArray()));
        
        $dpGudang = Gudang::all()->keyBy('kode');
        $optGudang = array_merge(['' => ' - none - '], array_map(function($item) {
            return $item['kode'] .' - '. $item['gudang'];
        }, $dpGudang->toArray()));
        
        $this->layout->render('pengembalian/update', [
            'box' => false,
            'title' => 'Update Pengembalian',
            'pageTitle' => 'Pengembalian',
            'pageSubTitle' => 'Update',
            'activeMenu' => 'pengembalian',
            'pengembalian' => $pengembalian,
            'items' => $items,
            'dpProject' => $dpProject,
            'optProject' => $optProject,
            'dpGudang' => $dpGudang,
            'optGudang' => $optGudang
        ]);
    }
    
    public function actionCreateProcess()
    {
        $this->output->set_content_type('application/json');
        
        $project = $this->input->post('project');
        $kdGudang = $this->input->post('kode_gudang');
        $tglPengembalian = $this->input->post('tanggal_pengembalian');
        $draft = $this->input->post('draft');
        
        $kdBarang = $this->input->post('kode_barang'); // array
        $qty = $this->input->post('qty'); // array
        $note = $this->input->post('note'); // array
        $kondisi = $this->input->post('kondisi'); // array
        
        if (empty($project) || trim($project) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Project asal tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdGudang) || trim($kdGudang) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Gudang tujuan tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdBarang)) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Material tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $retTpl = 'RET'. date('ymd');
        
        $last = Pengembalian::whereBetween('created_at', [$currentDate .' 00:00:00', $currentDate .' 23:59:59'])
            ->whereNotNull('kode')
            ->orderBy('id', 'desc')
            ->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = str_replace($retTpl, '', $last->kode);
        }
        
        DB::beginTransaction();
        try {
            $error = false;
            
            $pengembalian = new Pengembalian;
            $pengembalian->kode_project = $project;
            $pengembalian->kode_gudang = $kdGudang;
            $pengembalian->tanggal_pengembalian = $tglPengembalian == '' ? null : $tglPengembalian;
            $pengembalian->status = $draft == 'Y' ? 'DR' : 'RT';
            $pengembalian->draft = $draft;
            
            $pengembalian->created_at = $currentDateTime;
            $pengembalian->created_by = $userId;
            $pengembalian->updated_at = $currentDateTime;
            $pengembalian->updated_by = $userId;
            $pengembalian->ip_address = $this->input->ip_address();
            $pengembalian->useragent = $this->agent->agent_string();
            if ($pengembalian->save()) {
                $pengembalian->kode = $retTpl . ($increment + 1);
                $pengembalian->save();
                
                foreach($kdBarang as $i => $kodeBarang) {
                    $item = new PengembalianBarang;
                    $item->id_pengembalian = $pengembalian->id;
                    $item->kode_barang = $kodeBarang;
                    $item->qty = $qty[$i];
                    $item->note = $note[$i];
                    $item->kondisi = $kondisi[$i];
                    
                    $item->approved = 'W';
                    $item->created_at = $currentDateTime;
                    $item->created_by = $userId;
                    $item->updated_at = $currentDateTime;
                    $item->updated_by = $userId;
                    $item->ip_address = $this->input->ip_address();
                    $item->useragent = $this->agent->agent_string();
                    if (!$item->save()) {
                        $error = true;
                    }
                }
            } else {
                $error = true;
            }
            
            if ($error) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            $this->session->set_flashdata('success', '<b>Berhasil!</b> Anda berhasil menambahkan permintaan pengembalian material.');
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' =>  $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionUpdateProcess($id)
    {
        $this->output->set_content_type('application/json');
        
        $pengembalian = Pengembalian::find($id);
        if (!$pengembalian) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Pengembalian tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $project = $pengembalian->kode_project;
        $kdGudang = $this->input->post('kode_gudang');
        $tglPengembalian = $this->input->post('tanggal_pengembalian');
        $draft = $this->input->post('draft');
        
        $kdBarang = $this->input->post('kode_barang'); // array
        $qty = $this->input->post('qty'); // array
        $note = $this->input->post('note'); // array
        $kondisi = $this->input->post('kondisi'); // array
        
        if (empty($project) || trim($project) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Project asal tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdGudang) || trim($kdGudang) == '') {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Gudang tujuan tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        if (empty($kdBarang)) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Material tidak boleh kosong',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');
        $retTpl = 'RET'. date('ymd');
        
        $last = Pengembalian::whereBetween('created_at', [$currentDate .' 00:00:00', $currentDate .' 23:59:59'])
            ->orderBy('id', 'desc')
            ->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = str_replace($retTpl, '', $last->kode);
        }
        
        DB::beginTransaction();
        try {
            $error = false;
            
            $pengembalian->kode_gudang = $kdGudang;
            $pengembalian->tanggal_pengembalian = $tglPengembalian == '' ? null : $tglPengembalian;
            $pengembalian->status = $draft == 'Y' ? 'DR' : 'RT';
            $pengembalian->draft = $draft;
            
            $pengembalian->updated_at = $currentDateTime;
            $pengembalian->updated_by = $userId;
            $pengembalian->ip_address = $this->input->ip_address();
            $pengembalian->useragent = $this->agent->agent_string();
            
            if ($pengembalian->save()) {
                // new kode lists
                $idx = [];
                foreach($kdBarang as $i => $kode) {
                    $idx[] = $kondisi[$i] .'-'. $kode;
                }

                // on remove
                foreach($pengembalian->items as $item) {
                    if (!in_array($item->kondisi .'-'. $item->kode_barang, $idx)) {
                        $item->delete();
                    }
                }
                
                // insert new
                foreach($kdBarang as $i => $kodeBarang) {
                    $item = PengembalianBarang::where('id_pengembalian', $pengembalian->id)
                        ->where('kode_barang', $kodeBarang)
                        ->where('kondisi', $kondisi[$i])
                        ->first();
                    if (!$item) {
                        $item = new PengembalianBarang;
                        $item->id_pengembalian = $pengembalian->id;
                        $item->kode_barang = $kodeBarang;
                        $item->kondisi = $kondisi[$i];
                        $item->approved = 'W';
                        $item->created_at = $currentDateTime;
                        $item->created_by = $userId;
                    }
                    
                    if ($item->qty != $qty[$i]) {
                        $item->approved = 'W';
                        $item->approved_at = null;
                        $item->approved_by = null;
                    }
                    
                    $item->qty = $qty[$i];
                    $item->note = $note[$i];
                    $item->updated_at = $currentDateTime;
                    $item->updated_by = $userId;
                    $item->ip_address = $this->input->ip_address();
                    $item->useragent = $this->agent->agent_string();
                    
                    if (!$item->save()) {
                        $error = true;
                    }
                }
            } else {
                $error = true;
            }
            
            if ($error) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
            
            $this->session->set_flashdata('success', '<b>Berhasil!</b> Anda berhasil memperbaharui permintaan pengembalian material.');
            DB::commit();
            return $this->output->set_output(json_encode([
                'status' => 1,
                'message' => '',
                'data' => ''
            ]));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' =>  $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
    
    public function actionLookupBarang()
    { 
        $this->output->set_content_type('application/json');
        $kdb = $this->input->get('kdb');
        $kdi = $this->input->get('kdi');
        $asal = $this->input->get('asal');
        $kda = $this->input->get('kda');

        $this->db->select('id, xid, kode_barang, kode_kategori, kategori, deskripsi, brand, tipe, tahun, lokasi_gudang, lokasi_project, qty, kondisi');
        $this->db->from('m_barang_items_kondisi_vd');
        
        if ($asal == 'W') {
            $this->db->where('lokasi_gudang', $kda);
        } else {
            $this->db->where('lokasi_project', $kda);
        }
        if ($kdb != null && $kdb != '') {
            $this->db->where_not_in('xid', explode(',', $kdb));
        }

        $results = [];
        $query = $this->db->get();
        if ($query && $query->num_rows()) {
            $dataProvider = $query->result_object();
            foreach($dataProvider as $model) {
                $results[] = [
                    'DT_RowId' => $model->xid,
                    'empty' => '',
                    'id' => $model->xid,
                    'kode' => $model->kode_barang,
                    'kode_kategori' => $model->kode_kategori,
                    'kategori' => $model->kode_kategori .' - '. $model->kategori,
                    'deskripsi' => $model->deskripsi,
                    'brand' => $model->brand,
                    'tipe' => $model->tipe,
                    'tahun' => $model->tahun,
                    // 'kondisi' => $model->kondisi .' - '. $model->condition,
                    'kondisi' => $model->kondisi,
                    'nama_kondisi' => (
                        $model->kondisi == 'G' 
                        ? '<span style="font-size: 10px" class="label label-primary">Good</span>' 
                        : (
                            $model->kondisi == 'R' 
                            ? '<span style="font-size: 10px" class="label label-warning">Repair</span>' 
                            : (
                                $model->kondisi == 'D' 
                                ? '<span style="font-size: 10px" class="label label-danger">Damaged</span>' 
                                : '<span style="font-size: 10px" class="label label-dark">Good</span>'
                            )
                        )
                    ),
                    'gudang' => $model->lokasi_gudang,
                    'project' => $model->lokasi_project,
                    'asal' => $asal,
                    'quantity' => $model->qty,
                ];
            }
        }

        echo json_encode([
            'data' => $results
        ]);
    }
    
    public function actionLoadSurat($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Pengembalian::find($id);
        if ($model) {
            $userId = $this->ion_auth->user_data('id');
            $currentDateTime = date('Y-m-d H:i:s');
            $note = $this->input->post('note');
            
            try {
                DB::beginTransaction();
                $suratJalan = SuratJalan::where('kode_request', $model->kode)->first();
                if ($suratJalan) {
                    return $this->output->set_output(json_encode([
                        'status' => 1,
                        'message' => '',
                        'data' => $suratJalan->toArray()
                    ]));
                } else {
                    $suratJalan = new SuratJalan;
                    $suratJalan->kode_request = $model->kode;
                    $suratJalan->from = $model->kode_project .' - '. $model->project->singkatan;
                    $suratJalan->to = $model->kode_gudang .' - '. $model->gudang->gudang;
                    $suratJalan->address = $model->gudang->alamat;
                    $suratJalan->delivery_date = date('Y-m-d');
                    $suratJalan->created_at = $currentDateTime;
                    $suratJalan->created_by = $userId;
                    $suratJalan->updated_at = $currentDateTime;
                    $suratJalan->updated_by = $userId;
                    $suratJalan->ip_address = $this->input->ip_address();
                    $suratJalan->useragent = $this->agent->agent_string();
                    if ($suratJalan->save()) {
                        // $model->status = 'SH';
                        $model->id_surat_jalan = $suratJalan->id;
                        $model->ip_address = $this->input->ip_address();
                        $model->useragent = $this->agent->agent_string();
                        $model->save();
                        DB::commit();

                        return $this->output->set_output(json_encode([
                            'status' => 1,
                            'message' => '',
                            'data' => $suratJalan->toArray()
                        ]));
                    }
                }
                DB::rollBack();
            } catch (\Exception $ex) {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => $ex->getMessage(),
                    'data' => null
                ]));
            }
        }
        
        return $this->output->set_output(json_encode([
            'status' => 0,
            'message' => 'Permintaan tidak ditemukan',
            'data' => ''
        ]));
    }

    public function actionProsesSurat($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Pengembalian::find($id);
        if ($model) {
            $suratJalan = SuratJalan::where('kode_request', $model->kode)->first();
            if ($suratJalan) {

                $userId = $this->ion_auth->user_data('id');
                $currentDateTime = date('Y-m-d H:i:s');
                $note = $this->input->post('note');
                
                try {
                    DB::beginTransaction();
                    $model->status = 'SH';
                    $model->updated_at = $currentDateTime;
                    $model->updated_by = $userId;
                    $model->ip_address = $this->input->ip_address();
                    $model->useragent = $this->agent->agent_string();
                    if ($model->save()) {
                        $suratJalan->address = $this->input->post('address');
                        $suratJalan->delivery_date = $this->input->post('delivery-date');
                        $suratJalan->carrier_name = $this->input->post('carrier-name');
                        $suratJalan->vehicle = $this->input->post('vehicle');
                        $suratJalan->attn = $this->input->post('attn');
                        $suratJalan->delivery_no = $this->input->post('delivery-no');
    
                        $suratJalan->updated_at = $currentDateTime;
                        $suratJalan->updated_by = $userId;
                        $suratJalan->ip_address = $this->input->ip_address();
                        $suratJalan->useragent = $this->agent->agent_string();
    
                        if ($suratJalan->save()) {
                            DB::commit();
                            return $this->output->set_output(json_encode([
                                'status' => 1,
                                'message' => '',
                                'data' => null
                            ]));
                        }   
                    }
                    DB::rollBack();
                } catch (\Exception $ex) {
                    DB::rollBack();
                    return $this->output->set_output(json_encode([
                        'status' => 0,
                        'message' => $ex->getMessage(),
                        'data' => null
                    ]));
                }
            }
        }
        
        return $this->output->set_output(json_encode([
            'status' => 0,
            'message' => 'Permintaan tidak ditemukan',
            'data' => ''
        ]));
    }
    
    public function actionCetakSurat($id)
    {
        $model = Pengembalian::find($id);
        if (!$model) {
            show_404();
        }
        $surat = $model->suratJalan;
        if (!$surat) {
            show_404();
        }

        // delivery date
        $deliveryDate = $surat->delivery_date;
        try {
            $dt = Carbon::createFromFormat('Y-m-d', $deliveryDate);
            $deliveryDate = $dt->format('l, d M Y');
        } catch (\Exception $ex) {
            $deliveryDate = $surat->delivery_date;
        }
         
        $this->export->pdf('surat-jalan-rpt', [
            'model' => $model,
            'items' => $model->items,
            'total' => count($model->items),
            'surat' => $surat,
            'deliveryDate' => $deliveryDate,
            // 'logo' => 'data:image/png;base64,'. base64_encode(file_get_contents(FCPATH .'assets/wp/logo2.png')),
            'dompdf' => [
                'name' => 'surat-jalan.pdf'
            ]
        ]);
    }
}