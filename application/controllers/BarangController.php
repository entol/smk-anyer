<?php

class BarangController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Barang', 'barang/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_barang_vd');
        $xcrud->table_db('m_barang');
        $xcrud->primary_key = 'id';
        
        $xcrud->columns('kode,kode_kategori,deskripsi,brand,tipe,tahun,model,qty,nama_lokasi');
        $xcrud->fields('kode,kode_kategori,deskripsi,brand,tipe,tahun,model,satuan,sum_qty', false, 'Info', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        $xcrud->relation('kode_kategori', 'm_kategori_material', 'kode', ['kode', 'kategori'], '', '', '', ' - ');
        // $xcrud->relation('kondisi', 'm_kondisi', 'kode', ['kode', 'kondisi'], '', '', '', ' - ');
        
        $xcrud->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));
        
        $xcrud->subselect('sum_qty', "SELECT CONCAT(sum(qty_baik), ',', sum(qty_perbaikan), ',', sum(qty_rusak), ',', sum(qty_hilang)) FROM m_barang_items WHERE kode_barang = {kode}");
        // $xcrud->subselect('total_qty_baik', 'SELECT sum(qty_baik) FROM m_barang_items WHERE kode_barang = {kode}');
        // $xcrud->subselect('total_qty_perbaikan', 'SELECT sum(qty_perbaikan) FROM m_barang_items WHERE kode_barang = {kode}');
        // $xcrud->subselect('total_qty_rusak', 'SELECT sum(qty_rusak) FROM m_barang_items WHERE kode_barang = {kode}');
        // $xcrud->subselect('total_qty_hilang', 'SELECT sum(qty_hilang) FROM m_barang_items WHERE kode_barang = {kode}');
        
        $xcrud->change_type('satuan', 'select', '', [
            'EA' => 'EA',
            'KG' => 'KILOGRAM',
            'PC' => 'PCS',
            'RO' => 'ROLL',
            'SE' => 'SET',
            'UN' => 'UNIT',
            
        ]);
        $xcrud->change_type('lokasi', 'radio', 'W', [
            'W' => 'Gudang',
            'P' => 'Proyek'
        ]);

        $xcrud->label('id', 'ID');
        $xcrud->label('kode', 'Kode Material');
        $xcrud->label('kode_kategori', 'Kategori');
        $xcrud->label('model', 'Serial No');
        $xcrud->label('brand', 'Brand');
        $xcrud->label('tipe', 'Size/Tipe');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('tahun', 'Tahun');
        $xcrud->label('satuan', 'Satuan');
        $xcrud->label('nama_lokasi', 'Lokasi');
        $xcrud->label('sum_qty', 'Total Qty');
        // $xcrud->label('total_qty_baik', 'Total Qty Baik');
        // $xcrud->label('total_qty_perbaikan', 'Total Qty Perbaikan');
        // $xcrud->label('total_qty_rusak', 'Total Qty Rusak');
        // $xcrud->label('total_qty_hilang', 'Total Qty Hilang');
        $xcrud->label('qty', 'Total Qty');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'Ip Address');
        $xcrud->label('useragent', 'Useragent');
        
        // $xcrud->order_by("CASE kondisi WHEN 'G' THEN 'A' ELSE kondisi END", 'asc');
        $xcrud->order_by("id", 'desc');
        $xcrud->column_callback('sum_qty', 'barang_sum_qty', realpath(__DIR__ .'/../callbacks/barang.php'));
        // $xcrud->column_callback('kondisi', 'barang_kondisi_cc', realpath(__DIR__ .'/../callbacks/barang.php'));
        $xcrud->before_remove('barang_before_remove', realpath(__DIR__ .'/../callbacks/barang.php'));
       
        
        // items
        $items = $xcrud->nested_table('Lokasi Items','kode','m_barang_items','kode_barang'); // nested table
        $items->columns('nama_lokasi,qty_baik,qty_perbaikan,qty_rusak,qty_hilang,created_at,updated_at');
        $items->fields('nama_lokasi,qty_baik,qty_perbaikan,qty_rusak,qty_hilang,created_at,updated_at');
        $items->sum('qty_baik,qty_perbaikan,qty_rusak,qty_hilang');

        $items->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));
        // $items->change_type('lokasi', 'radio', 'W', [
        //     'W' => 'Gudang',
        //     'P' => 'Proyek'
        // ]);
        // $items->relation('lokasi_gudang', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        // $items->relation('lokasi_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');

        $items->label('nama_lokasi', 'Lokasi');
        $items->label('qty_baik', 'Qty Baik');
        $items->label('qty_rusak', 'Qty Rusak');
        $items->label('qty_perbaikan', 'Qty Perbaikan');
        $items->label('qty_hilang', 'Qty Hilang');



        // history items
        $history = $xcrud->nested_table('History Distribusi','kode','m_barang_history','kode_barang'); // nested table
        $history->columns('nama_lokasi,created_at');
        $history->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));

        $history->label('id', 'ID');
        $history->label('kode', 'Kode Item');
        $history->label('kode_barang', 'Kode Material');
        $history->label('nama_lokasi', 'Lokasi');
        $history->label('created_at', 'Tanggal');
        
        $history->unset_add();
        $history->unset_edit();
        $history->unset_remove();
        $history->order_by('id','desc');


        
        // maintenance
        $maintenance = $xcrud->nested_table('History Maintenance','kode','t_perbaikan','kode_barang'); // nested table
        $maintenance->columns('kode,kode_lokasi,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan,status');
        $maintenance->fields('kode_lokasi,kode_barang,keterangan,kepala_mekanik,tanggal_pendaftaran,estimasi_waktu,jenis_perbaikan', false, 'Perbaikan', 'view');
        $maintenance->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
            
        $maintenance->relation('status', 'm_status_perbaikan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $maintenance->relation('jenis_perbaikan', 'm_jenis_perbaikan', 'kode', ['kode', 'jenis_perbaikan'], '', '', '', ' - ');
        $maintenance->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $maintenance->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $maintenance->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
        
        $maintenance->validation_required('kode,kode_lokasi,kode_barang,keterangan,jenis_perbaikan,draft');
        $maintenance->pass_default('tanggal_pendaftaran', date('Y-m-d'));
        $maintenance->pass_default('draft', 'Y');
        $maintenance->column_pattern('estimasi_waktu','{value} Hari');

        $maintenance->before_insert('perbaikan_before_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $maintenance->after_insert('perbaikan_after_insert', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $maintenance->before_update('perbaikan_before_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $maintenance->after_update('perbaikan_after_update', realpath(__DIR__ .'/../callbacks/perbaikan.php'));
        $maintenance->after_remove ('perbaikan_after_remove', realpath(__DIR__ .'/../callbacks/perbaikan.php'));

        $maintenance->label('kode', 'Kode');
        $maintenance->label('kode_barang', 'Kode Barang');
        $maintenance->label('keterangan', 'Keterangan');
        $maintenance->label('kode_lokasi', 'Lokasi Barang');
        $maintenance->label('kepala_mekanik', 'Kepala Mekanik');
        $maintenance->label('tanggal_pendaftaran', 'Tanggal Pendaftaran');
        $maintenance->label('estimasi_waktu', 'Estimasi Waktu');
        $maintenance->label('status', 'Status');
        $maintenance->label('jenis_perbaikan', 'Jenis Perbaikan');
        $maintenance->label('created_at', 'Created At');
        $maintenance->label('created_by', 'Created By');
        $maintenance->label('updated_at', 'Updated At');
        $maintenance->label('updated_by', 'Updated By');
        $maintenance->label('ip_address', 'IP Address');
        $maintenance->label('useragent', 'User Agent');
        $maintenance->field_tooltip('estimasi_waktu','Estimas dalam hari');
        $maintenance->field_tooltip('draft','Simpan sebagai draft?');
        
        $maintenance->unset_add();
        $maintenance->unset_edit();
        // $maintenance->unset_view();
        $maintenance->unset_remove();
        $maintenance->where("status IN ('AP', 'MT', 'FX', 'FA')");
        $maintenance->order_by('id', 'desc');
        
        $this->layout->render('barang/index', [
            'box' => true,
            'boxTitle' => 'Material',
            'title' => 'Material',
            'pageTitle' => 'Material',
            'pageSubTitle' => 'Semua Material',
            'xcrudContent' => $xcrud->render()
        ]);
    }

    public function actionKelola($id = null)
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Barang', 'barang/index');
        $this->breadcrumbs->push('Tambah', 'barang/create');
        
        $xcrud = xcrud_get_instance('kelola-barang');
        $xcrud->table('m_barang');
        $xcrud->fields('kode,kode_kategori,deskripsi,brand,tipe,tahun,model,satuan,qty,lokasi,lokasi_gudang,lokasi_project', false, 'Info', 'add');
        
        if ($this->ion_auth->in_group(['admin', 'matecon-admin'])) {
            $xcrud->fields('kode,kode_kategori,deskripsi,brand,tipe,tahun,model,satuan,qty,lokasi,lokasi_gudang,lokasi_project', false, 'Info', 'edit');
        } else {
            $xcrud->fields('kode,kode_kategori,deskripsi,model,brand,tipe,tahun', false, 'Info', 'edit');
        }

        $xcrud->relation('kode_kategori', 'm_kategori_material', 'kode', ['kode', 'kategori'], '', '', '', ' - ');
        $xcrud->relation('lokasi_gudang', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('lokasi_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        // $xcrud->relation('kondisi', 'm_kondisi', 'kode', ['kode', 'kondisi'], '', '', '', ' - ');
        
        $xcrud->change_type('satuan', 'select', '', [
            'EA' => 'EA',
            'KG' => 'KILOGRAM',
            'PC' => 'PCS',
            'RO' => 'ROLL',
            'SE' => 'SET',
            'UN' => 'UNIT',
            
        ]);
        $xcrud->change_type('lokasi', 'radio', 'W', [
            'W' => 'Gudang',
            'P' => 'Proyek'
        ]);
        // $xcrud->pass_default('kondisi', 'G');
        $xcrud->pass_default('qty', 1);
        $xcrud->set_attr('qty', [
            'min' => 1
        ]);
        
        $xcrud->label('kode', 'Kode Material');
        $xcrud->label('kode_kategori', 'Kategori');
        $xcrud->label('model', 'Serial No');
        $xcrud->label('brand', 'Brand');
        $xcrud->label('tipe', 'Size/Tipe');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('tahun', 'Tahun');
        $xcrud->label('satuan', 'Satuan');
        $xcrud->label('nama_lokasi', 'Lokasi');
        
        $xcrud->validation_required('kode,kode_kategori,deskripsi,satuan,lokasi,created_at,created_by,updated_at,updated_by,ip_address,useragent');
        $xcrud->before_insert('barang_before_insert', realpath(__DIR__ .'/../callbacks/barang.php'));
        $xcrud->after_insert('barang_after_insert', realpath(__DIR__ .'/../callbacks/barang.php'));
        $xcrud->before_update('barang_before_update', realpath(__DIR__ .'/../callbacks/barang.php'));
        $xcrud->after_update('barang_after_update', realpath(__DIR__ .'/../callbacks/barang.php'));
        
        
        // items
        $items = $xcrud->nested_table('Lokasi Items','kode','m_barang_items','kode_barang'); // nested table
        $items->columns('nama_lokasi,qty_baik,qty_perbaikan,qty_rusak,qty_hilang,created_at,updated_at');
        $items->fields('nama_lokasi,qty_baik,qty_perbaikan,qty_rusak,qty_hilang,created_at,updated_at');
        $items->sum('qty_baik,qty_perbaikan,qty_rusak,qty_hilang');

        $items->subselect('nama_lokasi', implode(' ', [
            "CASE WHEN `lokasi` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {lokasi_gudang})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {lokasi_project})",
            "END"
        ]));
        $items->validation_required('nama_lokasi,qty_baik,qty_rusak');

        $items->label('nama_lokasi', 'Lokasi');
        $items->label('qty_baik', 'Qty Baik');
        $items->label('qty_rusak', 'Qty Rusak');
        $items->label('qty_perbaikan', 'Qty Perbaikan');
        $items->label('qty_hilang', 'Qty Hilang');

        $items->unset_add();
        // $items->unset_view();
        $items->unset_edit();
        $items->unset_remove();

        if ($id == null) {
            $render = $xcrud->render('create');
            $title = 'Tambah Material';
            $ev = 'create';
        } else {
            $render = $xcrud->render('edit', $id);
            $title = 'Edit Material';
            $ev = '';
        }
       
        $this->layout->render('barang/kelola', [
            'box' => true,
            'boxTitle' => $title,
            'title' => 'Material',
            'pageTitle' => 'Material',
            'pageSubTitle' => $title,
            'event' => $ev,
            'xcrudContent' => $render
        ]);
    }
}