<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class MonitoringKehilanganController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Monitoring Kehilangan', 'monitoring-kehilangan/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_kehilangan');
        $xcrud->columns('kode,kode_barang,kode_lokasi,kondisi,keterangan,tanggal,status');

        $xcrud->fields('kode,kode_barang,kode_lokasi,keterangan,deskripsi,tanggal,status', false, 'Kehilangan', 'view');
        $xcrud->fields('created_at,created_by,updated_at,updated_by,ip_address,useragent', false, 'Other', 'view');
        
        $xcrud->relation('kode_lokasi', 'm_lokasi_barang_vd', 'kode_lokasi', 'nama_lokasi');
        $xcrud->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi']);
        
        $xcrud->no_editor('deskripsi');
        $xcrud->relation('status', 'm_status_kehilangan', 'kode', ['kode', 'status'], '', '', '', ' - ');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        
        $xcrud->change_type('draft', 'radio', 'Y', [
            'Y' => 'Ya',
            'N' => 'Tidak'
        ]);
        $xcrud->change_type('kondisi', 'radio', 'G', [
            'G' => 'G - Good',
            'R' => 'R - Repair',
            'D' => 'D - Damage' 
        ]);

        $xcrud->validation_required('kode,kode_lokasi,kode_barang,keterangan,deskripsi,tanggal,draft');
        $xcrud->pass_default('tanggal', date('Y-m-d'));
        $xcrud->pass_default('draft', 'Y');
        
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();

        $xcrud->label('kode', 'Kode');
        $xcrud->label('kode_barang', 'Kode Barang');
        $xcrud->label('keterangan', 'Keterangan');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('kode_lokasi', 'Lokasi Barang');
        $xcrud->label('tanggal', 'Perkiraan Kehilangan');
        $xcrud->label('status', 'Status');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');
        $xcrud->field_tooltip('draft','Simpan sebagai draft?');
        
        if ($this->ion_auth->in_group('pm')) {
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_lokasi IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        }
        $xcrud->order_by('id', 'desc');

        $this->layout->render('monitoring-kehilangan/index', [
            'box' => true,
            'boxTitle' => 'Monitoring Kehilangan',
            'title' => 'Monitoring Kehilangan',
            'pageTitle' => 'Kehilangan',
            'pageSubTitle' => 'Semua Kehilangan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}