<?php

class JenisPerbaikanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Jenis Perbaikan', 'jenis-perbaikan/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_jenis_perbaikan');
        $xcrud->columns('kode,jenis_perbaikan');
        $xcrud->fields('kode,jenis_perbaikan,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $xcrud->fields('kode,jenis_perbaikan', false, false, 'create');
        $xcrud->fields('kode,jenis_perbaikan', false, false, 'edit');

        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('jenis_perbaikan', 'Jenis Perbaikan');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,jenis_perbaikan');

        $xcrud->before_insert('jenis_perbaikan_before_insert', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));
        // $xcrud->after_insert('jenis_perbaikan_after_insert', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));
        $xcrud->before_update('jenis_perbaikan_before_update', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));
        // $xcrud->after_update('jenis_perbaikan_after_update', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));
        // $xcrud->before_remove('jenis_perbaikan_before_remove', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));
        // $xcrud->after_remove ('jenis_perbaikan_after_remove', realpath(__DIR__ .'/../callbacks/jenis-perbaikan.php'));

        $this->layout->render('jenis-perbaikan/index', [
            'box' => true,
            'boxTitle' => 'Jenis Perbaikan',
            'title' => 'Jenis Perbaikan',
            'pageTitle' => 'Jenis Perbaikan',
            'pageSubTitle' => 'Semua Jenis Perbaikan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}