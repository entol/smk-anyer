<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class AuthController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	public function actionIndex()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->breadcrumbs->push('Dashboard', 'site');
			$this->breadcrumbs->push('Authentication', 'auth');
			$this->breadcrumbs->push('Users', 'auth/index');

			$xcrud = xcrud_get_instance();
			$xcrud->table('a_users');
			$xcrud->columns('nama,username,email');
			$xcrud->fields('nama,username,email,password');
			
			// $xcrud->relation('id_provinsi','m_provinsi','id_prov','nama');
			// $xcrud->relation('id_kabupaten','m_kabupaten','id_kab','nama', '', '', '', '', '', 'id_prov', 'id_provinsi');
			// $xcrud->relation('id_kecamatan','m_kecamatan','id_kec','nama', '', '', '', '', '', 'id_kab','id_kabupaten');
			// $xcrud->relation('id_kelurahan','m_kelurahan','id_kel','nama', '', '', '', '', '', 'id_kec','id_kecamatan');

			// $xcrud->change_type('lokasi', 'select', '', [
			// 	'DS' => 'Desa',
			// 	'KEC' => 'Kecamatan',
			// 	'DPMD' => 'DPMD',
			// 	'BPKAD' => 'BPKAD'
			// ]);

			$xcrud->change_type('password', 'password');
			// $xcrud->label('id_provinsi', 'Provinsi');
			// $xcrud->label('id_kabupaten', 'Kabupaten');
			// $xcrud->label('id_kecamatan', 'Kecamatan');
			// $xcrud->label('id_kelurahan', 'Desa');

			$xcrud->before_insert('auth_user_before_insert');
			$xcrud->before_update('auth_user_before_update');
			$xcrud->after_insert('auth_user_after_insert');
			
			$xcrud->button('auth/activate/{id}', lang('index_inactive_link'), 'fa fa-fw fa-eye', 'btn-primary', [], [
				'active', '!=', '1' // == 0
			]);
			$xcrud->button('auth/deactivate/{id}', lang('index_active_link'), 'fa fa-fw fa-eye-slash', 'btn-inverse', [], [
				'active', '=', '1' // == 1
			]);
			$xcrud->unset_remove(true, 'id', '=', 1);
			$xcrud->validation_pattern('email', 'email');
			$xcrud->validation_required('name');
			$xcrud->validation_required('username');
			// $xcrud->validation_required('password');
			$xcrud->validation_required('lokasi');
		
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			// $this->data['users'] = $this->ion_auth->users()->result();
			// foreach ($this->data['users'] as $k => $user)
			// {
				// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			// $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
			// exit;
			
			$this->layout->renderString($xcrud->render(), [
				'box' => true,
				'boxTitle' => 'Users',
				'title' => 'Users',
				'pageTitle' => 'Users',
				'pageSubTitle' => 'All users'
			]);
		}
	}
	
	public function actionGroups()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->breadcrumbs->push('Dashboard', 'site');
			$this->breadcrumbs->push('Authentication', 'auth/index');
			$this->breadcrumbs->push('Groups', 'auth/groups');
            
			$xcrud = xcrud_get_instance();
			$xcrud->table('a_groups');
            $xcrud->fields('name,description', false, false, 'view');
            $xcrud->fields('name,description', false, false, 'add');
            
            $xcrud->unset_remove(true, 'id', '=', '1');
            
            $xcrud->validation_required('name, description');

			// buttons
			$xcrud->button(site_url('auth/user-groups/{id}'), 'Users', 'fa fa-fw fa-users', 'btn-success', [
				'enable_label' => true
			]);
			$xcrud->button(site_url('auth/router-groups/{id}'), 'Routers', 'fa fa-fw fa-link', 'btn-info', [
				'enable_label' => true
			]);
			$xcrud->unset_view();
            
            $xcrud->readonly('name', 'edit');
			
			$this->layout->renderString($xcrud->render(), [
				'box' => true,
				'boxTitle' => 'Groups',
				'title' => 'Groups',
				'pageTitle' => 'Groups',
				'pageSubTitle' => 'All groups'
			]);
		}
	}
	
	public function actionUserGroups($id = null)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->breadcrumbs->push('Dashboard', 'site');
			$this->breadcrumbs->push('Authentication', 'auth/index');
			$this->breadcrumbs->push('User Groups', 'auth/user-groups');
			$xcrud = xcrud_get_instance();
			$xcrud->table('a_users_groups');
			$xcrud->order_by('user_id', 'asc');
			$xcrud->relation('user_id', 'a_users', 'id', ['username', 'nama'], '', '', '', ' - ');
			$xcrud->relation('group_id', 'a_groups', 'id', 'name');
			$xcrud->label('user_id', 'Username');
			$xcrud->label('group_id', 'Group');
            

			$pageSubTitle = 'Groups';
			if ($id !== null) {
				$xcrud->where('group_id', $id);
				$xcrud->columns('user_id');
				$xcrud->fields('user_id');
				$xcrud->pass_var('group_id', $id);

				$group = AuthGroups::where('id', $id)->first();
				$pageSubTitle = 'for <b>'. $group->name .'</b>';
			} else {
                redirect('auth/groups');
            }
			
			$this->layout->renderString($xcrud->render(), [
				'box' => true,
				'boxTitle' => 'User Groups',
				'title' => 'User Groups',
				'pageTitle' => 'Users',
				'pageSubTitle' => $pageSubTitle
			]);
		}
	}
	
	public function actionRouterGroups($id = null)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->breadcrumbs->push('Dashboard', 'site');
			$this->breadcrumbs->push('Authentication', 'auth/index');
			$this->breadcrumbs->push('User Routers', 'auth/user-routers');
			$xcrud = xcrud_get_instance();
			$xcrud->table('a_routers_groups');
			$xcrud->relation('group_id', 'a_groups', 'id', 'name');
			$xcrud->label('group_id', 'Group');
			$xcrud->limit(100);
            #$xcrud->order_by('a_routers_groups.controller_name asc');
			$xcrud->validation_required('controller_name');
			$xcrud->validation_required('method_name');
			$xcrud->validation_required('group_id');
			
			$routers = $this->ion_auth->get_routers();
		$class = ['' => '- none -'];
		$method = ['' => '- none -'];
			
			foreach($routers as $className => $og) {
				$class[$className] = $className;
				foreach($og as $m) {
					$method[$m] = $m;
				}
			}
			
			$modules = ['' => '- none -'];
			foreach($this->ion_auth->get_modules() as $module) {
				$modules[$module] = $module;
			}
			

			ksort($class);
			ksort($method);
			ksort($modules);
			$xcrud->change_type('controller_name', 'select', '', $class);
			$xcrud->change_type('method_name', 'select', '', $method);
			$xcrud->change_type('module_name', 'select', '', $modules);
			$xcrud->order_by('group_id', 'asc');
			$xcrud->order_by('module_name', 'asc');
			$xcrud->order_by('controller_name', 'asc');

			$xcrud->validation_required('controller_name');
			$xcrud->validation_required('method_name');

			$pageSubTitle = 'Groups';
			if ($id !== null) {
				$xcrud->where('group_id', $id);
				$xcrud->columns('group_id', true);
				$xcrud->fields('group_id', true);
				$xcrud->pass_var('group_id', $id);

				$group = AuthGroups::where('id', $id)->first();
				$pageSubTitle = 'for <b>'. $group->name .'</b>';
			}
			
			$this->layout->renderString($xcrud->render(), [
				'box' => true,
				'boxTitle' => 'Router Groups',
				'title' => 'Router Groups',
				'pageTitle' => 'Routers',
				'pageSubTitle' => $pageSubTitle
			]);
		}
	}
    
    public function actionRouterGroupTidakTerdaftar()
    {
        header("Content-Type: application/json");
        $id = $this->input->post('id');
        $filter = $this->input->post('filter');
        
        $group = AuthGroups::where('id', $id)->first();
        if (!$group) {
            echo json_encode([
                'status' => 0,
                'message' => 'Group tidak ditemukan'
            ]);
            exit;
        }
        
        $routerDataProvider = AuthRoutersGroups::where('group_id', $id)->get();
        $router = $this->ion_auth->get_routers_all();
        
        foreach($routerDataProvider as $model) {
            if (empty($model->module_name)) {
                $module = '*';
            } else {
                $module = $model->module_name;
            }
            
            if (isset($router[$module])) {
                if (isset($router[$module][$model->controller_name])) {
                    $methods = $router[$module][$model->controller_name];
                    foreach($methods as $k => $v) {
                        if ($v == $model->method_name) {
                            unset($router[$module][$model->controller_name][$k]);
                        }
                    }
                    
                    if (count($router[$module][$model->controller_name]) === 0) {
                        unset($router[$module][$model->controller_name]);
                    }
                }
                
                if (count($router[$module]) === 0) {
                    unset($router[$module]);
                }
            }
        }
        
        if (!empty($filter)) {
            $routerForFilter = [];
            foreach($router as $module => $controllers) {
                foreach($controllers as $controller => $methods) {
                    foreach($methods as $method) {
                        $routerForFilter[] = $module .'/'. $controller .'/'. $method;
                    }
                }
            }
            
            $routerFiltered = [];
            foreach($routerForFilter as $rt) {
                if (strpos(strtolower($rt), strtolower($filter)) !== false) {
                    $routerFiltered[] = $rt;
                }
            }
            
            $router2 = [];
            foreach($routerFiltered as $rt) {
                list($fmodule, $fcontroller, $fmethod) = explode('/', $rt);
                foreach($router as $module => $controllers) {
                    foreach($controllers as $controller => $methods) {
                        foreach($methods as $method) {
                            if ($module == $fmodule && $controller == $fcontroller && $method == $fmethod) {
                                if (!isset($router2[$module])) {
                                    $router2[$module] = [];
                                }
                                if (!isset($router2[$module][$controller])) {
                                    $router2[$module][$controller] = [];
                                }
                                $router2[$module][$controller][] = $method;
                            }
                        }
                    }
                }
            }
        } else {
            $router2 = $router;
        }
            
        echo json_encode([
            'status' => 1,
            'data' => $router2
        ]);
    }
    
    public function actionRouterGroupTerdaftar()
    {
        header("Content-Type: application/json");
        $id = $this->input->post('id');
        $filter = $this->input->post('filter');
        
        $group = AuthGroups::where('id', $id)->first();
        if (!$group) {
            echo json_encode([
                'status' => 0,
                'message' => 'Group tidak ditemukan'
            ]);
            exit;
        }
        
        $routerQuery = AuthRoutersGroups::where('group_id', $id);
        if (!empty($filter)) {
            if ($filter == '*') {
                $routerQuery->where(function($query) {
                    $query->whereNull('module_name');
                    $query->orWhere('module_name', '');
                });
            } else {
                $routerQuery->where(function($query) use ($filter) {
                    $query->where('method_name', 'like', '%'. $filter .'%');
                    $query->orWhere('controller_name', 'like', '%'. $filter .'%');
                    $query->orWhere('module_name', 'like', '%'. $filter .'%');
                });
            }
        }
        
        $routerDataProvider = $routerQuery->get();
        $router = [];
        
        foreach($routerDataProvider as $model) {
            if (empty($model->module_name)) {
                $module = '*';
            } else {
                $module = $model->module_name;
            }
            
            if (!isset($router[$module])) {
                $router[$module] = [];
            }
                
            if (!isset($router[$module][$model->controller_name])) {
                $router[$module][$model->controller_name] = [];
            }
            
            $router[$module][$model->controller_name][] = $model->method_name;
        }
        
        echo json_encode([
            'status' => 1,
            'data' => $router
        ]);
    }
    
    public function actionRouterGroupMove()
    {
        header("Content-Type: application/json");
        $id = $this->input->post('id');
        $to = $this->input->post('to');
        $data = $this->input->post('data');
        
        $group = AuthGroups::where('id', $id)->first();
        if (!$group) {
            echo json_encode([
                'status' => 0,
                'message' => 'Group tidak ditemukan'
            ]);
            exit;
        }
        
        if ($to == 'right') {
            foreach($data as $dt) {
                $dtr = explode('/', $dt);
                
                $router = new AuthRoutersGroups;
                $router->group_id = $id;
                $router->controller_name = $dtr[1];
                $router->method_name = $dtr[2];
                $router->module_name = $dtr[0] == '*' ? null : $dtr[0];
                $router->save();
            }
        } elseif ($to == 'left') {
            foreach($data as $dt) {
                $dtr = explode('/', $dt);
                
                
                $query = AuthRoutersGroups::where('group_id', $id);
                $query->where('controller_name', $dtr[1]);
                $query->where('method_name', $dtr[2]);
                $query->where(function($query) use ($dtr) {
                    if ($dtr[0] == '*') {
                        $query->whereNull('module_name');
                        $query->orWhere('module_name', '');
                    } else {
                        $query->where('module_name', $dtr[0]);
                    }
                });
                $query->delete();
            }
        } elseif ($to == 'left-all') {
            $data = AuthRoutersGroups::where('group_id', $id)->delete();
        }
        
        echo json_encode([
            'status' => 1,
            'data' => ''
        ]);
    }

	public function actionButtons()
	{
		$this->breadcrumbs->push('Dashboard', 'site');
		$this->breadcrumbs->push('Authentication', 'auth/index');
		$this->breadcrumbs->push('Buttons', 'auth/buttons');

		$xcrud = xcrud_get_instance();
		$xcrud->table('a_buttons');
		$xcrud->unset_view();
		$xcrud->validation_required('name');
		$xcrud->validation_required('identity');
        $xcrud->order_by('identity', 'desc');
        $xcrud->order_by('name', 'asc');

		$xcrud->button(site_url('auth/buttons-groups/{id}'), 'Groups', 'fa fa-fw fa-users', 'btn-success', [
			'enable_label' => true
		]);

		$this->layout->renderString($xcrud->render(), [
			'box' => true,
			'boxTitle' => 'Buttons',
			'title' => 'Buttons',
			'pageTitle' => 'Buttons',
			'pageSubTitle' => 'All buttons'
		]);
	}

	public function actionButtonsGroups($button_id = null)
	{
		$this->breadcrumbs->push('Dashboard', 'site');
		$this->breadcrumbs->push('Authentication', 'auth/index');
		$this->breadcrumbs->push('Buttons', 'auth/buttons');
		$this->breadcrumbs->push('Buttons Groups', 'auth/buttons-groups/'. $button_id);

		$xcrud = xcrud_get_instance();
		$xcrud->table('a_buttons_groups');

		$xcrud->validation_required('button_id');
		$xcrud->validation_required('group_id');

		$xcrud->relation('button_id', 'a_buttons', 'id', ['name', 'identity'], '', '', '', ' #');
		$xcrud->relation('group_id', 'a_groups', 'id', 'name');

		$pageSubTitle = 'Groups';
		if ($button_id !== null) {
			$xcrud->where('button_id', $button_id);
			$xcrud->pass_default('button_id', $button_id);
			$xcrud->pass_var('button_id', $button_id);
			$xcrud->readonly('button_id');
			$xcrud->disabled('button_id');

			$button = AuthButtons::where('id', $button_id)->first();
			$pageSubTitle = 'for <b>'. $button->name .'</b>';
		}
		$xcrud->no_editor('expression');
		$xcrud->change_type('expression', 'text');

		$xcrud->label('button_id', 'Button');
		$xcrud->label('group_id', 'Group');

		$this->layout->renderString($xcrud->render(), [
			'box' => true,
			'boxTitle' => 'Buttons Groups',
			'title' => 'Buttons Groups',
			'pageTitle' => 'Buttons Groups',
			'pageSubTitle' => $pageSubTitle,
			'activeMenu' => 'auth-buttons'
		]);
	}

	/**
	 * Log the user in
	 */
	public function actionLogin()
	{
		$this->data['title'] = $this->lang->line('login_heading');
        $loginTheme = $this->app->getConfig('login_theme');
        

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('site', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array(
				'class' => 'form-control input-lg form-control-lg',
				'placeholder' => 'Username',
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'autocomplete' => 'off',
				'value' => set_value('identity'),
                'required' => true
			);
			$this->data['password'] = array(
				'class' => 'form-control input-lg form-control-lg',
				'placeholder' => 'Password',
				'name' => 'password',
				'id' => 'password',
				'autocomplete' => 'new-password',
				'type' => 'password',
				'value' => set_value('password'),
                'required' => true
			);
            
            $this->data['layout'] = 'layouts/'. $this->app->getConfig('theme') .'/login.php';
            return $this->layout->render('auth/'. $loginTheme, $this->data);
            // $this->_render_page('auth' . DIRECTORY_SEPARATOR . $loginTheme, $this->data);
		}
	}

	/**
	 * Log the user out
	 */
	public function actionLogout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('auth/login', 'refresh');
	}

	/**
	 * Change password
	 */
	public function actionChangePassword()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() === FALSE)
		{
			// display the form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id' => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id' => 'new',
				'type' => 'password',
				'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id' => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
			);
			$this->data['user_id'] = array(
				'name' => 'user_id',
				'id' => 'user_id',
				'type' => 'hidden',
				'value' => $user->id,
			);

			// render
			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	/**
	 * Forgot password
	 */
	public function actionForgotPassword()
	{
		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'ion_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
			);

			if ($this->config->item('identity', 'ion_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'ion_auth') != 'email')
				{
					$this->ion_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->ion_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	/**
	 * Reset password - final step for forgotten password
	 *
	 * @param string|null $code The reset code
	 */
	public function actionResetPassword($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Activate the user
	 *
	 * @param int         $id   The user ID
	 * @param string|bool $code The activation code
	 */
	public function actionActivate($id, $code = FALSE)
	{
		if ($code !== FALSE)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Deactivate the user
	 *
	 * @param int|string|null $id The user ID
	 */
	public function actionDeactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int)$id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() === FALSE)
		{
			$this->breadcrumbs->push('Dashboard', 'site');
			$this->breadcrumbs->push('Users', 'auth/index');
			$this->breadcrumbs->push('Deactivate', 'auth/deactivate/'. $id);
			
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			// $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'deactivate_user', $this->data);
			$this->layout->render('auth/deactivate_user', array_merge_recursive([
				'box' => true,
				'boxTitle' => 'Activities',
				'title' => 'Activities',
				'selectedMenu' => 'activities'
			], $this->data));
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					return show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	/**
	 * Create a new user
	 */
	public function actionCreateUser()
	{
		$this->data['title'] = $this->lang->line('create_user_heading');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
		}
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'company' => $this->input->post('company'),
				'phone' => $this->input->post('phone'),
			);
		}
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data))
		{
			// check to see if we are creating the user
			// redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['identity'] = array(
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['email'] = array(
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->form_validation->set_value('company'),
			);
			$this->data['phone'] = array(
				'name' => 'phone',
				'id' => 'phone',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['password'] = array(
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'create_user', $this->data);
		}
	}
	/**
	* Redirect a user checking if is admin
	*/
	public function actionRedirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('auth', 'refresh');
		}
		redirect('/', 'refresh');
	}

	/**
	 * Edit a user
	 *
	 * @param int|string $id
	 */
	public function actionEditUser($id)
	{
		$this->data['title'] = $this->lang->line('edit_user_heading');

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'trim|required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim|required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'company' => $this->input->post('company'),
					'phone' => $this->input->post('phone'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}

				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					// Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData))
					{

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp)
						{
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

				// check to see if we are updating the user
				if ($this->ion_auth->update($user->id, $data))
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					$this->redirectUser();

				}
				else
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					$this->redirectUser();

				}

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'edit_user', $this->data);
	}

	/**
	 * Create a new group
	 */
	public function actionCreateGroup()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash');

		if ($this->form_validation->run() === TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if ($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'create_group', $this->data);
		}
	}

	/**
	 * Edit a group
	 *
	 * @param int|string $id
	 */
	public function actionEditGroup($id)
	{
		// bail if no group id given
		if (!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if ($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'edit_group', $this->data);
	}

	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

}
