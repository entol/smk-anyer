<?php

class XiiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('inflector');
        $this->load->helper('file');
        $this->load->helper('text');
    }

    public function actionSetup()
    {
        $sql = require_once(FCPATH .'init.db');
        $this->db->query($sql);
    }

    public function actionTest()
    {
        $query = $this->db->get('c_setting');
        $data = $query->result_array();
        print_r($data);
    }

    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Xii', 'xii/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->query('SELECT TABLE_NAME as name, REPLACE(LOWER(TABLE_TYPE), \'base \', \'\') as type FROM information_schema.tables WHERE table_schema = DATABASE()');
        $xcrud->columns('name, type');
        $xcrud->order_by('type', 'asc');
        $xcrud->order_by('name', 'asc');
        $xcrud->unset_limitlist();
        $xcrud->unset_pagination();
        $xcrud->limit('all');
        
        $xcrud->button(site_url('xii/view/{name}'), 'View', 'fa fa-fw fa-search', 'btn btn-sm btn-info btn-detail', [
            'label' => false,
            'data-table' => '{name}'
        ]);
        $xcrud->button(site_url('xii/generate/{name}/{type}'), 'Generate', 'fa fa-fw fa-code', 'btn btn-sm btn-inverse btn-generate', [
            'label' => true,
            'data-table' => '{name}'
        ]);

        $this->layout->render('xii/index', [
            'xcrudContent' => $xcrud->render(),
            'box' => true,
            'boxTitle' => 'Xii Generator',
            'title' => 'Xii Generator',
            'pageTitle' => 'Xii Generator',
            'pageSubTitle' => 'Automation CRUD Creator'
        ]);
    }
    
    public function actionView($table)
    {
        Xcrud_config::$load_bootstrap = true;
        Xcrud_config::$load_jquery = true;
        $xcrud = xcrud_get_instance();
        $xcrud->query('SELECT
                TABLE_NAME as table_name,
                COLUMN_NAME as column_name,
                ORDINAL_POSITION as position,
                IS_NULLABLE as nullable,
                DATA_TYPE as data_type,
                COLUMN_TYPE AS column_type,
                CHARACTER_MAXIMUM_LENGTH AS max_length,
                NUMERIC_PRECISION AS num_precision,
                NUMERIC_SCALE as num_scale,
                COLUMN_KEY AS column_key,
                EXTRA AS other
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = \''. $table .'\' AND TABLE_SCHEMA = DATABASE()');
            
        $xcrud->order_by('position', 'asc');
        $xcrud->order_by('column_name', 'asc');
        $xcrud->unset_limitlist();
        $xcrud->unset_pagination();
        $xcrud->limit('all');
        
        echo $xcrud->render();
    }

    public function actionGenerate($table, $type)
    {
        $query = $this->db->query('SELECT
                TABLE_NAME as table_name,
                COLUMN_NAME as column_name,
                ORDINAL_POSITION as position,
                IS_NULLABLE as nullable,
                DATA_TYPE as data_type,
                COLUMN_TYPE AS column_type,
                CHARACTER_MAXIMUM_LENGTH AS max_length,
                NUMERIC_PRECISION AS num_precision,
                NUMERIC_SCALE as num_scale,
                COLUMN_KEY AS column_key,
                EXTRA AS other
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = \''. $table .'\' AND TABLE_SCHEMA = DATABASE()');
            
        if ($query->num_rows()) {
            $models = $query->result_array();
        } else {
            $models = [];
        }
        
        $postdata = [
            'list' => true,
            'add' => $type == 'view' ? false : true,
            'edit' => $type == 'view' ? false : true,
            'remove' => $type == 'view' ? false : true,
            'view' => $type == 'view' ? false : true,
            'sortable' => true,
            'show_ai' => false,
            'csv' => false,
            'print' => false,
            'search' => true,
            'pagination' => true,
            'number' => true,
            'limit_selector' => true,
            'benchmark' => true,
            'duplicate' => false,
        ];
        foreach($models as $model) {
            $col = $model['column_name'];
            if (!isset($postdata[$col])) {
                $postdata['__columns__'][$col] = [];
            }
            
            $add = $type == 'view' ? false : true;
            $edit = $type == 'view' ? false : true;
            $list = true;
            $view = $type == 'view' ? false : true;
            $ruleValue = '';
            $ruleOn = '';
            
            if (in_array($col, [
                'created_at',
                'created_by',
                'updated_at',
                'updated_by',
                'ip_address',
                'useragent',
                'user_agent'])) {
                $list = false;
                $add = false;
                $edit = false;
                
                if ($col == 'created_at') {
                    $ruleValue = 'datetime';
                    $ruleOn = 'add';
                }
                if ($col == 'created_by') {
                    $ruleValue = 'auth';
                    $ruleOn = 'add';
                }
                if ($col == 'updated_at') {
                    $ruleValue = 'datetime';
                    $ruleOn = 'add_edit';
                }
                if ($col == 'updated_by') {
                    $ruleValue = 'auth';
                    $ruleOn = 'add_edit';
                }
                if ($col == 'ip_address') {
                    $ruleValue = 'ip';
                    $ruleOn = 'add_edit';
                }
                if ($col == 'useragent' || $col == 'user_agent') {
                    $ruleValue = 'agent';
                    $ruleOn = 'add_edit';
                }
            }
            
            $postdata['__columns__'][$col]['label'] = humanize($col);
            $postdata['__columns__'][$col]['list'] = $list;
            $postdata['__columns__'][$col]['add'] = $add;
            $postdata['__columns__'][$col]['edit'] = $edit;
            $postdata['__columns__'][$col]['remove'] = $type == 'view' ? false : true;
            $postdata['__columns__'][$col]['view'] = $view;
            $postdata['__columns__'][$col]['required'] = $model['nullable'] !== 'YES';
            $postdata['__columns__'][$col]['cut'] = 30;
            $postdata['__columns__'][$col]['rule_value'] = $ruleValue;
            $postdata['__columns__'][$col]['rule_on'] = $ruleOn;
        }
        
        $tables = [];
        $tableQuery = $this->db->query('SELECT TABLE_NAME as name, REPLACE(LOWER(TABLE_TYPE), \'base \', \'\') as type FROM information_schema.tables WHERE table_schema = DATABASE()');
        if ($tableQuery->num_rows()) {
            foreach($tableQuery->result_object() as $x) {
                $tables[] = $x->name;
            }
        }
        
        $this->load->view('xii/generate', [
            'table' => $table,
            'type' => $type,
            'models' => $models,
            'postdata' => $postdata,
            'tables' => $tables
        ]);
    }
    
    public function actionGenerateFile($table, $type, $replace = 'P')
    {
        header('Content-Type: application/json');
        $controller = $_POST['controller'];
        $id = $this->camel2id( substr($controller, 0, strrpos($controller, 'Controller')) );
        $module = $this->input->post('module');
        $modelName = $this->input->post('model');
        $moduleName = humanize($module);
        $controllerName = $this->input->post('controller');
        $funcId = trim(str_replace('-', '_', $this->camel2id($module) .'-'. $id), '_');
        
        $models = [];
        $query = $this->db->query('SELECT
                TABLE_NAME as table_name,
                COLUMN_NAME as column_name,
                ORDINAL_POSITION as position,
                IS_NULLABLE as nullable,
                DATA_TYPE as data_type,
                COLUMN_TYPE AS column_type,
                CHARACTER_MAXIMUM_LENGTH AS max_length,
                NUMERIC_PRECISION AS num_precision,
                NUMERIC_SCALE as num_scale,
                COLUMN_KEY AS column_key,
                EXTRA AS other
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = \''. $table .'\' AND TABLE_SCHEMA = DATABASE()');
            
        if ($query->num_rows()) {
            foreach($query->result_object() as $model) {
                $models[$model->column_name] = $model;
            }
        }
        
        $columns = [];
        $tabs = [];
        $fields = [];
        $addFields = [];
        $editFields = [];
        $labels = [];
        $required = [];
        $cuts = [];
        $fieldTypes = [];
        $increment = false;
        $pks = [];
        
        $rules = [
            'add' => [],
            'edit' => []
        ];
        
        foreach($_POST['__columns__'] as $col => $dt) {
            $model = $models[$col];
            $tab = $dt['tab'];
            if (empty($tab)) {
                $tab = '*';
            }
            
            if (!isset($fields[$tab])) {
                $fields[$tab] = [];
            }
            if (!isset($addFields[$tab])) {
                $addFields[$tab] = [];
            }
            if (!isset($editFields[$tab])) {
                $editFields[$tab] = [];
            }
            
            if ($model->other == 'auto_increment') {
                $increment = true;
                
                if ($this->input->post('show_ai') === 'Y') {
                    if ($dt['list'] == 'Y') {
                        $columns[] = $col;
                    }
                    if ($dt['view'] == 'Y') {
                        $fields[$tab][] = $col;
                    }
                    if ($dt['add'] == 'Y') {
                        $addFields[$tab][] = $col;
                    }
                    if ($dt['edit'] == 'Y') {
                        $editFields[$tab][] = $col;
                    }
                    if ($dt['required'] = 'Y') {
                        $required[] = $col;
                    }
                }
            } else {
                if ($dt['list'] == 'Y') {
                    $columns[] = $col;
                }
                if ($dt['view'] == 'Y') {
                    $fields[$tab][] = $col;
                }
                if ($dt['add'] == 'Y') {
                    $addFields[$tab][] = $col;
                }
                if ($dt['edit'] == 'Y') {
                    $editFields[$tab][] = $col;
                }
                if ($dt['required'] == 'Y') {
                    $required[] = $col;
                }
                
                if ($dt['type'] != '') {
                    $fieldTypes[] = $this->getFieldType($col, $dt);
                }
            }
            
            $labels[$col] = $dt['label'];
            
            if (isset($dt['cut'])) {
                $cuts[$col] = $dt['cut'];
            }
            
            if ($model->column_key == 'PRI') {
                $pks[] = $col;
            }
            
            if ($dt['rule_value'] != '' && $dt['rule_on']) {
                if ($dt['rule_on'] == 'add' || $dt['rule_on'] == 'add_edit') {
                    $rules['add'][$col] = $this->getRuleType($col, $dt);
                }
                if ($dt['rule_on'] == 'edit' || $dt['rule_on'] == 'add_edit') {
                    $rules['edit'][$col] = $this->getRuleType($col, $dt);
                }
            }
        }
        
        $ns = 'app\\models';
        if ($module != '') {
            $ns = 'app\\modules\\'. $module .'\\models';
        }
        
        $pk = '';
        if (count($pks) === 1) {
            $pk = "'$pks[0]'";
        } elseif (count($pks)) {
            $pk = "['". implode("', '", $pks) ."']";
        }
        
        $controllerTpl = $this->load->view('xii/t_controller', [
            'id' => $id,
            'tableName' => $table,
            'controllerName' => $controllerName,
            'module' => $module,
            'moduleName' => $moduleName,
            'columns' => $columns,
            'fields' => $fields,
            'addFields' => $addFields,
            'editFields' => $editFields,
            'labels' => $labels,
            'required' => $required,
            'cuts' => $cuts,
            'duplicate' => $this->input->post('duplicate') == 'Y',
            'list' => $this->input->post('list') == 'N',
            'add' => $this->input->post('add') == 'N',
            'edit' => $this->input->post('edit') == 'N',
            'view' => $this->input->post('view') == 'N',
            'remove' => $this->input->post('remove') == 'N',
            'sortable' => $this->input->post('sortable') == 'N',
            'pagination' => $this->input->post('pagination') == 'N',
            'number' => $this->input->post('number') == 'N',
            'csv' => $this->input->post('csv') == 'N',
            'print' => $this->input->post('print') == 'N',
            'limit_selector' => $this->input->post('limit_selector') == 'N',
            'benchmark' => $this->input->post('benchmark') == 'N',
            'fieldTypes' => $fieldTypes,
            'title' => $this->input->post('title'),
            'idUnderscore' => $funcId,
            'rules' => $rules
        ], true);
        
        $viewTpl = $this->load->view('xii/t_index', [], true);
        
        $modelTpl = $this->load->view('xii/t_model', [
            'ns' => $ns,
            'modelName' => $modelName,
            'increment' => $increment,
            'pk' => $pk
        ], true);
        
        $callbackTpl = $this->load->view('xii/t_callback', [
            'ns' => $ns,
            'modelName' => $modelName,
            'increment' => $increment,
            'pk' => $pk,
            'idUnderscore' => $funcId,
            'rules' => $rules
        ], true);
        
        
        if ($replace == 'P') {
            echo json_encode([
                'status' => 2,
                'message' => '',
                'data' => [
                    'controller' => highlight_code($controllerTpl),
                    'view' => highlight_code($viewTpl),
                    'model' => highlight_code($modelTpl),
                    'callback' => highlight_code($callbackTpl),
                ]
            ]);
        } else {
            
            $controllerPath = 'controllers/';
            $viewPath = 'views/'. $id .'/';
            $modelPath = 'models/';
            $callbackPath = 'callbacks/';
            
            if ($module != '') {
                $controllerPath = 'modules/'. $module .'/controllers/';
                $viewPath = 'modules/'. $module .'/views/'. $id .'/';
                $modelPath = 'modules/'. $module .'/models/';
                $callbackPath = 'modules/'. $module .'/callbacks/';
                
                if (!is_dir(APPPATH . 'modules/'. $module .'/')) {
                    mkdir(APPPATH . 'modules/'. $module .'/', 0777);
                }
                if (!is_dir(APPPATH . $controllerPath)) {
                    mkdir(APPPATH . $controllerPath, 0777);
                }
                if (!is_dir(APPPATH . $modelPath)) {
                    mkdir(APPPATH . $modelPath, 0777);
                }
                if (!is_dir(APPPATH . 'modules/'. $module .'/views/')) {
                    mkdir(APPPATH . 'modules/'. $module .'/views/', 0777);
                }
            }
            
            if (!is_dir(APPPATH . $viewPath)) {
                mkdir(APPPATH . $viewPath, 0777);
            }
            if (!is_dir(APPPATH . $callbackPath)) {
                mkdir(APPPATH . $callbackPath, 0777);
            }
            
            $controllerFile = APPPATH . $controllerPath . $controllerName .'.php';
            $viewFile = APPPATH . $viewPath .'index.php';
            $modelFile = APPPATH . $modelPath . $modelName .'.php';
            $callbackFile = APPPATH . $callbackPath . $id .'.php';
            
            if ($replace == 'Y' || (!file_exists($controllerFile) && !file_exists($viewFile)  && !file_exists($modelFile))) {
                write_file($controllerFile, $controllerTpl);
                write_file($viewFile, $viewTpl);
                write_file($modelFile, $modelTpl);
                write_file($callbackFile, $callbackTpl);
                
                echo json_encode([
                    'status' => 1
                ]);
                exit;
            } 
            if (file_exists($controllerFile)) {
                echo json_encode([
                    'status' => 0
                ]);
                exit;
            }
            
            if (file_exists($viewFile)) {
                echo json_encode([
                    'status' => 0
                ]);
                exit;
            }
            
            if (file_exists($modelFile)) {
                echo json_encode([
                    'status' => 0
                ]);
                exit;
            }
        }
    }   
    
    public function actionGetColumns()
    {
        $table = $this->input->get('table');
        
        $query = $this->db->query('SELECT
                TABLE_NAME as table_name,
                COLUMN_NAME as column_name,
                ORDINAL_POSITION as position,
                IS_NULLABLE as nullable,
                DATA_TYPE as data_type,
                COLUMN_TYPE AS column_type,
                CHARACTER_MAXIMUM_LENGTH AS max_length,
                NUMERIC_PRECISION AS num_precision,
                NUMERIC_SCALE as num_scale,
                COLUMN_KEY AS column_key,
                EXTRA AS other
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = \''. $table .'\' AND TABLE_SCHEMA = DATABASE()');
            
        $models = [];
        if ($query->num_rows()) {
            foreach($query->result_object() as $model) {
                $models[] = $model->column_name;
            }
        }
        
        header("Content-Type: application/json");
        echo json_encode($models);
    }
    
    protected function getRuleType($col, $data)
    {
        $ruleValue = $data['rule_value'];
        if ($ruleValue == 'date') {
            return "\$postdata->set('{$col}', date('Y-m-d'));\n";
        } elseif ($ruleValue == 'datetime') {
            return "\$postdata->set('{$col}', date('Y-m-d H:i:s'));\n";
        } elseif ($ruleValue == 'time') {
            return "\$postdata->set('{$col}', date('H:i:s'));\n";
        } elseif ($ruleValue == 'year') {
            return "\$postdata->set('{$col}', date('Y'));\n";
        } elseif ($ruleValue == 'year') {
            return "\$postdata->set('{$col}', time());\n";
        } elseif ($ruleValue == 'auth') {
            return "\$postdata->set('{$col}', \$CI->ion_auth->user_data('id'));\n";
        } elseif ($ruleValue == 'ip') {
            return "\$postdata->set('{$col}', \$CI->input->ip_address());\n";
        } elseif ($ruleValue == 'agent') {
            return "\$postdata->set('{$col}', \$CI->agent->agent_string());\n";
        }
        return '';
    }
    
    protected function getFieldType($col, $data)
    {
        $type = $data['type'];
        
        if (in_array($type, ['select', 'radio', 'checkboxes', 'multiselect'])) {
            $lists = [];
            $options = trim($data['options'], ';');
            if ($options != '') {
                $options1 = explode(';', $options);
                foreach($options1 as $opt) {
                    if (strpos($opt, '|') !== false) {
                        $options2 = explode('|', $opt);
                        $lists[$options2[0]] = $options2[1];
                    } else {
                        $lists[$opt] = $opt;
                    }
                }
            }
            
            $html = "\$xcrud->change_type('{$col}', '{$type}', '', [\n";
            foreach($lists as $a => $b) {
                $html .= "            '{$a}' => '{$b}',\n";
            }
            $html .= '        ]);';
            
            return $html;
        } elseif ($type == 'relation') {
            $display = implode("', '", $data['rel_display']);
            
            $html = "\$xcrud->relation('{$col}', '{$data['rel_table']}', '{$data['rel_field']}', ['{$display}'], '', '', '', '{$data['rel_sep']}');";
            
            return $html;
        } else {
            return "\$xcrud->change_type('{$col}', '{$type}');";
        }
    }
    
    protected function camel2id($name, $separator = '-', $strict = true)
    {
        $regex = $strict ? '/[A-Z]/' : '/(?<![A-Z])[A-Z]/';
        if ($separator === '_') {
            return strtolower(trim(preg_replace($regex, '_\0', $name), '_'));
        }

        return strtolower(trim(str_replace('_', $separator, preg_replace($regex, $separator . '\0', $name)), $separator));
    }
    
}