<?php

class JabatanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Jabatan', 'jabatan/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_jabatan');
        $xcrud->columns('jabatan,level');
        $xcrud->fields('jabatan,level', false, false, 'view');
        $xcrud->fields('jabatan,level', false, false, 'add');
        $xcrud->fields('jabatan,level', false, false, 'edit');

        $xcrud->change_type('level', 'int');
        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'name'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'name'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('jabatan', 'Jabatan');
        $xcrud->label('level', 'Level');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('jabatan,level');
        $xcrud->unset_csv();
        $xcrud->unset_print();

        $xcrud->before_insert('jabatan_before_insert', realpath(__DIR__ .'/../callbacks/jabatan.php'));
        // $xcrud->after_insert('jabatan_after_insert', realpath(__DIR__ .'/../callbacks/jabatan.php'));
        $xcrud->before_update('jabatan_before_update', realpath(__DIR__ .'/../callbacks/jabatan.php'));
        // $xcrud->after_update('jabatan_after_update', realpath(__DIR__ .'/../callbacks/jabatan.php'));
        // $xcrud->before_remove('jabatan_before_remove', realpath(__DIR__ .'/../callbacks/jabatan.php'));
        // $xcrud->after_remove ('jabatan_after_remove', realpath(__DIR__ .'/../callbacks/jabatan.php'));

        $this->layout->render('jabatan/index', [
            'box' => true,
            'boxTitle' => 'Jabatan',
            'title' => 'Jabatan',
            'pageTitle' => 'Jabatan',
            'pageSubTitle' => 'Semua Jabatan',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}