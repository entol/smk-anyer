<?php

class UserController extends CI_Controller
{
    public function actionIndex()
    {
		$this->breadcrumbs->push('Dashboard', 'site');
		$this->breadcrumbs->push('Auth', 'auth/index');
		
		$user = $this->ion_auth->user()->row();
		
		$xcrud = xcrud_get_instance();
		$xcrud->table('a_users');
		
		$xcrud->columns('nama,username,email,lokasi');
		$xcrud->fields('nama,username,email,password');
		
		$xcrud->change_type('password', 'password');

		$xcrud->before_insert('auth_user_before_insert');
		$xcrud->before_update('auth_user_before_update');
		$xcrud->after_insert('auth_user_after_insert');
		
		$xcrud->unset_remove(true, 'id', '=', 1);
		$xcrud->validation_pattern('email', 'email');
		$xcrud->validation_required('nama');
		$xcrud->validation_required('username');
	
		$xcrud->readonly('username');
		$xcrud->unset_add();
		$xcrud->unset_view();
		$xcrud->hide_button('save_return');
		$xcrud->hide_button('return');
		$xcrud->set_lang('save_edit', '<i class="fa fa-fw fa-save"></i> Simpan');
	
		//list the users
		// $this->data['users'] = $this->ion_auth->users()->result();
		// foreach ($this->data['users'] as $k => $user)
		// {
			// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
		// }

		// $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		// exit;
		
		$this->layout->renderString($xcrud->render('edit', $user->id), [
			'box' => true,
			'boxTitle' => 'Users',
			'title' => 'Users',
			'selectedMenu' => 'auth-users'
		]);
    }
}
