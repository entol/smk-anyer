<?php

class KondisiController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Kondisi Barang', 'kondisi/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_kondisi');
        $xcrud->columns('kode,kondisi,deskripsi');
        $xcrud->fields('kode,kondisi,deskripsi,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $xcrud->fields('kode,kondisi,deskripsi', false, false, 'add');
        $xcrud->fields('kode,kondisi,deskripsi', false, false, 'edit');

        $xcrud->relation('created_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');
        $xcrud->relation('updated_by', 'a_users', 'id', ['nik', 'nama'], '', '', '', ' - ');

        $xcrud->label('id', 'Id');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('kondisi', 'Kondisi');
        $xcrud->label('deskripsi', 'Deskripsi');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'User Agent');

        $xcrud->validation_required('kode,kondisi');
        $xcrud->unset_csv();
        $xcrud->unset_print();

        $xcrud->before_insert('kondisi_before_insert', realpath(__DIR__ .'/../callbacks/kondisi.php'));
        // $xcrud->after_insert('kondisi_after_insert', realpath(__DIR__ .'/../callbacks/kondisi.php'));
        $xcrud->before_update('kondisi_before_update', realpath(__DIR__ .'/../callbacks/kondisi.php'));
        // $xcrud->after_update('kondisi_after_update', realpath(__DIR__ .'/../callbacks/kondisi.php'));
        // $xcrud->before_remove('kondisi_before_remove', realpath(__DIR__ .'/../callbacks/kondisi.php'));
        // $xcrud->after_remove ('kondisi_after_remove', realpath(__DIR__ .'/../callbacks/kondisi.php'));

        $this->layout->render('kondisi/index', [
            'box' => true,
            'boxTitle' => 'Kondisi Barang',
            'title' => 'Kondisi Barang',
            'pageTitle' => 'Kondisi Barang',
            'pageSubTitle' => 'Semua Kondisi Barang',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}