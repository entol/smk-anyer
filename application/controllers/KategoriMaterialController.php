<?php

class KategoriMaterialController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Kategori Material', 'kategori-material/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('m_kategori_material');
        $xcrud->columns('kode,kategori');
        $xcrud->fields('kode,kategori,created_at,created_by,updated_at,updated_by,ip_address,useragent', false, false, 'view');
        $xcrud->fields('kode,kategori', false, false, 'add');
        $xcrud->fields('kode,kategori', false, false, 'edit');

        $xcrud->label('id', 'ID');
        $xcrud->label('kode', 'Kode');
        $xcrud->label('kategori', 'Kategori');
        $xcrud->label('created_at', 'Created At');
        $xcrud->label('created_by', 'Created By');
        $xcrud->label('updated_at', 'Updated At');
        $xcrud->label('updated_by', 'Updated By');
        $xcrud->label('ip_address', 'IP Address');
        $xcrud->label('useragent', 'Useragent');

        $xcrud->validation_required('kode,kategori,created_at,created_by,updated_at,updated_by,ip_address,useragent');
        $xcrud->unset_csv();
        $xcrud->unset_print();

        $xcrud->before_insert('kategori_material_before_insert', realpath(__DIR__ .'/../callbacks/kategori-material.php'));
        // $xcrud->after_insert('kategori_material_after_insert', realpath(__DIR__ .'/../callbacks/kategori-material.php'));
        $xcrud->before_update('kategori_material_before_update', realpath(__DIR__ .'/../callbacks/kategori-material.php'));
        // $xcrud->after_update('kategori_material_after_update', realpath(__DIR__ .'/../callbacks/kategori-material.php'));
        // $xcrud->before_remove('kategori_material_before_remove', realpath(__DIR__ .'/../callbacks/kategori-material.php'));
        // $xcrud->after_remove ('kategori_material_after_remove', realpath(__DIR__ .'/../callbacks/kategori-material.php'));

        $this->layout->render('kategori-material/index', [
            'box' => true,
            'boxTitle' => 'Kategori Material',
            'title' => 'Kategori Material',
            'pageTitle' => 'Kategori Material',
            'pageSubTitle' => 'Semua Kategori Material',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}