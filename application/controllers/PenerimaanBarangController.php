<?php

use app\models\Project;
use app\models\Request;
use app\models\RequestBarang;
use app\models\Barang;
use app\models\BarangItems;
use app\models\BarangHistory;
use app\models\Gudang;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class PenerimaanBarangController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
        $this->breadcrumbs->push('Penerimaan', 'penerimaan/index');
        
        $isPm = $this->ion_auth->in_group('pm');
        $isMatecon = $this->ion_auth->in_group('matecon');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('t_request');
        $xcrud->columns('kode,kode_project,permintaan_asal,permintaan_asal_detail,tanggal_request, status');
        $xcrud->fields('kode,kode_project,permintaan_asal,permintaan_asal_detail', '', '', 'view');
        
        $xcrud->relation('kode_project', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->change_type('permintaan_asal', 'radio', '', [
            'W' => 'Gudang',
            'P' => 'Project',
        ]);
        $xcrud->relation('kode_gudang_asal', 'm_gudang', 'kode', ['kode', 'gudang'], '', '', '', ' - ');
        $xcrud->relation('kode_project_asal', 't_project', 'kode', ['kode', 'nama'], '', '', '', ' - ');
        $xcrud->relation('status', 'm_status', 'kode', ['selanjutnya'], '', '', '', ' - ');
        $xcrud->change_type('approved', 'radio', '', [
            'Y' => 'Ya',
            'N' => 'Tidak',
        ]);
        
        $xcrud->subselect('permintaan_asal_detail', implode(' ', [
            "CASE WHEN `permintaan_asal` = 'W' THEN",
            "(SELECT CONCAT(`kode`, ' - ', `gudang`) FROM m_gudang WHERE `kode` = {kode_gudang_asal})",
            "ELSE",
            "(SELECT CONCAT(`kode`, ' - ', `nama`) FROM t_project WHERE `kode` = {kode_project_asal})",
            "END"
        ]));

        $xcrud->label('kode', 'Kode Request');
        $xcrud->label('kode_project', 'Project Tujuan');
        $xcrud->label('permintaan_asal', 'Permintaan Asal');
        $xcrud->label('permintaan_asal_detail', 'Lokasi Asal');
        $xcrud->label('kode_gudang_asal', 'Gudang Asal');
        $xcrud->label('kode_project_asal', 'Project Asal');
        $xcrud->label('tanggal_request', 'Tanggal Request');
        $xcrud->label('tanggal_shipment', 'Tanggal Shipment');
        $xcrud->label('tanggal_required', 'Tanggal Order');
        $xcrud->label('status', 'Status');
        
		$xcrud->button('javascript:;', 'Terima', 'fas fa-fw fa-people-carry', 'btn-primary btn-terima', [
			'enable_label' => true,
            'data-id' => '{id}'
		], ['status', '=', 'SH']);
        
         
        $xcrud->validation_required('kode,kode_project,permintaan_asal');
        $xcrud->unset_csv();
        $xcrud->unset_print();
        $xcrud->unset_edit();
        $xcrud->unset_add();
        $xcrud->unset_remove();
        
        if ($isMatecon) {
            // PM Approve, Verification Matecon & Request Warehouse
            $xcrud->where("status IN ('SH')");
        } elseif ($isPm) {
            $xcrud->where("status IN ('SH')");
            $kodeProject = Project::getMyProject(true);
            if ($kodeProject && count($kodeProject)) {
                $xcrud->where("kode_project IN ('". implode("', '", $kodeProject) ."')");
            } else {
                $xcrud->where("1=2");
            }
        } else {
            $xcrud->where("1=2");
        }
        $xcrud->order_by('id', 'desc');

        $xcrud->column_callback('status', 'penerimaan_barang_status', realpath(__DIR__ .'/../callbacks/penerimaan-barang.php'));
        
        // Items
        $items = $xcrud->nested_table('Material','id','t_request_barang','id_request'); // nested table
        $items->columns('kode_barang,qty,note,remark');
        
        $items->relation('kode_barang', 'm_barang', 'kode', ['kode', 'deskripsi'], '', '', '', ' - ');
        // $items->relation('status', 'm_status', 'kode', ['status'], '', '', '', ' - ');
        
        $items->change_type('approved, approved_pm', 'select', '', [
            'S' => 'System',
            'Y' => 'Ya',
            'N' => 'Tidak',
            'W' => 'Menunggu'
        ]);

        $items->label('id', 'ID');
        $items->label('kode_request', 'Kode Request');
        $items->label('kode_barang', 'Kode Barang');
        $items->label('kode_item', 'Kode Item');
        $items->label('qty', 'Qty');
        $items->label('note', 'Note');
        
        $items->where("approved = 'Y'");
        
        $items->unset_add();
        $items->unset_edit();
        $items->unset_remove();
        $items->unset_view();
        $items->unset_csv();
        $items->unset_print();
        
        
        // -------------------------
        // Highlight
        // -------------------------
        // $items->highlight_row('status', '=', 'PR', '', 'info');
        // $items->highlight_row('status', '=', 'SH', '', 'success');
        
        // -------------------------
        // Button
        // -------------------------
		// $items->button('javascript:;', 'Terima', 'fas fa-fw fa-people-carry', 'btn-danger btn-proses', [
            // 'enable_label' => true,
            // 'data-id' => '{id}',
            // 'title' => 'Proses'
		// ], ['status', '=', 'SH']);

        $this->layout->render('penerimaan-barang/index', [
            'box' => true,
            'boxTitle' => 'Penerimaan Material',
            'title' => 'Penerimaan',
            'pageTitle' => 'Penerimaan',
            'pageSubTitle' => 'Semua Penerimaan Material',
            'xcrudContent' => $xcrud->render()
        ]);
    }
    
    public function actionTerima($id)
    {
        $this->output->set_content_type('application/json');
        
        $model = Request::find($id);
        if (!$model) {
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => 'Permintaan tidak ditemukan',
                'data' => ''
            ]));
        }
        
        $userId = $this->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        $note = $this->input->post('note');
        
        try {
            DB::beginTransaction();
            $model->status = 'RC';
            $model->updated_at = $currentDateTime;
            $model->updated_by = $userId;
            $model->ip_address = $this->input->ip_address();
            $model->useragent  = $this->agent->agent_string();
            if ($model->save()) {
                $dp = $model->items;
                foreach($dp as $item) {
                    $barang = $item->barang;
                    if ($item->approved == 'Y') {
                        
                        // pengurangan qty pada asal
                        if ($model->permintaan_asal == 'W') {
                            $barangAsal = BarangItems::where('lokasi_gudang', $model->kode_gudang_asal)
                                            ->where('kode_barang', $item->kode_barang)
                                            ->where('lokasi', 'W')
                                            ->first();
                            } else {
                            $barangAsal = BarangItems::where('lokasi_project', $model->kode_project_asal)
                                            ->where('kode_barang', $item->kode_barang)
                                            ->where('lokasi', 'P')
                                            ->first();
                        }
                        if ($barangAsal) {
                            $barangAsal->qty_baik = $barangAsal->qty_baik - $item->qty;
                            if ($barangAsal->qty_baik <= 0) {
                                $barangAsal->delete();
                            } else {
                                $barangAsal->updated_at = $currentDateTime;
                                $barangAsal->updated_by = $userId;
                                $barangAsal->ip_address = $this->input->ip_address();
                                $barangAsal->useragent = $this->agent->agent_string();
                                $barangAsal->save();
                            }
                        }

                        // penambahan qty pada target
                        $barangTujuan = BarangItems::where('lokasi_project', $model->kode_project)
                                        ->where('kode_barang', $item->kode_barang)
                                        ->where('lokasi', 'P')
                                        ->first();
                        if (!$barangTujuan) {
                            $barangTujuan = new BarangItems;
                            $barangTujuan->kode_barang = $item->kode_barang;
                            $barangTujuan->lokasi = 'P';
                            $barangTujuan->lokasi_project = $model->kode_project;
                            $barangTujuan->created_at = $currentDateTime;
                            $barangTujuan->created_by = $userId;
                            $barangTujuan->qty_baik = 0;
                            $barangTujuan->qty_rusak = 0;
                        }
                        $barangTujuan->qty_baik = $barangTujuan->qty_baik + $item->qty;
                        $barangTujuan->updated_at = $currentDateTime;
                        $barangTujuan->updated_by = $userId;
                        $barangTujuan->ip_address = $this->input->ip_address();
                        $barangTujuan->useragent = $this->agent->agent_string();
                        $barangTujuan->save();
                        
                        // Model Barang
                        $barang->lokasi = 'P';
                        $barang->lokasi_gudang = null;
                        $barang->lokasi_project = $model->kode_project;
                        $barang->updated_at = $currentDateTime;
                        $barang->updated_by = $userId;
                        $barang->ip_address = $this->input->ip_address();
                        $barang->useragent = $this->agent->agent_string();
                        $barang->save(); 
                        
                        // create history
                        $history = new BarangHistory;
                        $history->kode_barang = $item->kode_barang;
                        $history->lokasi = 'P';
                        $history->lokasi_gudang = null;
                        $history->lokasi_project = $model->kode_project;
                        $history->created_at = $currentDateTime;
                        $history->created_by = $userId;
                        $history->updated_at = $currentDateTime;
                        $history->updated_by = $userId;
                        $history->ip_address = $this->input->ip_address();
                        $history->useragent = $this->agent->agent_string();
                        $history->save();
                    }
                }
                
                DB::commit();
                return $this->output->set_output(json_encode([
                    'status' => 1,
                    'message' => '',
                    'data' => ''
                ]));
            } else {
                DB::rollBack();
                return $this->output->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Gagal menyimpan data',
                    'data' => ''
                ]));
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->output->set_output(json_encode([
                'status' => 0,
                'message' => $ex->getMessage(),
                'data' => ''
            ]));
        }
    }
}