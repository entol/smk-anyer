<?php

/*
 * @link http://www.didanurwanda.com/
 * @copyright Copyright (c) 2017 Dida Nurwanda
 * @license http://www.didanurwanda.com/license/
 */

/**
 * Description of Layout
 *
 * @author Dida Nurwanda
 * @since 1.0
 */
class Layout
{

    public $ci;
    public $layout;
    public $baseLayout;
    private $data = [];
    public $box = 'layouts/box';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ci = & get_instance();
    }

    /**
     * Render view by file
     * 
     * @param string $view 
     * @param array $data
     * @param bool $return
     * @return string
     */
    public function render($view, $data = array(), $return = false)
    {
        $data = array_merge(array(
            'activeMenu' => '',
            'pageTitle' => '',
			'auth' => !$this->ci->ion_auth->logged_in() ? false : $this->ci->ion_auth->user()->row()
        ), $data);

        $content = $this->ci->load->view($view, $data, true);

        if ($return) {
            return $content;
        }
        
        $this->renderString($content, $data, $return);
    }
    
    /**
     * Render view by string
     * 
     * @param string $view 
     * @param array $data
     * @param bool $return
     * @return string
     */
    public function renderString($content = '', $data = array(), $return = false)
    {	
        if ($this->layout === null) {
            $this->layout = 'layouts/'. $this->ci->app->getConfig('theme') .'/main.php';
        }
        
        if (isset($data['layout'])) {
            $this->layout = $data['layout'];
        }
        
        if (isset($data['title'])) {
            $data['title'] = $data['title'] .' - '. $this->ci->app->getConfig('app_title');
        }
        
        $data = array_merge([
            'title' => $this->ci->app->getConfig('app_title'),
            'appName' => $this->ci->app->getConfig('app_name'),
            'appDescription' => $this->ci->app->getConfig('app_description'),
            'appKeywords' => $this->ci->app->getConfig('app_keywords'),
            'activeMenu' => '',
            'pageTitle' => '',
            'pageSubTitle' => ''
        ], $data);
		
        if (isset($data['box']) && $data['box'] === true) {
            $content = $this->ci->load->view($this->box, array_merge(array(
                'boxPeriode' => false,
                'boxTitle' => '',
                'boxSubTitle' => '',
                'content' => $content,
				'auth' => !$this->ci->ion_auth->logged_in() ? false : $this->ci->ion_auth->user()->row(),
            ), $data), true);
        }
        
        $data['content'] = $content;
        $this->data = $data;
        $result = $this->renderLayout($data, $content, $return);
        if ($return === true) {
            return $result;
        }
    }

    /**
     * Render main layout
     * 
     * @param string $view
     * @param array $data
     * @param string $content
     * @return string
     */
    private function renderLayout($data = array(), $content = null, $return = false)
    {
        return $this->ci->load->view($this->layout, $data, $return);
    }

    public function beginContent($baseLayout = 'base')
    {
        $this->baseLayout = dirname($this->layout) .'/'. $baseLayout;
        ob_start();
    }

    public function endContent()
    {
        $this->data['content'] = ob_get_clean();
        echo $this->ci->load->view($this->baseLayout, $this->data, true);
    }
}
