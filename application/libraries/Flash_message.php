<?php

/*
 * @link http://www.didanurwanda.com/
 * @copyright Copyright (c) 2017 Dida Nurwanda
 * @license http://www.didanurwanda.com/license/
 */

/**
 * Description of Flash_message
 *
 * @author Dida Nurwanda
 */
class Flash_message
{
    /**
     * @var CI_Controller
     */
    public $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();
    }
    
    public function getMessage()
    {
        $result = '';
        $msg_success = $this->ci->session->flashdata('msg_success');
        $msg_error = $this->ci->session->flashdata('msg_error');
        
        if ($msg_success) {
            $result .= '<div class="alert alert-success">'. $msg_success .'</div>';
        }
        
        if ($msg_error) {
            $result .= '<div class="alert alert-success">'. $msg_error .'</div>';
        }
        
        return $result == '' ? null : $result;
    }
    
    public function setMessage($status, $message)
    {
        $this->ci->session->set_flashdata('msg_'. strtolower(trim($status)), $message);
    }
    
}
