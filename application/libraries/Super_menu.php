<?php

class Super_menu
{
	public $ci;
	private $activeMenu;
	
	public function __construct()
	{
		$this->ci =& get_instance();
	}
	
	public function render($activeMenu = null)
	{
		$this->activeMenu = $activeMenu;
		$menuModel = [];
        $groupsQuery = $this->ci->ion_auth->get_routers_groups();
		if ($groupsQuery) {
			$groupsModel = $groupsQuery->result_object();
			$groups = [];
			foreach($groupsModel as $m) {
				if (!isset($groups[$m->controller_name])) {
					$groups[$m->controller_name] = [];
				}
				
				if (!in_array($m->method_name, $groups)) {
					$groups[$m->controller_name][] = $m->method_name;
				}
			}
			
			$menuModel = $this->modelMenuDepedency($groups);
		}

		$html = '<ul class="nav">';
		// $html .= '<li class="nav-header">Main Menu</li>';
		$html .= $this->renderItems($menuModel)['html'];
		$html .= '<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>';
		$html .= '</ul>';
		
		return $html;
	}
	
	private function renderItems($menuModel)
	{
		$class = lcfirst($this->ci->router->class);
		$method = lcfirst($this->ci->router->method);
		$module = lcfirst($this->ci->router->module);

		$actived = false;
		
		$html = '';
		foreach($menuModel as $model) {
			if (isset($model['items'])) {
				$childs = $this->renderItems($model['items']);
				if ($childs['active']) {
					$actived = true;
				}
				
				$html .= '<li class="has-sub '. ($childs['active'] ? 'active' : '') .'">';
				$html .= '<a href="javascript:;">';
				$html .= '<b class="caret pull-right"></b>';
				if ($model['icon'] !== '') {
					$html .= '<i class="'. $model['icon'] .'"></i> ';
				}
				$html .= '<span>'. $model['text'] .'</span>';
				$html .= '</a>';
				$html .= '<ul class="sub-menu">'. $childs['html'] .'</ul>';
				$html .='</li>';
			} else {
				if (!empty($module)) {
					$active = ($class == $model['controller_name'] && $method == $model['method_name'] && $module == $model['module_name']);
				} else {
					$active = ($class == $model['controller_name'] && $method == $model['method_name']);
				}

				if (!empty($this->activeMenu) && $this->activeMenu == $model['alias']) {
					$active = true;
				}

				if ($active) {
					$actived = true;
				}
				
				$activeClass = $active ? 'active' : '';
				$html .= '<li class="'. $activeClass .'">';
				$html .= '<a href="'. (empty($model['url']) ? 'javascript:;' : site_url($model['url'])) .'">';
				if ($model['icon'] !== '') {
					$html .= '<i class="'. $model['icon'] .'"></i> ';
				}
				$html .= '<span>'. $model['text'] .'</span>';
				$html .='</a>';
				$html .= '</li>';
			}
		}
		
		return [
			'html' => $html,
			'active' => $actived
		];
	}
	
	private function modelMenuDepedency($groups, $parentId = null)
	{
		$isAdmin = $this->ci->ion_auth->is_admin();
		
		$menu = [];
		$menuModel = AuthMenu::query()->where('parent_id', $parentId)->orderBy('sort_order', 'asc')->orderBy('text', 'asc')->get();
		foreach($menuModel as $model) {
			if ($isAdmin) {
				$dt = [
					'id' => $model->id,
					'text' => $model->text,
					'icon' => $model->icon,
					'url' => $model->url,
					'controller_name' => $model->controller_name,
					'method_name' => $model->method_name,
					'module_name' => $model->module_name,
					'alias' => $model->alias
				];
				
				$child = $this->modelMenuDepedency($groups, $model->id);
				if (count($child)) {
					$dt['items'] = $child;
				}
				$menu[] = $dt;
			} elseif (isset($groups[$model->controller_name])) {
				$group = $groups[$model->controller_name];
				if (in_array($model->method_name, $group)) {
					$dt = [
						'id' => $model->id,
						'text' => $model->text,
						'icon' => $model->icon,
						'url' => $model->url,
						'controller_name' => $model->controller_name,
						'method_name' => $model->method_name,
						'module_name' => $model->module_name,
						'alias' => $model->alias
					];
					
					$child = $this->modelMenuDepedency($groups, $model->id);
					if (count($child)) {
						$dt['items'] = $child;
					}
					$menu[] = $dt;
				}
			} elseif ($model->url == '') {
				$dt = [
					'id' => $model->id,
					'text' => $model->text,
					'icon' => $model->icon,
					'url' => $model->url,
					'controller_name' => $model->controller_name,
					'method_name' => $model->method_name,
					'module_name' => $model->module_name,
					'alias' => $model->alias
				];
				
				$child = $this->modelMenuDepedency($groups, $model->id);
				if (count($child)) {
					$dt['items'] = $child;
					$menu[] = $dt;
				}
			}
		}
		
		return $menu;
	}
}