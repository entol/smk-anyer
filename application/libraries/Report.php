<?php

/*
 * @link http://www.didanurwanda.com/
 * @copyright Copyright (c) 2017 Dida Nurwanda
 * @license http://www.didanurwanda.com/license/
 */

//use PHPExcel;
//use PHPExcel_IOFactory;
//use PHPExcel_Worksheet_PageSetup;

/**
 * Description of TReport
 *
 * @author Dida Nurwanda
 */
class Report
{
    public $_template;
    public $_data;
    public $_reportName;
    public $_striped = false;
    public $objReader;
    public $objPHPExcel;
    public $objWorksheet;
    public $objWriter;

    protected static $_paperSizes = array(
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER
            => 'LETTER',                 //    (8.5 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_SMALL
            => 'LETTER',                 //    (8.5 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_TABLOID
            => array(792.00, 1224.00),   //    (11 in. by 17 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEDGER
            => array(1224.00, 792.00),   //    (17 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL
            => 'LEGAL',                  //    (8.5 in. by 14 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STATEMENT
            => array(396.00, 612.00),    //    (5.5 in. by 8.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_EXECUTIVE
            => 'EXECUTIVE',              //    (7.25 in. by 10.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3
            => 'A3',                     //    (297 mm by 420 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4
            => 'A4',                     //    (210 mm by 297 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4_SMALL
            => 'A4',                     //    (210 mm by 297 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A5
            => 'A5',                     //    (148 mm by 210 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_B4
            => 'B4',                     //    (250 mm by 353 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_B5
            => 'B5',                     //    (176 mm by 250 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO
            => 'FOLIO',                  //    (8.5 in. by 13 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_QUARTO
            => array(609.45, 779.53),    //    (215 mm by 275 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STANDARD_1
            => array(720.00, 1008.00),   //    (10 in. by 14 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STANDARD_2
            => array(792.00, 1224.00),   //    (11 in. by 17 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NOTE
            => 'LETTER',                 //    (8.5 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO9_ENVELOPE
            => array(279.00, 639.00),    //    (3.875 in. by 8.875 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO10_ENVELOPE
            => array(297.00, 684.00),    //    (4.125 in. by 9.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO11_ENVELOPE
            => array(324.00, 747.00),    //    (4.5 in. by 10.375 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO12_ENVELOPE
            => array(342.00, 792.00),    //    (4.75 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO14_ENVELOPE
            => array(360.00, 828.00),    //    (5 in. by 11.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C
            => array(1224.00, 1584.00),  //    (17 in. by 22 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_D
            => array(1584.00, 2448.00),  //    (22 in. by 34 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_E
            => array(2448.00, 3168.00),  //    (34 in. by 44 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_DL_ENVELOPE
            => array(311.81, 623.62),    //    (110 mm by 220 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C5_ENVELOPE
            => 'C5',                     //    (162 mm by 229 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C3_ENVELOPE
            => 'C3',                     //    (324 mm by 458 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C4_ENVELOPE
            => 'C4',                     //    (229 mm by 324 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C6_ENVELOPE
            => 'C6',                     //    (114 mm by 162 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_C65_ENVELOPE
            => array(323.15, 649.13),    //    (114 mm by 229 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_B4_ENVELOPE
            => 'B4',                     //    (250 mm by 353 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_B5_ENVELOPE
            => 'B5',                     //    (176 mm by 250 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_B6_ENVELOPE
            => array(498.90, 354.33),    //    (176 mm by 125 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_ITALY_ENVELOPE
            => array(311.81, 651.97),    //    (110 mm by 230 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_MONARCH_ENVELOPE
            => array(279.00, 540.00),    //    (3.875 in. by 7.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_6_3_4_ENVELOPE
            => array(261.00, 468.00),    //    (3.625 in. by 6.5 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_US_STANDARD_FANFOLD
            => array(1071.00, 792.00),   //    (14.875 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_GERMAN_STANDARD_FANFOLD
            => array(612.00, 864.00),    //    (8.5 in. by 12 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_GERMAN_LEGAL_FANFOLD
            => 'FOLIO',                  //    (8.5 in. by 13 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_ISO_B4
            => 'B4',                     //    (250 mm by 353 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_JAPANESE_DOUBLE_POSTCARD
            => array(566.93, 419.53),    //    (200 mm by 148 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STANDARD_PAPER_1
            => array(648.00, 792.00),    //    (9 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STANDARD_PAPER_2
            => array(720.00, 792.00),    //    (10 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_STANDARD_PAPER_3
            => array(1080.00, 792.00),   //    (15 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_INVITE_ENVELOPE
            => array(623.62, 623.62),    //    (220 mm by 220 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_EXTRA_PAPER
            => array(667.80, 864.00),    //    (9.275 in. by 12 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL_EXTRA_PAPER
            => array(667.80, 1080.00),   //    (9.275 in. by 15 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_TABLOID_EXTRA_PAPER
            => array(841.68, 1296.00),   //    (11.69 in. by 18 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4_EXTRA_PAPER
            => array(668.98, 912.76),    //    (236 mm by 322 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_TRANSVERSE_PAPER
            => array(595.80, 792.00),    //    (8.275 in. by 11 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4_TRANSVERSE_PAPER
            => 'A4',                     //    (210 mm by 297 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_EXTRA_TRANSVERSE_PAPER
            => array(667.80, 864.00),    //    (9.275 in. by 12 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_SUPERA_SUPERA_A4_PAPER
            => array(643.46, 1009.13),   //    (227 mm by 356 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_SUPERB_SUPERB_A3_PAPER
            => array(864.57, 1380.47),   //    (305 mm by 487 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_PLUS_PAPER
            => array(612.00, 913.68),    //    (8.5 in. by 12.69 in.)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4_PLUS_PAPER
            => array(595.28, 935.43),    //    (210 mm by 330 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A5_TRANSVERSE_PAPER
            => 'A5',                     //    (148 mm by 210 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_JIS_B5_TRANSVERSE_PAPER
            => array(515.91, 728.50),    //    (182 mm by 257 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3_EXTRA_PAPER
            => array(912.76, 1261.42),   //    (322 mm by 445 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A5_EXTRA_PAPER
            => array(493.23, 666.14),    //    (174 mm by 235 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_ISO_B5_EXTRA_PAPER
            => array(569.76, 782.36),    //    (201 mm by 276 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A2_PAPER
            => 'A2',                     //    (420 mm by 594 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3_TRANSVERSE_PAPER
            => 'A3',                     //    (297 mm by 420 mm)
        PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3_EXTRA_TRANSVERSE_PAPER
            => array(912.76, 1261.42)    //    (322 mm by 445 mm)
    );
    
    /**
     * Constructor.
     */
    public function __construct($options = [])
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions($options = [])
    {
        if (is_array($options)) {
            foreach ($options as $key => $value) {
                $this->{'_' . $key} = $value;
            }
        }
    }

    protected function formatValue($value, $type = null, $options = [])
    {
        if ($type == 'date') {
            $value = $this->formatDate($value, $options);
        } elseif ($type == 'datetime') {
            $value = $this->formatDate($value, $options);
        } elseif ($type == 'time') {
            $value = $this->formatDate($value, $options);
        } elseif ($type == 'timestamp') {
            $value = $this->formatTimestamp($value, $options);
        } elseif ($type == 'number') {
            return $this->formatNumber($value, $options);
        } elseif ($type == 'currency') {
            return $this->formatCurrency($value, $options);
        } elseif ($type == 'image') {
            return $this->formatImage($value, $options);
        }

        return $value;
    }

    public function formatTimestamp($value, $options)
    {
        if (is_string($options)) {
            return date($options, $value);
        }

        return $value;
    }

    protected function formatDate($value, $options)
    {
        if (is_string($options)) {
            return $this->formatTimestamp(strtotime($value), $options);
        }

        return $value;
    }

    protected function formatNumber($value, $options)
    {
        if (is_array($options)) {
            if (!isset($options['prefix'])) {
                $options['prefix'] = '';
            }
            if (!isset($options['decimals'])) {
                $options['decimals'] = 0;
            }
            if (!isset($options['decimal_point'])) {
                $options['decimal_point'] = '.';
            }
            if (!isset($options['thousands_separator'])) {
                $options['thousands_separator'] = ',';
            }
            if (!isset($options['suffix'])) {
                $options['suffix'] = '';
            }
            return $options['prefix'] . number_format($value, $options['decimals'], $options['decimal_point'], $options['thousands_separator']) . $options['suffix'];
        }

        return $value;
    }

    protected function formatCurrency($value, $options)
    {
        if (is_string($options)) {
            return $this->formatNumber($value, ['prefix' => $options . ' ']);
        } elseif (is_array($options)) {
            if (isset($options['symbol'])) {
                $options['prefix'] = $options['symbol'] . ' ';
            }
            return $this->formatNumber($value, $options);
        }

        return $value;
    }

    protected function formatImage($value, $options)
    {
        if (file_exists($value) && is_array($options)) {
            if (isset($options['sheet']) && isset($options['coordinate'])) {
                if (!isset($options['width'])) {
                    $options['width'] = 100;
                }
                if (!isset($options['height'])) {
                    $options['height'] = 60;
                }
                if (!isset($options['offset_x'])) {
                    $options['offset_x'] = 0;
                }
                if (!isset($options['offset_y'])) {
                    $options['offset_y'] = 0;
                }
                if (!isset($options['name'])) {
                    $options['name'] = 'Image';
                }
                if (!isset($options['description'])) {
                    $options['description'] = '';
                }

                try {
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName($options['name']);
                    $objDrawing->setDescription($options['description']);
                    $objDrawing->setPath($value);
                    $objDrawing->setOffsetX($options['offset_x']);
                    $objDrawing->setOffsetY($options['offset_y']);
                    $objDrawing->setWidth($options['width']);
                    $objDrawing->setHeight($options['height']);
                    $objDrawing->setCoordinates($options['coordinate']);
                    $objDrawing->setWorksheet($options['sheet']);
                } catch (Exception $e) {

                }
            }
        }

        return '';
    }

    protected function renderTemplate()
    {
        $fileType = PHPExcel_IOFactory::identify($this->_template);
        $this->objReader = PHPExcel_IOFactory::createReader($fileType);
        $this->objPHPExcel = $this->objReader->load($this->_template);
        
        foreach ($this->_data as $id => $data) {
            // Single Row
            if (isset($data['row']) || isset($data['rows'])) {
                $cellFormats = isset($data['format']) ? $data['format'] : [];
                foreach ($this->objPHPExcel->getSheetNames() as $sheetId => $sheetName) {
                    $sheet = $this->objPHPExcel->getSheet($sheetId);
                    $cellDefine = [];
                    
                    foreach ($sheet->getRowIterator() as $no => $row) {
                        foreach ($row->getCellIterator() as $cell) {
                            $cellValue = trim($cell->getValue());
                            $column = $cell->getColumn();
                            if (preg_match_all("/\{" . $id . ":(\w*|#\+?-?(\d*)?)\}/", $cellValue, $matches)) {
                                if (!empty($matches[1][0])) {
                                    $cellName = $matches[1][0];
                                    
                                    if (isset($data['row'])) {
                                        $this->generateCellValue($data['row'], $cellFormats, $cellName, $sheet, $column . $no);
                                    }
                                    
                                    if (isset($data['rows']) && !isset($data['row'][$cellName])) {
                                        $cellDefine[$cellName] = [
                                            'column' => $column,
                                            'row' => $no
                                        ];
                                        $sheet->setCellValue($column . $no, '');
                                    }
                                }
                            }
                        }
                    }
                    
                    if (isset($data['rows']) && !empty($cellDefine)) {
                        // merge cells
                        $mergeCells = $sheet->getMergeCells();
                        
                        // values
                        $sequence = 1;
                        $dataTotal = count($data['rows']);
                        foreach($data['rows'] as $item) {
                            $no = $sequence - 1;
                            
                            // repeat column
                            $repeatStartAt = '';
                            $repeatEndAt = '';
                            $repeatRowAt = 0;
                            
                            $_cellDefineSequence = 1;
                            $_cellTotal = count($cellDefine);
                            foreach($cellDefine as $cellName => $cellOptions) {
                                $currentCell = $cellOptions['column'] . $cellOptions['row'];
                                $nextCell = $cellOptions['column'] . ($cellOptions['row'] + $no);
                                
                                // insert row
                                if ($_cellDefineSequence == 1 && $sequence < $dataTotal) {
                                    $sheet->insertNewRowBefore(($cellOptions['row'] + $no) + 1, 1);
                                }
                                
                                // copy
                                $xfIndex = $sheet->getCell($currentCell)->getXfIndex();
                                
                                // paste
                                $sheet->getCell($nextCell)->setXfIndex($xfIndex);

                                // value
                                if ($cellName == '#') {
                                    $sheet->setCellValue($nextCell, $sequence);
                                } elseif (isset($item[$cellName])) {
                                    $this->generateCellValue($item, $cellFormats, $cellName, $sheet, $nextCell);
                                }
                                
                                // striped
                                if ($this->_striped && !($sequence % 2)) {
                                    $sheet->getStyle($nextCell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                                    $sheet->getStyle($nextCell)->getFill()->getStartColor()->setRGB('F2F2F2');
                                }
                                
                                // merge needed
                                if ($_cellDefineSequence == 1) {
                                    $repeatStartAt = $cellOptions['column'];
                                } elseif ($_cellDefineSequence == $_cellTotal) {
                                    $repeatEndAt = $cellOptions['column'];
                                }
                                $repeatRowAt = $cellOptions['row'];
                                $_cellDefineSequence++;
                            }
                            
                            // merge
                            foreach($mergeCells as $mergeCell) {
                                if ($this->isSubrange($mergeCell, $repeatStartAt.$repeatRowAt .':'. $repeatEndAt.$repeatRowAt)) {
                                    $this->mergeCells($mergeCell, ($cellOptions['row'] + $no), $sheet);
                                }
                            }
                            
                            $sequence++;
                        }
                    }
                }
            }
        }
    }
    
    protected function mergeCells($merge, $row, $sheet)
    {
        $boundaries = PHPExcel_Cell::rangeBoundaries($merge);
        $cells = PHPExcel_Cell::stringFromColumnIndex($boundaries[0][0] - 1) . $row .':'.
                    PHPExcel_Cell::stringFromColumnIndex($boundaries[1][0] - 1) . $row;
                    
        // merge
        $sheet->mergeCells($cells);
        
        // style
        $style = $sheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($boundaries[0][0] - 1) . $row);
        $sheet->duplicateStyle($style, $cells);
    }

    protected function generateCellValue($data, $formats, $cellName, $sheet, $coordinate)
    {
        if (!empty($data[$cellName])) {
            $value = $data[$cellName];
            if (isset($formats[$cellName])) {
                foreach ($formats[$cellName] as $formatType => $options) {
                    if (is_numeric($formatType) && is_string($options)) {
                        $value = $this->formatValue($value, $options);
                    } elseif (is_string($formatType) && $formatType == 'image') {
                        $value = $this->formatValue($value, $formatType, array_merge([
                            'sheet' => $sheet,
                            'coordinate' => $coordinate
                        ], $options));
                    } elseif (is_string($formatType)) {
                        $value = $this->formatValue($value, $formatType, $options);
                    }
                }
            }
        } else {
            $value = '';
        }
        $sheet->setCellValue($coordinate, $value);
    }
    
    public function isSubrange($subRange, $range)
    {
        list($rangeStart, $rangeEnd) = PHPExcel_Cell::rangeBoundaries($range);
        list($subrangeStart, $subrangeEnd) = PHPExcel_Cell::rangeBoundaries($subRange);
        return (($subrangeStart[0] >= $rangeStart[0]) && 
            ($subrangeStart[1] >= $rangeStart[1]) && 
            ($subrangeEnd[0] <= $rangeEnd[0]) &&
            ($subrangeEnd[1] <= $rangeEnd[1]));
    }

    public function render($format = '', $reportName = 'Report', $data = [], $htmlInlineCss = false)
    {
        if (!empty($reportName)) {
            $this->_reportName = $reportName;
        }

        if (is_array($data) && !empty($data)) {
            $this->_data = $data;
        }

        if (file_exists($this->_template)) {
            $this->renderTemplate();
            
            $format = trim(strtolower($format));
            if ($format == 'xlsx' || $format == 'excel' || $format == 'excel2007') {
                $this->renderXlsx($reportName);
            } elseif ($format == 'xls' || $format == 'excel2003') {
                $this->renderXls($reportName);
            } elseif ($format == 'html') {
                return $this->renderHtml($reportName, $htmlInlineCss);
            } elseif ($format == 'pdf') {
                $this->renderPdf($reportName);
            } else {
                throw new \Exception('Render format unsupported');
            }
        } elseif ($this->_template) {
            throw new Exception('Unable to load template file: ' . $this->_template);
        } else {
            throw new \Exception('Template not found');
        }
    }

    protected function renderXlsx($reportName)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $reportName . '.xlsx"');
        header('Cache-Control: max-age=0');
        
        $this->objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $this->objWriter->save('php://output');
    }

    protected function renderXls($reportName)
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $reportName . '.xls"');
        header('Cache-Control: max-age=0');
        
        $this->objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
        $this->objWriter->save('php://output');
    }

    protected function renderHtml($reportName, $htmlInlineCss = false)
    {
        $this->objWriter = new PHPExcel_Writer_HTML($this->objPHPExcel);
        $this->objWriter->setUseInlineCss($htmlInlineCss);

        $html = '';
        $html .= $this->objWriter->generateHTMLHeader(true);
        $html .= $this->objWriter->generateSheetData();
        $html .= $this->objWriter->generateHTMLFooter();
        // $this->objPHPExcel->disconnectWorkSheets();
        return $html;
    }

    protected function renderPdf($reportName)
    {
        
        $html = $this->renderHtml($reportName, true);
        
        //  Default PDF paper size
        $paperSize = 'A4';
        
        if (is_null($this->objWriter->getSheetIndex())) {
            $orientation = ($this->objPHPExcel->getSheet(0)->getPageSetup()->getOrientation()
                == PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)  ? 'L' : 'P';
            $printPaperSize = $this->objPHPExcel->getSheet(0)->getPageSetup()->getPaperSize();
            $printMargins = $this->objPHPExcel->getSheet(0)->getPageMargins();
        } else {
            $orientation = ($this->objPHPExcel->getSheet($this->objWriter->getSheetIndex())->getPageSetup()->getOrientation()
                == PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE) ? 'L' : 'P';
            $printPaperSize = $this->objPHPExcel->getSheet($this->objWriter->getSheetIndex())->getPageSetup()->getPaperSize();
            $printMargins = $this->objPHPExcel->getSheet($this->objWriter->getSheetIndex())->getPageMargins();
        }
        
        $marginLeft = $printMargins->getLeft() * 2.5;
        $marginTop = $printMargins->getTop() * 2.5;
        $marginRight = $printMargins->getRight() * 2.5;
        $marginBottom = $printMargins->getBottom() * 2.5;
        
        if (isset(self::$_paperSizes[$printPaperSize])) {
            $paperSize = self::$_paperSizes[$printPaperSize];
        }
        
        $pdf = new \Dompdf\Dompdf;
        $pdf->setPaper(strtolower($paperSize), ($orientation == 'L' ? 'landscape' : 'portrait'));
        $pdf->loadHtml($html);
        $pdf->render();
        $pdf->stream("Report Capaian.pdf", ['Attachment' => false]);
    }
}
