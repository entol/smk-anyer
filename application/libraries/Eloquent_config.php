<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Yajra\Oci8\Connectors\OracleConnector;
use Yajra\Oci8\Oci8Connection;

class Eloquent_config
{
	public $ci;
	
	public function __construct()
	{
		$this->ci =& get_instance();
		include APPPATH .'config/database.php';  
		
		$capsule = new Capsule;
        
        // Database Manager
        $manager = $capsule->getDatabaseManager();
        $manager->extend('oracle', function($config) {
            $connector = new OracleConnector();
            $connection = $connector->connect($config);
            $db = new Oci8Connection($connection, $config["database"], $config["prefix"]);

            // set oracle session variables
            $sessionVars = [
                'NLS_TIME_FORMAT'         => 'HH24:MI:SS',
                'NLS_DATE_FORMAT'         => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_FORMAT'    => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                'NLS_NUMERIC_CHARACTERS'  => '.,'
            ];

            // Like Postgres, Oracle allows the concept of "schema"
            if (isset($config['schema'])) {
                $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
            }

            $db->setSessionVars($sessionVars);

            return $db;
        });
        
        
        $driver = 'mysql';
        if (in_array($db['default']['dbdriver'], array('mysql', 'mysqli'))) {
            $driver = 'mysql';
        } elseif (in_array($db['default']['dbdriver'], array('pdo'))) {
            $driver = explode(':', $db['default']['dsn'])[0];
            if ($driver == 'oci') {
                $driver = 'oracle';
            }
        } else {
            $driver = $db['default']['dbdriver'];
        }
        
        $dbconfig = array(
            'driver' => $driver,
            'host' => $db['default']['hostname'],
            'database' => $db['default']['database'],
            'username' => $db['default']['username'],
            'password' => $db['default']['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => $db['default']['dbprefix'],
			'options'   => [
				\PDO::ATTR_EMULATE_PREPARES => true
			]
		);
        
        if (isset($db['default']['port'])) {
            $dbconfig['port'] = $db['default']['port'];
        }
            
		$capsule->addConnection($dbconfig);
		$capsule->setAsGlobal();
		$capsule->bootEloquent();

		$events = new Illuminate\Events\Dispatcher;
		$events->listen('illuminate.query', function($query, $bindings, $time, $name) {
			// Format binding data for sql insertion

			foreach ($bindings as $i => $binding) {
			  if ($binding instanceof \DateTime)  {
				$bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
			  } else if (is_string($binding)) {
				$bindings[$i] = "'$binding'";
			  }
			}

			// Insert bindings into query
			$query = str_replace(array('%', '?'), array('%%', '%s'), $query);
			$query = vsprintf($query, $bindings);

			// Add it into CodeIgniter
			$db =& get_instance()->db;
			$db->query_times[] = $time;
			$db->queries[] = $query;
		});

		$capsule->setEventDispatcher($events);
	}
}