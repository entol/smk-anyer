<?php

use Dompdf\Dompdf;

define('DOMPDF_ENABLE_CSS_FLOAT', true);

class Export
{
    public function pdf($file, $data = [], $print = false)
    {
        $report = APPPATH .'reports/'. $file .'.php';
        if (file_exists($report)) {
            $html = get_instance()->load->view('../reports/'. $file, $data, true);
        } else {
            $html = get_instance()->load->view($file, $data, true);    
        }

        if ($print) {
            echo $html .'
            <script type="text/javascript">
                window.print();
                window.onfocus = function() {
                    setTimeout(function() {
                        window.close();
                    }, 500);
                }
            </script>';
            exit;
        }

        $size = isset($data['dompdf']['size']) ? $data['dompdf']['size'] : 'A4';
        $orientation = isset($data['dompdf']['orientation']) ? $data['dompdf']['orientation'] : 'portrait';
        $name = isset($data['dompdf']['name']) ? $data['dompdf']['name'] : 'report.pdf';

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper($size, $orientation);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream($name, [
            'Attachment' => false
        ]);
    }

    public function csv($dataProvider, $header, $body, $options = [])
    {
        $output = [];
        $headerOutput = [];
        foreach($header as $label) {
            $headerOutput[] = $this->csvItem($label);
        }
        $output[] = implode(';', $headerOutput);

        foreach($dataProvider as $index => $model) {
            if (is_callable($body)) {
                $tBodyOutput = call_user_func_array($body, [
                    $model, $index
                ]);
				$bodyOutput = [];
				foreach($tBodyOutput as $item) {
					$bodyOutput[] = $this->csvItem($item);
				}
            } elseif (is_array($body)) {
                $bodyOutput = [];
                foreach($body as $k) {
                    $bodyOutput[] = $this->csvItem(array_get_value($model, $k));
                }
            } else {
                $bodyOutput = [];
            }
            $output[] = implode(';', $bodyOutput);
        }

        $name = isset($options['name']) ? $options['name'] : 'export.csv';

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$name.'";');

        echo implode("\r\n", $output);
        exit;
    }

    private function csvItem($text)
    {
        if (strpos($text, ';') !== false) {
            return '"'. addslashes($text) .'"';
        }
        return $text;
    }
}