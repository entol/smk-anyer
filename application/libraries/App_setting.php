<?php

class App_setting
{
    private $ci;
    private $dt;
    private $bg = [];
    
    public function __construct()
    {
        $this->ci = get_instance();
        $this->ci->load->model(['Setting', 'Background']);
        $this->load();
    }
    
    private function load()
    {
        if (empty($this->dt)) {
            $this->dt = Setting::where('id', 1)->first();
        }

        if (empty($this->bg)) {
            foreach(Background::all() as $bg) {
                $this->bg[$bg->id] = $bg->background;
            }
        }
    }
    
    public function getConfig($key)
    {
        if ($key == 'login_background') {
            $id = array_get_value($this->dt, $key, 1);
            return array_get_value($this->bg, $id, null);
        }

        return array_get_value($this->dt, $key, null);
    }
    
    public function setConfig($key, $value)
    {
        $model = Setting::where('id', 1)->first();
        if (isset($this->dt->{$key})) {
            $model->{$key} = $value;
            $model->save();
            return true;
        }
        return false;
    }

    public function config($key)
    {
        return $this->getConfig($key);
    }

    public function backgrounds()
    {
        return $this->bg;
    }
}