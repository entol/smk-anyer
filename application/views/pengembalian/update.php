<div class="panel panel-inverse">
    <div class="panel-heading with-border">
        <h3 class="panel-title">Pengembalian</h3>
    </div>
    <div class="panel-body">
        <form method="post" id="form-pengembalian-barang">
        
            <div style="height: 50px" class="xcrud-action-header">
                <div class="pull-right btn-group">
                    <a href="<?= site_url('pengembalian/index') ?>" data-task="list" class="btn btn-sm btn-warning xcrud-action">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary xcrud-action">Simpan &amp; Kembali</button>
                </div>
            </div>
            
            <div class="form-horizontal">
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Project Asal<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <?= $pengembalian->kode_project .' - '. $pengembalian->project->nama ?>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Gudang Tujuan<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <?= form_dropdown('kode_gudang', $optGudang, $pengembalian->kode_gudang, [
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                </div>
               
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Tanggal Pengembalian</label>
                    <div class="col-sm-9">
                        <?= form_input('tanggal_pengembalian', $pengembalian->tanggal_pengembalian, [
                            'class' => 'form-control',
                            'autocomplete' => 'off'
                        ]) ?>
                    </div>
                </div> 
               
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Sebagai Draft<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <div class="radio radio-css">
                            <?= form_radio('draft', 'Y', $pengembalian->draft == 'Y', ['id' => 'draft_y']) ?>
                            <label class="form-check-label" for="draft_y">Ya</label>
                        </div>
                        <div class="radio radio-css">
                            <?= form_radio('draft', 'N', $pengembalian->draft == 'N', ['id' => 'draft_n']) ?>
                            <label class="form-check-label" for="draft_n">Tidak</label>
                        </div>
                    </div>
                </div>
               
                <div style="margin-top: 20px; height: 40px" class="xcrud-action-header">
                    <div class="pull-right btn-group">
                        <a href="javascript:;" class="btn btn-info btn-sm" id="btn-lookup-barang">
                            <i class="fa fa-plus-circle"></i>
                            Material
                        </a>
                    </div>
                </div>
                
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 25px">#</th>
                            <th>Kode Barang</th>
                            <th>Deskripsi</th>
                            <th>Kondisi</th>
                            <th>Qty</th>
                            <th>Catatan</th>
                            <th style="width: 60px"></th>
                        </tr>
                    </thead>
                    <tbody id="list-pengembalian-barang">
                        <?php if (count($items)): ?>
                        <?php foreach($items as $i => $item): ?>
                            <tr data-kode-barang="<?= $item->kode_barang ?>" data-kode-item="" class="<?= $item->approved == 'Y' ? 'success' : ''?>">
                                <td><?= ($i + 1) ?></td>
                                <td>
                                    <?= $item->kode_barang ?>
                                    <?= form_input([
                                        'name' => 'kode_barang['. $i .']', 
                                        'value' => $item->kode_barang,
                                        'class' => 'kode_barang',
                                        'type' => 'hidden'
                                    ]) ?>
                                </td>
                                <td><?= $item->barang->deskripsi ?></td>
                                <td><?= (
                                        $item->kondisi == 'G' 
                                        ? '<span style="font-size: 10px" class="label label-primary">Good</span>' 
                                        : (
                                            $item->kondisi == 'R' 
                                            ? '<span style="font-size: 10px" class="label label-warning">Repair</span>' 
                                            : (
                                                $item->kondisi == 'D' 
                                                ? '<span style="font-size: 10px" class="label label-danger">Damaged</span>' 
                                                : '<span style="font-size: 10px" class="label label-dark">Good</span>'
                                            )
                                        )
                                    ) ?></td>
                                <td>
                                    <?= form_input([
                                        'name' => 'qty['. $i .']', 
                                        'value' => $item->qty,
                                        'class' => 'form-control qty-barang',
                                        'type' => 'number',
                                        'min' => 1,
                                        'max' => (
                                            $item->kondisi == 'G'
                                            ? $item->barangItem->qty_baik
                                            : (
                                                $item->kondisi == 'R'
                                                ? $item->barangItem->qty_perbaikan
                                                : (
                                                    $item->kondisi == 'D'
                                                    ? $item->barangItem->qty_rusak
                                                    : $item->barangItem->qty_hilang
                                                )
                                            )
                                        )
                                    ]) ?>
                                    </td>
                                <td>
                                    <?= form_input([
                                        'name' => 'qty['. $i .']', 
                                        'value' => $item->qty,
                                        'class' => 'form-control qty-barang',
                                        'type' => 'hidden',
                                        'min' => 1
                                    ]) ?>
                                    <?= form_input([
                                        'name' => 'kondisi['. $i .']',
                                        'value' => $item->kondisi,
                                        'class' => 'form-control',
                                        'type' => 'hidden'
                                    ]) ?>
                                    <?= form_input([
                                        'name' => 'id_barang['. $i .']', 
                                        'value' => $item->kondisi .'-'. $item->barangItem->id,
                                        'class' => 'id-barang',
                                        'type' => 'hidden',
                                    ]) ?>
                                    <?= form_input('note['. $i .']', $item->note, [
                                        'class' => 'form-control',
                                        'autocomplete' => 'off'
                                    ]) ?>
                                </td>
                                <td>
                                    <a href="javascript:;" class="btn btn-sm btn-danger btn-hapus-lookup-barang"><i class="fa fa-fw fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        <?php else: ?>
                        <tr><td colspan="7">Belum ada material</td></tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        
        </form>
    
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var numBarang = <?= count($items) ?>;
    var selectedBarang = <?= count($items) ?>;
    
    function getKodeBarangRegistered() {
        var items = [];
        $('.id-barang').each(function() {
            var v = $(this).val();
            if (v != null && v != '') {
                items.push(v);
            }
        });
        return items;
    }
    function getKodeItemRegistered() {
        var items = [];
        $('.kode_item').each(function() {
            var v = $(this).val();
            if (v != null && v != '') {
                items.push(v);
            }
        });
        return items;
    }
    function clearListPengembalian() {
        numBarang = 0;
        selectedBarang = 0;
        $('#list-pengembalian-barang').html('<tr><td colspan="7">Belum ada material</td></tr>');
    }
    
    var lookupDialog = null;
    $(document).on('click', '#btn-lookup-barang', function() {
        var kdb = getKodeBarangRegistered().join(',');
        var kdi = getKodeItemRegistered().join(',');
        var kda = '<?= $pengembalian->kode_project ?>';
        var kg = $('[name="kode_gudang"]').val();
        
        if (kg == '') {
            $.alert('Silahkan pilih Gudang terlebih dahulu', 'Alert');
        } else {
            lookupDialog = $.confirm({
                title: 'Lookup Material',
                content: [
                    '<table id="lookup" class="display" style="width:100%">',
                        '<thead>',
                            '<tr>',
                                '<th></th>',
                                '<th>Kode</th>',
                                '<th>Kategori</th>',
                                '<th>Deskripsi</th>',
                                '<th>Brand</th>',
                                '<th>Tipe</th>',
                                '<th>Tahun</th>',
                                '<th>Kondisi</th>',
                                '<th>Qty</th>',
                            '</tr>',
                        '</thead>',
                    '</table>'
                ].join(''),
                onContentReady: function () {
                    let height = ($(window).height() - 320);
                    height = height < 200 ? 200 : height;
                    lookupDt = this.$content.find('#lookup').DataTable( {
                        ajax: '<?= site_url('pengembalian/lookup-barang') ?>?kdb='+ kdb +'&kdi='+ kdi +'&kda='+ kda,
                        columns: [
                            { data: 'empty' },
                            { data: 'kode' },
                            { data: 'kategori' },
                            { data: 'deskripsi' },
                            { data: 'brand' },
                            { data: 'tipe' },
                            { data: 'tahun' },
                            { data: 'nama_kondisi' },
                            { data: 'quantity' },
                        ],
                        columnDefs: [{
                            orderable: false,
                            className: 'select-checkbox',
                            targets: 0
                        }],
                        ordering: false,
                        scrollY: (height) +'px',
                        scrollCollapse: true,
                        paging: false,
                        select: {
                            style: 'multi',
                            selector: 'td:first-child'
                        },
                        order: [[ 1, 'asc' ]],
                        initComplete: function(){
                            $('.dataTables_filter input').unbind();
                            $('.dataTables_filter input').bind('keyup', function(e) {
                                if(e.keyCode == 13) {
                                    lookupDt.search( this.value ).draw(); 
                                }
                            });     
                        },
                    });
                },
                columnClass: 'xlarge',
                buttons: {
                    select: {
                        text: 'Pilih',
                        btnClass: 'btn-blue',
                        action: function () {
                            var jc = this;
                            var err = this.$content.find('.text-danger');
                            var selected = lookupDt.rows({ selected: true });
                            var datas = selected.data();
                            var total = selected.count();
                            for (var i = 0; i < total; i++) {
                                var data = datas[i];
                                numBarang++;
                                selectedBarang++;
                                var kdBarangInput = $('<input/>', {
                                    type: 'hidden',
                                    name: 'kode_barang['+ numBarang +']',
                                    'class': 'kode_barang'
                                }).val(data.kode);
                                
                                var idBarangInput = $('<input/>', {
                                    type: 'hidden',
                                    class: 'id-barang',
                                    name: 'id_barang['+ numBarang +']'
                                }).val(data.id);
                                
                                var qtyInput = $('<input/>', {
                                    type: 'number',
                                    min: 1,
                                    max: data.quantity,
                                    name: 'qty['+ numBarang +']',
                                    'class': 'form-control qty-barang'
                                }).val(1);

                                if (data.kondisi == 'G') {
                                    var kondisiLabel = $('<span/>', {
                                        'class': 'label label-primary',
                                        'style': 'font-size: 10px'
                                    }).text('Good');
                                } else if (data.kondisi == 'R') {
                                    var kondisiLabel = $('<span/>', {
                                        'class': 'label label-warning',
                                        'style': 'font-size: 10px'
                                    }).text('Repair');
                                } else if (data.kondisi == 'D') {
                                    var kondisiLabel = $('<span/>', {
                                        'class': 'label label-danger',
                                        'style': 'font-size: 10px'
                                    }).text('Damage');
                                } else {
                                    var kondisiLabel = $('<span/>', {
                                        'class': 'label label-dark',
                                        'style': 'font-size: 10px'
                                    }).text('Lost');
                                }
                                var kondisiInput = $('<input/>', {
                                    type: 'hidden',
                                    name: 'kondisi['+ numBarang +']'
                                }).val(data.kondisi);
                                
                                var noteInput = $('<input/>', {
                                    type: 'text',
                                    name: 'note['+ numBarang +']',
                                    'class': 'form-control',
                                    'autocomplete': 'off'
                                });
                                
                                var btnInput = $('<a/>', {
                                    href: 'javascript:;',
                                    'class': 'btn btn-sm btn-danger btn-hapus-lookup-barang'
                                }).html('<i class="fa fa-fw fa-trash"></i>');
                                
                                var $tr = $('<tr/>').data('kodeBarang', data.kode).data('kodeItem', '');
                                $('<td/>').text(numBarang).appendTo($tr);
                                $('<td/>').text(data.kode).append(kdBarangInput).appendTo($tr);
                                $('<td/>').text( data.deskripsi ).appendTo($tr);
                                $('<td/>').append(kondisiLabel).append(kondisiInput).appendTo($tr);
                                $('<td/>').append( qtyInput ).appendTo($tr);
                                $('<td/>').append(noteInput).append(idBarangInput).appendTo($tr);
                                $('<td/>').append(btnInput).appendTo($tr);
                                
                                if (numBarang === 1) {
                                    $('#list-pengembalian-barang').html('');
                                }
                                $('#list-pengembalian-barang').append($tr);

                            }
                        }
                    },
                    cancel: function() {
                        
                    }
                }
            });
        }
    });
    
    $(document).on('click', '.btn-hapus-lookup-barang', function() {
        var tr = $(this).closest('tr');
        tr.remove();
        selectedBarang--;
        
        if ($('#list-pengembalian-barang>tr').length == 0) {
            clearListPengembalian();
        }
    });
    
    // init jquery plugin
    $('[name="tanggal_pengembalian"]').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat:  'yy-mm-dd'
    });
    $('[name="project"], [name="kode_gudang"]').select2();

    $(window).on('resize', function() {
        $('[name="project"], [name="kode_gudang"]').each(function() {
            var $that = $(this);
            if ($that.data('select2')) {
                $that.select2('destroy');
                $that.select2();
            }
        });
    });
    
    $(document).on('change', '[name="project"], [name="kode_gudang"]', function() {
        clearListPengembalian();
    });
    
    $(document).on('keyup', '.qty-barang', function() {
        var value = $(this).val();
        var max = parseInt($(this).attr('max'));
        if (value == '') {
            $(this).val(1);
        } else if (!isNaN(max) && max > 0) {
            if (value > max) {
                $(this).val(max);
            }
        }
    });
    
    $("#form-pengembalian-barang").validate();
    $(document).on('submit', '#form-pengembalian-barang', function(e) {
        e.preventDefault();
        if (selectedBarang == 0) {
            $.alert('Silahkan pilih Materail terlebih dahulu.', 'Error');
        } else {
            var loader = $.alert({
                title: false,
                content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memuat</p></center>',
                buttons: {
                    ok: function() {}
                },
                onOpenBefore: function() {
                    loader.buttons.ok.hide();
                }
            });
            
            var frm = $(this).serializeArray();
            $.post('<?= site_url('pengembalian/update-process/'. $pengembalian->id) ?>', frm, function(e) {
                if (e.status == 1) {
                    window.location.href = '<?= site_url('pengembalian/index') ?>';
                } else {
                    $.alert(e.message, 'Kesalahan');
                }
                loader.close();
            }).fail(function() {
                $.alert('Terjadi kesalahan saat menghubungi server', 'Failed');
                loader.close();
            })
        }
    });
});
</script>

<style type="text/css">
.error {
    text-align: left;
    color: red;
}
.form-group.row {
    margin-bottom: 5px;
}
.form-horizontal .col-form-label {
    text-align: right;
}
table.table thead tr th {
    background: #eeeeee;
    border-color: #dddddd!important;
}
.jconfirm .xcrud-nested-container.xcrud-container {
    margin-top: 0;
}
.jconfirm .xcrud-nav {
    padding-top: 5px;
}
</style>