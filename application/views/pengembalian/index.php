<?= $xcrudContent ?>

<style type="text/css">
.xcrud-nested-container.xcrud-container {
    margin-top: 0;
}
</style>

<a href="" id="link-new-tab" style="display: none" target="_blank"></a>

<script type="text/javascript">
jQuery(document).ready(function() {
    function addButton() {
        $('[data-task="create"]').attr('href', '<?= site_url('pengembalian/create') ?>').removeClass('xcrud-action');
    }
    addButton();
    
    function editButton() {
        $('[data-task="edit"]').each(function() {
            var pk = $(this).data('primary');
            $(this).attr('href', '<?= site_url('pengembalian/update/') ?>'+ pk).removeClass('xcrud-action');
        });
    }
    editButton();
    
    $(document).on("xcrudafterrequest", function(event,container) {
        addButton();
        editButton();
    });
    
    $(document).on('click', '.btn-proses-surat', function(e) {
        e.preventDefault();
        var $that = $(this);
        var data = $that.data();
        
        var loader = $.alert({
            title: false,
            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memproses</p></center>',
            buttons: {
                ok: function() {}
            },
            onOpenBefore: function() {
                loader.buttons.ok.hide();
            }
        });
        $.post('<?= site_url('pengembalian/load-surat/') ?>'+ data.id, function(x) {
            loader.close();
            if (x.status == 1) {
                showSuratJalan(data.id, x.data);
            } else {
                $.alert(x.message, 'Error');
            }
        }).fail(function() {
            loader.close();
            $.alert('Permintaan proses gagal', 'Error');
        });
    });

    function showSuratJalan(id, data) {
        $.confirm({
            title: 'Surat Jalan',
            content: [
                '<form action="">',
                    '<div class="row">',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="from">From</label>',
                                '<input type="text" class="form-control" name="from" required="required" readonly="readonly"/>',
                            '</div>',
                        '</div>',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="to">To</label>',
                                '<input type="text" class="form-control" name="to" required="required" readonly="readonly" />',
                            '</div>',
                        '</div>',
                    '</div>',
                    '<div class="row">',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="carrier-name">Carrier Name</label>',
                                '<input type="text" class="form-control" name="carrier-name" required="required" />',
                            '</div>',
                        '</div>',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="address">Address</label>',
                                '<input type="text" class="form-control" name="address" required="required" />',
                            '</div>',
                        '</div>',
                    '</div>',
                    '<div class="row">',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="vehicle">Vehicle / Police No.</label>',
                                '<input type="text" class="form-control" name="vehicle" required="required" />',
                            '</div>',
                        '</div>',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="attn">Attn</label>',
                                '<input type="text" class="form-control" name="attn" />',
                            '</div>',
                        '</div>',
                    '</div>',
                    '<div class="row">',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="delivery-date">Day / Date</label>',
                                '<input type="date" class="form-control" name="delivery-date" required="required" />',
                            '</div>',
                        '</div>',
                        '<div class="col-sm-6">',
                            '<div class="form-group">',
                                '<label for="delivery-no">Delivery No</label>',
                                '<input type="text" class="form-control" name="delivery-no" required="required" />',
                            '</div>',
                        '</div>',
                    '</div>',
                '</form>'
            ].join(''),
            columnClass: 'large',
            buttons: {
                yes: {
                    text: 'Cetak',
                    btnClass: 'btn-primary',
                    action: function(){
                        var from = this.$content.find('[name="from"]').val();
                        if(!from || from == '') {
                            $.alert('Kolom <b>From</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var to = this.$content.find('[name="to"]').val();
                        if(!to || to == '') {
                            $.alert('Kolom <b>To</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var carrier = this.$content.find('[name="carrier-name"]').val();
                        if(!carrier || carrier == '') {
                            $.alert('Kolom <b>Carrier Name</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var address = this.$content.find('[name="address"]').val();
                        if(!address || address == '') {
                            $.alert('Kolom <b>Address</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var vehicle = this.$content.find('[name="vehicle"]').val();
                        if(!vehicle || vehicle == '') {
                            $.alert('Kolom <b>Vehicle / Police No</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var deliveryDate = this.$content.find('[name="delivery-date"]').val();
                        if(!deliveryDate || deliveryDate == '') {
                            $.alert('Kolom <b>Day / Date</b> tidak boleh kosong', 'Error');
                            return false;
                        }
                        var deliveryNo = this.$content.find('[name="delivery-no"]').val();
                        if(!deliveryNo || deliveryNo == '') {
                            $.alert('Kolom <b>Delivery No</b> tidak boleh kosong', 'Error');
                            return false;
                        }

                        var form = this.$content.find('form');
                        let that = this;
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memproses</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
                        $.post('<?= site_url('/pengembalian/proses-surat/') ?>'+ id, form.serialize(), function(x) {
                            loader.close();
                            if (x.status == 1) {
                                console.log(x);
                                $('#link-new-tab').attr('href', '<?= site_url('/pengembalian/cetak-surat/') ?>'+ id);
                                $('#link-new-tab').get(0).click();
                                
                                Xcrud.reload(".xcrud-container:first");
                                that.close();
                            } else {
                                $.alert(x.message, 'Error');
                            }
                        }).fail(function() {
                            loader.close();
                            $.alert('Permintaan proses gagal', 'Error');
                        });

                        return false;
                    }
                },
                no: {
                    text: 'Batal',
                    btnClass: 'btn-default',
                    action: function(){
                        
                    }
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('[name="from"]').val(data.from);
                this.$content.find('[name="to"]').val(data.to);
                this.$content.find('[name="carrier-name"]').val(data.carrier_name);
                this.$content.find('[name="address"]').val(data.address);
                this.$content.find('[name="vehicle"]').val(data.vehicle);
                this.$content.find('[name="attn"]').val(data.attn);
                this.$content.find('[name="delivery-date"]').val(data.delivery_date);
                this.$content.find('[name="delivery-no"]').val(data.delivery_no);

                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });   
    }
});
</script>