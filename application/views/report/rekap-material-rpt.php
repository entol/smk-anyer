<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Rekap Material</title>
    <style type="text/css">
        html {
            /* margin: 1.5cm 1cm; */
            margin: 0cm;
        }
        @page  
        { 
            /* size: A4 portrait;   /* auto is the initial value */ 
            size: A4 landscape;

            /* this affects the margin in the printer settings */ 
            margin: 1cm;  
        } 

        body {
          font-family: sans-serif;
          font-size: 10px;
          line-height: 15px;;
          /*padding: 0.05cm;*/
          /* margin: 1.5cm 1cm;  */
        }

        table.table {
          border-collapse: collapse;
          width: 98%;
          /*display: inline-block;*/
        }
        table.table>thead>tr>th,
        table.table>tr>th,
        table.table>tbody>tr>td,
        table.table>tr>td {
          text-align: left;
          border: 1px solid #000;
        }

        table.table>thead>tr>th,
        table.table>tr>th {
            padding: 0.45rem;
            text-align: center;
            font-weight: bold;
        }

        table.table>tbody>tr>td,
        table.table>tr>td {
            padding: 0.30rem;
            vertical-align: top;
        }

        /* table.table.table-items>thead>tr>th {
            border-top: 2px solid #000;
            border-bottom: 2px solid #000;
        }
        table.table.table-items>thead>tr>th:first-child {
            border-left: 2px solid #000;
        }
        table.table.table-items>thead>tr>th:last-child {
            border-right: 2px solid #000;
        }

        table.table.table-items>tbody>tr>td:first-child {
            border-left: 2px solid #000;
        }
        table.table.table-items>tbody>tr>td:last-child {
            border-right: 2px solid #000;
        }
        table.table.table-items>tbody>tr:last-child>td {
            border-bottom: 2px solid #000;
        } */

        thead:before, thead:after { display: none; }
        tbody:before, tbody:after { display: none; }

        table.table.table-no-border th,
        table.table.table-no-border td {
          border: 0px solid #bbb;
		  padding: 0.20rem;
        }

        table.table.table-no-border table th,
        table.table.table-no-border table td {
          border: 1px solid #333;
        }

        table.table.table-no-border td.no-border,
        table.table td.no-border {
          border: 0px solid #bbb;
        }

        table.table th.text-center,
        table.table td.text-center,
        .text-center {
            text-align: center;
        }
        table.table th.text-right,
        table.table td.text-right,
        .text-right {
            text-align: right;
        }

        table.table th.text-middle,
        table.table td.text-middle,
        .text-middle {
            vertical-align: middle;
        }

        .color-yellow {
            background: #ffff00;
        }

        .color-orange {
            background: #fabf8f;
        }

        .color-blue {
            background: #92cddc;
        }

        .color-green {
            background: #00b050;
        }

        .color-green {
            background: #00b050;
        }

        .color-pink {
            background: #f19696;
        }

        #h2 {
            line-height: 1px;
        }
        .text-bold {
            font-weight: bold;
        }
        .header {
            font-size: 8pt;
        }
        .text-top {
            vertical-align: top;
        }

        table.table.signature {
            page-break-inside: avoid;
        }
		
		.line1 {
			border-bottom: 2px solid black;
		}
		
		.line2 {
			height: 2px;
			border-bottom: 1px solid black;
		}

        .text-underline {text-decoration: underline}
		.page_break { page-break-before: always; }

        h3 {font-size: 13px}
        .m0 {margin: 0}
        .m5 {margin: 5px}
        .mb0 {margin-bottom: 0}
        .mb3 {margin-bottom: 2px}
        .mb5 {margin-bottom: 5px}
    </style>
</head>
<body>

<table style="width: 100%">
    <tr>
        <td id="header-logo" style="width: 180px" class="text-top">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPgAAABgCAYAAAAw5L8lAAAACXBIWXMAAAsTAAALEwEAmpwYAAAQiUlEQVR4Xu3de3QU1R0H8O+GRB7a8KoxQqFVwKrEA6Xa0oj1iET0FA4cUVtQsFCoqOVVKNBU3E4ixEAxLQiCUYtapUcrR0EqGpVggSMgLVZsfQDWeoAINMSIIeSx2z+G2Z397ezO7Ob+7sxO7uccTjbfWTY7d+9v5u7uzJ1AOBwOox06d0n0dmMLEEqzFbrk6D8H9AD2TY9dpihuC/itwCduALZ/Bvynji5xz7e6AZ/Moqmi8MvoAn/jEPCzjcCnX9Al3hYO0kRReGRUgY96Ftj8MU0zG0exj3qWJgqHlyfQJCq4Fdh7lKZiJfv7hmwaeM0FvwOOfUVT/who+s/Jg4EnxsQuS4fxeAqvZBvm/iuAgydp6g7PFfjfPgV+uI6m/vfHffo/IHnnUbztqkrvFDfgoQJXe56ogAbceyXw8I/okuRUG8oxeyhNdF5sf1ffgz/1LnDnizRVzFLZm3uxg/mR1WviRttbPQ8qiwYybDmgN4gqbnsBTW8vO250MEXn5baXOkTfewS4spKmip2bngG6dwJqF9Alimx0r+lmcVcdBIr60TSWlAL/7Aug7+9pqqTiZKPemWgHA9ztZO1VawjILqWp97C+B28JATkZ0AiZxlzkza3AOQ9Ef1f4mNvdKxtVqw2+GdsevPNi/Rhvr/piof4zt2NsbqehWd9wdX2QLpEnpwRovl+/rYpbPq8UtxPCC/zQSaDfCpq6q08u8N85NE2PcXKJseU8UAsMWBldLkNLGLh/K1ByHV2icHlzkv4zk4obEDxEv6oSeOcITd3R+Bugo/DNV3Jr3wGmb6ap4gf3XAWs3kNT99kN0YUVuBe2bC/+GBhzKU3l80JbKO2DXYG3eR93/1ag9C2ayrVnGnBlL5q6JxwE9tUA31lLl4hn9wLLUHMKuHA5TcVxcx0vWwV8cIKmYjx0A/DL12gqVpsOdLnuSXeLOxzU/3mpuA2D84E5CQ5pFGnFLprIx1ncbgpofMUtaqO1r4YmsdIu8MFrgOr/0JTf7KHRwva6h0YCl36dpmLN2kITuRqbaSKWW68z59ssY53m/CA255BWgQc04N3Pacpr3GV6w1SMpEu87d/30sRfOpumvvILruLukiN+g2X3NjDl9+BcK59I0cXAaxNpmlnCQfnt5gfGsQoycb1OWQHgq2Ka8ktpD8618la+8TW9MDK9uGVY/x5N5ODuD6kehNQWza1861OQB7SePTBJNsd7cK6VtyJ6GOMF372QfwofP5HZB8JhviMCz+8CvHc3TeVxtAdXxd127/ycJplNZp/g1NwKZJXQVIw904Bjv6KpXLZ7cJkvpF+L2+D39ROl+T6a8OHacz93ize+vk26Bz/yJU34qM6fObg3+tkdaCLerc/xrUf5CODWgTTlc7ieJlEJ9+DNrUDvh2jKQxW3YpDRF+ZXAX/5N03FWH4D8EsJ3287lbDAuYYuZud0AM5IHI4pbce115Plm78H/st0oYz9dwMD82jK72cbgS130FRnOUSXda6zKu7MsnYvTcTi3nsPXsNX3NV3ulPcAPDqQZpEWe7B68/QRDzuF1MRb/rLNMkcnCMPL/fluALnbAjD/+bTxH0y1ls0mR1r6yc0EYtzXThfW87nLYLlEJ3TgRlAj840ddfQx2iiUMOfook4l/SgiTheL+5fD6OJWDEFztkYADDoAqAf44uZrl2HaeJ9IjqXU8Fqmoj14QyaiMHZn2W2f1tI3YPvm04T941eTxOFKtlGE3G6Mhxv3hpqf8UdCtNEFylwzgYB9CN7vOjlj2jifTI72ErmCSXqGM4Y45yvXGbbp6K5lSY6aXtwmUf2ODXlJZoo1EzGCSVG2lyVI1WhMO+OyqvFDTjYg3NqcOE8WCeMy/VmEpmd7M/7aSJWooMz0nHtOqAD00kjAF+7L7meJunpkmDijSwA6MZ8YEvns3OJe8n8Kpoo1PgXaCLO5efTJH1TXgLe+pSm4nAVtwzZAPAF44EtC5m/BkjXsp008T6ZHe31QzQR6/17aJKevGXA8QaaiiOzzTmwD9HLBA1BRFq2gyYKVfQ0TcTpk0uT9PRfwVfcN/bP/OIGgOz2OFSd/zpNvE9mZ3vXZirethJxGSnOD9O+eyHwyu00zUzZnEPVqUNo4g2cxcLZ8WQZbDNTZ1t0jjs4OnWcbXxxN3/NvsM6RK8cTRN/4+p4nBskavFbNBGr4Tc0SQ1XGwPA4uHAwVk0zWysBb4nAw8BTRdnx5Ppvq008Q7ONv75EKD4GppmPtYCf/TvNPEnzo5Xt4AmfKqSnFcsQrojkSbGKY0B/Xmt9cFoM7eMJswF/lg7KHDOgysAoGsnmvC54U80Ead/micZhcNAR8bZheoZDpX1EtYC97u8ZYkPERTho1/QhA/3LD4fp3HG2Imv+KY0BoCDM4GvMZzs4pYvm2hiMeGD4kzBar7vYA0DetKED+csPrun0sSZ839HE7H6raCJ/7DvwTnfO7nlnFLg/eM0Fetvk2nC53uVNBHrqt40SW7Us/7sN25gL3BFUdyT9atCGon3ZAaetZVIQAOaQzQVb1hfmvDZc4Qm4qz+EU2Syy0DNn9MUyVdWUuLaCTeT31y3rWsYeOxeTThwz0f3d1X0iSxXsutPyhS0idtiP595o7EachaecUNAOefSxM+nPPRPTmWJokFNODoKZoqbZUFAJMH01i83YeB94/R1PsCGvAP5pMvzGReapZ7PrpJg2hiTebG0+8u/kPs71kA8MSY2JBLwSM08a57NrvT8QokXh2Dcz665TfQxJobbdyeRIbooy4xx3wWevz0VGNGzkfeoUv4yTxFkXs+OicX4FPFLd4ndbG/Rwp803hzzKd8p/7CPvNPusR9AY13Rk47N/anCR/O+ejsZvFpaFbFLUsgHA7HHGwpu+HTPQFBlPePeeOtg6x2mHB2nrX1jBMqJluX1pCcjeitlwPP3UpT7xJZd+b2jztUNSdLzve8hoCmT8Anao4uJz44AVy2iqbtA2dhA8B9SU65/PAEcKmEdh+Sn1nFzSmuwJsWid2aOPGv49G/+b/5Yq9dVnsa6LmUpt6SbI8nkraNJuKVDqeJrq5RTnF36wjsvYum7VdcgQNAyyI5wygrPZcCAehHQN2zGQil0PmNUzc5z/DKZL+tpolYY75NE933KnmPljN8Ogvo242m7ZtlgXeQdviLtTCAuzfrt2WPJmSTtffmnvseAF78CU309/wyivu1O1RxW0lYyrI6Xqbq150m3sY59z1gfb5393L+9/yAXtxFgi+DlMnMR41a7sEN4SDw5RkgV8LWP1NUTQRGXCxmZCFrIzp4DU3EozO2iGgfJ47OBfLPo6liSFrggD7jRTgo7wXzqvemAwUX0DQzvPs5TcTaRIbmsvrKB/eq4ray23R+gW2BG8JBYF8N8B3GObO96K8TgJsG0LTtZO29ZRTbKNOHazL+HiCv/TJdwvfgVgbn6w075EK6xF/OO0dfz3CQp7j9xFxoqrjTN/ZSmojheA9utvfslR+W7wTmefzY8lR8VQx0cXAl1OuepElqZHVQWQVX16h/oCaDrLbzi7QK3DC3UP8HAGP/DLz0YezyTDDm29Zf7yj2Whbpc5ar4vauuGPRRbn6CWDnZzR1V2EfYMcUmqauLXvGhmI510tvy3N0avtkYNgfacqjPRS3yNfMaK827cGTsSqkASv1nwdqY3NRjK9qrL6T9QoZxS3DzilA4RM05dEeilu0Uc8CL09g3IMriuK+lD5FVxQls2QDgKYJHPwriuIZag+uKD6mClxRfEwVuKL4mCpwRfExVeCK4mOqwBXFx9SBLoriY2oPrig+lvRY9NraWqxcefYA8rNmzJiBHj3I/Dwpqqurw6pVq9DS0hLJ0n3c2troge12/9+4r939KBHtYH6eTtk9vpPHtHsMKy0tLaivrwdg///TbVMAaGpqQllZWUzmtXa1u68VEf3FkMpzsXotLIfoZ86cwYMP2k/EFgymdhaA1QtqJZXHpUfhJfu/xn2T3ccsFAqhtNR+/mgnj0efpxN2j5vKYxYXFyMnx9mZLjU1NVi7Vp+6x+lzsLufWTgcRknJ2Tmuk3DymKm0gcHucc2PaXdfs+rqamzbto3GcVJ5TLp+yf6v1WsRtwdfvXo1jh8/Hvnd3DGam5uxZMmSyDJN05L+QbOSkhKYtyXz5s3DuefqF8KmL3gqj0uVlpZi0aJFNE6Lubjp8zE3fHl5ORYsWGBaKteYMWMwcOBAGuPkyZN45BH9ukxLliyJWwc3PPDAA2htbY38bu5fLS0tWLx4cWRZW/qBbLQQRdUNtW3bNlx77bU0TiimwNevXx8p7qysrLhCycnJQTAYxOnTp7F0qX65kGPHjiEvL/k1b8vKyiLFPX78eFxySeylTAOBQGSFjYZKtxFCoZCj52THbiseDAZRX1+PiooKNDY20sVxrB7D7m841aFDB8u9c15eHoLBYJvbVCRzcdPnkp2dHdcPKioqMGfOHPPdYtDHAMS1q1Pmv7dw4UJ07NjRtDRaN+YRYbqvRXV1dUoFHvMh20cfRS8YTYvbrHPnzrjooosAILKHSKapqSlymxY3ZV5pulW0M3PmTADOnpMIubm5NFKSSKXwunbtCgCRzwIywaBBg+KK2ywrKwvTpk2L/P7qq6+altq7+uqrAaRWF3FDdAC45ZZbaBRn0qRJNLK0f3905vt58+aZlojXvXt35Ofno6amJu0tpBVN09C/f3/cfnv8BbxF/Q0uJ06ciNwuLDw7v5bLnPSd2bNn08iTHnssepWBsWPHmpZY69WrV+T222+/jZEjR5qWJjdixAjs2LEDgPMRgGWB9+sn7jIRxhMCEHnPbWfYsGHYvn07jR256667hAxJzUNbADhw4EDclnPBggXo1KlTTOaGDRs2YMOGDTSOU1RURCNXGKM/Pzh82DQJuQTmfvn0009j4sSJ5B6x2L8Hr6mpoRE7c1G/8cYbpiWpCQaDCAaDCQujvLwcmqbFfDDkVePGjaORkqGM/n3o0CHbtzCWe3CRhg8fjjfffJPG7Iwt3fbt23H99dfTxSkpLCyMG95WVVVh586dAPRPf910880344orrqAxGhoasGzZMgDACy+8gIKCAnIPJVMVFBRg//79qKioSDpKtdyDl5fbz4OraVrkXzLXXBO9IvyWLVtMSxJLd3hODRkyBEBqH0oY7NatqKgopmEffvhh01Jv6NKlS8xzrKryxiT2xjcwySxfvtz2NfCCyZMnR24fOZLaZVSHDh1KI8fGjRuH887Tr9uUrI1iCrx3797mXxP6/PPoxa5mzZplWmKtWzf9uq67du2y/UrJ/GSnTLGYmjUFo0ePjtxO1gjJHD16lEaW7IZKmSQ/Pz9y+5VXXjEtieXkYBUz46u806dPkyXxTp06BQAYMGAAWeItffv2jdyurKw0LbFm7oepfMBmZe7cuZHbifp3TIFPnTo1clvTNMsPEDZu3Ig1a6KXqzSKNxnzRsB43xoKhUz3ABobG2Oe5G233YY+ffqY7pGeZMMXJx599FE8//zzNAYQO9IpLi42LfGP3bt3Y9OmTTSGpmmRYxt69uxJllozt5GmaXj88cdNS3Xr1q2L6QcTJkwwLfUmcx8zRh10R3b48OGY9WprvzSIehxFURRFUbzk//5kFB/+Up2pAAAAAElFTkSuQmCC" style="width: 150px" />
            <h3>PT. GUNA TEGUH ABADI</h3>
        </td>
        <td class="text-center text-top">
            <h1 class="text-underline">PT. GUNA TEGUH ABADI</h1>
            <h1>REKAP MATERIAL</h1>
        </td>
        <td id="header-note" style="width: 180px;">
        </td>
    </tr>
</table>


<div class="line1"></div>

<br />

<table style="width: 100%" class="table table-items">
    <thead>
        <tr>
            <th>#</th>
            <th>Kode Material</th>
            <th>Kategori</th>
            <th>Deskripsi</th>
            <th>Brand</th>
            <th>Size / Type</th>
            <th>Tahun</th>
            <th>Kondisi</th>
            <th>Quantity</th>
            <th>Lokasi</th>
        </tr>
    </thead>
    <tbody>
        <?php if ($total === 0): ?>
        <tr>
            <td colspan="10">Data tidak ditemukan</td>
        </tr>
        <?php else: ?>
        <?php foreach($dataProvider as $idx => $item): ?>
        <tr>
            <td><?= ($idx+1) ?></td>
            <td><?= $item->kode ?></td>
            <td><?= $item->kategori ?></td>
            <td><?= $item->deskripsi ?></td>
            <td><?= $item->brand ?></td>
            <td><?= $item->tipe ?></td>
            <td><?= $item->tahun ?></td>
            <td><?= $item->condition->kondisi ?></td>
            <td><?= $item->qty ?></td>
            <td><?= $item->getNamaLokasi() ?></td>
        </tr>
        <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>


</body>
</html>