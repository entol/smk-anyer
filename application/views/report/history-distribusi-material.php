<form method="get" id="search-rpt">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="kategori-material">Kategori</label>
                <?= form_dropdown('kategori', $opKategoriMaterial, $kategori, [
                    'class' => 'form-control form-control-sm-x',
                    'id' => 'kategori-material'
                ]) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="lokasi">Lokasi</label>
                <?= form_dropdown('lokasi', $opLokasi, $lokasi, [
                    'class' => 'form-control form-control-sm-x',
                    'id' => 'lokasi'
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="kondisi">Kondisi</label>
                <?= form_dropdown('kondisi', $opKondisi, $kondisi, [
                    'class' => 'form-control form-control-sm-x',
                    'id' => 'kondisi'
                ]) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="lokasi">Kode Material</label>
                <?= form_input('material', $material, [
                    'class' => 'form-control form-control-sm-x',
                    'id' => 'material'
                ]) ?>
            </div>
        </div>
    </div>

    <?= form_hidden('search-phrase', $phrase) ?>
    <?= form_hidden('search-column', $column) ?>

    <div class="row">
        <div class="col-sm-12">
            <button type="submit" name="print" value="1" formTarget="_blank" class="btn btn-warning pull-right m-l-5"><i class="fa fa-fw fa-print"></i> Cetak</button>
            <button type="submit" name="print" value="2" formTarget="_blank" class="btn btn-success pull-right m-l-5"><i class="fa fa-fw fa-file-excel"></i> Export</button>
            <button type="submit" name="cari" value="1" class="btn btn-primary pull-right"><i class="fa fa-fw fa-check"></i> Terapkan</button>
        </div>
    </div>
</form>


<?= $xcrudContent ?>

<script type="text/javascript">
jQuery(document).on('ready', function() {
    $('select').select2();

    $(window).resize(function() {
        $('select').each(function() {
            if ($(this).data('select2')) {
                $(this).select2('destroy');
            }
            $(this).select2();
        })
    });

    $('#search-rpt').submit(function(e) {
        var phrase = $('[name="phrase"]').val();
        var column = $('[name="column"]').val();

        $('[name="search-phrase"]').val(phrase);
        $('[name="search-column"]').val(column);
    });
});
</script>