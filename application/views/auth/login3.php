<!-- begin login -->
<div class="login login-with-news-feed">
    <!-- begin news-feed -->
    <div class="news-feed">
        <div class="news-image" style="background-image: url(<?= base_url('assets/color-admin/img/background/'. $this->app->getConfig('login_background')) ?>)"></div>
        <div class="news-caption">
            <h4 class="caption-title"><?= $this->app->getConfig('app_name') ?></h4>
            <p>
                <?= $this->app->getConfig('app_description') ?>
            </p>
        </div>
    </div>
    <!-- end news-feed -->
    <!-- begin right-content -->
    <div class="right-content">
        <!-- begin login-header -->
        <div class="login-header">
            <div class="brand">
                <?= $this->app->getConfig('company_name') ?>
                <small><?= $this->app->getConfig('login_description') ?></small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end login-header -->
        <!-- begin login-content -->
        <div class="login-content">
            <form action="<?= site_url('auth/login') ?>" method="POST" class="margin-bottom-0">
                
                <?php if (!empty($message)): ?>
                <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
                <?php endif ?>
                <div class="form-group m-b-15">
                    <?php echo form_input(array_merge($identity, [
                        'class' => 'form-control input-lg form-control-lg',
                        'autocomplete' => 'off',
                    ]));?>
                </div>
                <div class="form-group m-b-15">
                    <?php echo form_input(array_merge($password, [
                        'class' => 'form-control input-lg form-control-lg',
                        'autocomplete' => 'off',
                    ]));?>
                </div>
                <div class="checkbox checkbox-css m-b-30">
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember_me_checkbox"');?>
                    <label for="remember_me_checkbox">
                        Remember Me
                    </label>
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
                <?php if ($this->app->getConfig('allow_register') == 'Y'): ?>
                <div class="m-t-20 m-b-40 p-b-40 text-inverse">
                    Not a member yet? Click <a href="<?= site_url('auth/register') ?>" class="text-success">here</a> to register.
                </div>
                <?php endif ?>
                <hr />
                <p class="text-center text-grey-darker">
                    &copy; <?= $this->app->getConfig('app_name'); ?> All Right Reserved 2020
                </p>
            </form>
        </div>
        <!-- end login-content -->
    </div>
    <!-- end right-container -->
</div>
<!-- end login -->