<!-- begin login -->
<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <?= $this->app->getConfig('app_name'); ?>
            <small><?= $this->app->getConfig('login_description') ?></small>
        </div>
        <div class="icon">
            <i class="fa fa-lock"></i>
        </div>
    </div>
    <!-- end brand -->
    <!-- begin login-content -->
    <div class="login-content">
        <form action="<?= site_url('auth/login') ?>" method="POST" class="margin-bottom-0">
            <?php if (!empty($message)): ?>
            <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
            <?php endif ?>
            
            <div class="form-group m-b-20">
                <?php echo form_input(array_merge($identity, [
                    'class' => 'form-control input-lg form-control-lg inverse-mode',
                    'autocomplete' => 'off',
                ]));?>
            </div>
            <div class="form-group m-b-20">
                <?php echo form_input(array_merge($password, [
                    'class' => 'form-control input-lg form-control-lg inverse-mode',
                    'autocomplete' => 'off',
                ]));?>
            </div>
            <div class="checkbox checkbox-css m-b-20">
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember_checkbox"');?>
                <label for="remember_checkbox">
                    Remember Me
                </label>
            </div>
            <div class="login-buttons">
                <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
            </div>
        </form>
    </div>
    <!-- end login-content -->
    
    <div class="login-footer">
        <p class="text-center text-grey-darker">
            &copy; <?= $this->app->getConfig('app_name'); ?> All Right Reserved 2020
        </p>
    </div>
</div>
<!-- end login -->