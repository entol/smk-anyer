<form method="POST">
    <table class="table">
        <tr>
            <td style="width: 47%">
                <h5 class="text-center">Tidak Terdaftar</h5>
                <label>Filter :</label>
                <input type="text" class="form-control" id="filter_tidak_terdaftar">
                <hr />
                <select id="tidak_terdaftar" class="form-control" multiple="multiple" style="height: 400px">
                
                </select>
            </td>
            <td style="vertical-align: middle; text-align: center">
                <div class="m-b-5 m-t-5">
                    <button type="button" id="to-right-all" class="btn btn-primary">
                        <i class="fa fa-fw fa-angle-double-right"></i>
                    </button>
                </div>
                <div class="m-b-5">
                    <button type="button" id="to-right" class="btn btn-success">
                        <i class="fa fa-fw fa-angle-right"></i>
                    </button>
                </div>
                <div class="m-b-5">
                    <button type="button" id="to-left" class="btn btn-warning">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </button>
                </div>
                <div class="m-b-5">
                    <button type="button" id="to-left-all" class="btn btn-danger">
                        <i class="fa fa-fw fa-angle-double-left"></i>
                    </button>
                </div>
            </td>
            <td style="width: 47%">
                <h5 class="text-center">Terdaftar</h5>
                <label>Filter :</label>
                <input type="text" class="form-control" id="filter_terdaftar"><hr />
                <select id="terdaftar" class="form-control" multiple="multiple" style="height: 400px">
                
                </select>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
jQuery(document).ready(function() {
    function getTidakTerdaftar() {
        var filter = $('#filter_tidak_terdaftar').val();
        $('#tidak_terdaftar').html('');
        $.post('<?= site_url('auth/router-group-tidak-terdaftar') ?>', {
            id: <?= $id ?>,
            filter: filter
        }, function(r) {
            if (r.status === 1) {
                var html = '';
                $.each(r.data, function(module, controllers) {
                    if (module != '*') {
                        html += '<optgroup label="'+ module +'">';
                    }
                    $.each(controllers, function(controller, methods) {
                        $.each(methods, function(i, method) {
                            if (module == '*') {
                                html += '<option value="'+ module +'/'+ controller +'/'+ method +'">'+ controller +'/'+ method +'</option>';
                            } else {
                                html += '<option value="'+ module +'/'+ controller +'/'+ method +'">'+ module +'/'+ controller +'/'+ method +'</option>';
                            }
                        });
                    });
                    if (module != '*') {
                        html += '</optgroup>';
                    }
                });
                
                $('#tidak_terdaftar').html(html);
            } else {
                $.alert(r.message);
            }
        }).fail(function() {
            $.alert('Koneksi tidak stabil');
        });
    }
    
    function getTerdaftar() {
        var filter = $('#filter_terdaftar').val();
        $('#terdaftar').html('');
        $.post('<?= site_url('auth/router-group-terdaftar') ?>', {
            id: <?= $id ?>,
            filter: filter
        }, function(r) {
            if (r.status === 1) {
                var html = '';
                $.each(r.data, function(module, controllers) {
                    if (module != '*') {
                        html += '<optgroup label="'+ module +'">';
                    }
                    $.each(controllers, function(controller, methods) {
                        $.each(methods, function(i, method) {
                            if (module == '*') {
                                html += '<option value="'+ module +'/'+ controller +'/'+ method +'">'+ controller +'/'+ method +'</option>';
                            } else {
                                html += '<option value="'+ module +'/'+ controller +'/'+ method +'">'+ module +'/'+ controller +'/'+ method +'</option>';
                            }
                        });
                    });
                    if (module != '*') {
                        html += '</optgroup>';
                    }
                });
                
                $('#terdaftar').html(html);
            } else {
                $.alert(r.message);
            }
        }).fail(function() {
            $.alert('Koneksi tidak stabil');
        });
    }
    
    $(document).on('change', '#filter_tidak_terdaftar', function() {
        getTidakTerdaftar();
    });
    
    $(document).on('change', '#filter_terdaftar', function() {
        getTerdaftar();
    });
    
    getTidakTerdaftar();
    getTerdaftar();
    
    $(document).on('click', '#to-right', function() {
        var data = $('#tidak_terdaftar').val();
        if (data.length) {
            $('#tidak_terdaftar').attr('disabled', 'disabled');
            $('#terdaftar').attr('disabled', 'disabled');
            $.post('<?= site_url('auth/router-group-move') ?>', {
                to: 'right',
                data: data,
                id: <?= $id ?>
            }, function(r) {
                getTidakTerdaftar();
                getTerdaftar();
                $('#tidak_terdaftar').removeAttr('disabled');
                $('#terdaftar').removeAttr('disabled');
            });
        } else {
            $.alert('Silahkan pilih terlebih dahulu');
        }
    });
    
    $(document).on('click', '#to-right-all', function() {
        $('#tidak_terdaftar option').prop('selected', true);
        $('#to-right').get(0).click();
    });
    
    $(document).on('click', '#to-left', function() {
        var data = $('#terdaftar').val();
        if (data.length) {
            $('#tidak_terdaftar').attr('disabled', 'disabled');
            $('#terdaftar').attr('disabled', 'disabled');
            $.post('<?= site_url('auth/router-group-move') ?>', {
                to: 'left',
                data: data,
                id: <?= $id ?>
            }, function(r) {
                getTidakTerdaftar();
                getTerdaftar();
                $('#tidak_terdaftar').removeAttr('disabled');
                $('#terdaftar').removeAttr('disabled');
            });
        } else {
            $.alert('Silahkan pilih terlebih dahulu');
        }
    });
    
    $(document).on('click', '#to-left-all', function() {
        $('#tidak_terdaftar').attr('disabled', 'disabled');
        $('#terdaftar').attr('disabled', 'disabled');
        $.post('<?= site_url('auth/router-group-move') ?>', {
            to: 'left-all',
            data: [],
            id: <?= $id ?>
        }, function(r) {
            getTidakTerdaftar();
            getTerdaftar();
            $('#tidak_terdaftar').removeAttr('disabled');
            $('#terdaftar').removeAttr('disabled');
        });
    });
});
</script>