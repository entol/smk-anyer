<div class="login-cover">
    <div class="login-cover-image" style="background-image: url(<?= base_url('assets/color-admin/img/background/'. $this->app->config('login_background')) ?>)" data-id="login-cover-image"></div>
    <div class="login-cover-bg"></div>
</div>

<!-- begin login -->
<div class="login login-v2" data-pageload-addclass="animated fadeIn">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <?= $this->app->getConfig('app_name') ?>
            <small><?= $this->app->getConfig('login_description') ?></small>
        </div>
        <div class="icon">
            <i class="fa fa-lock"></i>
        </div>
    </div>
    <!-- end brand -->
    <!-- begin login-content -->
    <div class="login-content">
        <form action="<?= site_url('auth/login') ?>" method="POST" class="margin-bottom-0">
            
            <?php if (!empty($message)): ?>
            <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
            <?php endif ?>
            
            <div class="form-group m-b-20">
                <?php echo form_input(array_merge($identity, [
                    'class' => 'form-control input-lg form-control-lg',
                ]));?>
            </div>
            <div class="form-group m-b-20">
                <?php echo form_input(array_merge($password, [
                    'class' => 'form-control input-lg form-control-lg',
                ]));?>
            </div>
            <div class="checkbox checkbox-css m-b-20">
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember_me_checkbox"');?>
                <label for="remember_me_checkbox">
                    Remember Me
                </label>
            </div>
            <div class="login-buttons">
                <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
            </div>
            <?php if ($this->app->getConfig('allow_register') == 'Y'): ?>
            <div class="m-t-20">
                Not a member yet? Click <a href="<?= site_url('auth/register') ?>">here</a> to register.
            </div>
            <?php endif ?>
        </form>
    </div>
    <!-- end login-content -->
    
    <div class="login-footer">
        <p class="text-center text-grey-darker">
            &copy; <?= $this->app->getConfig('app_name'); ?> All Right Reserved 2020
        </p>
    </div>
</div>
<!-- end login -->

<?php if ($this->app->config('login_background_selection') == 'Y'): ?>
<ul class="login-bg-list clearfix">
    <li class="active"><a href="javascript:;" data-click="change-bg" data-img="<?= base_url('assets/color-admin/img/background/'. $this->app->config('login_background')) ?>" style="background-image: url(<?= base_url('assets/color-admin/img/background/'. $this->app->config('login_background')) ?>)"></a></li>
    <?php
    foreach($this->app->backgrounds() as $id => $bg):
        if ($id != $this->app->config('login_background')):
    ?>
    <li><a href="javascript:;" data-click="change-bg" data-img="<?= base_url('assets/color-admin/img/background/'. $bg) ?>" style="background-image: url(<?= base_url('assets/color-admin/img/background/'. $bg) ?>)"></a></li>
    <?php
        endif;
    endforeach;
    ?>
</ul>
<?php endif ?>