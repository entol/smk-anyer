<?php
/*
 * @link http://www.didanurwanda.com/
 * @copyright Copyright (c) 2019 Dida Nurwanda
 * @license http://www.didanurwanda.com/license/
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title>
		<?= $title ?>
	</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="<?= $appDescription ?>" name="description" />
	<meta content="<?= $appKeywords ?>" name="keywords" />
	<meta content="Dida Nurwanda" name="author" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic"
	 rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="<?= base_url('assets/color-admin/plugins/jquery-ui/jquery-ui.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/color-admin/plugins/bootstrap/4.0.0/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/color-admin/plugins/font-awesome/5.14/css/all.min.css') ?>" rel="stylesheet" />
	<!-- <link href="<?= base_url('assets/color-admin/plugins/font-awesome/4.0/css/font-awesome.min.css') ?>" rel="stylesheet" /> -->
	<!-- <link href="<?= base_url('assets/color-admin/plugins/animate/animate.min.css') ?>" rel="stylesheet" /> -->
	<link href="<?= base_url('assets/color-admin/css/material/style.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/color-admin/css/material/style-responsive.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/color-admin/css/material/theme/default.css') ?>" rel="stylesheet" id="theme" />
	<link href="<?= base_url('assets/jquery-confirm/jquery-confirm.min.css') ?>" rel="stylesheet" >
    <link href="<?= base_url('assets/color-admin/plugins/jstree/dist/themes/default/style.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/select2/dist/css/select2.min.css') ?>" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<?php 
	// grocery crud
	if (isset($css_files) && is_array($css_files)):
		foreach($css_files as $file): ?>
	<link href="<?php echo $file; ?>" type="text/css" rel="stylesheet"/>
		<?php
		endforeach;
	endif;
	?>

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?= base_url('assets/color-admin/plugins/pace/pace.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/plugins/jquery/jquery-1.9.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/plugins/jquery/jquery-migrate-1.1.0.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
	<script src="<?= base_url('assets/jquery-validation/jquery.validate.min.js') ?>"></script>
	<script src="<?= base_url('assets/jquery-validation/additional-methods.min.js') ?>"></script>
	<script src="<?= base_url('assets/jquery-validation/localization/messages_id.min.js') ?>"></script>

	<?php 
	// grocery crud
	if (isset($js_files) && is_array($css_files)):
		foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
		<?php
		endforeach;
	endif;
	?>
	<!-- ================== END BASE JS ================== -->

	<style type="text/css">
		.sidebar .sub-menu>li>a {
			padding: 5px 20px 5px 54px;
		}
		.sidebar .nav > li.nav-profile .cover.with-shadow::before {
			background: transparent!important; //rgba(23,35,41,.9);
		}

		/* common */
		.xcrud .xcrud-list .xcrud-row:hover td.xcrud-actions-fixed {
			position: sticky!important;
		}

		.text-left {
			text-align: left;
		}

		.text-center {
			text-align: center;
		}

		.text-right {
			text-align: right;
		}

		.text-justify {
			text-align: justify;
		}
	</style>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery.validator.setDefaults({
                errorElement: "em",
                errorPlacement: function ( error, element ) {
                    error.addClass("help-block");
                    // if (element.prop("type") === "checkbox") {
                        // error.insertAfter(element.parent("label"));
                    // } else {
                        element.parents("*[class*='col-']").append(error);
                    // }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".row").addClass("has-warning").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".row").addClass("has-success").removeClass("has-warning");
                }
            });
        });
    </script>
</head>

<body class="<?= isset($bodyClass) ? $bodyClass : '' ?>">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show">
		<div class="material-loader">
			<svg class="circular" viewBox="25 25 50 50">
				<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
				</circle>
			</svg>
			<div class="message">Loading...</div>
		</div>
	</div>
	<!-- end #page-loader -->

	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed page-with-wide-sidebar">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed navbar-toggle-left" data-click="sidebar-minify">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?= site_url() ?>" class="navbar-brand">
					<?= $appName ?>
				</a>
			</div>
			<!-- end navbar-header -->

			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<span class="d-none d-md-inline"><?= $this->ion_auth->user_data('name') ?></span>
						<?php if (!empty($this->ion_auth->user_data('avatar'))): ?>
						<img src="<?= base_url('uploads/users/'. $this->ion_auth->user_data('avatar')) ?>" alt="" />
						<?php else: ?>
						<img src="<?= base_url('assets/images/employee.png') ?>" alt="" />
						<?php endif ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?= site_url('user/index') ?>" class="dropdown-item">Edit Profile</a>
						<a href="<?= site_url('auth/logout') ?>" class="dropdown-item">Logout</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->

		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar" data-disable-slide-animation="true">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<?php if (!empty($this->ion_auth->user_data('avatar'))): ?>
								<img src="<?= base_url('uploads/users/'. $this->ion_auth->user_data('avatar')) ?>" alt="" />
								<?php else: ?>
								<img src="<?= base_url('assets/images/employee.png') ?>" alt="" />
								<?php endif ?>
							</div>
							<div class="info">
								<!-- <b class="caret pull-right"></b> -->
								<?= $this->ion_auth->user_data('name') ?>
								<small>Online</small>
							</div>
						</a>
					</li>
					<?php /* lock
					<li>
						<ul class="nav nav-profile">
							<li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
							<li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
						</ul>
					</li>
					*/ ?>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<?= $this->super_menu->render((isset($activeMenu) ? $activeMenu : null)) ?>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>

		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->

		<?= $content ?>

        <?php /*
		<!-- begin theme-panel -->
		<div class="theme-panel">
			<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn">
				<i class="fa fa-cog"></i>
			</a>
			<div class="theme-panel-content">
				<h5 class="m-t-0">Color Theme</h5>
				<ul class="theme-list clearfix">
					<li class="active">
						<a href="javascript:;" class="bg-cyan" data-theme="default" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/default.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default/Cyan">
						</a>
					</li>
					<li>
						<a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/blue.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">
						</a>
					</li>
					<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/purple.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">
						</a></li>
					<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/orange.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">
						</a>
					</li>
					<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/red.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">
						</a>
					</li>
					<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="<?= base_url('assets/color-admin/css/material/theme/black.css') ?>"
						 data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">
						</a>
					</li>
				</ul>
				<div class="divider"></div>
				<div class="row m-t-10">
					<div class="col-md-5 control-label double-line">Header Styling</div>
					<div class="col-md-7">
						<select name="header-styling" class="form-control form-control-sm">
							<option value="1">default</option>
							<option value="2">inverse</option>
						</select>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-5 control-label">Header</div>
					<div class="col-md-7">
						<select name="header-fixed" class="form-control form-control-sm">
							<option value="1">fixed</option>
							<option value="2">default</option>
						</select>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-5 control-label double-line">Sidebar Styling</div>
					<div class="col-md-7">
						<select name="sidebar-styling" class="form-control form-control-sm">
							<option value="1">default</option>
							<option value="2">grid</option>
						</select>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-5 control-label">Sidebar</div>
					<div class="col-md-7">
						<select name="sidebar-fixed" class="form-control form-control-sm">
							<option value="1">fixed</option>
							<option value="2">default</option>
						</select>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-5 control-label double-line">Sidebar Gradient</div>
					<div class="col-md-7">
						<select name="content-gradient" class="form-control form-control-sm">
							<option value="1">disabled</option>
							<option value="2">enabled</option>
						</select>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-12">
						<a href="javascript:;" class="btn btn-inverse btn-block btn-sm" data-click="reset-local-storage">
							Reset Local Storage
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- end theme-panel -->
        */ ?>

		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
			<i class="fa fa-angle-up"></i>
		</a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?= base_url('assets/color-admin/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') ?>"></script>
	<!--[if lt IE 9]>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/html5shiv.js') ?>"></script>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/respond.min.js') ?>"></script>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/excanvas.min.js') ?>"></script>
      <![endif]-->
	<script src="<?= base_url('assets/color-admin/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/plugins/js-cookie/js.cookie.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/js/theme/material.min.js') ?>"></script>
	<script src="<?= base_url('assets/jquery-confirm/jquery-confirm.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/plugins/jstree/dist/jstree.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/plugins/select2/dist/js/select2.min.js') ?>"></script>
	<script src="<?= base_url('assets/color-admin/js/apps.min.js') ?>"></script>
	<script src="<?= base_url('assets/app.js') ?>"></script>
	<!-- ================== END BASE JS ================== -->
	</script>
</body>

</html>
