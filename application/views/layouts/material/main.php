<?php $this->layout->beginContent() ?>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?= $this->breadcrumbs->show(); ?>
	<!-- end breadcrumb -->
	
	<!-- begin page-header -->
	<h1 class="page-header"><?= !empty($pageTitle) ? $pageTitle : '&nbsp;' ?> <small><?= !empty($pageSubTitle) ? $pageSubTitle : '&nbsp;' ?></small></h1>
	<!-- end page-header -->
	
	<!-- Page Content Here -->  
	<?= $content ?>
</div>
<!-- end #content -->
<?php $this->layout->endContent() ?>