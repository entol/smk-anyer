<!-- Default panel -->
<div class="panel panel-inverse">
    <div class="panel-heading with-border">
        <h3 class="panel-title"><?= $boxTitle ?></h3>

        <?php if ($boxPeriode): ?>
        <div class="pull-right panel-tools">
            <form class="form-inline">
                <div class="form-group">
                    <label for="month">Bulan</label>
                    <?= form_dropdown(array(
                        'options' => $this->main_model->getBulan()['dropdown'],
                        'selected' => $bulan,
                        'class' => 'form-control',
                        'name' => 'bulan'
                    )) ?>
                </div>
                <div class="form-group">
                    <label for="month">Tahun</label>
                    <?php
                    $tahun = [];
                    for ($i = 2016; $i <= date('Y') + 2; $i++) {
                        $tahun[$i] = $i;
                    }
                    ?>
                    <?= form_dropdown(array(
                        'options' => $this->main_model->getPeriode(true)['dropdown'],
                        'selected' => $periode,
                        'class' => 'form-control',
                        'name' => 'periode'
                    )) ?>
                </div>
                <button class="btn btn-flat btn-primary"><i class="fa fa-fw fa-search"></i></button>
            </form>
        </div>
        <?php endif; ?>
    </div>
    <div class="panel-body">
        <?php foreach(['info', 'success', 'danger'] as $item): ?>
        <?php if (($flashdata = $this->session->flashdata($item))): ?>
        <div class="alert alert-<?= $item ?>">
            <?= $flashdata ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif ?>
        <?php endforeach ?>
    
        
        <?= $content ?>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->