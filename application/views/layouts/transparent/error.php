<?php
/*
 * @link http://www.didanurwanda.com/
 * @copyright Copyright (c) 2019 Dida Nurwanda
 * @license http://www.didanurwanda.com/license/
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>
        <?= $title ?>
    </title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="<?= $appDescription ?>" name="description" />
    <meta content="<?= $appKeywords ?>" name="keywords" />
    <meta content="Dida Nurwanda" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic"
     rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/jquery-ui/jquery-ui.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/bootstrap/4.0.0/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/font-awesome/5.0/css/fontawesome-all.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/font-awesome/4.0/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/plugins/animate/animate.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/css/transparent/style.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/css/transparent/style-responsive.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/color-admin/css/transparent/theme/default.css') ?>" rel="stylesheet" id="theme" />
    <link href="<?= base_url('assets/jquery-confirm/jquery-confirm.min.css') ?>" rel="stylesheet" >
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?= base_url('assets/color-admin/plugins/pace/pace.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/plugins/jquery/jquery-1.9.1.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/plugins/jquery/jquery-migrate-1.1.0.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
    <!-- ================== END BASE JS ================== -->

    <style type="text/css">
        .error-content {
            padding-bottom: 25px;
        }
        .error-code, .error-content {
            position: relative;
        }

        /* common */
        .text-left {
            text-align: left;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-justify {
            text-align: justify;
        }
    </style>
</head>

<body class="pace-top">
    <!-- begin page-cover -->
    <div class="page-cover"></div>
    <!-- end page-cover -->

    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show">
        <div class="transparent-loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                </circle>
            </svg>
            <div class="message">Loading...</div>
        </div>
    </div>
    <!-- end #page-loader -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin error -->
        <div class="error">
            <div class="error-code m-b-10"><?= $statusCode ?></div>
            <div class="error-content">
                <div class="error-message"><?= $heading ?></div>
                <div class="error-desc m-b-30">
                    <?= $message ?>
                </div>
                <div>
                    <a href="<?= site_url() ?>" class="btn btn-success p-l-20 p-r-20">Go Home</a>
                </div>
            </div>
        </div>
        <!-- end error -->

        <!-- begin theme-panel -->
        <div class="theme-panel">
            <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn">
                <i class="fa fa-cog"></i>
            </a>
            <div class="theme-panel-content">
                <h5 class="m-t-0">Color Theme</h5>
                <ul class="theme-list clearfix">
                    <li class="active">
                        <a href="javascript:;" class="bg-cyan" data-theme="default" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/default.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default/Cyan">
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/blue.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">
                        </a>
                    </li>
                    <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/purple.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">
                        </a></li>
                    <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/orange.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">
                        </a>
                    </li>
                    <li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/red.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">
                        </a>
                    </li>
                    <li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="<?= base_url('assets/color-admin/css/transparent/theme/black.css') ?>"
                         data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">
                        </a>
                    </li>
                </ul>
                <div class="divider"></div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Header Styling</div>
                    <div class="col-md-7">
                        <select name="header-styling" class="form-control form-control-sm">
                            <option value="1">default</option>
                            <option value="2">inverse</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label">Header</div>
                    <div class="col-md-7">
                        <select name="header-fixed" class="form-control form-control-sm">
                            <option value="1">fixed</option>
                            <option value="2">default</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Sidebar Styling</div>
                    <div class="col-md-7">
                        <select name="sidebar-styling" class="form-control form-control-sm">
                            <option value="1">default</option>
                            <option value="2">grid</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label">Sidebar</div>
                    <div class="col-md-7">
                        <select name="sidebar-fixed" class="form-control form-control-sm">
                            <option value="1">fixed</option>
                            <option value="2">default</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Sidebar Gradient</div>
                    <div class="col-md-7">
                        <select name="content-gradient" class="form-control form-control-sm">
                            <option value="1">disabled</option>
                            <option value="2">enabled</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-inverse btn-block btn-sm" data-click="reset-local-storage">
                            Reset Local Storage
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end theme-panel -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?= base_url('assets/color-admin/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') ?>"></script>
    <!--[if lt IE 9]>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/html5shiv.js') ?>"></script>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/respond.min.js') ?>"></script>
        <script src="<?= base_url('assets/color-admin/crossbrowserjs/excanvas.min.js') ?>"></script>
      <![endif]-->
    <script src="<?= base_url('assets/color-admin/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/plugins/js-cookie/js.cookie.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/js/theme/transparent.min.js') ?>"></script>
    <script src="<?= base_url('assets/jquery-confirm/jquery-confirm.min.js') ?>"></script>
    <script src="<?= base_url('assets/color-admin/js/apps.min.js') ?>"></script>
    <!-- ================== END BASE JS ================== -->

    <script>
        $(document).ready(function () {
            App.init();
        });

    </script>
</body>

</html>
