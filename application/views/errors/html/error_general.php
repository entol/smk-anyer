<?php

$CI =& get_instance();
if( ! isset($CI))
{
    $CI = new CI_Controller();
}
$CI->load->helper('url');

echo $CI->layout->renderString('', [
	'layout' => 'layouts/'. $CI->app->getConfig('theme') .'/error.php',
	'statusCode' => $status_code,
	'heading' => $heading,
	'message' => $message,
	'title' => $status_code .' '. $heading
], true);