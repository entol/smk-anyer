<?= $xcrudContent ?>


<style type="text/css">
.xcrud-nested-container.xcrud-container {
    margin-top: 0;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    function viewButton() {
        // $('[data-task="view"].btn-info').remove();
    }
    viewButton();
    
    $(document).on("xcrudafterrequest", function(event,container) {
        viewButton();
    });
    
	$(document).on('click', '.btn-proses', function(e) {
		e.preventDefault();
		var $that = $(this);
        var data = $that.data();
		
		$.confirm({
			title: 'Proses',
			content: 'Apakah Anda yakin akan memproses kehilangan ini?',
			buttons: {
				yes: {
					text: 'Ya (Hilang)',
					btnClass: 'btn-primary',
					action: function(){
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memproses</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
						$.post('<?= site_url('kehilangan/proses/AP/') ?>'+ data.id, function(x) {
							loader.close();
                            if (x.status == 1) {
								Xcrud.reload(".xcrud-container:first");
							} else {
                                $.alert(x.message, 'Error');
							}
						}).fail(function() {
                            loader.close();
							$.alert('Permintaan proses gagal', 'Error');
						});
					}
				},
				no: {
					text: 'Tidak',
					btnClass: 'btn-default',
					action: function(){
						
					}
				},
			}
		});
	});
    
	$(document).on('click', '.btn-batal', function(e) {
		e.preventDefault();
		var $that = $(this);
        var data = $that.data();
		
		$.confirm({
			title: 'Batal',
			content: 'Apakah Anda yakin laporan ini dibatalkan?',
			buttons: {
				yes: {
					text: 'Ya (Batal)',
					btnClass: 'btn-primary',
					action: function(){
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memproses</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
						$.post('<?= site_url('kehilangan/proses/RT/') ?>'+ data.id, function(x) {
							loader.close();
                            if (x.status == 1) {
								Xcrud.reload(".xcrud-container:first");
							} else {
                                $.alert(x.message, 'Error');
							}
						}).fail(function() {
                            loader.close();
							$.alert('Permintaan proses gagal', 'Error');
						});
					}
				},
				no: {
					text: 'Tidak',
					btnClass: 'btn-default',
					action: function(){
						
					}
				},
			}
		});
	});
});
</script>