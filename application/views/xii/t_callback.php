<?= "<?" ?>php

function <?= $idUnderscore ?>_before_insert($postdata, $xcrud) {
<?php if (count($rules['add'])): ?>
    $CI =& get_instance();
<?php foreach($rules['add'] as $col => $s): ?>
    <?= $s ?>
<?php endforeach ?>
<?php else: ?>
    // your code
<?php endif ?>
}

function <?= $idUnderscore ?>_after_insert($postdata, $primary, $xcrud) {
    // your code
}

function <?= $idUnderscore ?>_before_update($postdata, $primary, $xcrud) {
<?php if (count($rules['edit'])): ?>
    $CI =& get_instance();
<?php foreach($rules['edit'] as $col => $s): ?>
    <?= $s ?>
<?php endforeach ?>
<?php else: ?>
    // your code
<?php endif ?>
}

function <?= $idUnderscore ?>_after_update($postdata, $primary, $xcrud) {
    // your code
}

function <?= $idUnderscore ?>_before_remove($primary, $xcrud) {
    // your code
}

function <?= $idUnderscore ?>_after_remove($primary, $xcrud) {
    // your code
}