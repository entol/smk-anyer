<style type="text/css">
.xii-columns.table thead tr th {
    text-align: center;
    background: #eeeeee;
}
.xii-common.table th {
    text-align: right;
}
.xii-common.table td {
    text-align: center;
}
.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content {
    overflow: hidden
}

.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content select.form-control.input-xs,
.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content select.form-control:not([size]):not([multiple]).input-xs {
    height: 20px;
    padding: 0;
    min-width: 150px;
}
.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content input.form-control.input-xs {
    min-width: 150px;
}

</style>

<form method="POST" id="x-f" data-table="<?= $table ?>" data-type="<?= $type ?>">
<div class="table-responsive">
<table class="xii-columns table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th style="min-width: 200px">Column</th>
            <th>Label</th>
            <th>List</th>
            <th>Add</th>
            <th>Edit</th>
            <th>View</th>
            <th>Tab Name</th>
            <th>Required</th>
            <th>Column Cut</th>
            <th>Type</th>
            <th>Options</th>
            <th>Rel Table</th>
            <th>Rel Field</th>
            <th>Rel Display</th>
            <th>Rel Separator</th>
            <th>Rule Value</th>
            <th>Rule On</th>
        </tr>
    </thead>
    
    <tbody>
        <?php foreach($models as $model): $col = $model['column_name']; ?>
        <tr>
            <td><?= $col ?></td>
            <td><input type="text" class="form-control input-xs" name="__columns__[<?= $col ?>][label]" value="<?= $postdata['__columns__'][$col]['label'] ?>" /></td>
            <td class="text-center">
                <input type="hidden" name="__columns__[<?= $col ?>][list]" value="N" />
                <input type="checkbox" name="__columns__[<?= $col ?>][list]" value="Y" <?= $postdata['__columns__'][$col]['list'] ? 'checked="checked"' : '' ?> />
            </td>
            <td class="text-center">
                <input type="hidden" name="__columns__[<?= $col ?>][add]" value="N" />
                <input type="checkbox" name="__columns__[<?= $col ?>][add]" value="Y" <?= $postdata['__columns__'][$col]['add'] ? 'checked="checked"' : '' ?>  />
            </td>
            <td class="text-center">
                <input type="hidden" name="__columns__[<?= $col ?>][edit]" value="N" />
                <input type="checkbox" name="__columns__[<?= $col ?>][edit]" value="Y" <?= $postdata['__columns__'][$col]['edit'] ? 'checked="checked"' : '' ?>  />
            </td>
            <td class="text-center">
                <input type="hidden" name="__columns__[<?= $col ?>][view]" value="N" />
                <input type="checkbox" name="__columns__[<?= $col ?>][view]" value="Y" <?= $postdata['__columns__'][$col]['view'] ? 'checked="checked"' : '' ?>  />
            </td>
            <td><input type="text" class="form-control input-xs" name="__columns__[<?= $col ?>][tab]" /></td>
            <td class="text-center">
                <input type="hidden" name="__columns__[<?= $col ?>][required]" value="N" />
                <input type="checkbox" name="__columns__[<?= $col ?>][required]" value="Y" <?= $postdata['__columns__'][$col]['required'] ? 'checked="checked"' : '' ?>  />
            </td>
            <td>
                <?php if (strpos($model['data_type'], 'text') !== false): ?>
                <input type="number" class="form-control input-xs" name="__columns__[<?= $col ?>][cut]" value="<?= $postdata['__columns__'][$col]['cut'] ?>" />
                <?php endif ?>
            </td>
            <td>
                <select class="form-control input-xs xii-type" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][type]">
                    <option value="">- default -</option>
                    <option value="text">Text</option>
                    <option value="select">Select</option>
                    <option value="radio">Radio</option>
                    <option value="checkboxes">CheckBox</option>
                    <option value="price">Price</option>
                    <option value="datetime">DateTime</option>
                    <option value="date">Date</option>
                    <option value="time">Time</option>
                    <option value="int">Int</option>
                    <option value="float">Float</option>
                    <option value="bool">Bool</option>
                    <option value="password">Password</option>
                    <option value="point">Point</option>
                    <option value="file">File</option>
                    <option value="image">Image</option>
                    <option value="relation">Relation</option>
                </select>
            </td>
            <td><input type="text" class="form-control input-xs xii-options" style="display: none" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][options]" /></td>
            <td>
                <select class="form-control input-xs xii-rel-table" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][rel_table]" style="display: none">
                    <?php foreach($tables as $x): ?>
                    <option value="<?= $x ?>"><?= $x ?></option>
                    <?php endforeach ?>
                </select>
            </td>
            <td>
                <select class="form-control input-xs xii-rel-field" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][rel_field]" style="display: none">
                </select>
            </td>
            <td>
                <select class="form-control input-xs xii-rel-display" style="height: 60px; display: none" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][rel_display][]" multiple="multiple">
                </select>
            </td>
            <td><input type="text" style="display: none" data-col="<?= $col ?>" class="form-control input-xs xii-rel-sep" name="__columns__[<?= $col ?>][rel_sep]"  value=" - "/></td>
            
            <td>
                <select class="form-control input-xs xii-type" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][rule_value]">
                    <option value=""></option>
                    <option value="date"        <?= $postdata['__columns__'][$col]['rule_value'] == 'date' ? 'selected="selected"' : '' ?>>date</option>
                    <option value="datetime"    <?= $postdata['__columns__'][$col]['rule_value'] == 'datetime' ? 'selected="selected"' : '' ?>>datetime</option>
                    <option value="time"        <?= $postdata['__columns__'][$col]['rule_value'] == 'time' ? 'selected="selected"' : '' ?>>time</option>
                    <option value="year"        <?= $postdata['__columns__'][$col]['rule_value'] == 'year' ? 'selected="selected"' : '' ?>>year</option>
                    <option value="timestamp"   <?= $postdata['__columns__'][$col]['rule_value'] == 'timestamp' ? 'selected="selected"' : '' ?>>timestamp</option>
                    <option value="auth"        <?= $postdata['__columns__'][$col]['rule_value'] == 'auth' ? 'selected="selected"' : '' ?>>auth</option>
                    <option value="ip"          <?= $postdata['__columns__'][$col]['rule_value'] == 'ip' ? 'selected="selected"' : '' ?>>ip address</option>
                    <option value="agent"       <?= $postdata['__columns__'][$col]['rule_value'] == 'agent' ? 'selected="selected"' : '' ?>>user agent</option>
                </select>
            </td>
            
            <td>
                <select class="form-control input-xs xii-type" data-col="<?= $col ?>" name="__columns__[<?= $col ?>][rule_on]">
                    <option value=""></option>
                    <option value="add_edit"    <?= $postdata['__columns__'][$col]['rule_on'] == 'add_edit' ? 'selected="selected"' : '' ?>>add & edit</option>
                    <option value="add"         <?= $postdata['__columns__'][$col]['rule_on'] == 'add' ? 'selected="selected"'      : '' ?>>add</option>
                    <option value="edit"        <?= $postdata['__columns__'][$col]['rule_on'] == 'edit' ? 'selected="selected"'     : '' ?>>edit</option>
                </select>
            </td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>
</div>

<h3>Other</h3>

<table class="xii-common table table-condensed">
    <tr>
        <th style="width: 30%">Module Name</th>
        <td style="width: 30%">
            <input type="text" name="module" value="" class="form-control input-xs" />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Controller Name</th>
        <td>
            <input type="text" name="controller" value="<?= ucfirst(camelize($table)) ?>Controller" class="form-control input-xs" />
        </td>
        <td></td>
    </tr>
    
    <tr>
        <th>Model Name</th>
        <td>
            <input type="text" name="model" value="<?= ucfirst(camelize($table)) ?>" class="form-control input-xs" />
        </td>
        <td></td>
    </tr>
    
    <tr>
        <th>Title</th>
        <td>
            <input type="text" name="title" value="<?= ucfirst(humanize($table)) ?>" class="form-control input-xs" />
        </td>
        <td></td>
    </tr>
    
    <tr>
        <th>List</th>
        <td>
            <input type="hidden" name="list" value="N" />
            <input type="checkbox" name="list" value="Y" <?= $postdata['list'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Add</th>
        <td>
            <input type="hidden" name="add" value="N" />
            <input type="checkbox" name="add" value="Y" <?= $postdata['add'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Edit</th>
        <td>
            <input type="hidden" name="edit" value="N" />
            <input type="checkbox" name="edit" value="Y" <?= $postdata['edit'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Remove</th>
        <td>
            <input type="hidden" name="remove" value="N" />
            <input type="checkbox" name="remove" value="Y" <?= $postdata['remove'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>View</th>
        <td>
            <input type="hidden" name="view" value="N" />
            <input type="checkbox" name="view" value="Y" <?= $postdata['view'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Show AI</th>
        <td>
            <input type="hidden" name="show_ai" value="N" />
            <input type="checkbox" name="show_ai" value="Y" <?= $postdata['show_ai'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Sortable</th>
        <td>
            <input type="hidden" name="sortable" value="N" />
            <input type="checkbox" name="sortable" value="Y" <?= $postdata['sortable'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Search</th>
        <td>
            <input type="hidden" name="search" value="N" />
            <input type="checkbox" name="search" value="Y" <?= $postdata['search'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Pagination</th>
        <td>
            <input type="hidden" name="pagination" value="N" />
            <input type="checkbox" name="pagination" value="Y" <?= $postdata['pagination'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Number</th>
        <td>
            <input type="hidden" name="number" value="N" />
            <input type="checkbox" name="number" value="Y" <?= $postdata['number'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>CSV</th>
        <td>
            <input type="hidden" name="csv" value="N" />
            <input type="checkbox" name="csv" value="Y" <?= $postdata['csv'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Print</th>
        <td>
            <input type="hidden" name="print" value="N" />
            <input type="checkbox" name="print" value="Y" <?= $postdata['print'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Limit Selector</th>
        <td>
            <input type="hidden" name="limit_selector" value="N" />
            <input type="checkbox" name="limit_selector" value="Y" <?= $postdata['limit_selector'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Benchmark</th>
        <td>
            <input type="hidden" name="benchmark" value="N" />
            <input type="checkbox" name="benchmark" value="Y" <?= $postdata['benchmark'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
    <tr>
        <th>Duplicate</th>
        <td>
            <input type="hidden" name="duplicate" value="N" />
            <input type="checkbox" name="duplicate" value="Y" <?= $postdata['duplicate'] ? 'checked="checked"' : '' ?>  />
        </td>
        <td></td>
    </tr>
</table>

</form>

<script type="text/javascript">
jQuery(document).ready(function() {
    window.xiprev = {
        close: function() {},
    };
    
    function status1(x, data, frmData) {
        window.xiprev.close();
        window.xigen.close();        
    }
    
    function status2(x, data, frmData) {
        window.xiprev = $.confirm({
            title: 'Preview "'+ data.table +'"',
            columnClass: 'xlarge',
            content: [
                '<h3>Controller</h3>',
                x.data.controller,
                '<hr /><h3>View</h3>',
                x.data.view,
                '<hr /><h3>Model</h3>',
                x.data.model,
                '<hr /><h3>Callback</h3>',
                x.data.callback
            ].join('<br />'),            
            onContentReady: function () {
                // var height = $(window).height();
                // $('iframe').css('height', height * 0.7 | 0);
            },
            // containerFluid: true,
            buttons: {
                Yes: { 
                    text: 'Generate', // text for button
                    btnClass: 'btn-blue', // class for the button
                    action: function(heyThereButton){
                        $.confirm({
                           title: 'Confirm',
                            content: 'Are you sure?',
                            buttons: {
                                Yes: function() {
                                    $.post('<?= site_url('xii/generate-file') ?>/'+ data.table +'/'+ data.type +'/N', frmData, function(x) {
                                       if (x.status == 0) {
                                           status0(x, data, frmData);
                                       } else if (x.status == 1) {
                                           status1(x, data, frmData);
                                       } else if (x.status == 2) {
                                           status2(x, data, frmData);
                                       }
                                    });
                                },
                                No: function() {
                                    
                                }
                            }
                        });
                    }
                },
                Close: function() {
                    
                }
            }
        });
    }
    
    function status0(x, data, frmData) {
       $.confirm({
        title: 'Replace!',
        content: 'Apakah Anda yakin akan menimpa file yang sudah ada?',
        buttons: {
            Yes: function() {
                $.post('<?= site_url('xii/generate-file') ?>/'+ data.table +'/'+ data.type +'/Y', frmData, function(x) {
                   if (x.status == 0) {
                       status0(x, data, frmData);
                   } else if (x.status == 1) {
                       status1(x, data, frmData);
                   } else if (x.status == 2) {
                       status2(x, data, frmData);
                   }
                });
            },
            No: function() {
                
            }
        }
    });
    }
    
   $('#x-f').on('submit', function(e) {
       e.preventDefault();
       var frmData = $(this).serializeArray();
       var data = $(this).data();
       
       
        $.post('<?= site_url('xii/generate-file') ?>/'+ data.table +'/'+ data.type +'/P', frmData, function(x) {
           if (x.status == 0) {
               status0(x, data, frmData);
           } else if (x.status == 1) {
               status1(x, data, frmData);
           } else if (x.status == 2) {
               status2(x, data, frmData);
           }
        });
   });
   
   $('.xii-type').on('change', function() {
        var value = $(this).val();
        var data = $(this).data();
        
        $('.xii-rel-table[data-col="'+ data.col +'"]').hide();
        $('.xii-rel-field[data-col="'+ data.col +'"]').hide();
        $('.xii-rel-display[data-col="'+ data.col +'"]').hide();
        $('.xii-rel-sep[data-col="'+ data.col +'"]').hide();
        $('.xii-options[data-col="'+ data.col +'"]').hide();
        
        if (value == 'relation') {
            $('.xii-rel-table[data-col="'+ data.col +'"]').show().trigger('change');
        } else if (value == 'select' || value == 'radio' || value == 'checkbox') {
            $('.xii-options[data-col="'+ data.col +'"]').show();
        }
   });
   $('.xii-rel-table').on('change', function() {
        var value = $(this).val();
        var data = $(this).data();
        
        $('.xii-rel-field[data-col="'+ data.col +'"]').hide();
        $('.xii-rel-display[data-col="'+ data.col +'"]').hide();
            $('.xii-rel-sep[data-col="'+ data.col +'"]').hide();
        
        $.get('<?= site_url('xii/get-columns') ?>', {table: value}, function(r) {
            var html = '';
            $.each(r, function(i, v) {
                html += '<option value="'+ v +'" '+ (i == 0 ? 'selected="selected"' : '') +'>'+ v +'</option>';
            });
            $('.xii-rel-field[data-col="'+ data.col +'"]').html(html).show();
            $('.xii-rel-display[data-col="'+ data.col +'"]').html(html).show();
            $('.xii-rel-sep[data-col="'+ data.col +'"]').show();
        });
   });
});
</script>