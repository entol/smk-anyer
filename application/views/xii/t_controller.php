<?= '<?php' ?>


class <?= $controllerName ?> extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function actionIndex()
    {
        $this->breadcrumbs->push('Dashboard', 'site/index');
<?php if ($module != ''): ?>
        $this->breadcrumbs->push('<?= $moduleName ?>', '<?= $module ?>');
<?php endif ?>
        $this->breadcrumbs->push('<?= $title ?>', '<?= $module != '' ? $module .'/' : '' ?><?= $id ?>/index');
        
        $xcrud = xcrud_get_instance();
        $xcrud->table('<?= $tableName ?>');
        $xcrud->columns('<?= implode(',', $columns) ?>');
<?php foreach($fields as $tab => $f): ?>
<?php if (count($f)): ?>
        $xcrud->fields('<?= implode(',', $f) ?>', false, <?= $tab == '*' ? 'false' : "'$tab'" ?>, 'view');
<?php endif ?>
<?php endforeach ?>
<?php foreach($addFields as $tab => $f): ?>
<?php if (count($f)): ?>
        $xcrud->fields('<?= implode(',', $f) ?>', false, <?= $tab == '*' ? 'false' : "'$tab'" ?>, 'create');
<?php endif ?>
<?php endforeach ?>
<?php foreach($editFields as $tab => $f): ?>
<?php if (count($f)): ?>
        $xcrud->fields('<?= implode(',', $f) ?>', false, <?= $tab == '*' ? 'false' : "'$tab'" ?>, 'edit');
<?php endif ?>
<?php endforeach ?>
<?php if (count($fieldTypes)): ?>

<?php foreach($fieldTypes as $type): ?>
        <?= $type ."\n" ?>
<?php endforeach ?>
<?php endif ?>

<?php foreach($labels as $col => $label): ?>
        $xcrud->label('<?= $col ?>', '<?= addslashes($label) ?>');
<?php endforeach ?>
<?php if (count($required)): ?>

        $xcrud->validation_required('<?= implode(',', $required) ?>');
<?php endif ?>
<?php if (count($cuts)): ?>

<?php foreach($cuts as $col => $cut): ?>
        $xcrud->column_cut(<?= (int) $cut ?>, '<?= $col ?>');
<?php endforeach ?>
<?php endif ?>
<?php if ($duplicate): ?>
        $xcrud->duplicate_button();
<?php endif ?>
<?php if ($list): ?>
        $xcrud->unset_list();
<?php endif ?>
<?php if ($add): ?>
        $xcrud->unset_add();
<?php endif ?>
<?php if ($edit): ?>
        $xcrud->unset_edit();
<?php endif ?>
<?php if ($view): ?>
        $xcrud->unset_view();
<?php endif ?>
<?php if ($remove): ?>
        $xcrud->unset_remove();
<?php endif ?>
<?php if ($sortable): ?>
        $xcrud->unset_sortable();
<?php endif ?>
<?php if ($pagination): ?>
        $xcrud->unset_pagination();
<?php endif ?>
<?php if ($number): ?>
        $xcrud->unset_numbers();
<?php endif ?>
<?php if ($csv): ?>
        $xcrud->unset_csv();
<?php endif ?>
<?php if ($print): ?>
        $xcrud->unset_print();
<?php endif ?>
<?php if ($limit_selector): ?>
        $xcrud->unset_limitlist();
<?php endif ?>
<?php if ($benchmark): ?>
        $xcrud->benchmark(false);
<?php endif ?>

        <?= count($rules['add']) ? '' : '// ' ?>$xcrud->before_insert('<?= $idUnderscore ?>_before_insert', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));
        // $xcrud->after_insert('<?= $idUnderscore ?>_after_insert', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));
        <?= count($rules['edit']) ? '' : '// ' ?>$xcrud->before_update('<?= $idUnderscore ?>_before_update', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));
        // $xcrud->after_update('<?= $idUnderscore ?>_after_update', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));
        // $xcrud->before_remove('<?= $idUnderscore ?>_before_remove', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));
        // $xcrud->after_remove ('<?= $idUnderscore ?>_after_remove', realpath(__DIR__ .'/../callbacks/<?= $id ?>.php'));

        $this->layout->render('<?= $id ?>/index', [
            'box' => true,
            'boxTitle' => '<?= $title ?>',
            'title' => '<?= $title ?>',
            'pageTitle' => '<?= $title ?>',
            'pageSubTitle' => 'Semua <?= $title ?>',
            'xcrudContent' => $xcrud->render()
        ]);
    }
}