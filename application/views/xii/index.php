<?= $xcrudContent ?>

<script type="text/javascript">
jQuery(document).ready(function() {
    $(document).on('click', '.btn-detail', function(e) {
        e.preventDefault();
        var data = $(this).data();
        
        $.confirm({
            title: 'Columns "'+ data.table +'"',
            columnClass: 'xlarge',
            content: '<iframe src="'+ $(this).attr('href') +'" style="border:0; height: 100%; width: 100%"></iframe>',            
            onContentReady: function () {
                var height = $(window).height();
                $('iframe').css('height', height * 0.7 | 0);
            },
            buttons: {
                Close: function() {
                    
                }
            }
        });
    });
    
    $(document).on('click', '.btn-generate', function(e) {
        e.preventDefault();
        var data = $(this).data();
        window.xigen = $.confirm({
            title: 'Generate "'+ data.table +'"',
            columnClass: 'xlarge',
            content: 'URL:'+ $(this).attr('href'),            
            onContentReady: function () {
                // var height = $(window).height();
                // $('iframe').css('height', height * 0.7 | 0);
            },
            // containerFluid: true,
            buttons: {
                x: {                                    
                    text: 'Preview', // text for button
                    btnClass: 'btn-blue', // class for the button
                    action: function(heyThereButton){
                        // alert('x');
                        var frm = this.$content.find('form');
                        frm.submit();
                        
                        return false;
                    }
                },
                Close: function() {
                    
                }
            }
        });
    });   
});
</script>