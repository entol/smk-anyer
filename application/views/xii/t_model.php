<?= "<?" ?>php

namespace <?= $ns ?>;

class <?= $modelName ?> extends \Illuminate\Database\Eloquent\Model
{
    public $table = '<?= $tableName ?>';
    public $primaryKey = <?= $pk ?>;
    public $timestamps = false;
    public $incrementing = <?= $increment ? 'true' : 'false' ?>;
}