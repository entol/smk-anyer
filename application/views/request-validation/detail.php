<div style="height: 50px" class="xcrud-action-header">
    <div class="pull-right btn-group">
        <a href="<?= site_url('request-validation/index') ?>" data-task="list" class="btn btn-sm btn-warning">Kembali</a>
    </div>
</div>

<?= $xcrudContent ?>

<h4 class="text-black-lighter"># Validasi Request</h4>
<hr />
<table id="detail" class="display cell-border" style="width:100%">
    <thead>
        <tr>
            <th></th>
            <th>Kode</th>
            <th>Qty</th>
            <th>Note</th>
            <th>Approved</th>
            <th>ApprovedBy</th>
            <th>Remark</th>
            <th>Approved PM</th>
            <th>Approved PM By</th>
            <th>Remark PM</th>
            <!-- <th></th> -->
        </tr>
    </thead>
</table>

<script type="text/javascript">
jQuery(document).ready(function() {
    function getSelected(dt, status) {
        var selected = dt.rows({ selected: true });
        var count = selected.count();
        if (count === 0) {
            $.alert('Silahkan pilih material terlebih dahulu.', 'Perhatian');
            return false;
        } else {
            var data = selected.data();

            var ids = [];
            for(var i = 0; i<count; i++) {
                ids.push(data[i].DT_RowId);
            }
            
            var postdata = {
                status: status,
                data: ids
            };
            if (ids) {
                $.confirm({
                    title: status == 'Y' ? 'Setuju' : 'Tolak',
                    content: status == 'Y'
                        ? 'Apakah Anda yakin akan menyetujui permintaan material tersebut?'
                        : 'Apakah Anda yakin tidak akan menyetujui permintaan material tersebut?',
                    buttons: {
                        yes: {
                            text: status == 'Y' ? 'Ya (Setuju)' : 'Ya (Tolak)',
                            btnClass: status == 'Y' ? 'btn-primary' : 'btn-danger',
                            action: function(){
                                var loader = $.alert({
                                    title: false,
                                    content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memuat</p></center>',
                                    buttons: {
                                        ok: function() {}
                                    },
                                    onOpenBefore: function() {
                                        loader.buttons.ok.hide();
                                    }
                                });

                                $.post('<?= site_url('request-validation/approve/') ?>', postdata, function(x) {
                                    loader.close();
                                    if (x.status == 1) {
                                        dt.rows({ selected: true }).deselect();
                                        dt.ajax.reload();
                                    } else {
                                        $.alert(x.message, 'Perhatian!');
                                    }
                                }).fail(function() {
                                    loader.close();
                                    $.alert('Proses approve gagal', 'Error');
                                });
                            }
                        },
                        no: {
                            text: 'Tidak',
                            btnClass: 'btn-default',
                            action: function(){
                                
                            }
                        },
                    }
                });
            }
        }
    }
    var lookupDt = $('#detail').DataTable( {
        dom: 'Bfrtip',
        ajax: '<?= site_url('request-validation/detail-verifikasi/'. $id) ?>',
        columns: [
            { data: 'empty' },
            { data: 'nama_barang' },
            { data: 'qty' },
            { data: 'note' },
            { data: 'approved' },
            { data: 'approved_by' },
            { data: 'remark' },
            { data: 'approved_pm' },
            { data: 'approved_pm_by' },
            { data: 'remark_pm' },
            // { data: 'action' }
        ],
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        buttons: [
            <?php if ($this->ion_auth->in_button('request-validation-accept')): ?>
            {
                text: '<i class="fa fa-fw fa-check-circle"></i> Setujui',
                className: 'btn btn-success width-100',
                action: function ( e, dt, node, config ) {
                    getSelected(dt, 'Y');
                }
            },
            <?php endif ?>
            <?php if ($this->ion_auth->in_button('request-validation-dont-accept')): ?>
            {
                text: '<i class="fa fa-fw fa-ban"></i> Tolak',
                className: 'btn btn-danger width-100',
                action: function ( e, dt, node, config ) {
                    getSelected(dt, 'N');
                }
            },
            <?php endif ?>
        ],
        ordering: false,
        scrollY: '300px',
        scrollCollapse: true,
        paging: false,
        createdRow: function( row, data, dataIndex ) {
            // Set the data-status attribute, and add a class
            // $( row ).find('td:eq(0)').attr('data-approved', data.is_approved ? 'Y' : 'N').addClass(data.is_approved ? 'approved' : '');
            var row = $(row);
            if (data.is_approved == 'Y') {
                row.addClass('bg-lime-transparent-8 text-black');
            }
            if (data.is_approved == 'N') {
                row.addClass('bg-red-transparent-8 text-black');
            }

            // first.append('<span></span>');
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    });
});
</script>