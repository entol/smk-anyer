<?= $xcrudContent ?>
<style type="text/css">
.xcrud-nested-container.xcrud-container {
    margin-top: 0;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    function viewButton() {
        $('[data-task="view"].btn-info').remove();
    }
    viewButton();
    
    $(document).on("xcrudafterrequest", function(event,container) {
        viewButton();
    });
    
    $(document).on('click', '.btn-qty', function(e) {
		e.preventDefault();
		var $that = $(this);
        var data = $that.data();
        $.confirm({
            title: 'Update Qty',
            content: [
                '<form action="" class="formName">',
                '<div class="form-group">',
                '<input type="number" class="qty form-control" required />',
                '<em class="text-danger" style="display: none">Error</em>',
                '</div>',
                '</form>'
           ].join(''),
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var jc = this;
                        var err = this.$content.find('.text-danger');

                        var qty = this.$content.find('.qty').val();
                        if(!qty){
                            err.show();
                            err.text('Qty tidak boleh kosong');
                            return false;
                        }
                        if (isNaN(qty)) {
                            err.show();
                            err.text('Qty harus berupa angka');
                            return false;
                        }
                        if (qty <= 0) {
                            err.show();
                            err.text('Qty tidak boleh kurang dari 1');
                            return false;
                        }
                        
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memuat</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
                        
                        $.post('<?= site_url('request-validation/update-qty/') ?>'+ data.id, {
                            qty: qty
                        }, function(x) {
                            loader.close();
							if (x.status == 1) {
								Xcrud.reload(".xcrud-nested-container:first");
							} else {
                                $.alert(x.message, 'Error');
							}
						}).fail(function() {
                            loader.close();
							$.alert('Proses approve gagal', 'Error');
						});
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('.qty').val(data.qty);
                var err = this.$content.find('.text-danger');
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            
                this.$content.find('.qty').on('change', function (e) {
                    e.preventDefault();
                    err.hide();
                });
            }
        });
    });
    
	$(document).on('click', '.btn-approve', function(e) {
		e.preventDefault();
		var $that = $(this);
        var data = $that.data();
		
		$.confirm({
			title: 'Terima !',
			content: 'Apakah Anda yakin akan menyetujui permintaan ini?',
			buttons: {
				yes: {
					text: 'Ya',
					btnClass: 'btn-primary',
					action: function(){
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memuat</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
						$.post('<?= site_url('request-validation/approve/') ?>'+ data.id +'/Y/'+ data.pm, function(x) {
							loader.close();
                            if (x.status == 1) {
								Xcrud.reload(".xcrud-nested-container:first");
							} else {
                                $.alert(x.message, 'Perhatian!');
							}
						}).fail(function() {
                            loader.close();
							$.alert('Proses approve gagal', 'Error');
						});
					}
				},
				no: {
					text: 'Tidak',
					btnClass: 'btn-default',
					action: function(){
						
					}
				},
			}
		});
	});
    
	$(document).on('click', '.btn-disapprove', function(e) {
		e.preventDefault();
		var $that = $(this);
        var data = $that.data();
		
		$.confirm({
			title: 'Tolak !',
			content: '',
            content: [
                '<p>Apakah Anda yakin akan menolak permintaan ini?</p>',
                '<form action="" class="formName">',
                '<div class="form-group">',
                '<input type="text" placeholder="Berikan keterangan" class="remark form-control" required />',
                '<em class="text-danger" style="display: none">Error</em>',
                '</div>',
                '</form>'
           ].join(''),
			buttons: {
				yes: {
					text: 'Ya',
					btnClass: 'btn-primary',
					action: function(){
                        var jc = this;
                        var err = this.$content.find('.text-danger');
                        var remark = this.$content.find('.remark').val();
                        if(!remark || remark == null || remark.trim() == ''){
                            err.show();
                            err.text('Keterangan tidak boleh kosong');
                            return false;
                        }
                        
                        var loader = $.alert({
                            title: false,
                            content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Loading...</p></center>',
                            buttons: {
                                ok: function() {}
                            },
                            onOpenBefore: function() {
                                loader.buttons.ok.hide();
                            }
                        });
						$.post('<?= site_url('request-validation/approve/') ?>'+ data.id +'/N/'+ data.pm, {
                            note: remark
                        }, function(x) {
							loader.close();
                            if (x.status == 1) {
								Xcrud.reload(".xcrud-nested-container:first");
							} else {
                                $.alert(x.message, 'Error');
							}
						}).fail(function() {
                            loader.close();
							$.alert('Proses validasi gagal', 'Error');
						});
					}
				},
				no: {
					text: 'Tidak',
					btnClass: 'btn-default',
					action: function(){
						
					}
				},
			},
            onContentReady: function () {
                // bind to events
                var jc = this;
                var err = this.$content.find('.text-danger');
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            
                this.$content.find('.remark').on('change', function (e) {
                    e.preventDefault();
                    err.hide();
                });
            }
		});
	});
});
</script>