<?= $xcrudContent ?>

<script type="text/javascript">
jQuery(document).ready(function() {
    function addButton() {
        $('[data-task="create"]').attr('href', '<?= site_url('request/create') ?>').removeClass('xcrud-action');
    }
    addButton();
    
    function editButton() {
        $('[data-task="edit"]').each(function() {
            var pk = $(this).data('primary');
            $(this).attr('href', '<?= site_url('request/update/') ?>'+ pk).removeClass('xcrud-action');
        });
    }
    editButton();
    
    $(document).on("xcrudafterrequest", function(event,container) {
        addButton();
        editButton();
    });
});
</script>