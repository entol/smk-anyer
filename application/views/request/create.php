<div class="panel panel-inverse">
    <div class="panel-heading with-border">
        <h3 class="panel-title">Request</h3>
    </div>
    <div class="panel-body">
        <form method="post" id="form-request-barang">
        
            <div style="height: 50px" class="xcrud-action-header">
                <div class="pull-right btn-group">
                    <a href="<?= site_url('request/index') ?>" data-task="list" class="btn btn-sm btn-warning xcrud-action">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary xcrud-action">Simpan &amp; Kembali</button>
                </div>
            </div>
            
            <div class="form-horizontal">
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Project Tujuan<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <?= form_dropdown('project', $optMyProject, '', [
                            'class' => 'form-control form-control-sm-x',
                            'required' => 'required',
                        ]) ?>
                    </div>
                </div>
               
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Permintaan Asal<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <div class="radio radio-css">
                            <?= form_radio('permintaan_asal', 'W', true, ['id' => 'permintaan_asal_w']) ?>
                            <label class="form-check-label" for="permintaan_asal_w">Gudang</label>
                        </div>
                        <div class="radio radio-css">
                            <?= form_radio('permintaan_asal', 'P', '', ['id' => 'permintaan_asal_p']) ?>
                            <label class="form-check-label" for="permintaan_asal_p">Project</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row" id="row-asal-gudang">
                    <label class="col-form-label col-sm-3">Gudang Asal<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <?= form_dropdown('kode_gudang_asal', $optGudang, '', [
                            'class' => 'form-control form-control-sm-x'
                        ]) ?>
                    </div>
                </div>
               
                <div class="form-group row" id="row-asal-project">
                    <label class="col-form-label col-sm-3">Project Asal<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <?= form_dropdown('kode_project_asal', $optProject, '', [
                            'class' => 'form-control form-control-sm-x'
                        ]) ?>
                    </div>
                </div>
               
               
                <?php /*
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Tanggal Permintaan Shipment</label>
                    <div class="col-sm-9">
                        <?= form_input('tanggal_shipment', '', [
                            'class' => 'form-control form-control-sm-x'
                        ]) ?>
                    </div>
                </div>
               
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Tanggal Permintaan Sampai</label>
                    <div class="col-sm-9">
                        <?= form_input('tanggal_required', '', [
                            'class' => 'form-control form-control-sm-x'
                        ]) ?>
                    </div>
                </div>
                */ ?>
               
                <div class="form-group row">
                    <label class="col-form-label col-sm-3">Sebagai Draft<span class="text-danger" style="margin-right: -9px"> *</span></label>
                    <div class="col-sm-9">
                        <div class="radio radio-css">
                            <?= form_radio('draft', 'Y', '', ['id' => 'draft_y']) ?>
                            <label class="form-check-label" for="draft_y">Ya</label>
                        </div>
                        <div class="radio radio-css">
                            <?= form_radio('draft', 'N', true, ['id' => 'draft_n']) ?>
                            <label class="form-check-label" for="draft_n">Tidak</label>
                        </div>
                    </div>
                </div>
                
               
                <div style="margin-top: 20px; height: 40px" class="xcrud-action-header">
                    <div class="pull-right btn-group">
                        <a href="javascript:;" class="btn btn-info btn-sm" id="btn-lookup-barang">
                            <i class="fa fa-plus-circle"></i>
                            Material
                        </a>
                    </div>
                </div>
                
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 25px">#</th>
                            <th>Kode Barang</th>
                            <th>Deskripsi</th>
                            <th>Qty</th>
                            <th>Catatan</th>
                            <th style="width: 60px"></th>
                        </tr>
                    </thead>
                    <tbody id="list-request-barang">
                        <tr><td colspan="6">Belum ada material</td></tr>
                    </tbody>
                </table>
               
               <!--
               <div class="form-group row">
                  <label class="col-form-label col-sm-3">Tipe<span class="text-danger" style="margin-right: -9px"> *</span></label>
                  <div class="col-sm-9"><input class="xcrud-input form-control form-control-sm-x" data-required="1" autocomplete="off" type="text" data-type="text" data-name="m_barang.tipe" value="" name="bV9iYXJhbmcudGlwZQ--" maxlength="100"></div>
               </div>
               <div class="form-group row">
                  <label class="col-form-label col-sm-3">Deskripsi<span class="text-danger" style="margin-right: -9px"> *</span></label>
                  <div class="col-sm-9"><input class="xcrud-input form-control form-control-sm-x" data-required="1" autocomplete="off" type="text" data-type="text" data-name="m_barang.deskripsi" value="" name="bV9iYXJhbmcuZGVza3JpcHNp" maxlength="250"></div>
               </div>
               <div class="form-group row">
                  <label class="col-form-label col-sm-3">Tahun<span class="text-danger" style="margin-right: -9px"> *</span></label>
                  <div class="col-sm-9"><input class="xcrud-input form-control form-control-sm-x" data-required="1" autocomplete="off" type="text" data-type="text" data-name="m_barang.tahun" value="" name="bV9iYXJhbmcudGFodW4-" maxlength="255"></div>
               </div>
               -->
            </div>
        
        </form>
    
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var numBarang = 0;
    var selectedBarang = 0;
    
    function getKodeBarangRegistered() {
        var items = [];
        $('.id-barang').each(function() {
            var v = $(this).val();
            if (v != null && v != '') {
                items.push(v);
            }
        });
        return items;
    }
    function getKodeItemRegistered() {
        var items = [];
        $('.kode_item').each(function() {
            var v = $(this).val();
            if (v != null && v != '') {
                items.push(v);
            }
        });
        return items;
    }
    function clearListRequest() {
        numBarang = 0;
        selectedBarang = 0;
        $('#list-request-barang').html('<tr><td colspan="6">Belum ada material</td></tr>');
    }
    
    var lookupDialog = null;
    var lookupDt = null;
    $(document).on('click', '#btn-lookup-barang', function() {
        var kdb = getKodeBarangRegistered().join(',');
        var kdi = getKodeItemRegistered().join(',');
        var asal = $('[name="permintaan_asal"]:checked').val();
        var kda = '';
        if (asal == 'W') {
            kda = $('[name="kode_gudang_asal"]').val();
        } else {
            kda = $('[name="kode_project_asal"]').val();
        }
        
        if (kda == '') {
            $.alert('Silahkan pilih '+ (asal == 'W' ? 'Gudang' : 'Project') + ' terlebih dahulu', 'Alert');
        } else {
            lookupDialog = $.confirm({
                title: 'Lookup Material',
                content: [
                    '<table id="lookup" class="display" style="width:100%">',
                        '<thead>',
                            '<tr>',
                                '<th></th>',
                                '<th>Kode</th>',
                                '<th>Kategori</th>',
                                '<th>Deskripsi</th>',
                                '<th>Brand</th>',
                                '<th>Tipe</th>',
                                '<th>Tahun</th>',
                                // '<th>Kondisi</th>',
                                '<th>Qty</th>',
                            '</tr>',
                        '</thead>',
                    '</table>'
                ].join(''),
                onContentReady: function () {
                    let height = ($(window).height() - 320);
                    height = height < 200 ? 200 : height;
                    lookupDt = this.$content.find('#lookup').DataTable( {
                        ajax: '<?= site_url('request/lookup-barang') ?>?kdb='+ kdb +'&kdi='+ kdi +'&asal='+ asal +'&kda='+ kda,
                        columns: [
                            { data: 'empty' },
                            { data: 'kode' },
                            { data: 'kategori' },
                            { data: 'deskripsi' },
                            { data: 'brand' },
                            { data: 'tipe' },
                            { data: 'tahun' },
                            // { data: 'kondisi' },
                            { data: 'quantity' },
                        ],
                        columnDefs: [{
                            orderable: false,
                            className: 'select-checkbox',
                            targets: 0
                        }],
                        ordering: false,
                        scrollY: (height) +'px',
                        scrollCollapse: true,
                        paging: false,
                        select: {
                            style: 'multi',
                            selector: 'td:first-child'
                        },
                        order: [[ 1, 'asc' ]],
                        initComplete: function(){
                            $('.dataTables_filter input').unbind();
                            $('.dataTables_filter input').bind('keyup', function(e) {
                                if(e.keyCode == 13) {
                                    lookupDt.search( this.value ).draw(); 
                                }
                            });     
                        },
                    });
                },
                columnClass: 'xlarge',
                buttons: {
                    select: {
                        text: 'Pilih',
                        btnClass: 'btn-blue',
                        action: function () {
                            var jc = this;
                            var err = this.$content.find('.text-danger');
                            var selected = lookupDt.rows({ selected: true });
                            var datas = selected.data();
                            var total = selected.count();
                            for (var i = 0; i < total; i++) {
                                var data = datas[i];
                                numBarang++;
                                selectedBarang++;
                                var kdBarangInput = $('<input/>', {
                                    type: 'hidden',
                                    name: 'kode_barang['+ numBarang +']',
                                    'class': 'kode_barang'
                                }).val(data.kode);
                                
                                var idBarangInput = $('<input/>', {
                                    type: 'hidden',
                                    class: 'id-barang',
                                    name: 'id_barang['+ numBarang +']'
                                }).val(data.id);
                                
                                var qtyInput = $('<input/>', {
                                    type: 'number',
                                    min: 1,
                                    max: data.quantity,
                                    name: 'qty['+ numBarang +']',
                                    'class': 'form-control qty-barang'
                                }).val(1);

                                // if (data.kode_kategori != '8') {
                                //     qtyInput.attr('readonly', 'readonly');
                                // }
                                
                                var noteInput = $('<input/>', {
                                    type: 'text',
                                    name: 'note['+ numBarang +']',
                                    'class': 'form-control form-control-sm-x'
                                });
                                
                                var btnInput = $('<a/>', {
                                    href: 'javascript:;',
                                    'class': 'btn btn-sm btn-danger btn-hapus-lookup-barang'
                                }).html('<i class="fa fa-fw fa-trash"></i>');
                                
                                var $tr = $('<tr/>').data('kodeBarang', data.kode).data('kodeItem', '');
                                $('<td/>').text(numBarang).appendTo($tr);
                                $('<td/>').text(data.kode).append(kdBarangInput).appendTo($tr);
                                $('<td/>').text( data.deskripsi ).appendTo($tr);
                                $('<td/>').append(qtyInput).appendTo($tr);
                                $('<td/>').append(noteInput).append(idBarangInput).appendTo($tr);
                                $('<td/>').append(btnInput).appendTo($tr);
                                
                                if (numBarang === 1) {
                                    $('#list-request-barang').html('');
                                }
                                $('#list-request-barang').append($tr);

                            }
                        }
                    },
                    cancel: function() {
                        
                    }
                }
            });
        }
    });
    
    $(document).on('click', '.btn-hapus-lookup-barang', function() {
        var tr = $(this).closest('tr');
        tr.remove();
        selectedBarang--;
        
        if ($('#list-request-barang>tr').length == 0) {
            clearListRequest();
        }
    });
    
    // init jquery plugin
    $('[name="tanggal_shipment"], [name="tanggal_required"]').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat:  'yy-mm-dd'
    });

    $('[name="project"], [name="kode_gudang_asal"], [name="kode_project_asal"]').select2();

    $(window).on('resize', function() {
        $('[name="project"], [name="kode_gudang_asal"], [name="kode_project_asal"]').each(function() {
            var $that = $(this);
            if ($that.data('select2')) {
                $that.select2('destroy');
                $that.select2();
            }
        });
    });
    
    $(document).on('change', '[name="permintaan_asal"]', function() {
        var asal = $('[name="permintaan_asal"]:checked').val();
        $('#row-asal-gudang').hide();
        $('#row-asal-project').hide();
        if (asal == 'W') {
            $('[name="kode_project_asal"]').val('');
            $('#row-asal-gudang').show();
        } else if (asal == 'P') {
            $('[name="kode_gudang_asal"]').val('');
            $('#row-asal-project').show();
        }
        $(window).trigger('resize');
    });
    $('[name="permintaan_asal"]').trigger('change');
    
    $(document).on('change', '[name="permintaan_asal"], [name="kode_gudang_asal"], [name="kode_project_asal"]', function() {
        clearListRequest();
    });
    
    $(document).on('change', '.qty-barang', function() {
        var value = $(this).val();
        var max = parseInt($(this).attr('max'));
        if (value == '') {
            $(this).val(1);
        } else if (!isNaN(max) && max > 0) {
            if (value > max) {
                $(this).val(max);
            }
        }
    });
    
    var projectLists = <?= json_encode($optProject) ?>;
    $(document).on('change', '[name="project"]', function() {
        var asal = $('[name="kode_project_asal"]');
        var proj = $(this).val();
        
        var options = '';
        for(var i in projectLists) {
            if (i != proj) {
                options += '<option value="'+ i +'">'+ projectLists[i] +'</option>';
            }
        }
        asal.html(options).val('').trigger('change');
    });
    
    $("#form-request-barang").validate();
    
    $(document).on('submit', '#form-request-barang', function(e) {
        e.preventDefault();
        if (selectedBarang == 0) {
            $.alert('Silahkan pilih Materail terlebih dahulu.', 'Error');
        } else {
            var loader = $.alert({
                title: false,
                content: '<center><div class="fa-4x"><i class="fas fa-spinner fa-spin"></i></div><p>Memuat</p></center>',
                buttons: {
                    ok: function() {}
                },
                onOpenBefore: function() {
                    loader.buttons.ok.hide();
                }
            });
            
            var frm = $(this).serializeArray();
            $.post('<?= site_url('request/create-process') ?>', frm, function(e) {
                if (e.status == 1) {
                    window.location.href = '<?= site_url('request/index') ?>';
                } else {
                    $.alert(e.message, 'Kesalahan');
                }
                loader.close();
            }).fail(function() {
                $.alert('Terjadi kesalahan saat menghubungi server', 'Failed');
                loader.close();
            })
        }
    });
});
</script>

<style type="text/css">
.error {
    text-align: left;
    color: red;
}
.form-group.row {
    margin-bottom: 5px;
}
.form-horizontal .col-form-label {
    text-align: right;
}
</style>