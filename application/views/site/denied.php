<div class="row" style="margin-top: 40px">
    <div class="col-sm-offset-3 col-sm-6">
        <div class="callout callout-warning">
            <h4>Akses Ditolak!</h4>

            <p>Anda tidak memiliki akses ke halaman ini.</p>
        </div>
    </div>
</div>