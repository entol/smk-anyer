<?= $xcrudContent ?>

<script type="text/javascript">
jQuery(document).on('ready', function() {
    function lokasiChange() {
        var lokasi = $('[data-name="m_barang.lokasi"]:checked').val();
        var gudang = $('[data-name="m_barang.lokasi_gudang"]');
        var $gudang = gudang.closest('.row');
        var project = $('[data-name="m_barang.lokasi_project"]');
        var $project = project.closest('.row');
        
        $gudang.hide();
        $project.hide();
        
        if (lokasi == 'W') {
            $gudang.show();
            project.val('').trigger('change');
        } else {
            $project.show();
            gudang.val('').trigger('change');
        }
        $(window).trigger('resize');
    }

    function changeButton() {
        $('.xcrud-action[data-task="list"]').each(function() {
            var inContainer = $(this).closest('.xcrud-nested-container').length > 0;
            if (inContainer === false) {
                $(this).attr('href', '<?= site_url('/barang/index') ?>').removeClass('xcrud-action');
            }
        })
    }

    function notif() {
        $('.alert-keep').remove();
        $('input[data-name="m_barang.kode"]').closest('.form-horizontal').prepend('<div class="alert alert-warning alert-keep"><strong>Perhatian!</strong> Tekan tombol Simpan ketika data pada halaman ini selesai diubah.</div>');
    }
    
    function onInit() { 
        changeButton();   
        lokasiChange();
        <?php if ($event != 'create'): ?>
        notif();
        <?php endif ?>
    }
    
    $(document).on("xcrudafterrequest", function(event,container, data) {
        if (data.after == 'list' && data.instance == 'kelola-barang') {
            window.location.href = '<?= site_url('/barang/index') ?>';
            $('.xcrud-ajax').remove();
            $('.xcrud-container').html('<div class="alert alert-success"><strong>Berhasil!</strong> Data barang berhasil ditambahkan. Mohon tunggu ...</div>');
        }

        onInit();
    });
    
    $(document).on('change', '[data-name="m_barang.lokasi"]', function() {
        lokasiChange();
    });

    onInit();
});
</script>