<?= $xcrudContent ?>

<script type="text/javascript">
jQuery(document).on('ready', function() {
    function changeButton() {
        $('.xcrud-action[data-task="create"]').attr('href', '<?= site_url('/barang/kelola') ?>').removeClass('xcrud-action');
        $('a[data-task="edit"]').each(function() {
            var data = $(this).data();
            $(this).attr('href', '<?= site_url('/barang/kelola/') ?>'+ data.primary).removeClass('xcrud-action');
        });
    }

    function onInit() {
        changeButton();
    }
    
    $(document).on("xcrudafterrequest", function(event,container) {
        onInit();
    });

    onInit();
});
</script>