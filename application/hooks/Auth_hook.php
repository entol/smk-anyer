<?php

class Auth_hook
{

    public function __get($property)
    {
        if (!property_exists(get_instance(), $property)) {
            show_error('property: <strong>' . $property . '</strong> not exist.');
        }
        return get_instance()->$property;
    }

    public function validate()
    {
        // router
		$class = lcfirst($this->router->class);
        $method = lcfirst($this->router->method);
        $module = lcfirst($this->router->module);

        $allows = [
            '' => [
                'authController' => [
                    'actionLogin',
                    'actionLogout'
                ],
                'xcrudAjaxController' => [
                    'actionIndex'
                ],
                'xiiController' => ['*']
            ]
        ];
        
        foreach($allows as $hmvc => $controllers) {
            foreach($controllers as $controller => $actions) {
                foreach($actions as $action) {
                    if ($module == '') {
                        if ($class == $controller && $method == $action) {
                            return true;
                        } elseif ($class == $controller && '*' == $action) {
                            return true;
                        }
                    } else {
                        if ($module == $hmvc && $class == $controller && $method == $action) {
                            return true;
                        } elseif ($module == $hmvc && $class == $controller && '*' == $action) {
                            return true;
                        }
                    }
                }
            }
        }

        if (!$this->ion_auth->logged_in()) {
            if ($this->input->is_ajax_request()) {
                echo '<div class="xcrud-error" style="position:relative;line-height:1.25;padding:15px;color:#BA0303;margin:10px;border:1px solid #BA0303;border-radius:4px;font-family:Arial,sans-serif;background:#FFB5B5;box-shadow:inset 0 0 80px #E58989;">Wrong request!</div>';
                exit;
            } else {
                redirect('auth/login', 'refresh');
            }
        }

        // Check router
        if ($this->ion_auth->in_router($class, $method, $module)) {
            return true;
        } else {
            if ($this->ion_auth->is_admin() && $class == 'authController') {
                return true;
            } else {
				show_error('You are not allowed to perform this action.', 403);
				exit;
            }
        }
    }

}
