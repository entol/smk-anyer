<?php

class Language_hook
{

    public function __get($property)
    {
        if (!property_exists(get_instance(), $property)) {
            show_error('property: <strong>' . $property . '</strong> not exist.');
        }
        return get_instance()->$property;
    }

    public function validate()
    {
        // set language
        $lang = $this->app->config('language');
        $this->config->set_item('language', $lang);
    }
}