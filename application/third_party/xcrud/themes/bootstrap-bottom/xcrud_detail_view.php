<?php echo $this->render_table_name($mode); ?>

<?php 
	if (trim($this->render_button('return','list','','btn btn-sm btn-warning')) != ''):
?>
<div class="row">
	<div style="height: 50px" class="xcrud-action-header col-sm-9">
	    <div class="pull-right btn-group">
	        <?= $this->render_button('return','list','','btn btn-sm btn-warning') ?>
	    </div>
	</div>
</div>
<?php endif ?>

<div class="xcrud-view">
<?php echo $mode == 'view' ? $this->render_fields_list($mode,array('tag'=>'table','class'=>'table')) : $this->render_fields_list($mode,'div','div','label','div'); ?>
</div>

<?php 
	if (
		trim($this->render_button('save_return','save','list','btn btn-sm btn-primary','','create,edit')) != '' ||
		trim($this->render_button('save_new','save','create','btn btn-sm btn-success','','create,edit')) != '' ||
		trim($this->render_button('save_edit','save','edit','btn btn-sm btn-info','','create,edit')) != ''
	):
?>
<div class="row">
	<div style="height: 50px" class="xcrud-action-header col-sm-9">
	    <div class="pull-right btn-group">
	        <?php 
	        echo $this->render_button('save_return','save','list','btn btn-sm btn-primary','','create,edit');
	        echo $this->render_button('save_new','save','create','btn btn-sm btn-success','','create,edit');
	        echo $this->render_button('save_edit','save','edit','btn btn-sm btn-info','','create,edit');
			?>
	    </div>
	</div>
</div>
<?php endif ?>

<div class="xcrud-nav">
	<?php echo $this->render_benchmark(['tag' => 'p', 'class' => 'text-muted pull-left']); ?>
</div>
