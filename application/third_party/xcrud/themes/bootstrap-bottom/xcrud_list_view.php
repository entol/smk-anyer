<?php echo $this->render_table_name(); ?>

        <div class="xcrud-top-actions" style="margin-bottom: 10px">
            <div class="btn-group pull-right">
                <?php echo $this->print_button('btn btn-sm btn-primary','fa fa-print');
                echo $this->csv_button('btn btn-sm btn-primary','fa fa-file-excel-o'); ?>
            </div>
            <?php if ($this->is_create): ?>
            <?php echo $this->add_button('btn btn-sm btn-success','fa fa-plus-circle'); ?>
            <?php endif; ?>
            <?php echo $this->render_search(); ?>
            <div class="clearfix"></div>
        </div>

        <div class="xcrud-list-container">
        <table class="xcrud-list table table-bordered table-hover">
            <thead>
                <?php echo $this->render_grid_head('tr', 'th'); ?>
            </thead>
            <tbody>
                <?php echo $this->render_grid_body('tr', 'td'); ?>
            </tbody>
            <tfoot>
                <?php echo $this->render_grid_footer('tr', 'td'); ?>
            </tfoot>
        </table>
        </div>
        <?php 
        $limitlist = $this->render_limitlist(true);
        $pagination = $this->render_pagination();
        $benchmark = $this->render_benchmark(['tag' => 'p', 'class' => 'text-muted pull-left']);

        if (trim($limitlist) != '' || trim($pagination) != ''): ?>
        <div class="xcrud-nav">
            <?php echo $limitlist; ?>
            <?php echo $pagination; ?>
        </div>
        <?php endif ?>

        <?php if (trim($benchmark) != ''): ?>
        <div class="xcrud-nav">
            <?php echo $benchmark; ?>
        </div>
        <?php endif ?>
