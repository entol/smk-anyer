<?php

function auth_user_before_insert($postdata, $xcrud) {
    $ci = get_instance();

	$manual_activation = $ci->config->item('manual_activation', 'ion_auth');
		
	$ip_address = $ci->input->ip_address();
	$salt = $ci->ion_auth_model->store_salt ? $ci->ion_auth_model->salt() : FALSE;
	$password = $ci->ion_auth_model->hash_password($postdata->get('password'), $salt);
	
	$postdata->set('password', $password);
	$postdata->set('active', ($manual_activation === FALSE ? 1 : 0));
	$postdata->set('ip_address', $ip_address);
	$postdata->set('created_on', time());
	
	return true;
}


function auth_user_after_insert($postdata, $primary, $xcrud) {
    $ci = get_instance();
	
	$group = new AuthUsersGroups;
	$group->group_id = 2;
	$group->user_id = $primary;
	$group->save();

	return true;
}

function auth_user_before_update($postdata, $primary, $xcrud) {
    $ci = get_instance();
		
	if ($postdata->get('password') != '' && $postdata->get('password') != null) {
		$ip_address = $ci->input->ip_address();
		$salt = $ci->ion_auth_model->store_salt ? $ci->ion_auth_model->salt() : FALSE;
		$password = $ci->ion_auth_model->hash_password($postdata->get('password'), $salt);
		
		$postdata->set('password', $password);
		$postdata->set('ip_address', $ip_address);
	}
	
	return true;
}

function sp2d_before_update($postdata, $primary, $xcrud)
{
	if ($postdata->get('sp2d') != '' && $postdata->get('tanggal_sp2d') != null) {
		$postdata->set('status', 7);
	}
	
	return true;
}