<?php

use app\models\Perbaikan;
use app\models\PerbaikanParts;
use app\models\PerbaikanHistory;
use app\models\Barang;

function perbaikan_before_insert($postdata, $xcrud) {
    $CI =& get_instance();
    $postdata->set('created_at', date('Y-m-d H:i:s'));
    $postdata->set('created_by', $CI->ion_auth->user_data('id'));
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
    
    $draft = $postdata->get('draft');
    if ($draft == 'Y') {
        $postdata->set('status', 'DR');
    } else {
        $postdata->set('status', 'RQ');
    }
}

function perbaikan_after_insert($postdata, $primary, $xcrud) {
    // your code
    PerbaikanHistory::createHistory($primary, $postdata->get('status'));
    Perbaikan::generateKode($primary);
}

function perbaikan_before_update($postdata, $primary, $xcrud) {
    $CI =& get_instance();
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
    
    $draft = $postdata->get('draft');
    if ($draft == 'Y') {
        $postdata->set('status', 'DR');
    } else {
        $postdata->set('status', 'RQ');
    }
    
    // $kodeBarang = $postdata->get('kode_barang');
    // $perbaikan = Perbaikan::find($primary);
    // if ($perbaikan && $perbaikan->kode_barang != $kodeBarang) {
    //     $barang = Barang::where('kode', $kodeBarang)->first();
    //     if ($barang) {
    //         $postdata->set('lokasi_barang', $barang->lokasi);
    //         $postdata->set('lokasi_kode_project', $barang->lokasi_project);
    //         $postdata->set('lokasi_kode_gudang', $barang->lokasi_gudang);
    //     }
    // }
}

function perbaikan_after_update($postdata, $primary, $xcrud) {
    // your code
    PerbaikanHistory::createHistory($primary, $postdata->get('status'));
}

function perbaikan_before_remove($primary, $xcrud) {
    // your code
}

function perbaikan_after_remove($primary, $xcrud) {
    // your code
    PerbaikanParts::where('id_perbaikan', $primary)->delete();
    PerbaikanHistory::where('id_perbaikan', $primary)->delete();
}