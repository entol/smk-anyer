<?php

use app\models\Kondisi;
use app\models\Barang;
use app\models\BarangItems;
use app\models\BarangHistory;

function barang_before_insert($postdata, $xcrud) {
    $CI =& get_instance();

    $lokasi = $postdata->get('lokasi');
    $lokasi_gudang = $postdata->get('lokasi_gudang');
    $lokasi_project = $postdata->get('lokasi_project');
    
    if ($lokasi == 'W' && empty($lokasi_gudang)) {
        return $xcrud->set_exception('lokasi_gudang','Tidak boleh kosong','error');
    } elseif ($lokasi == 'P' && empty($lokasi_project)) {
        return $xcrud->set_exception('lokasi_project','Tidak boleh kosong','error');
    } else {
        $postdata->set('created_at', date('Y-m-d H:i:s'));
        $postdata->set('created_by', $CI->ion_auth->user_data('id'));
        $postdata->set('updated_at', date('Y-m-d H:i:s'));
        $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
        $postdata->set('ip_address', $CI->input->ip_address());
        $postdata->set('useragent', $CI->agent->agent_string());
    }
}

function barang_after_insert($postdata, $primary, $xcrud) {
    // your code
    $barang = Barang::find($primary);
    if ($barang) {
        $barang->afterCreate();
    }
}

function barang_before_update($postdata, $primary, $xcrud) {
    $CI =& get_instance();
    
    $lokasi = $postdata->get('lokasi');
    $lokasi_project = $postdata->get('lokasi_project');
    $lokasi_gudang = $postdata->get('lokasi_gudang');
    if ($lokasi == 'W' && empty($lokasi_gudang)) {
        return $xcrud->set_exception('lokasi_gudang','Lokasi gudang tidak boleh kosong','error');
    } elseif ($lokasi == 'P' && empty($lokasi_project)) {
        return $xcrud->set_exception('lokasi_project','Lokasi project tidak boleh kosong','error');
    } else {
        $barang = Barang::find($primary);
        if ($barang) {
            $items = $barang->items;
            if (is_array($items) || is_object($items)) {
                $totalQty = 0;
                
                $currentLocation = 0;
                foreach($items as $item) {
                    $totalQty = $totalQty + $item->qty_baik;
                    $totalQty = $totalQty + $item->qty_perbaikan;
                    $totalQty = $totalQty + $item->qty_rusak;
                    $totalQty = $totalQty + $item->qty_hilang;

                    if ($lokasi == 'W' && $lokasi_gudang == $item->lokasi_gudang) {
                        $currentLocation = $item->qty_baik + $item->qty_perbaikan + $item->qty_rusak + $item->qty_hilang;
                    } elseif ($lokasi == 'P' && $lokasi_project == $item->lokasi_project) {
                        $currentLocation = $item->qty_baik + $item->qty_perbaikan + $item->qty_rusak + $item->qty_hilang;
                    }
                }

                $qty = (int) $postdata->get('qty');
                $totalQty = $totalQty-$currentLocation;
                if ($qty < $totalQty) {        
                    return $xcrud->set_exception('qty','Jumlah qty tidak dapat kurang dari <b>'. $totalQty .'</b>', 'error');
                }
            }
        }
        
        // $postdata->set('qty_total', $total);
        $postdata->set('updated_at', date('Y-m-d H:i:s'));
        $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
        $postdata->set('ip_address', $CI->input->ip_address());
        $postdata->set('useragent', $CI->agent->agent_string());
    }
}

function barang_after_update($postdata, $primary, $xcrud) {
    // your code
    $barang = Barang::find($primary);
    if ($barang) {
        $barang->afterUpdate();
    }
}

function barang_before_remove($primary, $xcrud) {
    // your code
    $barang = Barang::find($primary);
    if ($barang) {
        BarangItems::where('kode_barang', $barang->kode)->delete();
        BarangHistory::where('kode_barang', $barang->kode)->delete();
    }
}

function barang_after_remove($primary, $xcrud) {
    // your code
}


function barang_lokasi($value, $fieldname, $primary_key, $row, $xcrud) {
    if ($row['m_barang_vd.lokasi'] == 'W') {
        // return 'Gudang : '. 
    }
    
    return print_r($row, true);
}

function barang_sum_qty($value) {
    $ex = explode(',', $value);
    if (count($ex) === 4) {
        $html = '<span class="label label-primary" style="font-size: 12px; margin-right: 5px">'. $ex[0] .' Good</span>';
        $html .= '<span class="label label-warning" style="font-size: 12px; margin-right: 5px">'. $ex[1] .' Repair</span>';
        $html .= '<span class="label label-danger" style="font-size: 12px; margin-right: 5px">'. $ex[2] .' Damage</span>';
        $html .= '<span class="label label-dark" style="font-size: 12px; margin-right: 5px">'. $ex[3] .' Lost</span>';

        return $html;
    }
    return $value;
}

function barang_kondisi_cc($value, $fieldname, $primary_key, $row, $xcrud) {
    $kondisi = $row['m_barang_vd.kondisi'];
    $label = 'primary';
    if ($kondisi == 'D') {
        $label = 'danger';
    } elseif ($kondisi == 'L') {
        $label = 'danger';
    } elseif ($kondisi == 'R') {
        $label = 'warning';
    }
    return '<span class="label label-'. $label .'">'. $value .'</span>';
}