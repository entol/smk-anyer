<?php

use app\models\Status;

global $dpStatus;
$dpStatus = Status::all()->keyBy('kode');

function pengiriman_barang_status($value, $fieldname, $primary_key, $row, $xcrud) {
    global $dpStatus;
    $status = $row['t_request.status'];
    
    $color = 'primary';
    if ($status == 'AP') {
        $color = 'warning';
    } elseif ($status == 'PR') {
        $color = 'info';
    } elseif ($status == 'SH') {
        $color = 'success';
    }
    
    return '<center><span class="label label-'. $color .'" style="font-size: 9px">'. $dpStatus[$status]->selanjutnya .'</span></center>';
}