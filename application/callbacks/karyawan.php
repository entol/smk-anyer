<?php

use app\models\Karyawan;

function karyawan_before_insert($postdata, $xcrud) {
    $CI =& get_instance();
    
    $nik = $postdata->get('nik');
    $userLogin = $postdata->get('user_login');
    
    if ($userLogin == 'Y') {
        if ($nik == '' || empty(trim($nik))) {
            $xcrud->set_exception('nik', 'User Login dipilih. NIK harus diisi sebagai username', 'error');
            return false;
        } elseif ($nik != '' && !empty(trim($nik)) && strlen(trim($nik)) < 3) {
            $xcrud->set_exception('nik', 'NIK terlalu pendek', 'error');
            return false;
        } else {
            $user = AuthUsers::where('username', $nik)->first();
            if ($user) {
                $xcrud->set_exception('nik', 'NIK telah terdaftar', 'error');
                return false;
            } else {
                $salt = $CI->ion_auth_model->store_salt ? $CI->ion_auth_model->salt() : FALSE;
                $password = $CI->ion_auth_model->hash_password('12345678', $salt);
                
                $user = new AuthUsers;
                $user->ip_address = $CI->input->ip_address();
                $user->username = $nik;
                $user->password = $password;
                $user->email = $postdata->get('email');
                $user->active = 1;
                $user->nik = $nik;
                $user->nama = $postdata->get('nama');
                if ($user->save()) {
                    if ($postdata->get('id_jabatan') == 4) {
                        $CI->ion_auth->add_to_group(9, $user->id);
                    } elseif ($postdata->get('id_jabatan') == 6) {
                        $CI->ion_auth->add_to_group(10, $user->id);
                    } else {
                        $CI->ion_auth->add_to_group(2, $user->id);
                    }
                }
            }
        }
    }
    
    if ($nik != '' && !empty(trim($nik)) && strlen(trim($nik)) < 3) {
        $xcrud->set_exception('nik', 'NIK terlalu pendek', 'error');
        return false;
    }
    
    $postdata->set('created_at', date('Y-m-d H:i:s'));
    $postdata->set('created_by', $CI->ion_auth->user_data('id'));
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
}

function karyawan_after_insert($postdata, $primary, $xcrud) {
    // your code
    
    $nik = $postdata->get('nik');
    $userLogin = $postdata->get('user_login');
    
    $user = AuthUsers::where('username', $nik)->first();
    if ($user) {
        $user->id_peg = $primary;
        $user->save();
    }
    
}

function karyawan_before_update($postdata, $primary, $xcrud) {
    $CI =& get_instance();
    
    $nik = $postdata->get('nik');
    $userLogin = $postdata->get('user_login');
    $username = $nik;
    $old = Karyawan::where('id', $primary)->first();
    if ($old) {
        $username = $old->nik;
    }
    $salt = $CI->ion_auth_model->store_salt ? $CI->ion_auth_model->salt() : FALSE;
    $password = $CI->ion_auth_model->hash_password('12345678', $salt);
    
    if ($userLogin == 'Y') {
        if ($nik == '' || empty(trim($nik))) {
            $xcrud->set_exception('nik', 'User Login dipilih. NIK harus diisi sebagai username', 'error');
            return false;
        } elseif ($nik != '' && !empty(trim($nik)) && strlen(trim($nik)) < 3) {
            $xcrud->set_exception('nik', 'NIK terlalu pendek', 'error');
            return false;
        } else {
            if ($username != $nik) { // jika nik berubah
                $user = AuthUsers::where('username', $nik)->first();
                if ($user) {
                    $xcrud->set_exception('NIK', 'Nik telah terdaftar', 'error');
                    return false;
                }
            }
        }
    }
    
    if ($nik != '' && !empty(trim($nik)) && strlen(trim($nik)) < 3) {
        $xcrud->set_exception('nik', 'NIK terlalu pendek', 'error');
        return false;
    }
    
    $user = AuthUsers::where('username', $username)->first();
    $isNewUser = false;
    if (!$user && $userLogin == 'Y') {
        $user = new AuthUsers;
        $user->password = $password;
        $isNewUser = true;
    }
    
    if ($user) {
        $user->ip_address = $CI->input->ip_address();
        $user->username = $nik;
        $user->email = $postdata->get('email');
        $user->active = $userLogin == 'Y' ? 1 : 0;
        $user->nik = $nik;
        $user->nama = $postdata->get('nama');
        $user->id_peg = $primary;
        if ($user->save()) {
            if (!$isNewUser) {
                $CI->ion_auth->remove_from_group(null, $user->id);
            }
            if ($postdata->get('id_jabatan') == 4) {
                $CI->ion_auth->add_to_group(9, $user->id);
            } elseif ($postdata->get('id_jabatan') == 6) {
                $CI->ion_auth->add_to_group(10, $user->id);
            } else {
                $CI->ion_auth->add_to_group(2, $user->id);
            }
        }
    }
    
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
}

function karyawan_after_update($postdata, $primary, $xcrud) {
    // your code
}

function karyawan_before_remove($primary, $xcrud) {
    // your code
}

function karyawan_after_remove($primary, $xcrud) {
    // your code
    $user = AuthUsers::where('id_peg', $primary)->first();
    if ($user) {
        $user->delete();
    }
}