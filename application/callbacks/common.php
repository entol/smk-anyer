<?php

if (!function_exists('info_behaviours')) {
	function info_behaviours($postdata, $isNewRecord = false) {
		$CI =& get_instance();

		if ($isNewRecord === true) {
			$postdata->set('created_at', date('Y-m-d H:i:s'));
			$postdata->set('created_by', $CI->ion_auth->user_data('id'));
		}
		$postdata->set('updated_at', date('Y-m-d H:i:s'));
		$postdata->set('updated_by', $CI->ion_auth->user_data('id'));
		$postdata->set('ip_address', $CI->input->ip_address());
		$postdata->set('useragent', $CI->agent->agent_string());
	}
}

if (!function_exists('read_useragent')) {
	function read_useragent($value, $fieldname, $primary_key, $row, $xcrud) {
		$CI =& get_instance();
		$CI->agent->parse($value);

		return '<b>OS</b>: '. $CI->agent->platform() .'<br /><b>Browser</b>: '. $CI->agent->browser() .' '. $CI->agent->version();
	}
}

if (!function_exists('read_created_by')) {
	function read_created_by($value, $fieldname, $primary_key, $row, $xcrud) {
		$user = AuthUsers::find($value);
		if ($user) {
			$pegawai = $user->pegawai;
			if ($pegawai) {
				return $pegawai->nama;
			}

			return $user->username;
		}
		return null;
	}
}

if (!function_exists('read_updated_by')) {
	function read_updated_by($value, $fieldname, $primary_key, $row, $xcrud) {
		$user = AuthUsers::find($value);
		if ($user) {
			$pegawai = $user->pegawai;
			if ($pegawai) {
				return $pegawai->nama;
			}

			return $user->username;
		}
		return null;
	}
}

if (!function_exists('read_nama_pegawai')) {
	function read_nama_pegawai($value, $fieldname, $primary_key, $row, $xcrud) {
		$nama = '';
		if (isset($row['peg_pegawai.gelar_depan']) && trim($row['peg_pegawai.gelar_depan']) != '') {
			$nama .= $row['peg_pegawai.gelar_depan'] .' ';
		}
		$nama .= $row['peg_pegawai.nama'];
		if (isset($row['peg_pegawai.gelar_belakang']) && trim($row['peg_pegawai.gelar_belakang']) != '') {
			$nama .= ', '. $row['peg_pegawai.gelar_belakang'];
		}

		return $nama;
	}
}