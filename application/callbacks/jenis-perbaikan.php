<?php

function jenis_perbaikan_before_insert($postdata, $xcrud) {
    $CI =& get_instance();
    $postdata->set('created_at', date('Y-m-d H:i:s'));
    $postdata->set('created_by', $CI->ion_auth->user_data('id'));
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
}

function jenis_perbaikan_after_insert($postdata, $primary, $xcrud) {
    // your code
}

function jenis_perbaikan_before_update($postdata, $primary, $xcrud) {
    $CI =& get_instance();
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
}

function jenis_perbaikan_after_update($postdata, $primary, $xcrud) {
    // your code
}

function jenis_perbaikan_before_remove($primary, $xcrud) {
    // your code
}

function jenis_perbaikan_after_remove($primary, $xcrud) {
    // your code
}