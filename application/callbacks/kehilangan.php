<?php

use app\models\Kehilangan;
use app\models\BarangItems;

function kehilangan_before_insert($postdata, $xcrud) {
    $CI =& get_instance();
    $kodeLokasi = $postdata->get('kode_lokasi');
    $kodeBarang = $postdata->get('kode_barang');
    $kondisi = $postdata->get('kondisi');
    
    $barangItem = BarangItems::where('kode_barang', $kodeBarang)->where(function($query) use ($kodeLokasi) {
        $query->where('lokasi_gudang', $kodeLokasi)->orWhere('lokasi_project', $kodeLokasi);
    })->first();
    
    if ($barangItem) {
        if ($kondisi == 'G' && $barangItem->qty_baik <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
        if ($kondisi == 'R' && $barangItem->qty_perbaikan <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
        if ($kondisi == 'D' && $barangItem->qty_rusak <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
    }

    $postdata->set('created_at', date('Y-m-d H:i:s'));
    $postdata->set('created_by', $CI->ion_auth->user_data('id'));
    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
    
    $draft = $postdata->get('draft');
    if ($draft == 'Y') {
        $postdata->set('status', 'DR');
    } else {
        $postdata->set('status', 'RQ');
    }
}

function kehilangan_after_insert($postdata, $primary, $xcrud) {
    // your code
    Kehilangan::generateKode($primary);
}

function kehilangan_before_update($postdata, $primary, $xcrud) {
    $CI =& get_instance();
    $kodeLokasi = $postdata->get('kode_lokasi');
    $kodeBarang = $postdata->get('kode_barang');
    $kondisi = $postdata->get('kondisi');
    
    $barangItem = BarangItems::where('kode_barang', $kodeBarang)->where(function($query) use ($kodeLokasi) {
        $query->where('lokasi_gudang', $kodeLokasi)->orWhere('lokasi_project', $kodeLokasi);
    })->first();

    if ($barangItem) {
        if ($kondisi == 'G' && $barangItem->qty_baik <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
        if ($kondisi == 'R' && $barangItem->qty_perbaikan <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
        if ($kondisi == 'D' && $barangItem->qty_rusak <= 0) {
            return $xcrud->set_exception('kondisi', 'Material tidak ditemukan pada kondisi ini.', 'error');
        }
    }

    $postdata->set('updated_at', date('Y-m-d H:i:s'));
    $postdata->set('updated_by', $CI->ion_auth->user_data('id'));
    $postdata->set('ip_address', $CI->input->ip_address());
    $postdata->set('useragent', $CI->agent->agent_string());
    
    $draft = $postdata->get('draft');
    if ($draft == 'Y') {
        $postdata->set('status', 'DR');
    } else {
        $postdata->set('status', 'RQ');
    }
}

function kehilangan_after_update($postdata, $primary, $xcrud) {
    // your code
}

function kehilangan_before_remove($primary, $xcrud) {
    // your code
}

function kehilangan_after_remove($primary, $xcrud) {
    // your code
}