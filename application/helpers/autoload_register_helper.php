<?php
	
spl_autoload_register(function ($className) {
    if (strpos($className, 'app') === 0) {
        $fileName = str_replace('app\\', APPPATH, $className) .'.php';
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $fileName);
        if (file_exists($fileName)) {
            require $fileName;
            return true;
        }
    }
    return false;
});