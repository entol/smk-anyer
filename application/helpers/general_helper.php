<?php

function array_get_value($array, $key, $default = null)
{
	if (is_array($array) && isset($array[$key])) {
		return $array[$key];
	}
	if (is_object($array) && isset($array->{$key})) {
		return $array->{$key};
	}
	return $default;
}

function array_remove(&$array, $key)
{
	if (is_array($array) && isset($array[$key])) {
		$value = $array[$key];
		unset($array[$key]);
		
		return $value;
	}
	return null;
}

function array_remove_value(&$array, $value)
{
	if (is_array($array)) {
		foreach($array as $key => $val) {
			if ($val == $value) {
				unset($array[$key]);
			}
		}
	}

	return $array;
}

function human_filesize($bytes, $decimals = 2) {
	if ($bytes >= 1125899906842624) {
        $bytes = number_format($bytes / 1125899906842624, $decimals) . ' PB';
	} elseif ($bytes >= 1099511627776) {
        $bytes = number_format($bytes / 1099511627776, $decimals) . ' TB';
	} elseif ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, $decimals) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, $decimals) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, $decimals) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function nbsp($length = 1) {
    $html = '';
    for($i = 0; $i < $length; $i++) {
        $html .= '&nbsp;';
    }
    return $html;
}