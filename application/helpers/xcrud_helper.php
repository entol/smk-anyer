<?php

require_once APPPATH . 'third_party/xcrud/xcrud.php';

if (!function_exists('xcrud_get_instance')) {
    function xcrud_get_instance($name = false) {
        $langAbbr = [
            'english' => 'en',
            'indonesian' => 'id'
        ];

        if ($name === false) {
            $name = time() .'-'. rand(0, 1000) . rand(0, 1000);
            $name = md5($name);
        }

        $CI = &get_instance();
        Xcrud_config::$scripts_url = base_url('');
        Xcrud_config::$language = array_get_value($langAbbr, $CI->app->config('language'), 'en');
        $xcrud = Xcrud::get_instance($name);
        return $xcrud;
    }
}

if (!function_exists('xcrud_store_session')) {
    function xcrud_store_session() {
        $CI = &get_instance();
        $CI->session->set_userdata(array('xcrud_sess' => Xcrud::export_session()));
    }
}

if (!function_exists('xcrud_restore_session')) {
    function xcrud_restore_session() {
        $CI = &get_instance();
        Xcrud::import_session($CI->session->userdata('xcrud_sess'));
    }
}

