<?php

namespace app\models;

class Karyawan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_karyawan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}