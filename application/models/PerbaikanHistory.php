<?php

namespace app\models;

class PerbaikanHistory extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_perbaikan_history';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public function perbaikan()
    {
    	return $this->hasOne('app\models\Perbaikan', 'id', 'id_perbaikan');
    }
    
    public static function createHistory($id, $status)
    {
        $agent = get_instance()->agent->agent_string();
        $ip = get_instance()->input->ip_address();
        $userId = get_instance()->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        
        $history = PerbaikanHistory::where('id_perbaikan', $id)->orderBy('id', 'desc')->first();
        
        // create draft 
        if (!$history && $status == 'RQ') {
            $draft = new PerbaikanHistory;
            $draft->id_perbaikan = $id;
            $draft->status = 'DR';
            $draft->created_at = $currentDateTime;
            $draft->created_by = $userId;
            $draft->updated_at = $currentDateTime;
            $draft->updated_by = $userId;
            $draft->ip_address = $ip;
            $draft->useragent = $agent;
            $draft->save();
        }
        
        $history = PerbaikanHistory::where('id_perbaikan', $id)->orderBy('id', 'desc')->first();
        
        if (($history && $history->status != $status) || !$history) {
            $other = new PerbaikanHistory;
            $other->id_perbaikan = $id;
            $other->status = $status;
            $other->created_at = $currentDateTime;
            $other->created_by = $userId;
            $other->updated_at = $currentDateTime;
            $other->updated_by = $userId;
            $other->ip_address = $ip;
            $other->useragent = $agent;
            $other->save();
        }
    }
}