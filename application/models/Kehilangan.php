<?php

namespace app\models;

class Kehilangan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_kehilangan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public static function generateKode($id)
    {
        $currentDate = date('Y-m-d');
        $repTpl = 'LST'. date('ymd');
        
        $last = Kehilangan::whereBetween('created_at', [$currentDate .' 00:00:00', $currentDate .' 23:59:59'])
            ->whereNotNull('kode')
            ->orderBy('id', 'desc')
            ->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = (int) str_replace($repTpl, '', $last->kode);
        }
        
        $model = Kehilangan::find($id);
        $model->kode = $repTpl . ($increment + 1);
        $model->save();
    }
    
    public function barang()
    {
        $lokasi = $this->kode_lokasi;
        return $this->hasOne('app\models\BarangItems', 'kode_barang', 'kode_barang')
            ->where(function($query) use ($lokasi) {
                $query->where('lokasi_gudang', $lokasi)->orWhere('lokasi_project', $lokasi);
            });
    }
}