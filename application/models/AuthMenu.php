<?php

class AuthMenu extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_menu';
    public $timestamps = false;
    public $incrementing = true;
}