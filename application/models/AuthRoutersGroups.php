<?php

class AuthRoutersGroups extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_routers_groups';
    public $timestamps = false;
    public $incrementing = false;
}