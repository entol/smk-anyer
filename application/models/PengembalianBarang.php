<?php

namespace app\models;

class PengembalianBarang extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_pengembalian_barang';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public function pengembalian()
    {
    	return $this->hasOne('app\models\Pengembalian', 'id', 'id_pengembalian');
    }
    
    public function barang()
    {
    	return $this->hasOne('app\models\Barang', 'kode', 'kode_barang');
    }
    
    public function barangItem()
    {
        return $this->hasOne('app\models\BarangItems', 'kode_barang', 'kode_barang')
            ->where('lokasi', 'P')
            ->where('lokasi_project', $this->pengembalian->kode_project);
    }
}