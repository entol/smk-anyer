<?php

namespace app\models;

class Status extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_status';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}