<?php

namespace app\models;

class BarangVd extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_barang_vd';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}