<?php

class AuthUsers extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_users';
    public $timestamps = false;
    public $incrementing = true;

    public function pegawai()
    {
    	return $this->belongsTo('\app\modules\peg\models\Pegawai', 'id_peg', 'id');
    }
}