<?php

namespace app\models;

class RequestBarang extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_request_barang';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public function request()
    {
    	return $this->hasOne('app\models\Request', 'id', 'id_request');
    }
    
    public function barang()
    {
    	// return $this->hasOne('app\models\BarangVd', 'kode', 'kode_barang')
    	return $this->hasOne('app\models\Barang', 'kode', 'kode_barang');
    }
}