<?php

class Background extends Illuminate\Database\Eloquent\Model
{
    public $table = 'c_background';
    public $timestamps = false;
    public $incrementing = true;
}