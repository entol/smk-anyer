<?php

namespace app\models;

class ProjectMember extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_project_member';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}