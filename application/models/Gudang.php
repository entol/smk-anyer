<?php

namespace app\models;

class Gudang extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_gudang';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}