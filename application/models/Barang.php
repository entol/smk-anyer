<?php

namespace app\models;

class Barang extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_barang';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            $model->afterCreate();
        });
        self::updated(function($model) {
            $model->afterUpdate();
        });
    }

    public function afterCreate()
    {   
        $this->createItem();
    }

    public function afterUpdate()
    {
        if ($this->lokasi == 'W') {
            $barangItem = BarangItems::where('kode_barang', $this->kode)->where('lokasi_gudang', $this->lokasi_gudang)->first();
        } else {
            $barangItem = BarangItems::where('kode_barang', $this->kode)->where('lokasi_project', $this->lokasi_project)->first();
        }

        if ($barangItem) {
            $items = $this->items;
            if (is_array($items) || is_object($items)) {
                $totalQty = 0;
                foreach($items as $item) {
                    $totalQty = $totalQty + $item->qty_baik;
                    $totalQty = $totalQty + $item->qty_perbaikan;
                    $totalQty = $totalQty + $item->qty_rusak;
                    $totalQty = $totalQty + $item->qty_hilang;
                }
    
                if ($totalQty != $this->qty) {
                    $sesuaikan = $this->qty - $totalQty;

                    $item->qty_baik = $item->qty_baik + $sesuaikan;
                    $item->updated_at = $this->updated_at;
                    $item->updated_by = $this->updated_by;
                    $item->ip_address = $this->ip_address;
                    $item->useragent = $this->useragent;
                    $item->save();
                }
            }
        } else {
            $this->createItem();
        }
    }

    private function createItem()
    {
        $lokasi = new BarangItems;
        $lokasi->kode_barang = $this->kode;
        $lokasi->lokasi = $this->lokasi;
        $lokasi->lokasi_project = $this->lokasi_project;
        $lokasi->lokasi_gudang = $this->lokasi_gudang;
        $lokasi->qty_baik = $this->qty;
        $lokasi->qty_perbaikan = 0;
        $lokasi->qty_rusak = 0;
        $lokasi->qty_hilang = 0;
        $lokasi->created_at = $this->created_at;
        $lokasi->created_by = $this->created_by;
        $lokasi->updated_at = $this->updated_at;
        $lokasi->updated_by = $this->updated_by;
        $lokasi->ip_address = $this->ip_address;
        $lokasi->useragent = $this->useragent;
        $lokasi->save();
    }

    public function items()
    {
    	return $this->hasMany('app\models\BarangItems', 'kode_barang', 'kode');
    }

    public function unit()
    {
    	return $this->hasOne('app\models\Satuan', 'kode', 'satuan');
    }

    public function condition()
    {
    	return $this->hasOne('app\models\Kondisi', 'kode', 'kondisi');
    }

    public function kategori()
    {
    	return $this->hasOne('app\models\KategoriMaterial', 'kode', 'kode_kategori');
    }

    public function gudang()
    {
        return $this->hasOne('app\models\Gudang', 'kode', 'lokasi_gudang');
    }

    public function project()
    {
        return $this->hasOne('app\models\Project', 'kode', 'lokasi_project');
    }

    public function getNamaLokasi()
    {
        if ($this->lokasi == 'P') {
            return $this->lokasi_project .' - '. $this->project->singkatan;
        } else {
            return $this->lokasi_gudang .' - '. $this->gudang->gudang;
        }
    }
}