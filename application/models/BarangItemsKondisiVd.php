<?php

namespace app\models;

class BarangItemsKondisiVd extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_barang_items_kondisi_vd';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function barang()
    {
        return $this->hasOne('app\models\Barang', 'kode', 'kode_barang');
    }
    
    public function gudang()
    {
        return $this->hasOne('app\models\Gudang', 'kode', 'lokasi_gudang');
    }

    public function project()
    {
        return $this->hasOne('app\models\Project', 'kode', 'lokasi_project');
    }

    public function condition()
    {
    	return $this->hasOne('app\models\Kondisi', 'kode', 'kondisi');
    }

    public function getNamaLokasi()
    {
        if ($this->lokasi == 'P') {
            return $this->lokasi_project .' - '. $this->project->singkatan;
        } else {
            return $this->lokasi_gudang .' - '. $this->gudang->gudang;
        }
    }

}