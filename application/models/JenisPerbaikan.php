<?php

namespace app\models;

class JenisPerbaikan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_jenis_perbaikan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}