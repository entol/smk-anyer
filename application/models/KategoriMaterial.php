<?php

namespace app\models;

class KategoriMaterial extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_kategori_material';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}