<?php

namespace app\models;

class PerbaikanParts extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_perbaikan_parts';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}