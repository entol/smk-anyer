<?php

namespace app\models;

class PengembalianHistory extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_pengembalian_history';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public function pengembalian()
    {
    	return $this->hasOne('app\models\Pengembalian', 'id', 'id_pengembalian');
    }
    
    public static function createHistory($id, $status)
    {
        $agent = get_instance()->agent->agent_string();
        $ip = get_instance()->input->ip_address();
        $userId = get_instance()->ion_auth->user_data('id');
        $currentDateTime = date('Y-m-d H:i:s');
        
        $history = PengembalianHistory::where('id_pengembalian', $id)->orderBy('id', 'desc')->first();
        
        // create draft 
        if (!$history && $status == 'RT') {
            $draft = new PengembalianHistory;
            $draft->id_pengembalian = $id;
            $draft->status = 'DR';
            $draft->created_at = $currentDateTime;
            $draft->created_by = $userId;
            $draft->updated_at = $currentDateTime;
            $draft->updated_by = $userId;
            $draft->ip_address = $ip;
            $draft->useragent = $agent;
            $draft->save();
        }
        
        $history = PengembalianHistory::where('id_pengembalian', $id)->orderBy('id', 'desc')->first();
        
        if (($history && $history->status != $status) || !$history) {
            $other = new PengembalianHistory;
            $other->id_pengembalian = $id;
            $other->status = $status;
            $other->status_lama = $history ? $history->status : null;
            $other->created_at = $currentDateTime;
            $other->created_by = $userId;
            $other->updated_at = $currentDateTime;
            $other->updated_by = $userId;
            $other->ip_address = $ip;
            $other->useragent = $agent;
            $other->save();
        }
    }
} 