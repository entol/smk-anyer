<?php

namespace app\models;

class Request extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_request';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function save(array $options = [])
    {
        $result = parent::save($options);
        RequestHistory::createHistory($this->id, $this->status);
        return $result;
    }
    
    public function projectTujuan()
    {
    	return $this->hasOne('app\models\Project', 'kode', 'kode_project');
    }
    
    public function items()
    {
    	return $this->hasMany('app\models\RequestBarang', 'id_request', 'id');
    }
    
    public function itemsApproved()
    {
    	return $this->hasMany('app\models\RequestBarang', 'id_request', 'id')->where('approved', 'Y');
    }

    public function projectAsal()
    {
        return $this->hasOne('app\models\Project', 'kode', 'kode_project_asal');
    }

    public function gudangAsal()
    {
        return $this->hasOne('app\models\Gudang', 'kode', 'kode_gudang_asal');
    }

    public function suratJalan()
    {
        return $this->hasOne('app\models\SuratJalan', 'kode_request', 'kode');
    }
}