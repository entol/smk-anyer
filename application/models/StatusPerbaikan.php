<?php

namespace app\models;

class StatusPerbaikan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_status_perbaikan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}