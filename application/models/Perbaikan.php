<?php

namespace app\models;

class Perbaikan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_perbaikan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function save(array $options = [])
    {
        $result = parent::save($options);
        PerbaikanHistory::createHistory($this->id, $this->status);
        return $result;
    }
    
    public static function generateKode($id)
    {
        $currentDate = date('Y-m-d');
        $repTpl = 'REP'. date('ymd');
        
        $last = Perbaikan::whereBetween('created_at', [$currentDate .' 00:00:00', $currentDate .' 23:59:59'])
            ->whereNotNull('kode')
            ->orderBy('id', 'desc')
            ->first();
        if (!$last) {
            $increment = 0;
        } else {
            $increment = (int) str_replace($repTpl, '', $last->kode);
        }
        
        $model = Perbaikan::find($id);
        $model->kode = $repTpl . ($increment + 1);
        $model->save();
    }
    
    public function barang()
    {
        $lokasi = $this->kode_lokasi;
        return $this->hasOne('app\models\BarangItems', 'kode_barang', 'kode_barang')
            ->where(function($query) use ($lokasi) {
                $query->where('lokasi_gudang', $lokasi)->orWhere('lokasi_project', $lokasi);
            });
    }
}