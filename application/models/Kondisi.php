<?php

namespace app\models;

class Kondisi extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_kondisi';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}