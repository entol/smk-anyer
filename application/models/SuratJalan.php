<?php

namespace app\models;

class SuratJalan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_surat_jalan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;
} 