<?php

class AuthButtons extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_buttons';
    public $timestamps = false;
    public $incrementing = true;
}