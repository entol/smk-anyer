<?php

class AuthGroups extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_groups';
    public $timestamps = false;
    public $incrementing = false;
    
    public function userGroups()
    {
    	return $this->hasMany('AuthUsersGroups', 'group_id', 'id');
    }
}