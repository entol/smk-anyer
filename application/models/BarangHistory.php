<?php

namespace app\models;

class BarangHistory extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_barang_history';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}