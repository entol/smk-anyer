<?php

namespace app\models;

class Satuan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_satuan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}