<?php

class AuthButtonsGroups extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_buttons_groups';
    public $timestamps = false;
    public $incrementing = true;
}