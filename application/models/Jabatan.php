<?php

namespace app\models;

class Jabatan extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_jabatan';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}