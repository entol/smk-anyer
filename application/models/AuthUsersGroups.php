<?php

class AuthUsersGroups extends Illuminate\Database\Eloquent\Model
{
    public $table = 'a_users_groups';
    public $timestamps = false;
    public $incrementing = false;
    
    public function user()
    {
    	return $this->belongsTo('\AuthUsers', 'user_id', 'id');
    }
}