<?php

class Setting extends Illuminate\Database\Eloquent\Model
{
    public $table = 'c_setting';
    public $timestamps = false;
    public $incrementing = true;
}