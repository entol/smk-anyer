<?php

namespace app\models;

class BarangItems extends \Illuminate\Database\Eloquent\Model
{
    public $table = 'm_barang_items';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}