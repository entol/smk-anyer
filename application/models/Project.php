<?php

namespace app\models;

class Project extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_project';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    
    public static function getMyProject($kode = false)
    {
        $idPeg = get_instance()->ion_auth->user_data('id_peg');
        $dp = Project::where('manager', $idPeg)->get();
        
        if ($kode === true) {
            $projects = $dp->keyBy('kode')->toArray();
            $listKode = [];
            if ($projects && count($projects)) {
                $listKode = array_keys($projects);
            }
            
            return $listKode;
        }
        
        return $dp;
    }
}