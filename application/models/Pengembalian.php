<?php

namespace app\models;

class Pengembalian extends \Illuminate\Database\Eloquent\Model
{
    public $table = 't_pengembalian';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function save(array $options = [])
    {
        $result = parent::save($options);
        PengembalianHistory::createHistory($this->id, $this->status);
        return $result;
    }
    
    public function project()
    {
    	return $this->hasOne('app\models\Project', 'kode', 'kode_project');
    }
    
    public function gudang()
    {
    	return $this->hasOne('app\models\Gudang', 'kode', 'kode_gudang');
    }
    
    public function items()
    {
    	return $this->hasMany('app\models\PengembalianBarang', 'id_pengembalian', 'id');
    }

    public function suratJalan()
    {
        return $this->hasOne('app\models\SuratJalan', 'kode_request', 'kode');
    }
}