<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

// Check routing and session
$hook['post_controller_constructor'][] = array(
    "class"    => "Auth_hook",// any name of class that you want
    "function" => "validate",// a method of class
    "filename" => "Auth_hook.php",// where the class declared
    "filepath" => "hooks"// this is location inside application folder
);

// Setting language
$hook['post_controller_constructor'][] = array(
    "class"    => "Language_hook",// any name of class that you want
    "function" => "validate",// a method of class
    "filename" => "Language_hook.php",// where the class declared
    "filepath" => "hooks"// this is location inside application folder
);