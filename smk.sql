/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : smk_framework

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 26/09/2023 22:45:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for a_buttons
-- ----------------------------
DROP TABLE IF EXISTS `a_buttons`;
CREATE TABLE `a_buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `identity` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_buttons
-- ----------------------------
BEGIN;
INSERT INTO `a_buttons` VALUES (1, 'Maintenance Selesai', 'maintenance-selesai');
INSERT INTO `a_buttons` VALUES (2, 'Maintenance Proses', 'maintenance-proses');
INSERT INTO `a_buttons` VALUES (3, 'Create Material', 'material-create');
INSERT INTO `a_buttons` VALUES (4, 'Update Material', 'material-update');
INSERT INTO `a_buttons` VALUES (5, 'Material Delete', 'material-delete');
INSERT INTO `a_buttons` VALUES (6, 'Request Create', 'request-create');
INSERT INTO `a_buttons` VALUES (7, 'Request Update', 'request-update');
INSERT INTO `a_buttons` VALUES (8, 'Request Delete', 'request-delete');
INSERT INTO `a_buttons` VALUES (9, 'Request Validation Accept', 'request-validation-accept');
INSERT INTO `a_buttons` VALUES (10, 'Request Validation Don\'t Accept', 'request-validation-dont-accept');
INSERT INTO `a_buttons` VALUES (11, 'Pengiriman Proses', 'pengiriman-proses');
INSERT INTO `a_buttons` VALUES (12, 'Pengiriman Surat Jalan', 'pengiriman-surat-jalan');
INSERT INTO `a_buttons` VALUES (13, 'Pengembalian Create', 'pengembalian-create');
INSERT INTO `a_buttons` VALUES (14, 'Pengembalian Update', 'pengembalian-update');
INSERT INTO `a_buttons` VALUES (15, 'Pengembalian Delete', 'pengembalian-delete');
INSERT INTO `a_buttons` VALUES (16, 'Pengembalian Surat Jalan', 'pengembalian-surat-jalan');
INSERT INTO `a_buttons` VALUES (17, 'Pengembalian Terima', 'pengembalian-terima');
INSERT INTO `a_buttons` VALUES (18, 'Maintenance Create', 'maintenance-create');
INSERT INTO `a_buttons` VALUES (19, 'Maintenance Update', 'maintenance-update');
INSERT INTO `a_buttons` VALUES (20, 'Maintenance Delete', 'maintenance-delete');
INSERT INTO `a_buttons` VALUES (21, 'Karyawan Create', 'karyawan-create');
INSERT INTO `a_buttons` VALUES (22, 'Karyawan Update', 'karyawan-update');
INSERT INTO `a_buttons` VALUES (23, 'Karyawan Delete', 'karyawan-delete');
INSERT INTO `a_buttons` VALUES (24, 'Project Create', 'project-create');
INSERT INTO `a_buttons` VALUES (25, 'Project Update', 'project-update');
INSERT INTO `a_buttons` VALUES (26, 'Project Delete', 'project-delete');
INSERT INTO `a_buttons` VALUES (27, 'Pengembalian Batal', 'pengembalian-batal');
INSERT INTO `a_buttons` VALUES (28, 'Kehilangan Create', 'kehilangan-create');
INSERT INTO `a_buttons` VALUES (29, 'Kehilangan Update', 'kehilangan-update');
INSERT INTO `a_buttons` VALUES (30, 'Kehilangan Delete', 'kehilangan-delete');
INSERT INTO `a_buttons` VALUES (31, 'Kehilangan Proses', 'kehilangan-proses');
INSERT INTO `a_buttons` VALUES (32, 'Kehilangan Batal', 'kehilangan-batal');
COMMIT;

-- ----------------------------
-- Table structure for a_buttons_groups
-- ----------------------------
DROP TABLE IF EXISTS `a_buttons_groups`;
CREATE TABLE `a_buttons_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `button_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `expression` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_buttons_groups
-- ----------------------------
BEGIN;
INSERT INTO `a_buttons_groups` VALUES (2, 2, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (3, 1, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (4, 3, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (5, 3, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (6, 4, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (7, 4, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (8, 5, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (9, 5, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (10, 6, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (11, 6, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (12, 6, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (13, 7, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (14, 7, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (15, 7, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (16, 8, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (17, 8, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (18, 8, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (19, 9, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (20, 9, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (21, 9, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (22, 10, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (23, 10, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (24, 10, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (25, 11, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (26, 11, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (27, 12, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (28, 12, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (29, 11, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (30, 12, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (31, 14, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (32, 14, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (33, 14, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (34, 15, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (35, 15, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (36, 15, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (37, 13, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (38, 13, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (39, 13, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (40, 17, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (41, 17, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (43, 16, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (44, 16, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (45, 16, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (46, 19, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (47, 19, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (48, 19, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (49, 1, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (50, 1, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (51, 2, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (52, 2, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (53, 20, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (54, 20, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (55, 20, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (56, 18, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (57, 18, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (58, 18, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (59, 21, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (60, 21, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (61, 23, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (62, 23, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (63, 22, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (64, 22, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (65, 25, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (66, 25, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (67, 25, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (68, 26, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (69, 26, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (70, 24, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (71, 24, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (72, 27, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (73, 27, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (74, 27, 11, NULL);
INSERT INTO `a_buttons_groups` VALUES (75, 30, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (76, 30, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (77, 30, 11, NULL);
INSERT INTO `a_buttons_groups` VALUES (78, 30, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (79, 29, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (80, 29, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (81, 29, 11, NULL);
INSERT INTO `a_buttons_groups` VALUES (82, 29, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (83, 28, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (84, 28, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (85, 28, 11, NULL);
INSERT INTO `a_buttons_groups` VALUES (86, 28, 9, NULL);
INSERT INTO `a_buttons_groups` VALUES (87, 31, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (88, 31, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (89, 31, 11, NULL);
INSERT INTO `a_buttons_groups` VALUES (90, 32, 1, NULL);
INSERT INTO `a_buttons_groups` VALUES (91, 32, 10, NULL);
INSERT INTO `a_buttons_groups` VALUES (92, 32, 11, NULL);
COMMIT;

-- ----------------------------
-- Table structure for a_groups
-- ----------------------------
DROP TABLE IF EXISTS `a_groups`;
CREATE TABLE `a_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_groups
-- ----------------------------
BEGIN;
INSERT INTO `a_groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `a_groups` VALUES (10, 'matecon', 'User Access');
COMMIT;

-- ----------------------------
-- Table structure for a_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `a_login_attempts`;
CREATE TABLE `a_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_login_attempts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for a_menu
-- ----------------------------
DROP TABLE IF EXISTS `a_menu`;
CREATE TABLE `a_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `controller_name` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `sort_order` int(11) DEFAULT 1000,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_menu
-- ----------------------------
BEGIN;
INSERT INTO `a_menu` VALUES (1, 'Dashboard', 'fa fa-home', 'site/index', NULL, 'siteController', 'actionIndex', NULL, 'dashboard', 0);
INSERT INTO `a_menu` VALUES (13, 'Authentication', 'fa fa-quote-right', NULL, NULL, NULL, NULL, NULL, 'authentiaction', 102);
INSERT INTO `a_menu` VALUES (14, 'Users', NULL, 'auth/index', 13, 'authController', 'actionIndex', NULL, 'auth-users', 1000);
INSERT INTO `a_menu` VALUES (15, 'Groups', NULL, 'auth/groups', 13, 'authController', 'actionGroups', NULL, 'auth-groups', 1000);
INSERT INTO `a_menu` VALUES (16, 'User Groups', NULL, 'auth/user-groups', 13, 'authController', 'actionUserGroups', NULL, 'auth-users-groups', 1000);
INSERT INTO `a_menu` VALUES (17, 'Router Groups', NULL, 'auth/router-groups', 13, 'authController', 'actionRouterGroups', NULL, 'auth-router-groups', 1000);
INSERT INTO `a_menu` VALUES (18, 'Menu', NULL, 'menu/index', 13, 'menuController', 'actionIndex', NULL, 'auth-menu', 1000);
INSERT INTO `a_menu` VALUES (19, 'General', NULL, 'setting/index', 39, 'settingController', 'actionIndex', NULL, 'setting', 1);
INSERT INTO `a_menu` VALUES (39, 'Settings', 'fa fa-cogs', NULL, NULL, NULL, NULL, NULL, NULL, 103);
INSERT INTO `a_menu` VALUES (40, 'Background', NULL, 'setting/background', 39, NULL, NULL, NULL, 'setting-background', 1000);
INSERT INTO `a_menu` VALUES (43, 'Master Data', 'fa fa-database', NULL, NULL, NULL, NULL, NULL, NULL, 100);
INSERT INTO `a_menu` VALUES (51, 'Buttons', NULL, 'auth/buttons', 13, 'authController', 'actionButtons', NULL, 'auth-buttons', 1000);
INSERT INTO `a_menu` VALUES (94, 'Jabatan', NULL, 'jabatan/index', 43, 'jabatanController', 'actionIndex', NULL, 'm-jabatan', 8);
INSERT INTO `a_menu` VALUES (95, 'Karyawan', NULL, 'karyawan/index', 43, 'karyawanController', 'actionIndex', NULL, 'karyawan', 11);
INSERT INTO `a_menu` VALUES (97, 'Gudang', NULL, 'gudang/index', 43, 'gudangController', 'actionIndex', NULL, 'm-gudang', 4);
INSERT INTO `a_menu` VALUES (98, 'Kondisi Material', NULL, 'kondisi/index', 43, 'kondisiController', 'actionIndex', NULL, 'm-kondisi', 4);
INSERT INTO `a_menu` VALUES (99, 'Status', NULL, 'status/index', 43, 'statusController', 'actionIndex', NULL, 'm-status', 1000);
COMMIT;

-- ----------------------------
-- Table structure for a_routers_groups
-- ----------------------------
DROP TABLE IF EXISTS `a_routers_groups`;
CREATE TABLE `a_routers_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_name` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_routers_groups
-- ----------------------------
BEGIN;
INSERT INTO `a_routers_groups` VALUES (34, 'barangController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (35, 'barangItemsController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (36, 'gudangController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (37, 'jabatanController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (38, 'karyawanController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (39, 'kategoriMaterialController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (40, 'kondisiController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (41, 'lokasiBarangController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (42, 'menuController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (55, 'settingController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (56, 'settingController', 'actionBackground', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (57, 'siteController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (58, 'statusController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (59, 'userController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (60, 'xcrudAjaxController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (61, 'xiiController', 'actionIndex', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (62, 'xiiController', 'actionView', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (63, 'xiiController', 'actionGenerate', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (64, 'xiiController', 'actionGenerateFile', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (65, 'xiiController', 'actionGetColumns', NULL, 9);
INSERT INTO `a_routers_groups` VALUES (314, 'monitoringPerbaikanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (315, 'monitoringRequestController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (316, 'penerimaanBarangController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (317, 'penerimaanBarangController', 'actionTerima', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (318, 'penerimaanPengembalianController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (319, 'penerimaanPengembalianController', 'actionTerima', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (320, 'pengembalianController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (321, 'pengembalianController', 'actionCreate', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (322, 'pengembalianController', 'actionUpdate', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (323, 'pengembalianController', 'actionCreateProcess', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (324, 'pengembalianController', 'actionUpdateProcess', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (325, 'pengembalianController', 'actionKirim', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (326, 'pengembalianController', 'actionLookupBarang', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (327, 'pengirimanBarangController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (328, 'pengirimanBarangController', 'actionProses', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (329, 'pengirimanBarangController', 'actionProsesSemua', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (330, 'pengirimanBarangController', 'actionProsesSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (331, 'perbaikanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (332, 'perbaikanController', 'actionLists', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (333, 'perbaikanController', 'actionProses', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (334, 'perbaikanController', 'actionFixed', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (335, 'perbaikanController', 'actionFailed', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (336, 'projectController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (337, 'requestController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (338, 'requestController', 'actionCreate', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (339, 'requestController', 'actionUpdate', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (340, 'requestController', 'actionCreateProcess', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (341, 'requestController', 'actionUpdateProcess', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (342, 'requestController', 'actionLookupBarang', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (343, 'requestValidationController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (344, 'requestValidationController', 'actionApprove', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (345, 'requestValidationController', 'actionUpdateQty', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (346, 'settingController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (347, 'settingController', 'actionBackground', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (348, 'siteController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (349, 'statusController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (350, 'statusPerbaikanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (351, 'userController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (352, 'xcrudAjaxController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (398, 'pengirimanBarangController', 'actionProses', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (399, 'pengirimanBarangController', 'actionProsesSemua', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (400, 'pengirimanBarangController', 'actionProsesSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (401, 'perbaikanController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (402, 'perbaikanController', 'actionLists', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (403, 'perbaikanController', 'actionProses', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (404, 'perbaikanController', 'actionFixed', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (405, 'perbaikanController', 'actionFailed', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (406, 'statusPerbaikanController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (407, 'barangController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (408, 'gudangController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (409, 'jabatanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (410, 'jenisPerbaikanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (411, 'karyawanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (412, 'kategoriMaterialController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (413, 'kondisiController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (414, 'menuController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (415, 'monitoringPengembalianController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (416, 'monitoringPerbaikanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (417, 'monitoringRequestController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (418, 'penerimaanBarangController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (419, 'penerimaanBarangController', 'actionTerima', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (420, 'penerimaanPengembalianController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (421, 'penerimaanPengembalianController', 'actionTerima', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (422, 'pengembalianController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (423, 'pengembalianController', 'actionCreate', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (424, 'pengembalianController', 'actionUpdate', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (425, 'pengembalianController', 'actionCreateProcess', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (426, 'pengembalianController', 'actionUpdateProcess', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (427, 'pengembalianController', 'actionKirim', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (428, 'pengembalianController', 'actionLookupBarang', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (429, 'pengirimanBarangController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (430, 'pengirimanBarangController', 'actionProses', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (431, 'pengirimanBarangController', 'actionProsesSemua', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (432, 'pengirimanBarangController', 'actionProsesSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (433, 'perbaikanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (434, 'perbaikanController', 'actionLists', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (435, 'perbaikanController', 'actionProses', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (436, 'perbaikanController', 'actionFixed', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (437, 'perbaikanController', 'actionFailed', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (438, 'projectController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (439, 'requestController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (440, 'requestController', 'actionCreate', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (441, 'requestController', 'actionUpdate', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (442, 'requestController', 'actionCreateProcess', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (443, 'requestController', 'actionUpdateProcess', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (444, 'requestController', 'actionLookupBarang', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (445, 'requestValidationController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (446, 'requestValidationController', 'actionApprove', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (447, 'requestValidationController', 'actionUpdateQty', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (448, 'settingController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (449, 'settingController', 'actionBackground', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (450, 'siteController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (451, 'statusController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (452, 'statusPerbaikanController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (453, 'userController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (454, 'xcrudAjaxController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (455, 'xiiController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (456, 'xiiController', 'actionView', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (457, 'xiiController', 'actionGenerate', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (458, 'xiiController', 'actionGenerateFile', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (459, 'xiiController', 'actionGetColumns', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (460, 'barangController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (464, 'karyawanController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (468, 'monitoringPengembalianController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (469, 'monitoringPerbaikanController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (470, 'monitoringRequestController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (491, 'projectController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (503, 'siteController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (506, 'userController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (507, 'xcrudAjaxController', 'actionIndex', NULL, 2);
INSERT INTO `a_routers_groups` VALUES (513, 'barangController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (514, 'gudangController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (515, 'jabatanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (516, 'jenisPerbaikanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (517, 'karyawanController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (518, 'kategoriMaterialController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (519, 'kondisiController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (520, 'menuController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (521, 'monitoringPengembalianController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (522, 'barangController', 'actionIndex', NULL, 7);
INSERT INTO `a_routers_groups` VALUES (523, 'gudangController', 'actionIndex', NULL, 7);
INSERT INTO `a_routers_groups` VALUES (524, 'reportController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (525, 'reportController', 'actionRekapMaterial', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (526, 'pengirimanBarangController', 'actionLoadSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (527, 'pengirimanBarangController', 'actionCetakSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (528, 'reportController', 'actionHistoryDistribusiMaterial', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (529, 'pengembalianController', 'actionLoadSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (530, 'pengembalianController', 'actionProsesSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (531, 'pengembalianController', 'actionCetakSurat', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (532, 'requestValidationController', 'actionDetail', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (533, 'requestValidationController', 'actionDetailVerifikasi', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (534, 'pengembalianController', 'actionLoadSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (535, 'pengembalianController', 'actionProsesSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (536, 'pengembalianController', 'actionCetakSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (537, 'pengirimanBarangController', 'actionLoadSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (538, 'pengirimanBarangController', 'actionCetakSurat', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (539, 'reportController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (540, 'reportController', 'actionRekapMaterial', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (541, 'reportController', 'actionHistoryDistribusiMaterial', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (542, 'requestValidationController', 'actionDetail', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (543, 'requestValidationController', 'actionDetailVerifikasi', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (544, 'pengembalianController', 'actionLoadSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (545, 'pengembalianController', 'actionProsesSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (546, 'pengembalianController', 'actionCetakSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (547, 'pengirimanBarangController', 'actionLoadSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (548, 'pengirimanBarangController', 'actionCetakSurat', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (549, 'reportController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (550, 'reportController', 'actionRekapMaterial', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (551, 'reportController', 'actionHistoryDistribusiMaterial', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (552, 'requestValidationController', 'actionDetail', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (553, 'requestValidationController', 'actionDetailVerifikasi', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (562, 'barangController', 'actionKelola', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (563, 'kehilanganController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (564, 'kehilanganController', 'actionProses', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (565, 'barangController', 'actionKelola', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (566, 'kehilanganController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (567, 'kehilanganController', 'actionProses', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (568, 'barangController', 'actionKelola', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (569, 'kehilanganController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (570, 'kehilanganController', 'actionProses', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (574, 'kehilanganController', 'actionLists', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (575, 'kehilanganController', 'actionLists', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (576, 'kehilanganController', 'actionLists', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (577, 'monitoringKehilanganController', 'actionIndex', NULL, 1);
INSERT INTO `a_routers_groups` VALUES (578, 'monitoringKehilanganController', 'actionIndex', NULL, 11);
INSERT INTO `a_routers_groups` VALUES (579, 'monitoringKehilanganController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (581, 'siteController', 'actionIndex', NULL, 10);
INSERT INTO `a_routers_groups` VALUES (582, 'reportController', 'actionIndex', NULL, 10);
COMMIT;

-- ----------------------------
-- Table structure for a_users
-- ----------------------------
DROP TABLE IF EXISTS `a_users`;
CREATE TABLE `a_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `id_peg` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `nik` (`nik`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=968 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_users
-- ----------------------------
BEGIN;
INSERT INTO `a_users` VALUES (1, '::1', 'admin', '$2y$07$fFgkA3bPb36vZl1PrLHHUeoA6m3nITUUSd/ZjMazgBi8RLIgacn.O', '', 'admin@admin.com', '', NULL, NULL, 'IIo6hfHyu6SfK6xetnuyUu', 1268889823, 1695742999, 1, '', 'Admin', NULL, 1);
INSERT INTO `a_users` VALUES (967, '::1', 'smk', '$2y$07$xTicMXG99qaB0M/kNmOEq.F1e8SXtBrV/aa0LHKi7jnmskXynDCza', NULL, 'smkanyer@gmail.com', NULL, NULL, NULL, NULL, 1695742238, 1695742554, 1, NULL, 'smk', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for a_users_groups
-- ----------------------------
DROP TABLE IF EXISTS `a_users_groups`;
CREATE TABLE `a_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `a_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `a_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of a_users_groups
-- ----------------------------
BEGIN;
INSERT INTO `a_users_groups` VALUES (1, 1, 1);
INSERT INTO `a_users_groups` VALUES (3, 967, 10);
COMMIT;

-- ----------------------------
-- Table structure for c_background
-- ----------------------------
DROP TABLE IF EXISTS `c_background`;
CREATE TABLE `c_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `background` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c_background
-- ----------------------------
BEGIN;
INSERT INTO `c_background` VALUES (1, 'bg-01', 'gnqafpr6xw084k48ks.jpg');
COMMIT;

-- ----------------------------
-- Table structure for c_data_dictonary
-- ----------------------------
DROP TABLE IF EXISTS `c_data_dictonary`;
CREATE TABLE `c_data_dictonary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `page_title` varchar(200) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `query_string` text DEFAULT NULL,
  `columns` text DEFAULT NULL,
  `fields` text DEFAULT NULL,
  `custom_script` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c_data_dictonary
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for c_setting
-- ----------------------------
DROP TABLE IF EXISTS `c_setting`;
CREATE TABLE `c_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) DEFAULT NULL,
  `app_name` varchar(200) NOT NULL,
  `app_name_abbr` varchar(50) NOT NULL,
  `app_title` varchar(200) NOT NULL,
  `app_description` text DEFAULT NULL,
  `app_keywords` text DEFAULT NULL,
  `theme` varchar(100) NOT NULL,
  `login_theme` varchar(100) NOT NULL,
  `login_background` varchar(20) DEFAULT NULL,
  `login_background_selection` enum('Y','N') DEFAULT NULL,
  `login_description` varchar(255) DEFAULT NULL,
  `allow_register` enum('Y','N') DEFAULT NULL,
  `language` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c_setting
-- ----------------------------
BEGIN;
INSERT INTO `c_setting` VALUES (1, 'SMK Negeri 1 Anyer', 'Codeigniter & XCRUD Framework SMK 1 Anyer', 'IMC', 'Learning PHP Framework', 'Learning PHP Framework using Codeigniter & Xcrud', NULL, 'material', 'login3', '1', 'N', 'Please enter your username and password correctly.', 'N', 'english');
COMMIT;

-- ----------------------------
-- Table structure for m_barang
-- ----------------------------
DROP TABLE IF EXISTS `m_barang`;
CREATE TABLE `m_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `kode_kategori` varchar(10) NOT NULL,
  `model` varchar(200) DEFAULT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(250) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `satuan` varchar(2) DEFAULT NULL,
  `kondisi` varchar(10) DEFAULT NULL,
  `lokasi` varchar(1) DEFAULT NULL,
  `lokasi_gudang` varchar(10) DEFAULT NULL,
  `lokasi_project` varchar(30) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_barang
-- ----------------------------
BEGIN;
INSERT INTO `m_barang` VALUES (1, 'X-1292', '10', '82921020', 'SANYO', '720', 'TETST', '2023', 'EA', NULL, 'P', NULL, 'PJ0001', 1, '2023-08-20 13:37:48', 1, '2023-08-23 09:53:26', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang` VALUES (2, 'BA01212', '16', '721812912010221', 'HONDA', 'SEDAN', 'HONDA ACCORD', '2023', 'UN', NULL, 'P', NULL, 'PJ0023', 9, '2023-08-20 13:39:17', 1, '2023-08-20 14:51:00', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang` VALUES (3, '01298K', '11', '129120120', 'MAKOTO', '8', 'Grindra', '2012', 'PC', NULL, 'W', 'G0001', NULL, 5, '2023-08-23 09:52:51', 1, '2023-08-23 09:52:51', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
COMMIT;

-- ----------------------------
-- Table structure for m_barang_history
-- ----------------------------
DROP TABLE IF EXISTS `m_barang_history`;
CREATE TABLE `m_barang_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(10) DEFAULT NULL,
  `lokasi` varchar(1) DEFAULT NULL,
  `lokasi_gudang` varchar(10) DEFAULT NULL,
  `lokasi_project` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_barang_history
-- ----------------------------
BEGIN;
INSERT INTO `m_barang_history` VALUES (1, 'BA01212', 'P', NULL, 'PJ0023', '2023-08-20 14:51:00', 1, '2023-08-20 14:51:00', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
COMMIT;

-- ----------------------------
-- Table structure for m_barang_items
-- ----------------------------
DROP TABLE IF EXISTS `m_barang_items`;
CREATE TABLE `m_barang_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(10) DEFAULT NULL,
  `lokasi` varchar(1) DEFAULT NULL,
  `lokasi_gudang` varchar(10) DEFAULT NULL,
  `lokasi_project` varchar(30) DEFAULT NULL,
  `qty_baik` int(11) DEFAULT NULL,
  `qty_perbaikan` int(11) DEFAULT NULL,
  `qty_rusak` int(11) DEFAULT NULL,
  `qty_hilang` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode_barang` (`kode_barang`,`lokasi_gudang`) USING BTREE,
  UNIQUE KEY `kode_barang_2` (`kode_barang`,`lokasi_project`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_barang_items
-- ----------------------------
BEGIN;
INSERT INTO `m_barang_items` VALUES (1, 'X-1292', 'W', 'G0001', NULL, 0, 0, 0, 1, '2023-08-20 13:37:48', 1, '2023-08-20 13:57:06', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang_items` VALUES (2, 'BA01212', 'W', 'G0001', NULL, 8, 0, 0, 0, '2023-08-20 13:39:17', 1, '2023-08-20 14:51:00', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang_items` VALUES (3, 'BA01212', 'P', NULL, 'PJ0023', 1, NULL, 0, NULL, '2023-08-20 14:51:00', 1, '2023-08-20 14:51:00', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang_items` VALUES (4, '01298K', 'W', 'G0001', NULL, 5, 0, 0, 0, '2023-08-23 09:52:51', 1, '2023-08-23 09:52:51', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
INSERT INTO `m_barang_items` VALUES (5, 'X-1292', 'P', NULL, 'PJ0001', 1, 0, 0, 0, '2023-08-20 13:37:48', 1, '2023-08-23 09:53:26', 1, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36 Edg/115.0.1901.203');
COMMIT;

-- ----------------------------
-- Table structure for m_gudang
-- ----------------------------
DROP TABLE IF EXISTS `m_gudang`;
CREATE TABLE `m_gudang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `gudang` varchar(100) NOT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `provinsi` varchar(50) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_gudang
-- ----------------------------
BEGIN;
INSERT INTO `m_gudang` VALUES (1, 'G0001', 'Head Office', 'Cilegon', 'Banten', NULL, '2020-09-16 00:08:11', 1, '2020-09-16 00:08:27', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `m_jabatan`;
CREATE TABLE `m_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_jabatan
-- ----------------------------
BEGIN;
INSERT INTO `m_jabatan` VALUES (1, 'Direktur Utama', 1, '2020-09-15 20:20:02', 1, '2020-09-15 20:20:58', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jabatan` VALUES (2, 'Direktur', 2, '2020-09-15 20:20:09', 1, '2020-09-15 20:21:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jabatan` VALUES (3, 'Manager', 3, '2020-09-15 20:20:15', 1, '2020-09-15 20:21:19', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jabatan` VALUES (4, 'Project Manager', 4, '2020-09-15 20:21:26', 1, '2020-09-15 20:21:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jabatan` VALUES (5, 'Lain-lain', 5, '2020-09-15 20:22:34', 1, '2020-09-15 20:22:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jabatan` VALUES (6, 'Material Control', 5, '2020-09-15 20:22:34', 1, '2020-09-15 20:22:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_jenis_perbaikan
-- ----------------------------
DROP TABLE IF EXISTS `m_jenis_perbaikan`;
CREATE TABLE `m_jenis_perbaikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `jenis_perbaikan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_jenis_perbaikan
-- ----------------------------
BEGIN;
INSERT INTO `m_jenis_perbaikan` VALUES (1, 'PR', 'Perawatan', '2020-09-22 14:40:54', 1, '2020-09-22 14:40:54', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_jenis_perbaikan` VALUES (2, 'PP', 'Pergantian Part', '2020-09-22 14:41:05', 1, '2020-09-22 14:41:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `m_karyawan`;
CREATE TABLE `m_karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `user_login` varchar(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `nik` (`nik`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_karyawan
-- ----------------------------
BEGIN;
INSERT INTO `m_karyawan` VALUES (1, '0001', 'Jaka Wijaya', '1959-04-24', 'Bandung', NULL, 1, 'Y', '2020-09-15 22:00:01', 1, '2020-09-15 22:13:46', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_karyawan` VALUES (2, '0002', 'Risma Wahyuni', '1964-06-10', 'Semarang', NULL, 2, 'Y', '2020-09-15 22:14:46', 1, '2020-09-15 22:14:46', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_kategori_material
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori_material`;
CREATE TABLE `m_kategori_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_kategori_material
-- ----------------------------
BEGIN;
INSERT INTO `m_kategori_material` VALUES (2, '2', 'ELECTRICE WINCH & EXTRACTOR', '2020-09-15 12:12:04', 1, '2020-09-15 12:12:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (3, '3', 'CONTAINER', '2020-09-15 12:12:13', 1, '2020-10-21 10:22:22', 1, '192.168.1.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (4, '4', 'PULLEY & GINPOLE ', '2020-09-15 12:12:20', 1, '2020-09-15 12:12:20', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (5, '5', 'GENSET', '2020-09-15 12:12:28', 1, '2020-09-15 12:12:28', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (6, '6', 'COMPRESSOR', '2020-09-15 12:12:34', 1, '2020-09-15 12:12:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (7, '7', 'PLATE', '2020-09-15 12:12:40', 1, '2020-09-15 12:12:40', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (8, '8', 'SCAFFOLDING', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_kategori_material` VALUES (9, '9', 'WELDING ENGINE & TRAVO', '2020-09-15 12:12:53', 1, '2020-09-15 12:12:53', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (10, '10', 'MAESURING TOOLS', '2020-09-15 12:13:01', 1, '2020-09-15 12:13:01', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (11, '11', 'LIFTING GEAR', '2020-09-15 12:13:10', 1, '2020-09-15 12:13:10', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (12, '12', 'WRENCH', '2020-09-15 12:13:16', 1, '2020-09-15 12:13:16', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (13, '13', 'CRANE MATE', '2020-09-15 12:13:22', 1, '2020-09-15 12:13:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kategori_material` VALUES (14, '14', 'ELECTRIC TOOLS', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_kategori_material` VALUES (15, '15', 'SAFETY TOOLS', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_kategori_material` VALUES (16, '16', 'FACILITY', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for m_kondisi
-- ----------------------------
DROP TABLE IF EXISTS `m_kondisi`;
CREATE TABLE `m_kondisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `kondisi` varchar(30) NOT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_kondisi
-- ----------------------------
BEGIN;
INSERT INTO `m_kondisi` VALUES (1, 'G', 'Good', NULL, '2020-09-16 00:14:44', 1, '2020-09-16 00:14:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kondisi` VALUES (2, 'R', 'Repair', NULL, '2020-09-16 00:14:51', 1, '2020-09-16 00:14:51', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kondisi` VALUES (3, 'D', 'Damage', NULL, '2020-09-16 00:14:59', 1, '2020-09-16 00:14:59', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_kondisi` VALUES (4, 'L', 'Lost', NULL, '2020-09-16 00:15:05', 1, '2020-09-16 00:15:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_satuan
-- ----------------------------
DROP TABLE IF EXISTS `m_satuan`;
CREATE TABLE `m_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `satuan` varchar(30) NOT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_satuan
-- ----------------------------
BEGIN;
INSERT INTO `m_satuan` VALUES (5, 'EA', 'EA', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_satuan` VALUES (6, 'KG', 'Kilogram', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_satuan` VALUES (7, 'PC', 'PCS', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_satuan` VALUES (8, 'RO', 'ROLL', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_satuan` VALUES (9, 'SE', 'SET', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_satuan` VALUES (10, 'UN', 'Unit', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for m_status
-- ----------------------------
DROP TABLE IF EXISTS `m_status`;
CREATE TABLE `m_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `selanjutnya` varchar(200) DEFAULT NULL,
  `urutan` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_status
-- ----------------------------
BEGIN;
INSERT INTO `m_status` VALUES (1, 'RQ', 'Request', 'Permintaan Material', NULL, 2, '2020-09-16 10:07:41', 1, '2020-09-16 10:12:13', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (2, 'RJ', 'Rejected', 'Permintaan ditolak', NULL, 6, '2020-09-16 10:08:11', 1, '2020-09-16 10:12:21', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (3, 'AP', 'Approved', 'Permintaan disetujui', 'Menunggu Diproses', 6, '2020-09-16 10:09:31', 1, '2020-09-16 10:12:31', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (4, 'PR', 'Preparing', 'Mempersiapkan Material', 'Siap Antar', 7, '2020-09-16 10:10:06', 1, '2020-09-16 10:12:39', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (5, 'SH', 'Shipment', 'Mengirim Material', 'Sedang Diantar', 8, '2020-09-16 10:10:34', 1, '2020-09-16 10:12:48', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (6, 'RC', 'Received', 'Material diterima', NULL, 9, '2020-09-16 10:11:03', 1, '2020-09-16 10:13:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (7, 'DR', 'Draft', 'Draft Permintaan Material', NULL, 1, '2020-09-16 10:11:03', 1, '2020-09-16 10:13:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (8, 'VR', 'Verifikasi', 'Sedang di-Verifikasi', NULL, 5, '2020-09-16 10:11:03', 1, '2020-09-16 10:13:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (9, 'APP', 'Approved PM', 'Permintaan disetujui oleh PM', NULL, 4, '2020-09-16 10:11:03', 1, '2020-09-16 10:13:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (10, 'RJP', 'Rejected PM', 'Permintaan ditolak oleh PM', NULL, 4, '2020-09-16 10:11:03', 1, '2020-09-16 10:13:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (11, 'VRP', 'Verifikasi PM', 'Sedang di-Verifikasi oleh PM', NULL, 3, '2020-09-16 10:07:41', 1, '2020-09-16 10:12:13', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status` VALUES (12, 'RT', 'Return', 'Pengembalian Material', NULL, 2, '2020-09-16 10:07:41', 1, '2020-09-16 10:12:13', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_status_kehilangan
-- ----------------------------
DROP TABLE IF EXISTS `m_status_kehilangan`;
CREATE TABLE `m_status_kehilangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_status_kehilangan
-- ----------------------------
BEGIN;
INSERT INTO `m_status_kehilangan` VALUES (1, 'DR', 'Draft', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_status_kehilangan` VALUES (2, 'RQ', 'Lost Report', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_status_kehilangan` VALUES (3, 'AP', 'Approve', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `m_status_kehilangan` VALUES (4, 'RT', 'Cancel', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for m_status_perbaikan
-- ----------------------------
DROP TABLE IF EXISTS `m_status_perbaikan`;
CREATE TABLE `m_status_perbaikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `kode` (`kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_status_perbaikan
-- ----------------------------
BEGIN;
INSERT INTO `m_status_perbaikan` VALUES (1, 'DR', 'Draft', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status_perbaikan` VALUES (2, 'RQ', 'Maintenance Request', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status_perbaikan` VALUES (3, 'AP', 'Approve', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status_perbaikan` VALUES (4, 'MT', 'Maintenance', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status_perbaikan` VALUES (5, 'FX', 'Done/Fixed', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
INSERT INTO `m_status_perbaikan` VALUES (6, 'FA', 'Failed', '2020-09-22 14:41:17', 1, '2020-09-22 14:41:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for m_vendor
-- ----------------------------
DROP TABLE IF EXISTS `m_vendor`;
CREATE TABLE `m_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_vendor` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `up` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of m_vendor
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- View structure for m_barang_items_kondisi_vd
-- ----------------------------
DROP VIEW IF EXISTS `m_barang_items_kondisi_vd`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `m_barang_items_kondisi_vd` AS select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,case when `i`.`lokasi` = 'W' then `i`.`lokasi_gudang` else `i`.`lokasi_project` end AS `kode_lokasi`,`i`.`qty` AS `qty`,`i`.`kondisi` AS `kondisi`,`i`.`xid` AS `xid`,`b`.`kode` AS `kode`,`b`.`kode_kategori` AS `kode_kategori`,`k`.`kategori` AS `kategori`,`b`.`deskripsi` AS `deskripsi`,`b`.`brand` AS `brand`,`b`.`tipe` AS `tipe`,`b`.`tahun` AS `tahun` from (((select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,`i`.`qty_baik` AS `qty`,'G' AS `kondisi`,concat('G-',`i`.`id`) AS `xid` from `gta2`.`m_barang_items` `i` where `i`.`qty_baik` > 0 union all select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,`i`.`qty_perbaikan` AS `qty`,'R' AS `kondisi`,concat('R-',`i`.`id`) AS `xid` from `gta2`.`m_barang_items` `i` where `i`.`qty_perbaikan` > 0 union all select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,`i`.`qty_rusak` AS `qty`,'D' AS `kondisi`,concat('D-',`i`.`id`) AS `xid` from `gta2`.`m_barang_items` `i` where `i`.`qty_rusak` > 0 union all select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,`i`.`qty_hilang` AS `qty`,'L' AS `kondisi`,concat('L-',`i`.`id`) AS `xid` from `gta2`.`m_barang_items` `i` where `i`.`qty_hilang` > 0) `i` join `gta2`.`m_barang` `b` on(`i`.`kode_barang` = `b`.`kode`)) join `gta2`.`m_kategori_material` `k` on(`k`.`kode` = `b`.`kode_kategori`));

-- ----------------------------
-- View structure for m_barang_kondisi_vd
-- ----------------------------
DROP VIEW IF EXISTS `m_barang_kondisi_vd`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `m_barang_kondisi_vd` AS select `i`.`id` AS `id`,`i`.`kode_barang` AS `kode_barang`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,`i`.`qty` AS `qty`,`i`.`kondisi` AS `kondisi`,`b`.`kode` AS `kode`,`b`.`kode_kategori` AS `kode_kategori`,`k`.`kategori` AS `kategori`,`b`.`deskripsi` AS `deskripsi`,`b`.`brand` AS `brand`,`b`.`tipe` AS `tipe`,`b`.`tahun` AS `tahun`,case when `i`.`lokasi` = 'W' then `i`.`lokasi_gudang` else `i`.`lokasi_project` end AS `kode_lokasi` from (((select `gta2`.`m_barang_items`.`id` AS `id`,`gta2`.`m_barang_items`.`kode_barang` AS `kode_barang`,`gta2`.`m_barang_items`.`lokasi` AS `lokasi`,`gta2`.`m_barang_items`.`lokasi_gudang` AS `lokasi_gudang`,`gta2`.`m_barang_items`.`lokasi_project` AS `lokasi_project`,sum(`gta2`.`m_barang_items`.`qty_baik`) AS `qty`,'G' AS `kondisi` from `gta2`.`m_barang_items` where `gta2`.`m_barang_items`.`qty_baik` > 0 group by `gta2`.`m_barang_items`.`kode_barang` union all select `gta2`.`m_barang_items`.`id` AS `id`,`gta2`.`m_barang_items`.`kode_barang` AS `kode_barang`,`gta2`.`m_barang_items`.`lokasi` AS `lokasi`,`gta2`.`m_barang_items`.`lokasi_gudang` AS `lokasi_gudang`,`gta2`.`m_barang_items`.`lokasi_project` AS `lokasi_project`,sum(`gta2`.`m_barang_items`.`qty_perbaikan`) AS `qty`,'R' AS `kondisi` from `gta2`.`m_barang_items` where `gta2`.`m_barang_items`.`qty_perbaikan` > 0 group by `gta2`.`m_barang_items`.`kode_barang` union all select `gta2`.`m_barang_items`.`id` AS `id`,`gta2`.`m_barang_items`.`kode_barang` AS `kode_barang`,`gta2`.`m_barang_items`.`lokasi` AS `lokasi`,`gta2`.`m_barang_items`.`lokasi_gudang` AS `lokasi_gudang`,`gta2`.`m_barang_items`.`lokasi_project` AS `lokasi_project`,sum(`gta2`.`m_barang_items`.`qty_rusak`) AS `qty`,'D' AS `kondisi` from `gta2`.`m_barang_items` where `gta2`.`m_barang_items`.`qty_rusak` > 0 group by `gta2`.`m_barang_items`.`kode_barang` union all select `gta2`.`m_barang_items`.`id` AS `id`,`gta2`.`m_barang_items`.`kode_barang` AS `kode_barang`,`gta2`.`m_barang_items`.`lokasi` AS `lokasi`,`gta2`.`m_barang_items`.`lokasi_gudang` AS `lokasi_gudang`,`gta2`.`m_barang_items`.`lokasi_project` AS `lokasi_project`,sum(`gta2`.`m_barang_items`.`qty_hilang`) AS `qty`,'L' AS `kondisi` from `gta2`.`m_barang_items` where `gta2`.`m_barang_items`.`qty_hilang` > 0 group by `gta2`.`m_barang_items`.`kode_barang`) `i` join `gta2`.`m_barang` `b` on(`i`.`kode_barang` = `b`.`kode`)) join `gta2`.`m_kategori_material` `k` on(`k`.`kode` = `b`.`kode_kategori`));

-- ----------------------------
-- View structure for m_barang_vd
-- ----------------------------
DROP VIEW IF EXISTS `m_barang_vd`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `m_barang_vd` AS select `b`.`id` AS `id`,`b`.`kode` AS `kode`,`b`.`kode_kategori` AS `kode_kategori`,`b`.`model` AS `model`,`b`.`brand` AS `brand`,`b`.`tipe` AS `tipe`,`b`.`deskripsi` AS `deskripsi`,`b`.`tahun` AS `tahun`,`b`.`satuan` AS `satuan`,`b`.`kondisi` AS `kondisi`,coalesce(`i`.`qty_baik`,0) + coalesce(`i`.`qty_perbaikan`,0) + coalesce(`i`.`qty_rusak`,0) + coalesce(`i`.`qty_hilang`,0) AS `qty`,`b`.`qty` AS `total_qty`,`i`.`id` AS `item_id`,`i`.`lokasi` AS `lokasi`,`i`.`lokasi_gudang` AS `lokasi_gudang`,`i`.`lokasi_project` AS `lokasi_project`,case when `i`.`lokasi` = 'W' then `i`.`lokasi_gudang` else `i`.`lokasi_project` end AS `kode_lokasi`,`i`.`qty_baik` AS `qty_baik`,`i`.`qty_rusak` AS `qty_rusak`,`i`.`created_at` AS `created_at`,`i`.`created_by` AS `created_by`,`i`.`updated_at` AS `updated_at`,`i`.`updated_by` AS `updated_by`,`i`.`useragent` AS `useragent`,`i`.`ip_address` AS `ip_address` from (`m_barang_items` `i` join `m_barang` `b` on(`i`.`kode_barang` = `b`.`kode`));

-- ----------------------------
-- View structure for m_lokasi_barang_vd
-- ----------------------------
DROP VIEW IF EXISTS `m_lokasi_barang_vd`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `m_lokasi_barang_vd` AS select `x`.`lokasi` AS `lokasi`,`x`.`kode_lokasi` AS `kode_lokasi`,`x`.`nama_lokasi` AS `nama_lokasi`,sum(`x`.`qty_baik`) AS `qty` from (select case when `b`.`lokasi` = 'W' then concat(`g`.`kode`,' - ',`g`.`gudang`) else concat(`p`.`kode`,' - ',`p`.`singkatan`) end AS `nama_lokasi`,case when `b`.`lokasi` = 'W' then `g`.`kode` else `p`.`kode` end AS `kode_lokasi`,`b`.`qty_baik` AS `qty_baik`,`b`.`lokasi` AS `lokasi` from ((`gta2`.`m_barang_items` `b` left join `gta2`.`m_gudang` `g` on(`g`.`kode` = `b`.`lokasi_gudang` and `b`.`lokasi` = 'W')) left join `gta2`.`t_project` `p` on(`p`.`kode` = `b`.`lokasi_project` and `b`.`lokasi` = 'P'))) `x` group by `x`.`lokasi`,`x`.`nama_lokasi`;

SET FOREIGN_KEY_CHECKS = 1;
